<?php
include "include/session.php";
include 'include/connect_to_bd.php'; 
if(!isset($_GET['token'])){
  header("Location: contacts.php");
}else{
  $contact_md5=$_GET['token'];
  $token=$_SESSION['user_token'];
  $query_to_token_bd=mysql_query("SELECT email,mobile_phone FROM contacts WHERE md5_hash='$contact_md5'");
  if(mysql_num_rows($query_to_token_bd)!=0){
    $array_token=mysql_fetch_assoc($query_to_token_bd);
    $email_contact=$array_token['email'];
    $mobile=$array_token['mobile_phone'];
  }else{
    header("Location: contacts.php");
  }
}

function getUserId($token){
  $sql="SELECT client_id FROM token WHERE token='".$token."'";
  $query=mysql_query($sql);
  $arrayData=mysql_fetch_assoc($query);
  return $arrayData['client_id'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <!-- Include Editor style. -->
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_style.min.css' rel='stylesheet' type='text/css' />
  <script src="//cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
  <!-- Include JS file. -->
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/js/froala_editor.min.js'></script>
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  body{
    position: relative;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }
</style>

</head>
<style>

</style>


<body name="<?php echo $email_contact;  ?>" class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden" id="<?php echo $mobile; ?>">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main" id="<?php echo getUserId($_SESSION['user_token']);?>">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Contact Details</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12"><br>
              <div class="card">
                <div class="card-block">
                  <ul class="nav nav-tabs" style="border-bottom: 0!important" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="email_btn" data-toggle="tab" href="#email_center" role="tab" aria-controls="email_center">Email Center <i class="fa fa-envelope-o "></i></a>
                    </li>
                    <!--<li disabled="disabled" class="nav-item">
                      <a disabled="disabled" id="voice_btn" class="nav-link" data-toggle="tab" href="#voice_center" role="tab" aria-controls="voice_center">Voice Center  <i class="fa fa-microphone"></i></a>
                    </li>-->
                    <li class="nav-item">
                      <a class="nav-link" id="text_btn" data-toggle="tab" href="#text_center" role="tab" aria-controls="text_center">Text Center <i class="fa fa-file-text-o"></i></a>
                    </li>
                    <!--<li class="nav-item">
                      <a class="nav-link" id="postcard_btn" data-toggle="tab" href="#postcard_center" role="tab" aria-controls="postcard_center">Postcard Center <i class="fa fa-newspaper-o"></i></a>
                    </li>-->
                  </ul>

                  <div class="tab-content" style="border: 1px solid #e1e6ef!important;">
                    <br>
                    <div disabled class="tab-pane active" id="email_center" role="tabpanel" style="">

                      <div class="form-group close_tab1">
                        <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which emails bounce. A full report is available to you
                        under logs on your dashboard.</p> 
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab1','variable1')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>
                      </div>
                      <div class=" col-md-12 variable1" style="display: none">
                        <!--<a href="#">View Current Emails</a>-->
                        <textarea name="editor_email" id="editor_email" rows="10" cols="80">
                        </textarea>
                        <div class="col-md-12 py-4 text-center">
                          <button href="#voice_center" onclick="email_send(this)" class="btn btn-primary" >Send </button>
                          <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn btn-outline-success">Success</button>
                        </div>
                      </div>

                    </div>
                    <div class="tab-pane" id="voice_center" role="tabpanel">
                      <div class="form-group close_tab2">
                        <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which numbers do not accept the message and only
                          charged for delivered messages. A full report is available to you under logs on your dashboard.
                        </p> 
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab2','variable2')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>

                      </div>
                      <div class="col-md-12 form-group variable2" style="display: none">   
                        <!--<a href="#">View Current RVMD</a>-->
                        <h1>
                        </h1>
                        <input type="file" onchange="load()" id="import_audio" class="import_audio" accept="audio/wav">
                        <section class="experiment">
                          <p style="text-align: center;">
                            <video id="preview" controls style="display: none;border: 1px solid rgb(15, 158, 238); height: 240px; max-width: 100%; vertical-align: top; width: 320px;"></video>
                          </p>
                          <button onclick="plus_one()" class="btn btn-primary" id="record">Record</button>
                          <button class="btn btn-danger" id="stop" disabled>Stop</button><br>
                          <div id="container" ></div>
                        </section>
                        <div class="audio_list">
                        </div>
                        <div class="col-md-12 py-4 text-center">
                          <button onclick="save_voice()" class="btn btn-primary">Send </button>
                        </div>
                      </div>
                    </div>


                    <div class="tab-pane" id="text_center" role="tabpanel">
                      <div class="form-group close_tab3">
                        <label for="company"><p>Note: Note all messages are not guaranteed delivery. You will be notified of which texts bounce. A full report is available to you
                        under logs on your dashboard.</p> 
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab3','variable3')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>
                      </div> 

                      <div class="col-md-12 form-group variable3" style="display: none">
                        <!--<a href="#">View Current Text</a>-->
                        <textarea name="text_editor" id="text_editor" rows="10" cols="80">
                        </textarea>
                        <div class="col-md-12 py-4 text-center">
                          <button onclick="sms_send(this)" class="btn btn-primary">Send </button>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="postcard_center" role="tabpanel">
                      <div class="form-group close_tab4">
                        <label for="company"><p>Note: Note all messages are guaranteed delivery. You are charged for all postcards sent. If you receive a card back you must update the address or remove address from contacts information to stop any further attempts.</p> 
                          <p>You must agree to terms and conditions before proceeding:</p></label>

                          <div class="checkbox">
                            <label>
                              <input onchange="agree_check(this,'close_tab4','variable4')" type="checkbox" id="agree" name="agree" value="agree">
                              <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                            </label>
                          </div>
                        </div>

                        <div class="col-md-12 form-group variable4" style="display: none">  
                          <!--<a href="#">View Current Postcards</a>-->
                          <div class="card email-app">
                            <nav>
                              <p class="btn btn-primary btn-block">Name card</p>

                              <ul class="nav">
                               <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-file"></i> Open </a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-star"></i> Text</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-image"></i> Image</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-trash"></i> Delete</a>
                              </li>
                            </ul>
                          </nav>

                          <main>

                            <div class="col-md-12">
                              <div class="card">
                                <div class="card-header">
                                  <ul class="nav nav-tabs float-right" role="tablist">
                                    <li class="nav-item">
                                      <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab">Front</a>
                                    </li>
                                    <li class="nav-item">
                                      <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab">Back</a>
                                    </li>
                                  </ul>
                                </div>
                                <div class="card-block p-0">
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="front">
                                      <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Create Name">
                                    </div>
                                    <div class="tab-pane" id="back">
                                      test2
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </main>
                        </div>
                        <div class="col-md-12 py-4 text-center">
                          <button onclick="save_postcard()" class="btn btn-primary">Send</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <div style="display: none;text-align: center;"  class="final_save text-center"> 
                    <button onclick="save_all()" class="btn btn-primary">Save and View</button>
                  </div>
                  <div class="text-center">
                  <!--<button onclick="" class="btn btn-primary">Send</button>
                    <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn  btn-outline-success">Success</button>-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <div class="modal fade" id="Terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Terms and Conditions</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Terms and Conditions</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
</div>


<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="js/libs/ckeditor.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
<script src="js/app.js"></script>
<script>
  CKEDITOR.replace('editor_email');
  CKEDITOR.replace('text_editor',
  {
    toolbar :
    [
    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
    { name: 'tools', items : [ 'Maximize','-','About' ] }
    ]
  });
  var email=$('body').attr('name');
  var mobile=$('body').attr('id');
  var userId=$("main").attr('id');
  function email_send(id){
    $(id).attr("disabled","true");
    var editorEmail = CKEDITOR.instances['editor_email'].getData();
    $.ajax({
      url:"include/send_email_immediately.php",
      data:{email:email,editorEmail:editorEmail,userId:userId},
      type:"POST",
      success:function(data){
        //console.log(data);
        alert("Success Sended!");
        $(id).removeAttr("disabled");
      }
    })
  }

  function sms_send(id){
    $(id).attr("disabled","true");
    var text_editor = CKEDITOR.instances['text_editor'].getData();
    $.ajax({
      url:"include/send_sms_immediately.php",
      data:{mobile:mobile,text_editor:text_editor,userId:userId},
      type:"POST",
      success:function(data){
        alert("Success Sended!");
        $(id).removeAttr("disabled");
      }
    })
  }
      /*
        var name_file;
        var i;
        var dir;
        var id_campaign;
        var  error='';
        var id_audio;
        var dir_img;
        function plus_one(){
          i++;
        }
        function PostBlob(audioBlob, videoBlob, fileName) {
          var formData = new FormData();
          var bd="campaign_builder_rvmd";
          formData.append('filename', fileName);
          formData.append('audio-blob', audioBlob);
          formData.append('video-blob', videoBlob);
          formData.append('bd',bd );
          var campaign_id=id_campaign;
          formData.append('id_campaign',campaign_id);
          xhr('include/upload_audio.php', formData, function(ffmpeg_output) {
            preview.src = '../uploads_audio/' + fileName + '-merged.webm';
            preview.play();
            preview.muted = false;
            name_file=fileName + '.webm';
          });
        }
        var record = document.getElementById('record');
        var stop = document.getElementById('stop');
        var audio = document.querySelector('audio');
        var recordVideo = document.getElementById('record-video');
        var preview = document.getElementById('preview');
        var container = document.getElementById('container');
        var input_file=document.getElementById('import_audio');
        var recordAudio, recordVideo;
        record.onclick = function() {
          record.disabled = true;
          input_file.disabled=true;
          !window.stream && navigator.getUserMedia({
            audio: true,
            video: true
          }, function(stream) {
            window.stream = stream;
            onstream();
          }, function(error) {
            alert(JSON.stringify(error, null, '\t'));
          });
          window.stream && onstream();
          function onstream() {
            preview.src = window.URL.createObjectURL(stream);
            preview.play();
            preview.muted = true;
            recordAudio = RecordRTC(stream, {
              type: 'audio',
              recorderType: StereoAudioRecorder,
            onAudioProcessStarted: function() {
              recordVideo.startRecording();
            }
          });
            var videoOnlyStream = new MediaStream();
            videoOnlyStream.addTrack(stream.getVideoTracks()[0]);
            recordVideo = RecordRTC(videoOnlyStream, {
              type: 'video',
          });
            recordAudio.startRecording();
            stop.disabled = false;
          }
        };
        var fileName;
        stop.onclick = function() {
          record.disabled = true;
          stop.disabled = true;
          preview.src = '';
          preview.poster = 'ajax-loader.gif';
          fileName = Math.round(Math.random() * 99999999) + 99999999;
          recordAudio.stopRecording(function() {
            recordVideo.stopRecording(function() {
              PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
            });
          });
        };
        function xhr(url, data, callback) {
          var request = new XMLHttpRequest();
          request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
              callback(request.responseText);
            }
          };
    $('.sk-circle').css('display','block');
    send_audio(data);
  }
  function send_audio(data){
    $.ajax({
      url:"include/record_audio.php",
      data: data,
      processData: false,
      contentType: false,
      type: 'POST',
      success:function(data){
        parsing_audio_json(data);
      }
    })
    $('.sk-circle').css('display','none');
  }

  function save_text(){
    $('.sk-circle').css('display','block');
    var editor_text = CKEDITOR.instances['text_editor'].getData();
    $.ajax({
      url:"include/univesral_campaign/add_text_to_campaign.php",
      data:{editor_text:editor_text,id_campaign:id_campaign},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');

          }
        })
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },1500);
    $('#postcard_btn').tab('show');
    $('.sk-circle').css('display','none');
  }

  function save_voice(){
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },1500);
    $('#text_btn').tab('show');
    update_audio();
  }

  function save_postcard()
  {
    $('.final_save').css("display","block");
    setTimeout(function(){
 
    },1500);
  }

  function email_show(){
    $('.sk-circle').css('display','block');
    var editor_data = CKEDITOR.instances['editor_email'].getData();
    $.ajax({
      url:"include/univesral_campaign/add_email_to_campaign.php",
      data:{editor_data:editor_data,id_campaign:id_campaign},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
          }
        })
    $('#voice_btn').tab('show');
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },1500);
    $('.sk-circle').css('display','none');
  }

  function import_campaign(){
    $('.sk-circle').css('display','block');
    var name=$('.select_import_campaign').val();
    $('.wrapper_area').css('display','block');
    if(name=='None'){

    }else{
      $.ajax({
        url:"include/univesral_campaign/get_campaign_data.php",
        data:{name:name},
        type:"POST",
        success:function(data){
          $('.sk-circle').css('display','none');
          append_data(data);
        }
      })
    }
  }
  function append_data(data){
    $('.sk-circle').css('display','block');
    var json=JSON.parse(data);
    $('.name_campaign').val(json['name']+1);
    $('.select_type').val(json['campaign_type']);
    $('.select_assign_num').val(json['assign_num']);

    if(json['email']!=0){
      CKEDITOR.instances['editor_email'].setData(json['email']['text']);
      $('.close_tab1').css('display','none');
      $('.variable1').css('display','block');
      $('.final_save').css("display","block");
    }

    if(json['text']!=0){
      CKEDITOR.instances['text_editor'].setData(json['text']['text']);
      $('.variable3').css('display','block');
      $('.final_save').css("display","block");
      $('.close_tab3').css('display','none');
    }
    if(json['audio']!=0){
      dir_img=json['audio'].substring(3);
      $('.close_tab2').css('display','none');
      $('.variable2').css('display','block');
      var out='<audio name="default" controls id="'+json['audio'].substring(3)+'">';
      out+='<source src="'+json['audio'].substring(3)+'" type="audio/ogg; codecs=vorbis">';
      out+='<source src="'+json['audio'].substring(3)+'" type="audio/mpeg">';
      out+='</audio><a style="margin-left:10px" href="#del" onclick="delete_audio()">Delete</a>'; 
      $('.sk-circle').css('display','none');
      $('.audio_list').append(out);
      $('.final_save').css("display","block");
    }
    $('.sk-circle').css('display','none');
  }

  function hide_area(){
    $('.sk-circle').css('display','block');
   save_new_company();
   if(error==''){
    sessionStorage.removeItem('campaign_id');
    window.location='campaign-overview.php';
  }
  $('.sk-circle').css('display','none');
}

function notification(id,type_error){
  var parent=$(id).parent();
  $(parent).addClass('has-danger');
  $(id).addClass('form-control-danger');
  $(id).attr("data-content",type_error);
  $(id).popover('show');
  $('.danger_btn').css("display","inline-block");
  setTimeout(function(){
    $(id).popover('dispose');
    $('.danger_btn').css("display","none");
  },1500);
}

function change_input_data(id){
  var parent=$(id).parent();
  $(id).removeClass('form-control-danger');
  parent.removeClass('has-danger');
}

function save_new_company(){
  $('.sk-circle').css('display','block');
  var name_campaign=$('.name_campaign').val();
  var type_campaign=$('.select_type').val();
  var assign_num=$('.select_assign_num').val();
  if(name_campaign==''){
    error='empty_data';
    notification($('.name_campaign'),'Enter campaign name');
  }else{
    $.ajax({
      url:"include/univesral_campaign/save_new_company.php",
      data:{name_campaign:name_campaign,type_campaign:type_campaign,assign_num:assign_num},
      type:"POST",
      success:function(data){
        parsing_json(data);
      }
    })
  }
  $('.sk-circle').css('display','none');
}

function parsing_json(data){
  var json=JSON.parse(data);
  if(json['success']==1){
    id_campaign=json['campaign_id'];
    sessionStorage['campaign_id'] = id_campaign;
    //$('#name_campaign').val('');
    $(".success_btn").css('display','inline-block');
    setTimeout(function(){
      $(".success_btn").css('display','none');
    },1000);
    $(".select_import_campaign").attr("disabled", true);
    $(".name_campaign").attr("disabled", true);
    $(".select_type").attr("disabled", true);
    $(".select_assign_num").attr("disabled", true);
    $('.save_and_start').css('display','none');
    $('#accordion').css('display','block');
  }else{
    error='campaign_exist';
    notification($('.name_campaign'),'Campaign Exist');
    throw new SyntaxError("error_data");
  }
}

function parsing_audio_json(array)
{
  var json=JSON.parse(array);
  var dir="../"+json['dir'];
  var out='<audio controls id="'+dir+'">';
  out+='<source src="'+json['dir']+'" type="audio/ogg; codecs=vorbis">';
  out+='<source src="'+json['dir']+'" type="audio/mpeg">';
  out+='</audio><a style="margin-left:10px" href="#del" onclick="delete_audio()">Delete</a>'; 
  $('.sk-circle').css('display','none');
  $('.audio_list').append(out);
}


function load()
{
  $('.sk-circle').css('display','block');
  var input_file=document.getElementById('import_audio');
  var record = document.getElementById('record');
  input_file.disabled=true;
  record.disabled=true;
  var id=id_campaign;
  var data=new FormData();
  var file = $('.import_audio');
  data.append('upload', file.prop('files')[0]);
  data.append('id', id);
  data.append('bd', 'campaign_builder_rvmd');
  $.ajax({
    url:"include/upload_audio.php",
    data: data,
    processData: false,
    contentType: false,
    type:"POST",
    success:function(data){
      parsing_audio_json(data);
      file.val('');
      $('#record').attr('disabled');
      $('#input_file').attr('disabled');
    }
  })
  $('.sk-circle').css('display','none');
}



function delete_audio(){
  $('.sk-circle').css('display','block');
  var id=id_campaign;
  var dir1=$('audio').attr('id');
  dir='../'+dir1;
  var bd="campaign_builder_rvmd";
  var input_file=document.getElementById('import_audio');
  $.ajax({
    url:"include/delete_audio.php",
    data:{dir:dir,id:id,bd:bd},
    type:"POST",
    success:function(data){
      $('.audio_list').html('');
    }
  })
  $('.sk-circle').css('display','none');
  $('#record').removeAttr('disabled');
  input_file.disabled=false;

}
function update_audio()
{
  if($('audio').attr("name")=='default'){
    dir_img="../"+$('audio').attr("id");
  }else{
    dir_img=$('audio').attr('id');
  }
  $.ajax({
    url:"include/update_audio.php",
    data: {dir_img:dir_img,id_campaign:id_campaign},
    type: 'POST',
    success:function(data){
    }
  })
}

function save_all(){
  update_audio();
  save_text();
  email_show();
  window.location="campaign-overview.php";
}
*/

function agree_check(id,tab,div)
{
  $('.'+tab).css('display','none');
  $('.'+div).css('display','block');
}
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>

</body>

</html>


