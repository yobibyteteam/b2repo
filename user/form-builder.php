<?php
include "include/session.php";
include 'include/connect_to_bd.php';
function get_list_campaign(){
  $token=$_SESSION['user_token'];
  $query_model="SELECT * FROM token WHERE token='".$token."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0)
  {
    $array=mysql_fetch_assoc($query);
    $client_id=$array['client_id'];
    getUniversalCampaign($client_id);
    getDatedCampaign($client_id);
    //getHolidayCampaign($client_id);
  }
}
function getUniversalCampaign($client_id){
  global $campaignId;
  $query_model="SELECT id,name FROM campaign_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/campaign">'.$res['name'].'(Universal)</option>';
    }
  }
}
function getHolidayCampaign($client_id){
  global $nameCampaign;
  $query_model="SELECT id,name FROM holiday_builder WHERE client_id='".$client_id."' AND type='user' ";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option  value="'.$res['id'].'/holiday">'.$res['name'].'(Holiday)</option>';
    }
  }
}
function getDatedCampaign($client_id){
  global $campaignId;
  $query_model="SELECT id,name FROM dated_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/dated">'.$res['name'].'(Dated)</option>';
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  input:disabled{
    background-color: rgba(225, 230, 239, 0.51)!important;
  }
  .form-group{
    margin-bottom: 0!important;
  }
  .form-control{
    margin-bottom: 10px;
  }
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">       
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active ">Form Builder</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Form Builder</strong>
                  <!--<a href="#win3" style="float:right;" class="button button-blue" >Click Here for Video Tutorial</a>-->
                </div>
                <div class="card-block">
                 <label for="">Assigned Campaign</label>
                 <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                 <select id="select" name="select" class="check_campaign form-control">
                  <option value="0">Choose Campaign</option>
                  <?php get_list_campaign(); ?>
                </select><br>
                <div class="form_input">
                  <div class="form-group">
                    <label>First Name</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input style="margin-bottom: 10px;" disabled="" id="first_name" type="text" class="build_input important_input input_data form-control"  placeholder="Enter First Name">
                  </div>
                  <div class="form-group">
                    <label>Last Name</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" id="last_name" type="text" class="build_input input_data important_input form-control" placeholder="Enter Last Name">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_including(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input style="margin-bottom: 10px;" disabled="" id="email" type="text" class="build_input important_input input_data form-control"  placeholder="Enter Email">
                  </div>
                  <div class="form-group mobile_phone">
                    <label>Mobile Number</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" type="number" id="mobile_phone" value="mobile_phone" class="input_data build_input important_input form-control"  placeholder="Enter Mobile Phone">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_including(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>

                  <div class="form-group">
                    <label>Address</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" type="text" class="input_data important_input build_input form-control" id="address"  placeholder="Enter Street Address">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_including(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>

                  <div class="form-group">
                    <label>Address 1</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" type="text" class="input_data important_input build_input  form-control" id="address1"  placeholder="Enter Street Address">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" checked onchange="change_including(this)" class="switch-input">
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>

                  <div class="form-group">
                    <label>City</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" id="city" type="text" class="input_data important_input build_input form-control" placeholder="Enter your city">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_including(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label"  data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label>State</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" id="state" type="text" class="input_data build_input important_input form-control" placeholder="State name">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_including(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>
                  <div class="form-group">
                    <label>Postal Code</label>
                    <span class="required_fuild" style="color:red;vertical-align:middle">*</span>
                    <input disabled="" id="postal_code" type="text" class="input_data important_input build_input form-control" placeholder="Postal Code">
                    Include Field in Form:
                    <label style="padding-bottom: 10px;" class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_including(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                    Required Field:
                    <label class="switch switch-text switch-pill switch-primary">
                      <input type="checkbox" onchange="change_requiring(this)" class="switch-input" checked>
                      <span class="switch-label" data-on="On" data-off="Off"></span>
                      <span class="switch-handle"></span>
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <label>Redirect Link</label>
                  <span  style="color:red;vertical-align:middle">*</span>
                  <input  type="text" class="important_input redirect_link form-control" placeholder="Redirect Link">
                </div>
                <div class="form-group">
                  <label>Button Name:</label>
                  <span  style="color:red;vertical-align:middle">*</span>
                  <input  type="text" class="button_name form-control" placeholder="Button Name">
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row" style="background-color: #e1e6ef;">
                      <div class="form-group col-sm-6">
                        <input style="margin-top: 20px" type="text" class="new_input_name form-control"  placeholder="Enter custom name">
                        <label class="checkbox-inline" for="inline-checkbox1">
                          <input style="margin-top: 10px; margin-right: 3px;" type="radio" onclick="changeTypeField('textbox')" name="type_field" value="textbox">Text Box field
                        </label>
                        <label class="checkbox-inline" for="inline-checkbox2">
                          <input style="margin-top: 10px; margin-right: 3px;" type="radio" onclick="changeTypeField('date')" name="type_field" value="date">Date field
                        </label>
                        <label  class="checkbox-inline" for="inline-checkbox3">
                          <input style="margin-top: 10px; margin-right: 3px;" type="radio" onclick="changeTypeField('time')" name="type_field" value="time">Time field
                        </label>
                        <label  class="checkbox-inline" for="inline-checkbox4">
                          <input style="margin-top: 10px; margin-right: 3px;" type="radio" onclick="changeTypeField('dropdown')" name="type_field" value="dropdown">Drop Down field
                        </label>
                        <label  class="checkbox-inline" for="inline-checkbox5">
                          <input style="margin-top: 10px; margin-right: 3px;" type="radio" onclick="changeTypeField('textarea')" name="type_field" value="textarea">Text Area field
                        </label>
                      </div>
                      <div class="form-group col-sm-6">
                        <button style="margin-top: 20px" onclick="add_new_input(this)" class="cusom_field btn btn-primary">Add Custom Field</button>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                 <div class="card-block">
                  <div class="col-sm-12 text-center">
                    <button onclick="build_form(this)" style="padding: 0.5rem 2rem;" type="submit" class="saveForm btn btn-primary"> Save </button>
                    <button style="margin-left: 30px;display: none;padding: 0.5rem 2rem;" type="button" class="btn danger_btn btn-outline-danger">Error</button>
                    <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn btn-outline-success">Success</button>
                  </div>
                </div>
              </div>
              <div class="form-group text-left">
                <button onclick="generate_html()" data-toggle="modal" data-target="#htmlCode" class="form_name btn btn-primary" disabled="">Get Form Code</button>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</main>
</div>

<div class="modal fade" id="htmlCode" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Get Form</h4>
      </div>
      <div class="modal-body text_body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<script src="js/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="js/activity.js"></script>
<!-- Plugins and scripts required by this views -->
<!-- Custom scripts required by this view -->
<script src="js/views/charts.js"></script>
<script> 
  var type_field='';
  var dataArray=[];
  var dataArrayCustom=[];
  var md5;
  var campaignSelect;
  var redirectLink;
  var buttonName;
  var arrru = new Array ('Я','я','Ю','ю','Ч','ч','Ш','ш','Щ','щ','Ж','ж','А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н', 'О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ы','ы','Ь','ь','Ъ','ъ','Э','э');

  var arren = new Array ('Ya','ya','Yu','yu','Ch','ch','Sh','sh','Sh','sh','Zh','zh','A','a','B','b','V','v','G','g','D','d','E','e','E','e','Z','z','I','i','J','j','K','k','L','l','M','m','N','n', 'O','o','P','p','R','r','S','s','T','t','U','u','F','f','H','h','C','c','Y','y','`','`','\'','\'','E', 'e');

  function cToL(text){
    for(var i=0; i<arrru.length; i++){
      var reg = new RegExp(arrru[i], "g");
      text = text.replace(reg, arren[i]);
    }
    return text;
  }

  function build_form(){
    try{
      checkCampaign();
      checkRedirLink();
      buildInput();
      checkButton();
      saveNewForm();
      //buildForm();
    }catch(e){

    }
  }

  function checkButton(){
    var button_name=$(".button_name").val();
    if(button_name==''){
      alert("Enter Button Name");
      throw new SyntaxError("errors");
    }else{
      buttonName=button_name;
    }
  }

  function checkRedirLink(){
    var select=$(".redirect_link").val();
    if(select==''){
      alert("Enter Redirect Link");
      throw new SyntaxError("error");
    }else{
      redirectLink=select;
    }
  }

  function checkCampaign(){
    var select=$(".check_campaign").val();
    if(select=='0'){
      alert("Select Campaign");
      throw new SyntaxError("error");
    }else{
      campaignSelect=select;
    }
  }


  function change_including(id){
   var parent=$(id).parent();
   parent=parent.parent();
   var input=parent.find(".input_data");
   if(input.hasClass("build_input")){
    input.removeClass("build_input");
  }else{
    input.addClass("build_input");
  }
}

function change_requiring(id){
 var parent=$(id).parent();
 parent=parent.parent();
 var input=parent.find(".input_data");
 if(input.hasClass("important_input")){
  input.removeClass("important_input");
}else{
  input.addClass("important_input");
}

var spanRequired=parent.find('.required_fuild');
if($(spanRequired).css("display")=="inline"){
  spanRequired.css("display","none");
}else{
  spanRequired.css("display","inline");
}
}

function checkCopyField(name){
  var error=1;
  $(".build_input").each(function(){
    var nameBuild=$(this).attr('id');
    if(name==nameBuild){
      error=0;
      alert("Enter Another Name Of Field");
    }
  })
  if(error==1){
    var nameFieldCyrill=cToL(name);
    addingNewInput(type_field,name,nameFieldCyrill);
  }
}


function add_new_input(){
  var nameField=$(".new_input_name").val();
  if(type_field==''){
    alert("Select Type Of Field!");
  }else if(nameField==''){
    alert("Enter Field Name!");
  }else{
    checkCopyField(nameField);
  }
}

function addingNewInput(type,name,nameFieldCyrill){
  var out='';
  switch(type){

    case 'textbox':
    out+='<div class="form-group">';
    out+='<label>'+name+'</label><button class="btn btn-outline-danger btn-sm" onclick="deleteField(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button>';
    out+='<input id="'+nameFieldCyrill+'" placeholder="'+name+'" disabled="" type="text" class="build_input custom_field form-control"></div>';
    break;

    case 'date':
    out+='<div class="form-group">';
    out+='<label>'+name+'</label><button class="btn btn-outline-danger btn-sm" onclick="deleteField(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button>';
    out+='<input disabled type="date" id="'+nameFieldCyrill+'" class="custom_field build_input form-control"></div>';
    break;

    case 'time':  
    out+='<div class="form-group">';
    out+='<label>'+name+'</label><button class="btn btn-outline-danger btn-sm" onclick="deleteField(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button>';
    out+='<input id="'+nameFieldCyrill+'" disabled type="time" class="custom_field build_input form-control" ></div>';
    break;

    case 'dropdown':
    out+='<div class="form-group">';
    out+='<label>'+name+'</label><button class="btn btn-outline-danger btn-sm" onclick="deleteField(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button>';
    out+='<select id="'+nameFieldCyrill+'" name="'+nameFieldCyrill+'" class="build_input custom_field select_elem form-control"><option value="0">'+nameFieldCyrill+'</option></select>';
    out+='<div class="newInputName"></div><button onclick="addNewInput(this)"  class="btn btn-secondary add_answer">Add Answer</button>';
    out+='<div><br>';
    break;

    case 'textarea':
    out+='<div class="form-group">';
    out+='<label>'+name+'</label><button class="btn btn-outline-danger btn-sm" onclick="deleteField(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button>';
    out+='<textarea disabled id="'+nameFieldCyrill+'" rows="2" class="textarea_input_check build_input custom_field form-control" placeholder="'+name+'" ></textarea><div>'
    break;
  }
  $(".form_input").append(out);
  $(".new_input_name").val('');
}

function changeTypeField(type){
  type_field=type;
}


function deleteField(id){
  var parent=$(id).parent();
  parent.remove();
}

function addNewInput(id){
 var parent=$(id).parent();
 //parent=parent.parent();
 var out='<div><input style="width:30%;display:inline" placeholder="Enter Option Name" type="text" class="dropdownName form-control"><a href="#add" onclick="addingNewField(this)"><i style="margin:5px;vertical-align:middle;color:green;font-size:24px!important; " class="fa fa-check-circle-o" aria-hidden="true"></i></a><a class="delnewInput" style="display:none;" href="#del" onclick="deleteNewField(this)"><i style="margin:5px;vertical-align:middle;color:red;font-size:24px!important;" class="fa fa-trash-o" aria-hidden="true"></i></a></div>';
 var inputBlock=parent.find(".newInputName");
 inputBlock.append(out);
}

function deleteLastInput(id){

}

function deleteNewField(id){
  var parent=$(id).parent();
  var input=parent.find(".dropdownName");
  var inputName=input.attr('name');
  var select=parent.parent().parent();
  var selectBlock=select.find("select").find('option[value='+inputName+']');
  selectBlock.remove();
  parent.remove();
}

function addingNewField(id){
  var parent=$(id).parent();
  var select=parent.parent().parent();
  var selectBlock=select.find("select");
  var inputName=parent.find(".dropdownName");
  inputNameValue=inputName.val();
  var inputNameValuecToL=cToL(inputName.val());
  if(inputNameValue==''){
    alert("Enter Input Name");
  }else{
    var out='<option value="'+inputNameValue+'">'+inputNameValue+'</option>';
    inputName.attr("id",inputNameValuecToL);
    inputName.attr("name",inputNameValuecToL);
    inputName.attr("disabled","true");
    selectBlock.append(out);
    var delBtn=parent.find(".delnewInput");
    //console.log(delBtn);
    $(id).css('display',"none");
    delBtn.css('display',"inline");
  }
}

function buildInput(){
  dataArray=[];
  $(".build_input").each(function(){
    if($(this).hasClass("important_input")){
     $(this).attr("required","true"); 
   }
   dataArray.push($(this));
   dataArrayCustom.push($(this).attr('id'));
 })
}
function buildForm(md5){
  var out="";
  out+='&lt;link href="https://tracs.me/css/style.css" rel="stylesheet"&gt;';
  out+='&lt;form action="https://tracs.me/form/save_contact.php" method="POST"&gt;';
  out+='&lt;input type="hidden" value="'+redirectLink+'" name="redirectLink"&gt;';
  out+='&lt;input type="hidden" value="'+md5+'" name="md5"&gt;';
  out+='&lt;input type="hidden" value="'+campaignSelect+'" name="campaignSelect"&gt;';
  for(var k in dataArray){
    if(dataArray[k][0]['localName']=='input'){
     out+='<p>&lt;'+dataArray[k][0]['localName']+' ';
     out+='class="form-control" style="margin-bottom: 10px;" type="'+dataArray[k][0]['type']+'" ';
     var attrLength=dataArray[k][0]['attributes']['length'];
     for(var i=0;i<attrLength;i++){
      if(dataArray[k][0]['attributes'][i]['localName']=="required"){
        out+='required="" ';
      }
    }
    out+='name="'+dataArray[k][0]['id']+'" ';
    out+='placeholder="'+dataArray[k][0]['placeholder']+'"';
    out+='&gt;<p>';
  }else if(dataArray[k][0]['localName']=='textarea'){
    out+='<p>&lt;textarea style="margin-bottom: 10px;" class="form-control" placeholder="'+dataArray[k][0]['id']+'" name="'+dataArray[k][0]['id']+'" rows="3" ';
    out+='&gt;&lt;/textarea&gt;<p>';
  }else{
   out+='<p>&lt;select style="margin-bottom: 10px;" class="form-control" name="'+dataArray[k][0]['id']+'"&gt;';
   var length=dataArray[k][0]['options']['length'];
   for(var j=0;j<length;j++){
    if(j==0){
      out+='&lt;option selected="" disabled="" value="'+dataArray[k][0]['options'][j]['value']+'"&gt;'+dataArray[k][0]['options'][j]['text']+'&lt;/option&gt;';
    }else{
      out+='&lt;option value="'+dataArray[k][0]['options'][j]['value']+'"&gt;'+dataArray[k][0]['options'][j]['text']+'&lt;/option&gt;';
    } 
  }
  out+='&lt;/select&gt;<p>';
}
}
out+='&lt;input class="btn btn-outline-success" type="submit" value="'+buttonName+'"&gt;';
out+='&lt;/form&gt;';
$(".form_name").removeAttr("disabled");
$(".cusom_field").attr("disabled","true");
$(".saveForm").attr("disabled","true");
$(".text_body").html(out);
}
function saveNewForm(){
  $.ajax({
    url:"include/add_form.php",
    data:{dataArrayCustom:dataArrayCustom},
    type:"POST",
    success:function(data){
      buildForm(data);
    }
  })
}


</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>a
</body>

</html>