<?php 
require_once 'Twilio/autoload.php'; 
use Twilio\Rest\Client;
require_once 'Mailgun/vendor/autoload.php';
use Mailgun\Mailgun;
class Cron_Holiday extends Cron
{
	protected $arrayId=[];	
	function __construct(){}

	protected function getAllCampaign(){
		$arrayData=[];
		$queryModel="SELECT id,type,campaign_id FROM holiday_builder WHERE type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData[]=array($res['type'],$res['id']);
			}
		}
		return $arrayData;
	}

	protected function updateSuccessedValue(){
		if($this->arrayId){
			foreach ($this->arrayId as $key => $value) {
				$queryModel="UPDATE `contacts` SET `success_sended_holiday`='1' WHERE id='".$value."'";
				mysql_query($queryModel);
			}
		}

	}
	public function getAllAddr($key){
		$arrayData=[];
		$queryModel="SELECT first_name,last_name,address,country,city,state,postal_code FROM contacts WHERE assigned_campaign='".$key."' AND status='Active' AND assign_campaign_type='holiday' AND success_sended_holiday='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData=$res;
			}
		}
		return $arrayData;
	}

	public function getAllPostcard($key){
		$arrayData=[];
		$queryModel="SELECT * FROM postcard WHERE campaign_builder_id='".$key."' AND type_campaign='holiday' AND type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['type_postcard']=='back'){
					$arrayData['back']=$res;
				}else{
					$arrayData['front']=$res;
				}
			}
		}
		return $arrayData;
	}
	public function sendPostcard($dirImg,$frontFile='',$backFile='',$clientId,$arrayAddress,$delayDay){
		$frontLink='https://tracs.me/upload_photo/'.$dirImg.'/'.$frontFile;
		$backLink='https://tracs.me/upload_photo/'.$dirImg.'/'.$backFile;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://rest.clicksend.com/v3/post/postcards/send");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{
			\"file_urls\": [
				\"".$frontLink."\",
				\"".$backLink."\"
			],
			\"recipients\": [
				{
					\"address_name\": \"".$arrayAddress['first_name']." ".$arrayAddress['last_name']."\",
					\"address_line_1\": \"".$arrayAddress['address']."\",
					\"address_line_2\": \"\",
					\"address_city\": \"".$arrayAddress['city']."\",
					\"address_state\": \"".$arrayAddress['state']."\",
					\"address_postal_code\": \"".$arrayAddress['postal_code']."\",
					\"address_country\": \"".$arrayAddress['country']."\",
					\"return_address_id\": 1,
					\"custom_string\": \"\"
				}
			]
		}");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Content-Type: application/json",
			"Authorization: Basic Y29udGFjdFRSQUNTOjBGOTc5RTg5LTAyQzUtNzMxNy02NjVDLUZDNEJEOUI0MURBNg=="
		));
		$response = curl_exec($ch);
		$response=json_decode($response);
		if($response->http_code=='200'){
			$countPrice=Cron_Holiday::getCountPrice($clientId,'postcard');
			Cron_Holiday::addToStatistic($clientId,'campaign','postcard');
			Cron_Holiday::addBillingStat($clientId,$countPrice,'postcard');
			Cron_Holiday::addMoreValue($clientId,'postcard');
		}
	}
	protected function getAllContact($idCampaign){
		$arrayData='';
		$arrayId=[];
		$queryModel="SELECT email,spouse_email,id FROM contacts WHERE assigned_campaign='".$idCampaign."' AND status='Active' AND assign_campaign_type='holiday' AND success_sended_holiday='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['email']!=''){
					$arrayData[]=$res['email'];
					$this->arrayId[]=$res['id'];
				}
				if($res['spouse_email']!=''){
					$arrayData[]=$res['spouse_email'];
					$this->arrayId[]=$res['id'];
				}
			}
		}
		//Cron_Holiday::updateSuccessedValue($arrayId);
		return $arrayData;
	}
	public function getDataOfAdminCampaign($id){
		echo $id;
	}
	private function getGlobalHoliday($id){
		$queryModel="SELECT campaign_id FROM holiday_builder WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		$queryModel="SELECT `date` FROM holiday_builder WHERE id='".$arrayData['campaign_id']."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['date'];
	}
	protected function separatePostcard($arrayAddress,$arrayPostcard,$arrayDelayDay){
		$arrayData=[];
		foreach ($arrayPostcard as $ikey => $ivalue) {
			foreach ($arrayDelayDay as $jkey => $jvalue) {
				$delayDay=Cron_Holiday::getGlobalHoliday($ivalue['front']['campaign_builder_id']);
				if($ikey==$jkey){
					$delayDayF=$ivalue['front']['delay_day_value'];
					$delayDayS=$ivalue['back']['delay_day_value'];
					if($delayDayF==$delayDayS){
						$arrayData[$jkey]=Cron_Holiday::changeDatePostcard($delayDayF,$ivalue,$delayDay);
					}
				}
			}
		}
		return $arrayData;
	}

	public function getUserPhotoDir($id){
		$queryModel="SELECT dir_img FROM user WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['dir_img'];
	}
	protected function changeDatePostcard($delayDay,$arrayPostcard,$delayDate){
		$arrayData=[];
		$date=date("Y-m-d");
		$valueDate=date('Y-m-d',date(strtotime("+".$delayDay." day", strtotime($delayDate))));
		if($date==$valueDate){
			$arrayData=$arrayPostcard;
		}
		return $arrayData;
	}

	protected function getAllText($idCampaign){
		$arrayData='';
		//$date=date("Y-m-d");
		$queryModel="SELECT `text` FROM holiday_builder_text WHERE campaign_builder_id='".$idCampaign."' and type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['text']!=''){
					$arrayData[]=$res['text'];
				}
			}
		}
		return $arrayData;
	}

	protected function getDelayDayContact($idCampaign){
		$arrayData='';
		$queryModel="SELECT delay_day FROM holiday_builder_email WHERE campaign_builder_id='".$idCampaign."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData[]=$res['delay_day'];
			}
		}
		return $arrayData; 
	}

	protected function getDelayDayContactText($idCampaign){
		$arrayData='';
		$queryModel="SELECT delay_day FROM holiday_builder_text WHERE campaign_builder_id='".$idCampaign."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData[]=$res['delay_day'];
			}
		}
		return $arrayData; 
	}

	protected function getAllMobileOfContact($idCampaign){
		$arrayData='';
		$arrayId=[];
		$queryModel="SELECT mobile_phone,alt_phone,spouse_mobile,spouse_alternate,id FROM contacts WHERE assigned_campaign='".$idCampaign."' AND status='Active' AND assign_campaign_type='holiday' AND success_sended_holiday='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['mobile_phone']!='' && $res['alt_phone']!=''){
					$arrayData[]='+'.$res['mobile_phone'];
					$this->arrayId[]=$res['id'];
				}elseif($res['mobile_phone']=='' && $res['alt_phone']!=''){
					$arrayData[]='+'.$res['alt_phone'];
					$this->arrayId[]=$res['id'];
				}

				if($res['spouse_mobile']!='' && $res['spouse_alternate']!=''){
					$arrayData[]='+'.$res['spouse_mobile'];
					$this->arrayId[]=$res['id'];
				}elseif($res['spouse_mobile']=='' && $res['spouse_alternate']!=''){
					$arrayData[]='+'.$res['spouse_alternate'];
					$this->arrayId[]=$res['id'];
				}
			}
		}
		//Cron_Holiday::updateSuccessedValue($arrayId);
		return $arrayData;
	}
	protected function getAllEmail($idCampaign){
		$arrayData='';
		//$date=date("Y-m-d");
		$queryModel="SELECT `text` FROM holiday_builder_email WHERE campaign_builder_id='".$idCampaign."' and type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['text']!=''){
					$arrayData[]=$res['text'];
				}
			}
		}
		return $arrayData;
	}
	
	protected function getAllDateOfContact($idCampaign,$type=''){
		$date=date("Y-m-d");
		$arrayData='';
		$queryModel="SELECT date_of_birth,spouse_date_of_birth,child_dob FROM contacts WHERE assigned_campaign='".$idCampaign."' AND status='Active'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['date_of_birth']!=''){
					$arrayData[]=$res['date_of_birth'];
				}
				if($res['spouse_date_of_birth']!=''){
					$arrayData[]=$res['spouse_date_of_birth'];
				}
				if($res['child_dob']!=''){
					$arrayData[]=$res['child_dob'];
				}
			}
		}
		return $arrayData;
	}

	private function changeDateEmail($array,$value,$text){
		$arrayData='';
		$date=date("Y-m-d");
		$delatDay=Cron_Holiday::getUserPhotoDir();
		foreach ($array as $ikey => $ivalue) {
			$valueDate=date('Y-m-d',date(strtotime("+".$value." day", strtotime($ivalue))));
			if($date==$valueDate){
				$arrayData[]=$text;
			}
		}
		return $arrayData;
	}

	protected function separateEmail($arrayDate,$arrayDelayDay,$arrayEmail){
		$arrayData='';
		foreach ($arrayDate as $ikey => $ivalue) {
			foreach ($arrayDelayDay as $jkey => $jvalue) {
				if($ikey==$jkey){
					$arrayData[$ikey]=Cron_Holiday::changeDateEmail($ivalue,$jvalue[0],$arrayEmail[$ikey]);
				}
			}
		}
		return $arrayData;
	}


	private function changeDateText($array,$value,$text){
		$arrayData='';
		$date=date("Y-m-d");
		foreach ($array as $ikey => $ivalue) {
			$valueDate=date('Y-m-d',date(strtotime("+".$value." day", strtotime($ivalue))));
			if($date==$valueDate){
				$arrayData[]=$text;
			}
		}
		return $arrayData;
	}

	protected function separateText($arrayDate,$arrayDelayDay,$arrayEmail){
		//echo json_encode($arrayDelayDay);
		$arrayData='';
		foreach ($arrayDate as $ikey => $ivalue) {
			foreach ($arrayDelayDay as $jkey => $jvalue) {
				if($ikey==$jkey){
					$arrayData[$ikey]=Cron_Holiday::changeDateText($ivalue,$jvalue[0],$arrayEmail[$ikey]);
				}
			}
		}
		return $arrayData;
	}
	
	protected function updateTextData($email,$text){
		$queryModel="SELECT first_name,last_name,assign_campaign_type,user_id,mobile_phone,email,address,address1,city,state,postal_code FROM contacts WHERE email='{$email}' OR spouse_email='{$email}'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
			foreach ($arrayData as $key => $value) {
				$type='{$'.$key.'}';
				$request=strpos($text,$type);
				if($request){
					$text=str_replace($type, $value, $text);
				}
			}
		}
		return array('text'=>$text,'user_id'=>$arrayData['user_id'],'type'=>$arrayData['assign_campaign_type']);
	}

	protected function addToStatistic($userId,$typeCampaign,$type){
		$date=date("Y-m-d");
		$queryModel="INSERT INTO `statistics`(`user_id`, `type_campaign`, `date`,`type`) VALUES ('".$userId."','".$typeCampaign."','".$date."','".$type."')";
		mysql_query($queryModel);
	}

	protected function getCountPrice($id,$type){
		switch ($type) {
			case 'email':
			$type='base_price';
			break;
			case 'text':
			$type='text_price';
			break;
			case 'postcard':
			$type='postcard_price';
			break;
		}
		$sql="SELECT ".$type." FROM user WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData[$type];
	}

	protected function addBillingStat($id,$count,$type){
		$sql='INSERT INTO `billing_stat`( `user_id`, `count`,`type`) VALUES ("'.$id.'","'.$count.'","'.$type.'")';
		mysql_query($sql);
	}

	protected function addMoreValue($id,$type){
		switch ($type) {
			case 'email':
			$type='type_email';
			break;
			
			case 'text':
			$type='type_text';
			break;

			case 'postcard':
			$type='type_postcard';
			break;
		}
		$sql='UPDATE `payment` SET '.$type.'='.$type.'+1 WHERE user_id='.$id.'';
		mysql_query($sql);
	}

	protected function sendEmailData($email,$text){
		$mgClient = new Mailgun(KEY);
		$domain = DOMAIN;
		$data=Cron_Holiday::updateTextData($email[0],$text[0][0]);
		foreach ($email as $key => $value){
			$result = $mgClient->sendMessage("$domain",
				array('from'    =>  'Excited User <crm@tracs.me>',
					'to'      => 'Mailgun Devs '.$value.'',
					'subject' => 'Hello',
					'html'    => $data['text']));
			$typeAnt=$result->http_response_code;
			if($typeAnt=200){
				Cron_Holiday::addToStatistic($data['user_id'],$data['type'],'email');
				$countPrice=Cron_Holiday::getCountPrice($data['user_id'],'email');
				Cron_Holiday::addBillingStat($data['user_id'],$countPrice,'email');
				Cron_Holiday::addMoreValue($data['user_id'],'email');
			}
		}
	}

	protected function getUserId($value){
		$value = mb_substr($value, 1);
		$queryModel="SELECT user_id,assign_campaign_type FROM contacts WHERE mobile_phone='".$value."' OR spouse_mobile='".$value."' OR spouse_alternate='".$value."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
			return array("user_id"=>$arrayData['user_id'],"type"=>$arrayData['assign_campaign_type']);
		}
	}

	private function getMobileCampaign($id){
		$sql="SELECT assign_num FROM holiday_builder WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['assign_num'];
	}

	protected function sendTextData($text,$mobile,$key){
		$from=Cron_Holiday::getMobileCampaign($key);
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		foreach ($mobile as $key => $value){
			$message = $client->messages->create(
				$value, 
				array(
					'from' => $from,
					'body' => $text[0][0]
				)
			);
			if($message->status=='queued'){
				$userArray=Cron_Holiday::getUserId($value);
				Cron_Holiday::addToStatistic($userArray['user_id'],$userArray['type'],'text');
				$countPrice=Cron_Holiday::getCountPrice($userArray['user_id'],'text');
				Cron_Holiday::addBillingStat($userArray['user_id'],$countPrice,'text');
				Cron_Holiday::addMoreValue($userArray['user_id'],'text');

			}
		}
	}
}

?>