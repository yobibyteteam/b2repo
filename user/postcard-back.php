<html lang="en">
<head>


  <link rel="shortcut icon" href="img/favicon.png">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
  <link href="css/all.css" rel="stylesheet">
  <link href="css/colpick.css"  rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/component.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/chosen.css" type="text/css" >
  <link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
</head>
<style>
  body{background-color: white!important;}
  .canvas-container{
    width: 400px;
    height: 400px;
    position: relative;
    user-select: none;
    background-color: white;
    top: 15%;
    margin: 7.5px;
  }
</style>
<body>
  <div class="test" >
    <div class="row">
      <div class="col-md-3">
        <div class="control_el scroll_top">
          <table cellpadding="0" cellspacing="0" border="0" class="edit_text">
            <tr>
              <td>
                <div class="butt_fonts fonts_color tool"  >
                  <div class="fonts_color_vn" id="color_text">
                    <span class="text">a</span>
                    <div class="fonts_color_bl" style="background: none repeat scroll 0% 0% rgb(51, 51, 51);"></div>
                    <div class="clear"></div>
                  </div>   
                </div>
              </td>
              <td>
                <div class="butt_fonts fonts_b tool block_edit_text" action="bold">b</div>
                <div class="butt_fonts fonts_i tool block_edit_text" action="italic">i</div>
                <div class="butt_fonts fonts_u last tool block_edit_text" action="underline" >u</div>
              </td>
            </tr>
          </table>
        </div>
        <div class="center_text_block" style="width: 100%;margin:0 auto">
          <div class="add_text_block">
            <div class="panel_head">Editing Panel Back</div>
            <div class="clear"></div>
            <div class="block_show" style="overflow: hidden; display: block;">  
              <div class="filds_all">
                <div>
                  <textarea placeholder="Text" class="text_ar" style="resize: none; overflow-y: hidden; position: absolute; top: 0px; left: -9999px; height: 20px; width: 268px; line-height: normal; text-decoration: none solid rgb(0, 0, 0); letter-spacing: normal;" tabindex="-1">
                  </textarea>
                  <textarea name="fio_v" placeholder="Text" class="text_ar" style="resize: none; overflow-y: hidden;"></textarea>
                  <a onclick="del_input(this)" class="del_fil tool" cus="fio_v" title="Delete">
                  </a>
                </div>
              </div>  
              <button id="text_add" class=" btn btn-success add_fild tool">
                <span class="add_fild_text">Add Text</span>
              </button>
              <input accept="image/jpeg,image/png" type='file' id='file' onchange="readURL(this);" class="btn btn-default addNewFile" style=" margin-top: 7px;opacity: 0;position: relative;z-index: 1;cursor: pointer;width: 75%;"
              />
              <button id="img_add" style=" position: relative;bottom: 40px;width: 75%;" class="add_img_btn btn btn-primary"><b>Add Img</b></button>
              <p class="load_konstructor"></p>
              <br><br>
              <button style="position: relative;bottom: 50px;" class="btn btn-danger" onclick="clearAll()">Clear All</button>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-9">
        <div class="component">
          <img class="resize-image" id='preview' />
          <canvas id="canvas1" width="400" height="400" style="top: 15%; position: relative;"> </canvas>
          <div class="overlay">
            <div class="overlay-inner">
            </div>
          </div>
        </div>
        <!--<a style="font-size: 21px;color: #20a8d8;" onclick="capture();">Click to Preview Before Sending</a>-->
        <button style="margin-top: 15px;" data-toggle="modal" data-target="#readyCoupon"  class="btn btn-primary" onclick="viewCoupon();">View</button>
      </div>
      <div class="chnage_templ_top"> 
        <select name="tema" id="tema" style="visibility: hidden;"></select>   
      </div>
    </div>
    <div class="modal fade" id="readyCoupon" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
      <div style="margin:auto;" class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="panel-group" id="accordion" style="text-align: center;">
              <img style="width: 100%;" class="imageData" src="#" alt="#">
              <input type="file" name="imgInput" class="photo" id="imgInput" style="display: none" />
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="button" onclick="saveCoupon()" class="btn btn-success" data-dismiss="modal">Save</button>
          </div>
        </div>
      </div>
    </div>  
  </div>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/plugins.js"></script> 
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/libs/pace.min.js"></script>
  <script src="js/app.js"></script>
  <script type="text/javascript" src="js/html2canvas.js"></script>
  <script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
  <script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js" ></script> 
  <script type="text/javascript" src="js/colpick.js"></script>
  <script src="js/all.min.js" ></script>
  <script src="js/construktor.js" defer="defer"></script>
  <script type="text/javascript">
    var src = '';
    var necessarily_data_array=[];
    var token;
    var filename=0;
    var filekey=1;
    function viewCoupon(){
      $("#readyCoupon").modal("show");
    }
    if($.cookie('top_position')!=null){
      top_block_add = $.cookie('top_position');
    }else{top_block_add = "250px";}
    top_position_add = top_block_add.substr(0,3)*1;
    $(".popup_close").click(function(){
      $(".preview_pop").hide();
    });
    function rand( min, max ) { 
      if( max ) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      } else {
        return Math.floor(Math.random() * (min + 1));
      }
    }
    var canvasOperations = {
      loadFromUrl : function(size){
        fabric.Image.fromURL(src, function(image){
          canvas.add(image.set({
            id : 'canvas'+filename,
            alt : 'canvas'+filename,
            width : canvas.width / size,
            top:rand(20,150),
            left:rand(20,150),
            height : canvas.height / size,
            perPixelTargetFind : true,
            targetFindTolerance : 4
          }));
        });
      },
    }

    $(".show_menu_konstr").click(function(){
      $(".chuse_product_konstruktor").fadeIn(200);
    });
    $(".chuse_product_konstruktor_top").click(function(){
      $(".chuse_product_konstruktor").fadeOut(200); 
    })
    $( function() {
      $("#resize").resizable({autoHide:true,alsoResize:'#preview'});
    } );

    function del_input(id){
      var parent=$(id).parent();
      $(parent).remove();
    }

    function viewCoupon(){
      $(".component").css('border', '0px');
      $('.component').html2canvas({
        onrendered: function (canvas) {
          $(".imageData").attr('src',canvas.toDataURL("image/jpeg"));
          $(".component").css('border', '3px solid #49708A');
        }
      });
    }
    function saveCoupon(){
      var typeCampaign=parent.window.type_Campaign;
      var campaignName =parent.window.id_campaign;
      var file = $(".imageData").attr('src');
      var typePostCard='back';
      $.ajax({
        url: 'include/postcard/savePostcard.php',
        data: {file:file,campaignName:campaignName,typeCampaign:typeCampaign,typePostCard:typePostCard},
        type: 'POST',
        success:function(){}})
    }

    function addNewFile(filename){
      $(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');  
    }

    function clearAll(){
      canvas.clear();
      filename=0;
      filekey=1;
      $(".field").each(function(){
        $(this).hide();
      })
    }

    function delFile(id,idFile){
      var key=idFile+1;
      var name='canvas'+key;
      var lengthCanvas = canvas['_objects'].length;
      for(var k in canvas['_objects']){
        console.log(canvas['_objects'][k]['alt']);
        if(canvas['_objects'][k]['alt']==name){
          canvas['_objects'][k].remove();
          var parent=$(id).parent();
          $(parent).hide();
        }
      }
      filekey--;
      canvas.renderAll();
      $(".addNewFile").val('');
    }

    $(document).ready(function(){
      getLogoPicture();
    })

    function getLogoPicture(){
      $.ajax({
        url:"include/getPicture.php",
        type:"POST",
        success:function(data){
          var json=JSON.parse(data);
          if(json['postcard_tagline']){
            //$("textarea[name='fio_v']").val(json['postcard_tagline']);
            var text = json['postcard_tagline'].replace(/<[^>]+>/g,'');
            var fio_v = new  fabric.Text(text, {
              fontFamily: 'Arial',
              top:5,
              left:5,
              fontSize: 16,
              lockUniScaling :false,
              originX: 'left',
              originY: 'top'
            });  
            //fio_v._set("name_element", "fio_v");
            canvas.add(fio_v);
          }
          if(json['logo']){
            addPictureLogo(json['logo']);
          }
          if(json['picture']){
            addPictureLogo(json['picture']);
          }
        }
      })
    }

    function addPictureLogo(obj){
      src=obj;
      canvasOperations.loadFromUrl(3);
      //addNewFile(filename);
      //filename++;
      //filekey++;

    }

    function readURL(input){
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          src=e.target.result;
          canvasOperations.loadFromUrl(1.8);
        };
        addNewFile(filename);
        reader.readAsDataURL(input.files[0]);
        filename++;
        filekey++;
      }
    }

    $(".del_fil").click();
  </script>
</body>
</html>