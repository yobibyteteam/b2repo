<?php
require_once 'class/Audio.php';
include 'connect_to_bd.php';
session_start();
if(isset($_FILES["audio-blob"])){
	$audio=$_FILES["audio-blob"];
	$file_name=$_POST["filename"];
	$token=$_SESSION['user_token'];
	$id_campaign=$_POST['id_campaign'];
	$bd=$_POST['bd'];
	$delay_day_rvmd=$_POST['delay_day_rvmd'];
	$repeat_rvmd=$_POST['repeat_rvmd'];
	$audio=new Audio();

	try
	{
		$audio->get_id_client($token);
		$audio->get_dir_client($file_name);
		$audio->upload($_FILES["audio-blob"]["tmp_name"]);
		$audio->insert_into_db($id_campaign,$file_name,$bd,$delay_day_rvmd,$repeat_rvmd);
		$success=array("success"=>"1","dir"=>$audio->directoryView);
		echo str_replace('\/','/',json_encode($success));
	}catch(Exception $e)
	{
		echo json_encode(array("success" => "0","error" => $e->getMessage()));
	}



}
?>