<?php
include '../include/connect_to_bd.php';
if(isset($_GET['token'])){
  $id=$_GET['token'];
  $if=mysql_real_escape_string($id);
  $query=mysql_query("SELECT * FROM review_builder WHERE md5='$id'");
  $array=mysql_fetch_array($query);
  $user_id=$array['user_id'];
  $query_model="SELECT * FROM user WHERE id='".$user_id."'";
  $query=mysql_query($query_model);
  $array_user=mysql_fetch_array($query);
  $dir=getImage($array['name'],$array_user);
  //$dir='../upload_photo/'.$array_user['dir_img'].'/'.$array_user['logo'];
}else
{
 header("Location: ../");
}

function getImage($type,$arrayUser){
  switch ($type) {
    case 'logo':
    return '../upload_photo/'.$arrayUser['dir_img'].'/'.$arrayUser['logo']; 
    break;
    
    case 'picture':
    return '../upload_photo/'.$arrayUser['dir_img'].'/'.$arrayUser['picture'];
    break;
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="<?php echo $array['share_text']; ?>">
  <title>Review</title>

  <!-- Icons -->
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/simple-line-icons.css" rel="stylesheet">
  <link rel="stylesheet" href="jssocials-theme-flat.css">
  <link rel="stylesheet" href="jssocials.css">
  <!-- Main styles for this application -->
  <link href="../css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <style>
  @media (min-width: 768px){
    .col-md-8{
      max-width: 80%;
    }
  }

  .main .container-fluid{
    padding:0;
  }
  input[type="date"]{
    width: 100%;
    text-align: center;
    height: calc(2.09375rem + 2px);
    padding-left: 40px;
    color: #9faecb;
  }
  input[type='number'] {
    -moz-appearance: textfield;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;

  }
  input{
    border: 1px solid #e1e6ef;
  }
  textarea{
    border: 1px solid #e1e6ef;
  }
  .starclick:before{
    color:#e3cf7a!important;
    content: "\f005"!important;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }
  .rating {
    unicode-bidi: bidi-override;
    direction: rtl;
    font-size: 30px;
  }
  .rating span.star,
  .rating span.star {
    font-family: FontAwesome;
    font-weight: normal;
    font-style: normal;
    display: inline-block;
  }
  .rating span.star:hover,
  .rating span.star:hover {
    cursor: pointer;
  }
  .rating span.star:before,
  .rating span.star:before {
    content: "\f006";
    padding-right: 5px;
    color: #999999;
  }
  .rating span.star:hover:before,
  .rating span.star:hover:before,
  .rating span.star:hover ~ span.star:before,
  .rating span.star:hover ~ span.star:before {
    content: "\f005";
    color: #e3cf7a;
  }
  .main{
    margin-right: 0!important;
    margin-left: 0!important;
  }
  .card{
    display:block;
    background-color: #f2f4f8;
    border:0;
  }
  .text_h5{
    font-size: 20px;
    font-family: "Raleway", sans-serif;
    font-style: normal;
    font-weight: 300;
    color: #666666;
    text-align: center;
    line-height: 30px;
    margin-bottom: 20px;
  }
  .text_h1{
    font-size: 60px;
    font-family: "Raleway", sans-serif;
    font-style: normal;
    font-weight: 300;
    color: #454545;
    letter-spacing: -1px;
    text-align: center;
    line-height: 69px;
    margin-bottom: 20px
  }
  .text_h3{
    font-family: "Raleway", sans-serif;
    font-style: normal;
    font-weight: 300;
    color: #454545;
    letter-spacing: -1px;
    text-align: center;
    line-height: 40px;
    margin-bottom: 20px
  }
</style>

</head>

<body id="<?php echo $id; ?>" class="app header-fixed sidebar-fixed  aside-menu-fixed aside-menu-hidden">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>

  <!-- Main content -->
  <main class="main">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <div class="card" style="margin-bottom: 0!important;">
            <div class="card-block text-center" style="text-align: center;padding: 10px!important">
              <div class="img_wrapp">
                <img src="<?php echo $dir; ?>" alt="" style="max-width: 100%;height: auto;width:auto;">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="animated fadeIn form_question">
        <div class="important_quest">

        </div>
        <?php echo $array['form']; ?>
      </div>
    </div>

  </main>

  <div class="modal fade" id="write_friend" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Enter Your Friend Name</h4>
        </div>
        <div class="modal-body">
          <p>Enter Your Friend Name</p>
          <input placeholder="Enter Your Friend Name" type="text" class="form-control friend_name">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="hide_model()">Close</button>
          <button type="button" class="btn btn-primary" onclick="update_attr()">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="../js/libs/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="../js/libs/bootstrap.min.js"></script>
  <script src="jssocials.min.js"></script>
  <!-- Custom scripts required by this view -->
  <script src="../js/views/main.js"></script>
  <script>
    $( function() {
      $( "#review_birthday_mounth" ).datepicker();
    } );
    var id;
    var star_val;
    var json;
    var error;
    var array_answer=[];
    var array_question=[];
    var mobile;

  /*var out="<p><button class='copy_code btn btn-success add_item'>Copy Form</button></p>";
  $('.btn_save_form').append(out);
  $('.copy_code').click(function(){
    $('.sk-circle').css('display','block');
    $('#copy_code').modal();
    $('.sk-circle').css('display','none');
  })
    var out="<p><button class='btn btn-success' onclick='send()'>Send</button></p>";
    $('.btn_section').append(out);
    */
    function show_modal(id)
    {
      if($(id).val()=='Friend'){
        $('#write_friend').modal("show");
      }
    }

    function socialLink(){
      $("#share").jsSocials({
        url: "https://tracs.me",
        text: "<?php echo $array['share_text']?>",
        showLabel: false,
        showCount: false,
        shares: ["facebook", "googleplus", "linkedin"]
      });
    }
    function update_attr()
    {
      if($('.friend_name').val()==''){
        alert("Enter Name");
      }else{
        var text=$('.friend_name').val();
        var fn=$('.hear_about').find('option[value=Friend]');
        fn.attr('value','Friend('+text+')');
        $('#write_friend').modal('hide');
      }
    }

    function hide_model()
    {
      $('.hear_about').val('0');
      $('#write_friend').modal('hide');
    }


    function modal_friend(){
      $('.sk-circle').css('display','block');
      $('#write_friend').modal();
      $('.sk-circle').css('display','none');
    }

    function inner_html(array){
      out='<br>';
      out+='<div class="col-md-12 text-center 1">';
      out+='<p><h1 class="text_h1">Thank You For Taking The Time To Provide Feedback!</h1></p>';
      out+='<p><h5 class="text_h5">We are so happy to hear you had a great experience with us, we hope to do business with you again soon, and all of your friends and family!</h4></p>';
      out+='<p><h3 class="text_h3">If you could please take one more minute to leave us a review on either, Google, Facebook, or Yelp? Or at the very least, share us with your friends by clicking the appropriate image below. Thanks in advance!</h3></p>';
      out+='</div>';
      out+='<div class="col-md-12 text-center">';
      if(array['facebook_link']!=''){
        //out+='<button type="button" class="btn btn-md fa fa-facebook text" style="margin-right: 10px;color:#3b5998"></button>';
        out+='<a href="'+array['facebook_link']+'"><img src="http://contacttracs.com/wp-content/uploads/2017/09/Screen-Shot-2017-09-21-at-4.32.17-PM-e1506033312318.png"></a><br><br>';
      }
      if(array['google_link']!=''){
        //out+='<button type="button" class="btn btn-md fa fa-google-plus text" style="margin-right: 10px;color:#bb4b39"></button>';
        out+='<a href="'+array['google_link']+'"><img src="http://contacttracs.com/wp-content/uploads/2017/09/Screen-Shot-2017-09-21-at-4.33.14-PM-e1506033396566.png"</a><br><br>';
      }
      if(array['yelp_link']!=''){
        //out+='<button type="button" class="btn btn-md fa fa-yelp text" style="margin-right: 10px;color:#cb2027"></button>';
        out+='<a href="'+array['yelp_link']+'"><img src="http://contacttracs.com/wp-content/uploads/2017/09/Screen-Shot-2017-09-21-at-4.39.07-PM-e1506033637999.png"></a><br><br>';
      }
      out+='<br><p><h5 class="text_h5"><b>Or at least share us with your friends, thanks in advance!</b></h5></p>';
      out+='</div>';
      if(array['share_text']!=''){
        out+='<div>';
        out+='<div id="share"></div>';
        out+='</div>';
      }
      $('.card2').html(out).fadeIn("slow");
      socialLink();
    }

    function check_textarea()
    {
      $('textarea').each(function(){
        if($(this).val()==''){
          error="Empty Data";
          throw new SyntaxError("Empty Data");
        }
      });
    }

    function check_input()
    {
      $('.input_data').each(function(){
        if($(this).val()==''){
          error="Empty Data";
          throw new SyntaxError("Empty Data");
        }
      });
    }


    function check_select()
    {
      $('#hear_about').each(function(){
        if($(this).val()=='0'){
          error="Choose Answer";
          throw new SyntaxError("Empty Data");
        }
      });
    }
    /*
    $('select').on('change',function(){
      if($(this).val()=='Friend'){
       alert(1);
      }
    })*/

    function check_input_data()
    {
      var textarea_count=$('body').find('textarea');
      var input_count=$('body').find('textarea');
      var hear_about=$('body').find('select');
      if(input_count!='undefined')
      {
        check_input();
      }
      if(hear_about!='undefined')
      {
        check_select();
      }
    }


    function build_array()
    {
      var i=1;
      var answer = $('body').find('.build_input');
      answer.each(function(){
        var value=($(this).val());
        array_answer.push(value);
        i++;
      })
      for (var j = 1; j < i; j++) {
        array_question.push($('.question'+j).text());
      }
    }
    function send(){
      try{
        check_input_data();
        build_array();
        set_ajax(array_answer,array_question);
        var name=$('.input_data[placeholder="Enter Full Name"]').val();
        $("main").attr("name",name);
        token=$('.card2').attr('id');
        var out;
        out='<br><div class="col-md-12 text-center">';
        out+='Please rate your experience, 5 being the best:<br>';
        out+='<span class="rating"><span onclick="star(5)" class="star star5"></span><span onclick="star(4)" class="star star4"></span><span onclick="star(3)" class="star star3"></span><span onclick="star(2)" class="star star2"></span><span onclick="star(1)" class="star star1"></span></span><br><br>';
        out+='</div>';

        $('.card2').append(out).fadeIn("slow");
        $('.card2').html('');
        $('.card2').append(out);
      }catch(e){
        alert(error);
      }
    }
    function star(id){
      star_val=id;
      update_rate(star_val);
      var j;
      for (var i = 0; i <= id; i++) {
        $('.star'+i).addClass('starclick');
      }
      j=5-id+1;
      if(j>0){
        for (var i = id+1; i <=5; i++) {
          $('.star'+i).removeClass('starclick');
        }
      }
      if(star_val>3){
        $.ajax({
          url:"../user/include/get_review_data.php",
          data:{token:token},
          type:"POST",
          success:function(data){
            //alert(data);
            inner_html(JSON.parse(data));
          }
        })
      }else{
        out='<br><div class="col-md-12 text-center">';
        out+='<h1 class="text_h1">Thanks for your feedback!</h1><br><br>';
        out+='<h5 class="text_h5">Sorry we did not meet your 5 star expectations! We promise to make it right next time! A company manager may reach out to you about this experience.</h5><br><br>';
        out+='</div>';
        $('.card2').html('');
        $('.card2').append(out);
      }
    }

    function set_ajax(array1,array2)
    {
      mobile=$("#review_phone").val();
      var token=$('body').attr('id');
      $.ajax({
        url:"add_new_feedback.php",
        data:{array1:array1,array2:array2,token:token},
        type:"POST",
        success:function(data){
          //alert(data);
          parsing_data(data);
        }
      })
    }

    function update_rate(star){
      var name=$("main").attr("name");
      $.ajax({
        url:"add_star_rate.php",
        data:{id:id,star:star,mobile:mobile,name:name},
        type:"POST",
        success:function(data){
          //alert(data);
        }
      })
    }

    function parsing_data(array)
    {
      //alert(array);
      var json=JSON.parse(array);
      id=json['id'];

    }
    add_important_question();
    function add_important_question()
    {
      var hear_about=$(".hear_about_quest").html();
      $(".hear_about_quest").html(hear_about+"(<span style='color:red;vertical-align:middle'>*</span>)");
      //alert(hear_about);
      var card=$(".card2").html();
      //console.log(card);
      $(".card2").html('');
      var out='<br><h5>Feedback & Reputation Request Tool <br> Please Enter The Customer or Client`s Information Below</h5><br>';
      out+='<div class="col-md-8" style="margin:0 auto; flex:0;"><p class="holiday question1">Full Name?(<span style="color:red;vertical-align:middle">*</span>)</p></div>';
      out+='<div class="col-md-8" style="margin:0 auto; flex:0;"><input name="textarea-input" rows="5" class="build_input input_data form-control" placeholder="Enter Full Name"></input></div><br>';
      out+='<div class="col-md-8" style="margin:0 auto; flex:0;"><p class="holiday question2">Email?(<span style="color:red;vertical-align:middle">*</span>)</p></div>';
      out+='<div class="col-md-8" style="margin:0 auto; flex:0;"><input name="textarea-input" rows="5" class="build_input input_data form-control" placeholder="Enter Email"></input></div><br>';
      out+='<div class="col-md-8" style="margin:0 auto; flex:0;"><p class="holiday question3">Customer Mobile Phone?(<span style="color:red;vertical-align:middle">*</span>)</p></div>';
      out+='<div class="col-md-8" style="margin:0 auto; flex:0;"><input type="number" name="textarea-input" rows="5" class="checkMobile build_input input_data form-control" placeholder="Enter Mobile Phone" id="review_phone"></input></div><br>';
      $(".card2").html(out);
      $(".card2").append(card);
    }

  </script>
  <script type="text/javascript">
    $(".checkMobile").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl/cmd+A
           (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+C
           (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+X
           (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: home, end, left, right
           (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
             return;
           }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    });
  </script>
</body>

</html>
