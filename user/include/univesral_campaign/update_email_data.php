<?php
include '../connect_to_bd.php';
require_once '../classes/Universal_campaign.php';

$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_data'];
$delay_day_email=$_POST['delay_day_email'];
$un_campaign=new Universal_campaign();

try{
	$un_campaign->update_email_data($id_campaign,$editor_text,$delay_day_email);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>