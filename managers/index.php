<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$session_token=$_SESSION['user_token'];
$query=mysql_query("SELECT client_id FROM token WHERE token='$session_token'");
$array_token=mysql_fetch_assoc($query);

$user_id=getUserId($array_token['client_id']);
$text='';
function get_list_contacts($id){
    $query=mysql_query("SELECT * FROM contacts WHERE user_id='$id'");
    if(mysql_num_rows($query)!=0){
        while ($res =mysql_fetch_assoc($query)) {
            echo '
            <tr>
                <td>'.$res['first_name'].'</td>
                <td>'.$res['data_register'].'</td>
                <td><div class="col-md-9">';
                    if($res['type']=='Customer'){
                     echo '<select style="background:#67c2ef!important" onchange="select_type(this)"  name="'.$res['id'].'" class="badge badge-warning btn_user_table_warning select_type">
                     <option value="'.$res['type'].'">'.$res['type'].'</option>
                     ';
                 }else{
                    echo '<select onchange="select_type(this)"  name="'.$res['id'].'" class="badge badge-warning btn_user_table_warning select_type">
                    <option value="'.$res['type'].'">'.$res['type'].'</option>
                    ';
                }
                if($res['type']=='Customer'){
                    echo '<option value="Prospect">Prospect</option>
                </select>';
            }else{
                echo '<option value="Customer">Customer</option>
            </select>';
        }
        echo '</div></td>
        <td>
            <div class="col-md-9">';
                if($res['status']=='Active'){
                    echo '<select onchange="select_status(this)" name="'.$res['id'].'" class="badge badge-success btn_user_table_success select_status">
                    <option value="'.$res['status'].'">'.$res['status'].'</option>';
                }else{
                    echo '<select style="background:#ff5454!important" onchange="select_status(this)" name="'.$res['id'].'" class="badge badge-success btn_user_table_success select_status">
                    <option value="'.$res['status'].'">'.$res['status'].'</option>';
                }
                if($res['status']=='Active'){
                    echo '<option value="Inactive">Inactive</option>';
                }else{
                    echo '<option value="Active">Active</option>';
                }
                echo '</select></div>
            </td>
            <td class="table_button" style="width:113px!important;">

                <a class="btn btn-info " href="edit-profile.php?token='.$res['md5_hash'].'">
                    <i class="fa fa-edit "></i>
                </a>
                <button type="button" onclick="change_id('.$res['id'].')" class="btn btn-danger btn_user_table_danger" data-toggle="modal" data-target="#dangerModal">
                    <i class="fa fa-trash-o "></i>
                </button>

            </td>
        </tr>';
    }
}
}
function getUserId($managerId){
  $queryModel="SELECT user_id FROM managers WHERE id='".$managerId."'";
  $query=mysql_query($queryModel);
  $arrayData=mysql_fetch_assoc($query);
  return $arrayData['user_id'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <style>
      body{
          position: relative;
      }
      .sk-circle{
          display: none;
          z-index: 10001;
          width: 40px;
          height: 40px;
          position: absolute;
          top: 0;
          bottom: 0;
          right: 0;
          left: 0;
          margin: auto;
      }
  </style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
        <a class="navbar-brand" href="#"></a>
        <ul class="nav navbar-nav d-md-down-none b-r-1">
          <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>

    </ul>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item dropdown" style="margin-right: 30px!important">
        <a class="nav-link nav-pill avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img src="img/avatars/10.png" class="img-avatar" alt="admin@bootstrapmaster.com">
      </a>
      <div class="dropdown-menu dropdown-menu-right">

          <div class="dropdown-header text-center">
            <strong>Account</strong>
        </div>
        <a class="dropdown-item" href="include/logout_user.php"><i class="fa fa-lock"></i> Logout</a>
    </div>
</li>
</ul>
</header>

<div class="app-body">
    <?php include "sidebar.php"; ?>

    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="user-profile.php">Home</a></li>
        <li class="breadcrumb-item active">Contacts</li>
    </ol>
    <div class="container-fluid">



        <div class="animated fadeIn">

            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i> User Contacts
                    <div class="card-actions">

                    </div>
                </div>
                <div class="card-block">
                    <table class="table table-striped table-bordered datatable table-responsive">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Date registered</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php get_list_contacts($user_id); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this user?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" onclick="del_contact()" data-dismiss="modal" class="btn btn-primary">Yes</button>
                </div>
            </div>
        </div>
    </div>
</main>

</div>


<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>


<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>


<!-- GenesisUI main scripts -->

<script src="js/app.js"></script>





<!-- Plugins and scripts required by this views -->
<script src="js/libs/jquery.dataTables.js"></script>
<script src="js/libs/dataTables.bootstrap4.js"></script>


<!-- Custom scripts required by this view -->
<script src="js/views/tables.js"></script>
<script>
    var contacts_id_for_del;

    function change_id(id){
        contacts_id_for_del=id;
    }

    function del_contact()
    {
        $('.sk-circle').css('display','block');

        $.ajax({
            url:"include/contacts/delete_contacts.php",
            data:{contacts_id_for_del:contacts_id_for_del},
            type:"POST",
            success:function(data){
                window.location=location;
            }
        })
        $('.sk-circle').css('display','none');
    }

    function select_type(id)
    {
        if($(id).val()=='Customer'){
            $(id).attr('style','background:#67c2ef!important');
        }else{
            $(id).attr('style','background:');
        }
        $('.sk-circle').css('display','block');
        var type=$(id).val();
        var id=$(id).attr('name');
        //alert(type+"/"+id);
        $.ajax({
            url:"include/contacts/change_contact_type.php",
            data:{type:type,id:id},
            type:"POST",
            success:function(data){
                //alert(data);
            }
        })
        $('.sk-circle').css('display','none');
    }
    function select_status(id)
    {
        if($(id).val()=='Inactive'){
            $(id).attr('style','background:#ff5454!important');
        }else{
            $(id).attr('style','background:');
        }
        $('.sk-circle').css('display','block');
        var status=$(id).val();
        var id=$(id).attr('name');

        $.ajax({
            url:"include/contacts/change_contact_status.php",
            data:{status:status,id:id},
            type:"POST",
            success:function(data){
                //alert(data);
            }
        })
        $('.sk-circle').css('display','none');
    }
</script>
</body>

</html>
