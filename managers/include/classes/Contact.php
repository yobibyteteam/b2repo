<?php
include '../connect_to_bd.php';
include '../../../cron/config.php';
include '../../../Twilio/autoload.php';
use Twilio\Rest\Client;
class Contact
{
	var $array_main_data, $array_alt_data , $email_contact , $user_id , $current_contact_id , $user_token,$list_contact;

	function add_new_contact()
	{
		$date_current=date("Y-m-d");
		$md5=md5($date_current).mt_rand();
		$arrayData=explode("/", $this->array_main_data[0]);
		$query_model="INSERT INTO `contacts`( `assigned_campaign`,`assign_campaign_type`,`first_name`, `last_name`, `email`, `mobile_phone`, `alt_phone`, `address`, `address1`, `city`, `state`, `postal_code`, `date_of_birth`, `religion`, `type`, `status`, `spouse_first_name`, `spouse_last_name`, `spouse_email`, `spouse_mobile`, `spouse_alternate`, `spouse_date_of_birth`, `spouse_religion`, `child_name`, `child_dob`, `product_service_purchased`, `user_id` , `data_register`,`md5_hash`,`date_added`,`country`) VALUES ('".$arrayData[0]."','".$arrayData[1]."','".$this->array_main_data[1]."','".$this->array_main_data[2]."','".$this->array_main_data[3]."','".$this->array_main_data[4]."','".$this->array_main_data[5]."','".$this->array_main_data[6]."','".$this->array_main_data[7]."','".$this->array_main_data[8]."','".$this->array_main_data[9]."','".$this->array_main_data[10]."','".date("Y-m-d", strtotime($this->array_main_data[11]))."','".$this->array_main_data[12]."','".$this->array_main_data[13]."','".$this->array_main_data[14]."','".$this->array_main_data[15]."','".$this->array_main_data[16]."','".$this->array_main_data[17]."','".$this->array_main_data[18]."','".$this->array_main_data[19]."','".date("Y-m-d", strtotime($this->array_main_data[20]))."','".$this->array_main_data[21]."','".$this->array_main_data[22]."','".date("Y-m-d", strtotime($this->array_main_data[23]))."','".$this->array_main_data[24]."','".$this->user_id."','".$date_current."','".$md5."','".$date_current."','".$this->array_main_data[25]."')";
		$query=mysql_query($query_model);
		$this->current_contact_id=mysql_insert_id();
		if(!$query){
			throw new Exception("error_query", 1);	
		}	
	}

	function getLeadsrainListId($id,$type){
		switch ($type) {
			case 'campaign':
			$type='campaign_builder_rvmd';
			break;

			case 'dated':
			$type='dated_builder_rvmd';
			break;
		}
		$sql="SELECT leadsrain_list_id FROM ".$type." WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
			return $arrayData['leadsrain_list_id'];
		}else{
			return false;
		}
	}

	function getCurrCampaign($id){
		$sql="SELECT assigned_campaign,assign_campaign_type FROM contacts WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return array($arrayData['assigned_campaign'],$arrayData['assign_campaign_type']);
	}

	function add_field_input()
	{
		$count=count($this->array_alt_data);
		if($count>1){
			$j=0;
			for ($i=0; $i < $count/2; $i++) { 
				$query_model="INSERT INTO `custom_field_contacts`(`contacts_id`, `label`, `text`) VALUES ('".$this->current_contact_id."','".$this->array_alt_data[$j]."','".$this->array_alt_data[$j+1]."')";
				mysql_query($query_model);
				$j=$j+2;
			}
		}
	}
	static public function getContactData($id){
		$query_model="SELECT mobile_phone,user_id FROM contacts WHERE id='".$id."'";
		$query=mysql_query($query_model);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData;
	}
	static public function getReviewLink($id){
		$query_model="SELECT md5,welcome_text FROM review_builder WHERE user_id='".$id."'";
		$query=mysql_query($query_model);
		$arrayData=mysql_fetch_assoc($query);
		$link="https://tracs.me/review/overview.php?token=".$arrayData['md5'];
		return array("link"=>$link,"welcome_text"=>$arrayData['welcome_text']);
	}
	static public function deleteFromQuick($id){
		$query_model="DELETE FROM quick_appointment WHERE contacts_id='".$id."'";
		mysql_query($query_model);
	}
	static public function sendSms($phone,$text,$welcomeText){
		$phone='+'.$phone;
		$text=$welcomeText.' '.$text;
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		$message = $client->messages->create(
			$phone, 
			array(
				'from' => '+17202509876',
				'body' => $text
			)
		);
	}

	function get_current_user($token)
	{
		$this->user_token=$token;
		if($this->user_token==''){
			throw new Exception("Empty_token", 1);
		}else{
			$query_model="SELECT * FROM token WHERE token='".$this->user_token."'";
			$query=mysql_query($query_model);
			if(mysql_num_rows($query)==0){
				throw new Exception("not_found_token", 1);
			}else{
				$array_token=mysql_fetch_array($query);
				$this->user_id=$array_token['client_id'];
			}
		}
	}

	function check_originality_data(&$array1,&$array2)
	{
		$this->array_main_data = $array1;
		$this->array_alt_data = $array2;
		$this->email_contact = $this->array_main_data[3];
		$query_model="SELECT * FROM contacts WHERE email='".$this->email_contact."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			throw new Exception("contact_exist", 1);
		}
	}

	function change_type($type,$id)
	{
		$query_model="UPDATE `contacts` SET `type`='".$type."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function change_status($status,$id)
	{
		$query_model="UPDATE `contacts` SET `status`='".$status."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function delete_contacts($id)
	{
		$query_model="DELETE FROM  `contacts` WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function delete_custom_field($id)
	{
		$query_model="DELETE FROM  `custom_field_contacts` WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function getInfoCampaign($contact_id){
		$query_model="SELECT assigned_campaign,assign_campaign_type FROM contacts WHERE id='".$contact_id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$arrayContact=mysql_fetch_assoc($query);
			$arrayData=array($arrayContact['assigned_campaign'],$arrayContact['assign_campaign_type']);
		}
		return $arrayData;
	}
	function change_main_data(&$contact_id){
		$date=date("Y-m-d");
		$arrayData=explode("/", $this->array_main_data[14]);
		$arrayDate=Contact::getInfoCampaign($contact_id);
		$query_model='UPDATE `contacts` SET `first_name`="'.$this->array_main_data[0].'",`last_name`="'.$this->array_main_data[1].'",`email`="'.$this->array_main_data[2].'",`mobile_phone`="'.$this->array_main_data[3].'",`alt_phone`="'.$this->array_main_data[4].'",`address`="'.$this->array_main_data[5].'",`address1`="'.$this->array_main_data[6].'",`city`="'.$this->array_main_data[7].'",`state`="'.$this->array_main_data[8].'",`postal_code`="'.$this->array_main_data[9].'",`date_of_birth`="'.date("Y-m-d", strtotime($this->array_main_data[10])).'",`religion`="'.$this->array_main_data[11].'",`type`="'.$this->array_main_data[12].'",`status`="'.$this->array_main_data[13].'",`spouse_first_name`="'.$this->array_main_data[15].'",`spouse_last_name`="'.$this->array_main_data[16].'",`spouse_email`="'.$this->array_main_data[17].'",`spouse_mobile`="'.$this->array_main_data[18].'",`spouse_alternate`="'.$this->array_main_data[19].'",`spouse_date_of_birth`="'.date("Y-m-d", strtotime($this->array_main_data[20])).'",`spouse_religion`="'.$this->array_main_data[21].'",`child_name`="'.$this->array_main_data[22].'",`child_dob`="'.date("Y-m-d", strtotime($this->array_main_data[23])).'",`product_service_purchased`="'.$this->array_main_data[24].'",`country`="'.$this->array_main_data[25].'" WHERE id="'.$contact_id.'"';
		mysql_query($query_model);
		if($arrayDate[0]!=$arrayData[0] && $arrayDate[1]!=$arrayData[1]){
			$query_model='UPDATE contacts SET `assigned_campaign`="'.$arrayData[0].'",`assign_campaign_type`="'.$arrayData[1].'",`date_added`="'.$date.'" WHERE id="'.$contact_id.'"';
			mysql_query($query_model);
		}	
	}

	function change_alt_data(&$contact_id)
	{
		//echo json_encode($this->array_alt_data);
		$count=count($this->array_alt_data);
		if($count>1){
			$j=0;
			for ($i=0; $i < $count/2; $i++) {
				$query_model_2=mysql_query("SELECT * FROM `custom_field_contacts` WHERE `label`='".$this->array_alt_data[$j]."'");
				if(mysql_num_rows($query_model_2)!=0){
					$query_model="UPDATE `custom_field_contacts` SET `label`='".$this->array_alt_data[$j]."',`text`='".$this->array_alt_data[$j+1]."' WHERE `contacts_id`='".$contact_id."'";
					mysql_query($query_model);
					$j=$j+2;
				}else{
					$query_model="INSERT INTO `custom_field_contacts`(`contacts_id`, `label`, `text`) VALUES ('".$contact_id."','".$this->array_alt_data[$j]."','".$this->array_alt_data[$j+1]."')";
					mysql_query($query_model);
					$j=$j+2;
				}
			}
		}

	}

	function check_for_change(&$array1,&$array2){
		$this->array_main_data = $array1;
		$this->array_alt_data = $array2;
		$this->email_contact = $this->array_main_data[3];
		$query_model="SELECT * FROM contacts WHERE email='".$this->email_contact."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)>1){
			throw new Exception("email_exist", 1);
		}
	}
	function add_to_activity_log($text)
	{
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		$array_token=mysql_fetch_array($query);
		$id_user=$array_token['client_id'];
		$time_current=date("h:i:s");
		$date_current=date("Y-m-d");
		$query_model="INSERT INTO activity_log(`notification`,`date`,`time`,`user_id`,`type`) VALUES('".$text."','".$date_current."','".$time_current."','".$id_user."','user')";
		mysql_query($query_model);
	}

	function get_list_contact($id)
	{
		$query_model="SELECT first_name,email,mobile_phone,date_added FROM contacts WHERE assigned_campaign='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0)
		{
			throw new Exception("empty", 1);	
		}else{
			$i=1;
			while ($res=mysql_fetch_assoc($query)) {
				$this->list_contact[$i]=$res;
				$i++;
			}
			$this->list_contact["count"]=$i-1;
		}
	}
}


?>