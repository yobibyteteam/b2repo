<?php

class User
{
	var $array_user , $error , $array_data ,$dir ,$id ,$admin_name ,$time_current ,$date_current, $query ,$photo_name ,$dir_image;

	function set_data(&$input_data,$include_function){
		$md5=md5(rand(5,15).mt_rand().time());
		mkdir('../../../uploads_audio/'.$md5, 0777, true);
		$this->array_user=$input_data;
		$this->query=mysql_query("SELECT * FROM user WHERE mobile_phone='".$this->array_user[4]."' AND email='".$this->array_user[3]."'");
		$this->dir=md5($this->array_user[12]).time();
		$this->pass_md5=md5($this->array_user[14]);
		$this->array_data = array(
			array(
				'on_call_phone_number'=>$this->array_user[0],
				'first_name'=>$this->array_user[1],
				'last_name'=>$this->array_user[2],
				'email'=>$this->array_user[3],
				'mobile_phone'=>$this->array_user[4],
				'alt_phone'=>$this->array_user[5],
				'street_address'=>$this->array_user[6],
				'suite'=>$this->array_user[7],
				'state'=>$this->array_user[8],
				'city'=>$this->array_user[9],
				'postal_code'=>$this->array_user[10],
				'birthday_mounth'=>$this->array_user[11],
				'industry'=>$this->array_user[12],
				'username'=>$this->array_user[13],
				'password'=>$this->pass_md5,
				'admin_id'=>$this->array_user[15],
				'tagline1'=>$this->array_user[16],
				'tagline2'=>$this->array_user[17],
				'tagline3'=>$this->array_user[18],
				'email_tagline'=>$this->array_user[23],
				'postcard_tagline'=>$this->array_user[24],
				'base_price'=>$this->array_user[19],
				'postcard_price'=>$this->array_user[20],
				'rvm_price'=>$this->array_user[21],
				'text_price'=>$this->array_user[22],
				'logo'=>'',
				'picture'=>'',
				'type'=>'user',
				'dir_img'=>$this->dir,
				'md5'=>$md5,
				'include_function'=>$include_function
			)
		);
	}

	function set_data_for_change(&$input_data,&$id){
		$this->id=$id;
		$this->array_user=$input_data;
		$this->query=mysql_query("SELECT * FROM user WHERE mobile_phone='".$this->array_user[4]."' AND email='".$this->array_user[3]."'");
		$this->dir=md5($this->array_user[13]).time();
	}

	function check_email_for_change(){
		$query_model="SELECT * FROM user WHERE email='".$this->array_user[3]."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)>1){
			$this->error='email';
			throw new Exception("email", 1);	
		}	
	}
	function check_pass_for_change($id){
		$query_model="UPDATE `user` SET password='".md5($this->array_user[14])."' WHERE id='".$id."'";
		if($this->array_user[14]!='' || $this->array_user[15]!=''){
			if($this->array_user[14]!=$this->array_user[15]){
				$this->error='pass';
				throw new Exception("conf_pass", 1);
			}else{
				mysql_query($query_model);
			}
		}

	}

	function check_input_data(){
		if(mysql_num_rows($this->query)!=0){
			$this->error='user_exist';
			throw new Exception("user_exist", 1);
		}

	}

	function set_query(){
		$sql_query = '';
		for($i = 0, $cnt = count($this->array_data); $i < $cnt; $i++)
			$sql_query .= "('" . join("', '", $this->array_data[$i]) . "'), ";
		$query_to_user_db= 'INSERT INTO `user`(`' . join('`, `', array_keys($this->array_data[0])) . '`) VALUES ' . rtrim($sql_query, ', ');
		mysql_query($query_to_user_db);
		$this->id=mysql_insert_id();

	}
	function send_notification_email(){
		mail($this->array_user[3], "Registration", "Hello, ".$this->array_user[1].",".$this->array_user[2]." . \n You have been registered on the CRM website \n crm.yobibyte.in.ua \n Your Email:".$this->array_user[3]." \n Your Password: ".$this->array_user[14]."","From:crmsystem@admin.com");
	}

	function get_user_id_2(){
		$this->query=mysql_query("SELECT * FROM user WHERE mobile_phone='".$this->array_user[4]."' AND email='".$this->array_user[3]."'");
		$arr_id=mysql_fetch_array($this->query);
		$this->id=$arr_id['id'];
	}
	function get_admin_name(){
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		if(mysql_num_rows($query)!=0){
			$array_token=mysql_fetch_array($query);
			$id_admin=$array_token['client_id'];
			$query=mysql_query("SELECT * FROM admin WHERE id='$id_admin'");
			if(mysql_num_rows($query)!=0){
				$array_admin=mysql_fetch_array($query);
				$this->admin_name=$array_admin['name'];
			}
		}
	}

	function add_to_activity_log($text){
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		$array_token=mysql_fetch_array($query);
		$id_admin=$array_token['client_id'];
		$time_current=date("h:i:s");
		$date_current=date("Y-m-d");
		$query_model="INSERT INTO activity_log(`notification`,`date`,`time`,`user_id`,`type`) VALUES('".$text."','".$date_current."','".$time_current."','".$id_admin."','global_admin')";
		mysql_query($query_model);
	}

	function delete_user($id){
		$query_model="DELETE FROM user WHERE id='".$id."'";
		mysql_query($query_model);
	}

	function delete_dir_user($id){
		$query_model="SELECT * FROM user WHERE id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_user=mysql_fetch_array($query);
			if($array_user['logo']!=''){
				unlink("../../../upload_photo/".$array_user['dir_img']."/".$array_user['logo']);
			}
			if($array_user['picture']!=''){
				unlink("../../../upload_photo/".$array_user['dir_img']."/".$array_user['picture']);
			}
			rmdir("../../../upload_photo/".$array_user['dir_img']);
		}
	}

	function query_for_change($include_function){
		$date=date("Y-m-d",strtotime($this->array_user[11]));
		$query_to_user_db="UPDATE `user` SET 
		`on_call_phone_number`='".$this->array_user[0]."',
		`first_name`='".$this->array_user[1]."',
		`last_name`='".$this->array_user[2]."',
		`email`='".$this->array_user[3]."',
		`mobile_phone`='".$this->array_user[4]."',
		`alt_phone`='".$this->array_user[5]."',
		`street_address`='".$this->array_user[6]."',
		`suite`='".$this->array_user[7]."',
		`state`='".$this->array_user[8]."',
		`city`='".$this->array_user[9]."',
		`postal_code`='".$this->array_user[10]."',
		`birthday_mounth`='".$date."',
		`industry`='".$this->array_user[12]."',
		`username`='".$this->array_user[13]."',
		`admin_id`='".$this->array_user[16]."',
		`tagline1`='".$this->array_user[17]."',
		`tagline2`='".$this->array_user[18]."',
		`tagline3`='".$this->array_user[19]."',
		`email_tagline`='".$this->array_user[24]."',
		`postcard_tagline`='".$this->array_user[25]."',
		`base_price`='".$this->array_user[20]."',
		`postcard_price`='".$this->array_user[21]."',
		`rvm_price`='".$this->array_user[22]."',
		`text_price`='".$this->array_user[23]."',
		`include_function`='".$include_function."'
		WHERE id='".$this->id."'";
		mysql_query((string)$query_to_user_db);
	}

	function check_image_data($file_image,$user_id,$type_img){
		$this->photo_name=USER::str2url(rand(5,15).time().basename($file_image['name']));
		$query_model="SELECT * FROM user WHERE id='".$user_id."'";
		$query=mysql_query($query_model) or die("ERROR");
		$array_user=mysql_fetch_array($query);
		$this->dir_image=$array_user['dir_img'];
		if($type_img=='picture'){
			$filename='../../../upload_photo/'.$array_user['dir_img'].'/'.$array_user['picture'];
			if(file_exists($filename)){
				unlink($filename);
			}
			$query_model="UPDATE user SET picture='".$this->photo_name."' WHERE id='".$user_id."'";
			mysql_query($query_model);
		}else{
			$filename='../../../upload_photo/'.$array_user['dir_img'].'/'.$array_user['logo'];
			if(file_exists($filename)){
				unlink($filename);
			}
			$query_model="UPDATE user SET logo='".$this->photo_name."' WHERE id='".$user_id."'";
			mysql_query($query_model);
		}
	}

	function load_image($file_image){
		$uploaddir='../../../upload_photo/'.$this->dir_image.'/';
		$uploadfile=$uploaddir.$this->photo_name;
		copy($file_image['tmp_name'], $uploadfile);
		sleep(3);
	}

	function rus2translit($string){
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		);
		return strtr($string, $converter);
	}

	function str2url($str){
		$str = USER::rus2translit($str);
		$str = strtolower($str);
		$str = preg_replace('[^-a-z0-9_]', '-', $str);
		$str=preg_replace('/\s/','',$str);
		$str = trim($str, "-");
		return  $str;
	}

	static public function addDopPhone($array,$id){
		foreach ($array as $key => $value) {
			$query_model="SELECT * FROM user_dop_phone WHERE phone='".$value."'";
			$query=mysql_query($query_model);
			if(mysql_num_rows($query)==0){
				$query_model="INSERT INTO user_dop_phone(phone,user_id) VALUES('".$value."','".$id."')";
				mysql_query($query_model);
			}
		}
	}
}




?>