<?php
include '../connect_to_bd.php';
require_once '../classes/Quick_appointment.php';
session_start();
$session_id=$_SESSION['user_token'];
$array = $_POST['array'];

$appointment=new Quick_appointment();


try{
	$appointment->checkAnotherCampaign($array);
	$appointment->get_user_id($session_id);
	$appointment->add_new_appointment($array);
	$text="User add new appointment";
	$appointment->add_to_activity_log($text);
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>