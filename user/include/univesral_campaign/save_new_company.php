<?php 
session_start();
include '../connect_to_bd.php';
require_once '../classes/Universal_campaign.php';

$name_campaign = $_POST['name_campaign'];
$type_campaign = $_POST['type_campaign'];
$assign_num = $_POST['assign_num'];
$activity_reminder = $_POST['activity_reminder'];
$text="User add new universal campaign";
$un_campaign = new Universal_campaign();
$clientId=Universal_campaign::getUserId($_SESSION['user_token']);
try{
	$un_campaign->set_data($name_campaign,$type_campaign,$assign_num,$activity_reminder);
	$un_campaign->check_input_data($clientId);
	$un_campaign->get_id_client();
	$un_campaign->add_to_bd();
	$un_campaign->get_admin_name();
	$un_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>"1","campaign_id"=>$un_campaign->campaign_id));
}catch(Exception $e){
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}



?>