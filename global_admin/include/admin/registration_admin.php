<?php
include '../connect_to_bd.php';
require_once '../class/Admin.php';
require_once '../class/ActivityLog.php';
session_start();

$email=$_POST['email'];
$password=$_POST['password'];
$username=$_POST['username'];
$name=$_POST['name'];
$administrator=new Administrator();
$text='Administrator added a new administrator';
try{
	$adminName = Administrator::getAdminName($_SESSION['user_token']);
	$administrator->setData($username,$password,$email,$name);
	$administrator->addNewAdmin();

	$adminId=ActivityLog::getClientId($_SESSION['user_token']);
	ActivityLog::addToActivityLog($text,$adminId);
	
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>