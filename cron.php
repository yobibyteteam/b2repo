<?php 
include 'user/include/connect_to_bd.php';
require_once 'cron/config.php';
require_once 'cron/Cron.php';
require_once 'cron/Reminder.php';
require_once 'cron/broadcast_controller.php';
require_once 'cron/Cron_Broadcast.php';

$universal=new Cron('Universal');
$dated=new Cron('Dated');
//$holidayAdmin=new Cron('Admin_Holiday');
$holiday=new Cron('Holiday');
$reminder=new Reminder();
$reminder->remindMe();
$brodcast=new Broadcast();
$brodcast->action_sendPostcard();
$brodcast->action_sendRvmd();
$brodcast->action_sendEmail();
$brodcast->action_sendText();
$brodcast->action_deleteCampaign();

?>