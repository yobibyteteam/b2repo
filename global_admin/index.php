<?php
include 'include/session.php';
include 'include/connect_to_bd.php';
function getTotalUser(){
 $sql="SELECT * FROM user"; 
 $query=mysql_query($sql);
 return mysql_num_rows($query);
}
$totalSend=getTotalSend();
function getTotalSend(){
    $arrayData=["email"=>0,"text"=>0,"broadcast"=>0,"postcard"=>0,"rvmd"=>0];
    $sql="SELECT * FROM statistics"; 
    $query=mysql_query($sql);
    if(mysql_num_rows($query)!=0){
        while ($res=mysql_fetch_assoc($query)) {
         $arrayData[$res['type']]++;
     }
 }
 return $arrayData;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
    <style>
    p{font-size: 12px}
    h4{font-size: 14px}
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'componets/header.php';  ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item active ">Home / </li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                  <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <br>
                                <div class="card-box">
                                    <div class="card-header card-header-inverse card-header-primary">
                                        <div class="font-weight-bold mb-3">Total monthly billing</div>
                                        <div class="chart-wrapper mb-3">
                                            <canvas id="chart-11" style="height: 200px;" class="chart chart-line" ></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <br>
                                <div class="card-box">
                                    <div class="card-header card-header-inverse card-header-primary">
                                      <div class="font-weight-bold mb-3">Total YTD billing </div>
                                      <div class="chart-wrapper mb-3">
                                        <canvas id="chart-10" style="height: 200px;" class="chart chart-line"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4 col-lg-6">
                    <div class="card card-inverse card-primary" style="background-color: #79c447; border-color: transparent;">
                      <div class="card-block pb-0">
                        <h4 class="mb-0"><?php echo $totalSend['postcard']; ?></h4>
                        <p>Total postcards sent</p>
                    </div>
                    <div class="chart-wrapper px-3" style="height:100px;">
                        <canvas id="card-chart1" class="chart chart-line" height="70"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-6">
                <div class="card card-inverse card-primary" style="background-color: #79c447; border-color: transparent;">
                  <div class="card-block pb-0">
                    <h4 class="mb-0"><?php echo $totalSend['rvmd']; ?></h4>
                    <p>RVM</p>
                </div>
                <div class="chart-wrapper px-3" style="height:100px;">
                    <canvas id="card-chart2" class="chart chart-line" height="70"></canvas>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-lg-6">
            <div class="card card-inverse card-primary" style="background-color: #79c447; border-color: transparent;">
              <div class="card-block pb-0">
                <h4 class="mb-0"><?php echo $totalSend['text']; ?></h4>
                <p>Massage</p>
            </div>
            <div class="chart-wrapper px-3" style="height:100px;">
                <canvas id="card-chart3" class="chart chart-line" height="70"></canvas>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-lg-6">
        <div class="card card-inverse card-primary" style="background-color: #79c447; border-color: transparent;">
          <div class="card-block pb-0">
            <h4 class="mb-0"><?php echo $totalSend['email']; ?></h4>
            <p>Emails</p>
        </div>
        <div class="chart-wrapper px-3" style="height:100px;">
            <canvas id="card-chart4" class="chart chart-line" height="70"></canvas>
        </div>
    </div>
</div>     
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="card-title">Total number of users</h4><h4 class="mb-0"><?php echo getTotalUser(); ?></h4>
                        <br>
                        <div class="chart-wrapper" style="height:250px;margin-top:20px;">
                            <canvas id="main-chart" height="250"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<button type="button" class="btn btn-primary" id="show_payment_failed">Show Toast</button>
            <button type="button" class="btn btn-danger" id="show_payment_success">Clear Toasts</button>-->
        </div>
    </div>
    <div id="activity" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    <script src="js/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
</div>
</div>
</main>     
</div>

<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/activity.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="js/libs/Chart.js"></script>
<script src="js/app.js"></script>
<script src="js/libs/toastr.min.js"></script>
<script src="js/libs/gauge.min.js"></script>
<script src="js/libs/moment.min.js"></script>
<script src="js/libs/daterangepicker.js"></script>
<script src="js/views/main.js"></script>
<script src="js/views/widgets.js"></script>
<script src="js/views/charts.js"></script>
<!-- Notifications JS START -->
<script src="js/libs/toastr.js"></script>
<script src="componets/paymented_notification.js"></script>
<!-- Notifications JS END -->
</body>
</html>