<?php 
session_start();
include 'include/session.php'; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever Bootstrap 4 Admin Template">
    <meta name="author" content="Lukasz Holeczek">
    <meta name="keyword" content="Clever Bootstrap 4 Admin Template">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->
    <title>SignIn</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style_window.css">
    <style>
    body{
      position: relative;
  }
  .sk-circle{
      display: none;
      z-index: 10001;
      width: 40px;
      height: 40px;
      position: absolute;
      top: 0;
      bottom: 0;
      right: 0;
      left: 0;
      margin: auto;
  }
</style>
</head>
<body class="app flex-row align-items-center">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
</div>
<div class="container" id="cont" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group mb-0">
                <div class="card p-4" style="flex:1 0;">
                    <div class="card-block">
                        <h1>Login</h1>
                        <p class="text-muted">Sign In to your account</p>
                        <div class="input-group mb-3">
                            <span class="email_logo input-group-addon">@</span>
                            <input  data-toggle="popover" data-placement="bottom" type="text" id="username" class="form-control" placeholder="E-mail">
                        </div>
                        <div class="input-group mb-4">
                            <span class="input-group-addon"><i class="icon-lock"></i>
                            </span>
                            <input  data-toggle="popover" data-placement="bottom" id="password" type="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="keep_me" name="keep_me" value="keep_me">
                                <span class="text_checkbox">Keep me signed in</span>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button onclick="login__user(this)" type="button" class="btn btn-primary px-4 login_btn">Login</button>
                            </div>
                            <div class="col-6 text-right">
                                <button onclick="request_password()" type="button" class="btn btn-link px-0">Forgot password?</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Модальное окно -->  
<div class="modal fade" id="request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Password Recovery</h4>
            </div>
            <div class="modal-body">
                <hr>
                <p class="text-muted" id="text_modal_body">Insert address you used to create account.</p>
                <p class="text-muted" id="text_modal_body">You will receive instructions via email.</p>
                <br>
                <div class="input-group mb-3">
                    <span class="email_logo input-group-addon">@</span>
                    <input class="form-control" data-placement="bottom" onkeypress="change_input_data(this)" placeholder="E-mail" type="text" id="req_email">
                </div>
            </div>
            <div class="modal-footer-btn modal-footer">
                <button onclick="set_query()" type="button" onclick="request_pass()" class="btn btn-secondary" >Request Change</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Request Password</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <h5>Message successfully sent</h5><br>
            </div>
        </div>
    </div>
</div>

<form action="payment_failed.php" method="POST" style="display: none">
    <input type="hidden" name="count" class="countVal">
    <input type="submit" class="referPay">
</form>

<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script>
    var agree;
    $('html').keydown(function(eventObject){
        if (event.keyCode == 13) {
            $(".login_btn").attr("disabled","true"); 
            login__user();
        }
    });
    function login__user(id){
        var username=$("#username").val();
        var password=$("#password").val();
        var keep_me=$("#keep_me").prop('checked');
        if(keep_me==true){
            agree=1;
        }else{
            agree=0;
        }
        try{
            $(id).attr("disabled","true");
            check__login__data(username,password);
            set__ajax__query(username,password,agree);
        }catch(e){
            console.log(e);
            $(id).removeAttr("disabled");
        }
    }

    function set__ajax__query(username,password,keep_me){
        $('.sk-circle').css('display','block');
        $.ajax({
            url:"controller/authorizationController.php",
            type:"POST",
            data:{username:username,password:password,agree:agree},
            success:function(data){
                //alert(data);
                parsing_json(data);
            }
        })
        $('.sk-circle').css('display','none');
    }

    function parsing_json(arr){
        $('.sk-circle').css('display','none');
        var json=JSON.parse(arr);
        if(json['success']==1){
            sessionStorage['user_token'] = json['user_token'];
            window.location="index.php";
        }else{
            if(json['error']=='pass'){
                alert("Incorrect Password");
                $(".login_btn").removeAttr("disabled");
                throw new SyntaxError("Incorrect_data");
            }else if(json['error']=='access'){
                $(".countVal").val(json['count']);
                $(".referPay").click();
                //window.location="payment_failed.php";
            }else{
                alert("Email Not Found");
                $(".login_btn").removeAttr("disabled");
                throw new SyntaxError("Incorrect_data");
            }
        }
    }
    function check__login__data(username,password){
        if(username==''){
            alert("Enter Email");
            throw new SyntaxError("empty__data");
        }
        if(password==''){
            alert("Enter Password");
            throw new SyntaxError("empty__data");
        }
    }
    /* **************  AUTHORIZATION END  ************** */
    /* **************  REQUEST_PASS BEGIN  ************** */
    function request_password(){
        var req_email=$("#req_email").val();
        $("#request").modal();
            //$("#modal-success").modal();
        }
        function set_query(){
            try{
                request_email_check();
                request_ajax();
            }catch(e){
                console.log(data);
            }
        }
        function request_email_check(){
            var req_email=$("#req_email").val();
            if(req_email==''){
                alert("Incorrect Email");
                throw new SyntaxError("empty__data");
            }
        }

        function check_ajax_request_data(results){
            var json=JSON.parse(results);
            if(json['success']==1){
                $('#request').modal('hide');
                $('#modal-success').modal('show');
                $("#req_email").val('');
                setTimeout(function(){
                    $("#modal-success").modal('hide');
                },1700);
            }else{
                alert("Email Not Found");
                throw new SyntaxError("Incorrect email");
            }
        }

        function request_ajax(){
            $('.sk-circle').css('display','block');
            var req_email=$("#req_email").val();
            $.ajax({
                url:"controller/recoveryController.php",
                type:"POST",
                data:{req_email:req_email},
                success:function(data){
                    check_ajax_request_data(data);
                    $('.sk-circle').css('display','none');
                }
            })
            $('.sk-circle').css('display','none');
        }
    </script>

</body>

</html>
