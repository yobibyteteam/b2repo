<?php 
include 'include/connect_to_bd.php';
include "include/session.php";
function get_admin_name(){
  $sql=mysql_query("SELECT * FROM admin");
  while($result=mysql_fetch_assoc($sql)){
    echo "<option value='".$result['id']."'>".$result['username']."</option>";
  }
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  .fiieldest{
    margin-top: 15px;
    display: inline-block;
    margin-right: 25px;
  }
  .input-file-row-1:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
  }

  .input-file-row-1{
    display: inline-block;
    position: relative;
  }

  html[xmlns] .input-file-row-1{
    display: block;
  }

  * html .input-file-row-1 {
    height: 1%;
  }

  .upload-file-container { 
    position: relative;  
    height: 137px; 
    overflow: hidden; 
    background: url(http://i.imgur.com/AeUEdJb.png) top center no-repeat;
    float: left;
    margin-left: 23px;
  } 

  .upload-file-container:first-child { 
    margin-left: 0;
  } 

  .upload-file-container > img {
    opacity: 0;
    width: 93px;
    height: 93px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
  }

  .upload-file-container-text{
    font-size: 12px;
    color: #20a8d8;;
    line-height: 17px;
    text-align: center;
    display: block;
    position: absolute; 
    left: 0; 
    bottom: 0; 
    width: 100px; 
    height: 35px;
  }

  .upload-file-container-text > span{
    border-bottom: 1px solid #20a8d8;;
    cursor: pointer;
  }

  .upload-file-container input  { 
    position: absolute; 
    left: 0; 
    bottom: 0; 
    font-size: 1px; 
    opacity: 0;
    filter: alpha(opacity=0); 
    margin: 0; 
    padding: 0; 
    border: none; 
    width: 70px; 
    height: 50px; 
    cursor: pointer;
  }
</style>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <!-- Main content -->
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Profile</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>User Profile</strong>
                  
                </div>
                <div class="card-block" id="sortable">
                  <div class="form-group">
                    <label for="company">On Call Phone Number</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter On Call Phone Number">
                  </div>
                  <div class="form-group">
                    <label for="last_name">First Name</label>
                    <input id="last_name" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter First Name">
                  </div>

                  <div class="form-group">
                    <label for="vat">Last Name</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter Last Name">
                  </div>
                  <div class="form-group email">
                    <label for="street">E-mail</label>
                    <input onkeypress="change_input_data(this)" id="email" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter E-mail">
                  </div>
                  <div class="form-group mobile_phone">
                    <label for="street">Mobile Phone</label>
                    <input onkeypress="change_input_data(this)" id="mobile_phone" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter Mobile Phone">
                  </div>
                  <div class="form-group">
                    <label for="street">Alternative Phone</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter Alternative Phone">
                  </div>
                  <div class="form-group">
                    <label for="country">Street Address</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Street Address">
                  </div>

                  <div class="form-group">
                    <label for="street">Suite</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter Suite">
                  </div>

                  <div class="form-group">
                    <label for="country">State</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="State name">
                  </div>
                  <div class="row">

                    <div class="form-group col-sm-8">
                      <label for="city">City</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="city" placeholder="Enter your city">
                    </div>

                    <div class="form-group col-sm-4">
                      <label for="postal-code">Postal Code</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="postal-code" placeholder="Postal Code">
                    </div>

                  </div>
                  <!--/.row-->

                  <div class="form-group">
                    <label for="country">Birthday Mounth</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Birthday Mounth">
                  </div>

                  <div class="form-group">
                    <label for="country">Username</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Username">
                  </div>

                  <div class="form-group">
                    <label for="country">Password</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Password">
                  </div>
                  <div class="form-group">
                    <fieldset class="fiieldest">
                      <div class="input-file-row-1">
                        <div class="upload-file-container">
                          <img id="image1" src="#" alt="" />           
                          <div class="upload-file-container-text">
                            <span>Upload Picture</span>
                            <input type="file" name="imgInput1" class="photo" id="imgInput1" />
                          </div>
                        </div>        
                      </div>      
                    </fieldset>
                    <fieldset class="fiieldest">
                      <div class="input-file-row-1">
                        <div class="upload-file-container">
                          <img id="image2" src="#" alt="" />           
                          <div class="upload-file-container-text">
                            <span>Upload Logo</span>
                            <input type="file" name="imgInput2" class="photo" id="imgInput2" />
                          </div>
                        </div>        
                      </div>      
                    </fieldset>
                  </div>                           
                  <div class="form-group has-feedback">
                   <label for="country">Assigned agent </label>
                   <select id="select" name="select" class="input_data form-control">
                     <?php get_admin_name(); ?>
                   </select>
                 </div>
                 <div class="form-group">
                  <label for="country">Tagline 1</label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 1">
                </div>
                <div class="form-group">
                  <label for="country">Tagline 2</label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 2">
                </div>
                <div class="form-group">
                  <label for="country">Tagline 3</label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 3">
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <strong>Email Avatar</strong>
              </div>
              <div class="card-block">
                <div class="form-group">
                  <label for="country">Tagline 1 </label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 1">
                </div>
                <div class="form-group">
                  <label for="country">Tagline 2 </label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 2">
                </div>
                <div class="form-group">
                  <label for="country">Tagline 3 </label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 3 ">
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <strong>Postcard Avatar</strong>
              </div>
              <div class="card-block">
                <div class="form-group">
                  <label for="country">Tagline </label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline ">
                </div>
                <div class="form-group">
                  <label for="country">Tagline 2 </label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 2">
                </div>
                <div class="form-group">
                  <label for="country">Tagline 3 </label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="country" placeholder="Enter Tagline 3 ">
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <strong>Billing Preferences</strong>
              </div>
              <div class="card-block">
                <div class="form-group">
                  <label for="country">Base Price</label>
                  <input disabled  type="text" class="form-control" id="country" value="99.00">
                </div>
                <div class="form-group">
                  <label for="country">PostCard Price</label>
                  <input disabled  type="text" class="form-control" id="country" value="0.75">
                </div>
                <div class="form-group">
                  <label for="country">RVM Price</label>
                  <input disabled  type="text" class="form-control" id="country" value="0.08">
                </div>
                <div class="form-group">
                  <label  for="country">Text Price</label>
                  <input disabled type="text" class="form-control" id="country" value="0.03">
                </div>
              </div>
              <div class="form-group" style="text-align: center;">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-save" style="margin-right: 10px"> </i> Save </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>

<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>


<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>

<!-- GenesisUI main scripts -->

<script src="js/app.js"></script>

<!-- Plugins and scripts required by this views -->
<script src="../js/libs/toastr.min.js"></script>
<script src="js/libs/gauge.min.js"></script>
<script src="js/libs/moment.min.js"></script>
<script src="js/libs/daterangepicker.js"></script>


<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>
<script>
  var data_array=[];
  var user_id;
  function save_new_user(){
    try{
      checkInput();
      build_array();
      send_ajax_query();
      clear_input();
    }catch(e){
      console.log(e);
    }
  }
  function checkInput(){
    $('.input_data').each(function(){
      if($(this).val() == ''){
        notification(this,'Empty Data');
        throw new SyntaxError("empty_data");
      }
    });
  }
  function clear_input(){
    $('.input_data').each(function(){
      $(this).val('');
    });
    $(".upload-file-container > #image1").css('opacity','0');
    $(".upload-file-container > #image2").css('opacity','0');
    $('#image1').attr('src','');
    $('#image2').attr('src','');
  }
  function build_array(){
    var i=1;
    data_array=[];
    $('.input_data').each(function(){
      data_array.push($(this).val());
    });
  }
  function send_ajax_query(){
    $.ajax({
      url:"include/class/registration_user.php",
      type:"POST",
      data:{data_array:data_array},
      success:function(data){
        parsing_json(data);
      }
    })
  }
  function sendphoto(id){
    var file1 = $('#imgInput1');
    var formData = new FormData();
    formData.append('upload1', file1.prop('files')[0]);
    formData.append('type_photo', 'picture');
    formData.append('user_id', id);
    $.ajax({
      url: 'include/send.php',
      data: formData,
      processData: false,
      contentType: false,
      type: 'POST',
      success:function(data){
        console.log(data);
      }
    })
    var file1 = $('#imgInput2');
    var formData = new FormData();
    formData.append('upload1', file1.prop('files')[0]);
    formData.append('type_photo', 'logo');
    formData.append('user_id', id);
    $.ajax({
      url: 'include/send.php',
      data: formData,
      processData: false,
      contentType: false,
      type: 'POST',
      success:function(data){
        console.log(data);
      }
    })
  }

  function parsing_json(arr){
    var json=JSON.parse(arr);
    sendphoto(json['user_id']);
    if(json['success']=='1'){
      $(".success_btn").css('display','inline-block');
      setTimeout(function(){
        $(".success_btn").css('display','none');
      },1000);
    }else{
      var parent=$('#email').parent(".form-group");
      $(parent).addClass('has-danger');
      $('#email').addClass('form-control-danger');
      $('#email').attr("data-content",'User Exist');
      $('#email').popover('show');
      $(".danger_btn").css('display','inline-block');
      setTimeout(function(){
        $('#email').popover('dispose');
        $(".danger_btn").css('display','none');
      },2000);
    }
  }

  function change_input_data(id){
    var parent=$(id).parent(".form-group");
    $(id).removeClass('form-control-danger');
    $(parent).removeClass('has-danger');
  }

  function notification(id,type_error){
    var parent=$(id).parent(".form-group");
    $(parent).addClass('has-danger');
    $(id).addClass('form-control-danger');
    $(id).attr("data-content",type_error);
    $(id).popover('show');
    $('.danger_btn').css("display","inline-block");
    setTimeout(function(){
      $(id).popover('dispose');
      $('.danger_btn').css("display","none");
    },1500);

  }
</script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $("#sortable").sortable();
    $("#sortable").disableSelection();
  } );
  function readURL(input,id) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#image'+id).attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInput1").change(function(){
    readURL(this,'1');
    $(".upload-file-container > #image1").css('opacity','1');
  });
  $("#imgInput2").change(function(){
    readURL(this,'2');
    $(".upload-file-container > #image2").css('opacity','1');
  });
</script>
<script src="components/user_notifications.js"></script>
</body>

</html>