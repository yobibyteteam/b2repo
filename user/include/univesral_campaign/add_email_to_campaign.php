<?php
include '../connect_to_bd.php';
require_once '../classes/Universal_campaign.php';
session_start();
$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_data'];
$delay_day_email=$_POST['delay_day_email'];
$repeat_email=$_POST['repeat_email'];
$un_campaign=new Universal_campaign();
$clientId=Universal_campaign::getUserId($_SESSION['user_token']);
echo $clientId;

try{
	$un_campaign->add_email($id_campaign,$editor_text,$delay_day_email,$clientId,$repeat_email);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>