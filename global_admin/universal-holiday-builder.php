<?php
include "include/session.php";
include 'include/connect_to_bd.php';

function get_list_campaign(){
  $query_to_campaign_db=mysql_query("SELECT * FROM holiday_builder WHERE type='global_admin'");
  while ($result=mysql_fetch_assoc($query_to_campaign_db)) {
    echo '<option value="'.$result['name'].'">'.$result['name'].'</option>';
  }
}
function get_list_dated_campaign(){
  $query=mysql_query("SELECT * FROM holiday_builder WHERE type='global_admin'");
  while ($res=mysql_fetch_assoc($query)) {
    echo '<tr>
    <td>'.$res['name'].'</td>
    <td>Active</td>

    <td class="table_button" style="width:113px!important;">
      <a onclick=modal_show('.$res["id"].') class="btn btn-danger btn_table" href="#">
        <i class="fa fa-trash-o "></i>
      </a>
      <a  class="btn btn-info btn_table" href="overview-holiday-campaign.php?token='.$res['md5'].'">
        <i class="fa fa fa-edit"></i>
      </a>
    </td>
  </tr>';
}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/redmond/jquery-ui.css">
  <link href="js/fullcalendar/3.4.0/fullcalendar.min.css" rel='stylesheet'>
  <link href="js/fullcalendar/3.4.0/fullcalendar.print.css" rel='stylesheet' media='print'>
  <link href="css/all.css" rel="stylesheet">
  <link href="css/colpick.css"  rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/component.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/chosen.css" type="text/css" >
  <link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
  <style>
    .photoDataToInsert img{
      width: 50%;
    }
    .nav-tabs{
      border-bottom: 0!important;
    }
    .tab-content{
      border: 1px solid #e1e6ef!important;
    }
    @media (max-width: 414px){
      #span{
        display: none;
      }
      .side{
        display: inherit;
      }
    }
        @media (max-width: 700px){
      #postcard_preview{
        left:0!important;
      }
    }
        @media (max-width: 320px){
      #postcard_preview{
        left:-28%!important;
      }
    }
  </style>
  <title>Universal Holiday Builder</title>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'componets/header.php';  ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Universal Holiday Builder</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="card">
            <div class="card-header">
              Current Holiday Campaigns
            </div>
            <div class="card-block">
              <table class="table table-striped table-bordered datatable table-responsive">
                <thead>
                  <tr>
                    <th>Name of Campaign:</th>
                    <th>Status:</th>
                    <th>Actions:</th>
                  </tr>
                </thead>
                <tbody>
                  <?php get_list_dated_campaign();  ?>
                </tbody>
              </table>
            </div>
          </div>

          <div class="card">
            <div class="card-header">
              <strong>Create New Campaign from Scratch</strong>
            </div>
            <div class="card-block">
              <div class="form-group has-feedback">
                <label class=" form-control-label" for="text-input">Name Campaign:</label>
                <div >
                 <input type="text" onkeypress="change_input_data(this)" id="name_campaign" data-toggle="popover" data-placement="bottom" name="name_campaign" class="form-control name_campaign" placeholder="Create Name">
               </div>
             </div>
             <div class="text-left">

              <fieldset class="form-group">
                <label> Date:</label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i>
                  </span>
                  <input disabled onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" name="date_holiday" type="text" class="date_holiday form-control" placeholder="Select Date" id="date">
                </div>
              </fieldset>
              <br>
              <div class="text-center">
               <button onclick="save_new_company()" class="save_and_start btn btn-primary">Save and Start</button>
               <button style="display: none;" class="danger_btn btn btn-danger">Error</button>
               <button style="display: none;" class="success_btn btn btn-success">Success</button><br><br>
             </div>
             <div id='calendar'></div>
             <div class="panel-group" id="accordion" style="display: none;">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" onclick="removeSideBar();" id="email_btn" data-toggle="tab" href="#email_center" role="tab" aria-controls="home"><span id="span">Email Center</span> <i class="fa fa-envelope-o "></i></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" onclick="removeSideBar();" id="text_btn" data-toggle="tab" href="#text_center" role="tab" aria-controls="messages"><span id="span">Text Center</span> <i class="fa fa-file-text-o"></i></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" onclick="closeSideBar();" id="postcard_btn" data-toggle="tab" href="#postcard_center" role="tab" aria-controls="messages"><span id="span">Postcard Center</span> <i class="fa fa-newspaper-o"></i></a>
                </li>
              </ul>

              <div class="tab-content">
                <div class="tab-pane active" id="email_center" role="tabpanel">
                  <div class="form-group close_tab1">
                    <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which emails bounce. A full report is available to you
                      under logs on your dashboard.</p>
                      <p>You must agree to terms and conditions before proceeding:</p></label>
                      <div class="checkbox">
                        <label>
                          <input onchange="agree_check(this,'close_tab1','variable1')" type="checkbox" id="agree" name="agree" value="agree">
                          <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                        </label>
                      </div>
                    </div>
                    <div class=" col-md-12 variable1" style="display: none">
                      <div class="form-group">
                          <input type="text" class="form-control" id="company" placeholder="Enter email subject">
                      </div>
                      <textarea name="editor_email" id="editor_email" rows="10" cols="80">
                      </textarea>
                      <br>
                      <div class="form-group has-feedback">
                      <!--  <span>Include:</span>
                        <label class="checkbox-inline" for="inline-checkbox1">
                            <input style="margin:10px 3px;" type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="option1">Email Signature
                        </label>
                        <label class="checkbox-inline" for="inline-checkbox2">
                            <input style="margin:10px 3px;" type="checkbox" id="inline-checkbox2" name="inline-checkbox2" value="option2">Email Footer
                        </label>
                        <label class="checkbox-inline" for="inline-checkbox3">
                            <input style="margin:10px 3px;" type="checkbox" id="inline-checkbox3" name="inline-checkbox3" value="option3">Picture
                        </label>
                        <label class="checkbox-inline" for="inline-checkbox4">
                            <input style="margin:10px 3px;" type="checkbox" id="inline-checkbox4" name="inline-checkbox4" value="option3">Logo
                        </label>-->
                        <div class="radio">
                          <span style="margin-left: 3px;"> Send<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_email" value="0" type="number" id="adm_holiday_email_days" name="text" style="width: 60px; height: 30px; margin:0px 5px;">days before date</span>
                          <br>
                          <span><input type="checkbox" class="repeat_email" style="margin:10px 3px;">Repeat every year</span>
                        </div>
                      </div>
                      <div class="col-md-12 py-4 text-center">
                        <button data-toggle="tab" role="tab" href="#email" onclick="email_show()" class="btn btn-primary" >Save </button>
                        <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn btn-outline-success">Success</button>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="text_center" role="tabpanel">
                    <div class="form-group close_tab2">
                      <label for="company"><p>Note: Note all messages are not guaranteed delivery. You will be notified of which texts bounce. A full report is available to you
                        under logs on your dashboard.</p>
                        <p>You must agree to terms and conditions before proceeding:</p></label>
                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab2','variable2')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>
                      </div>
                      <div class="col-md-12 form-group variable2" style="display: none">
                        <textarea name="text_editor" id="text_editor" rows="10" cols="80">
                        </textarea>
                        <br>
                        <div class="form-group has-feedback">
                          <div class="radio">
                            <span style="margin-left: 3px;"> Send<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_text" value="0" type="number" id="adm_holiday_text_days" name="text" style="width: 60px; height: 30px; margin:0px 5px;">days before date</span>
                            <br>
                            <span><input type="checkbox" class="repeat_text" style="margin:10px 3px;">Repeat every year</span>
                          </div>
                        </div>
                        <div class="col-md-12 py-4 text-center">
                          <button onclick="save_text()" class="btn btn-primary">Save </button>
                        </div>
                      </div>
                    </div>

                    <div class="tab-pane" id="postcard_center" role="tabpanel">
                      <div class="form-group close_tab3">
                        <label for="company"><p>Note: Note all messages are guaranteed delivery. You are charged for all postcards sent. If you receive a card back you must update the address or remove address from contacts information to stop any further attempts.</p>
                          <p>You must agree to terms and conditions before proceeding:</p></label>
                          <div class="checkbox">
                            <label>
                              <input onchange="agree_check(this,'close_tab3','variable3')" type="checkbox" id="agree" name="agree" value="agree">
                              <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                            </label>
                          </div>
                        </div>
                        <div class="col-md-12 form-group variable3" style="display: none">
                          <main>
                            <div class="col-md-12" style="padding: 0;">
                              <div class="card" style="padding: 0;border:0;">
                                <div class="card-block p-0">
                                  <div class="tab-content">
                                    <div class="tab-pane active" id="front" style="padding: 0; overflow: auto;">
                                      <iframe scrolling="no" style="width: 1090px; height: 650px;" src="postcard-front.php" frameborder="0"></iframe>
                                    </div>
                                    <div class="tab-pane" id="back" style="padding: 0; overflow: auto;">
                                      <iframe  scrolling="no" src="postcard-back.php" style="width: 1090px; height: 650px;" frameborder="0"></iframe>
                                    </div>
                                  </div>
                                </div>
                                <ul id="postcard_preview" class="nav side" role="tablist" style="text-align:center; position:relative;">
                                  <li class="nav-item">
                                    <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab"><img src="../img/front.jpg" style="width:100px; height:100px;"><br>Front side</a>
                                  </li>
                                  <li class="nav-item">
                                    <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab"><img src="../img/back.jpg" style="width:100px; height:100px;"><br>Back side</a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </main>
                          <div class="form-group has-feedback">
                            <div class="radio">
                              <span style="margin-left: 3px;"> Send<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_postcard" value="0" type="number" id="adm_holiday_postcard_days" name="text" style="width: 60px; height: 30px; margin:0px 5px;">days before date</span>
                              <br>
                              <span><input type="checkbox" class="repeat_postcard" style="margin:10px 3px;">Repeat every year</span>
                            </div>
                          </div>
                          <div class="col-md-12 py-4 text-center">
                            <button onclick="save_postcard()" data-toggle="modal" data-target="#photoDataToInsert" class="btn btn-primary">View Postcard and Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div style="display: none;text-align: center;"  class="final_save text-center">
                      <button onclick="window.location='universal-holiday-builder.php'" class="btn btn-primary">Save and View</button>
                      <br><br><br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>

          <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-danger" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Delete</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Are you sure you want to delete this campaign?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  <button type="button" onclick="del_campaign()" data-dismiss="modal" class="btn btn-primary">Yes</button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="photoDataToInsert" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"></h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="panel-group photoDataToInsert"  style="text-align: center;">
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <script src="js/libs/jquery.min.js"></script>
          <script src="js/libs/bootstrap.min.js"></script>
          <!-- GenesisUI main scripts -->
          <script src="js/app.js"></script>
          <!-- Plugins and scripts required by this views -->
          <script src="js/libs/moment.min.js"></script>
          <script src="js/libs/tether.js"></script>
          <!-- Plugins and scripts required by all views -->
          <script src="ckeditor/ckeditor.js"></script>
          <script type="text/javascript" src="../nicedit.js"></script>
          <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
          <script src="jquery-ui-i18n.js"></script>
          <script src="js/fullcalendar-3.4.0/fullcalendar.min.js"></script>
          <script src="js/fullcalendar-3.4.0/gcal.min.js"></script>
          <script type="text/javascript">
            var src = '';

            var necessarily_data_array=[];
            var token;
            var filename=0;
            var filekey=1;
            var type_Campaign="holiday";
            var repeat_year=0;
            function viewCoupon(){
              $("#readyCoupon").modal("show");
            }

            if($.cookie('top_position')!=null){
              top_block_add = $.cookie('top_position');
            }else{top_block_add = "250px";}
            top_position_add = top_block_add.substr(0,3)*1;
            $(".popup_close").click(function(){
              $(".preview_pop").hide();
            });

            var canvasOperations = {
              loadFromUrl : function(){
                fabric.Image.fromURL(src, function(image){
                  canvas.add(image.set({
                    id : 'canvas'+filename,
                    alt : 'canvas'+filename,
                    width : canvas.width / 2,
                    height : canvas.height / 2,
                    perPixelTargetFind : true,
                    targetFindTolerance : 4
                  }));
                });
              },
            }

            $(".show_menu_konstr").click(function(){
              $(".chuse_product_konstruktor").fadeIn(200);
            });
            $(".chuse_product_konstruktor_top").click(function(){
              $(".chuse_product_konstruktor").fadeOut(200);
            })

            $( function() {
              $("#resize").resizable({autoHide:true,alsoResize:'#preview'});
            } );

            function del_input(id){
              var parent=$(id).parent();
              $(parent).remove();
            }

            function viewCoupon(){
              $(".component").css('border', '0px');
              $('.component').html2canvas({
                onrendered: function (canvas) {
                  $(".imageData").attr('src',canvas.toDataURL("image/png"));
                  $(".component").css('border', '3px solid #49708A');
                }
              });
            }

            function saveCoupon(){
              var file = $(".imageData").attr('src');
              var campaignName=$(".campaignName").val();
              var mobileNumber=$(".mobileNumber").val();
              if(mobileNumber==''){
                alert("Enter Mobile Number");
              }else{
                $.ajax({
                  url: 'include/coupon/savePostcard.php',
                  data: {file:file,campaignName:campaignName,mobileNumber:mobileNumber},
                  type: 'POST',
                  success:function(){}})
                window.location='coupon-builder.php';
              }
            }
            function addNewFile(filename){
              $(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');
            }

            function clearAll(){
              canvas.clear();
              filename=0;
              filekey=1;
              $(".field").each(function(){
                $(this).hide();
              })
            }

            function delFile(id,idFile){
              var key=idFile+1;
              var name='canvas'+key;
              console.log(name);
              var lengthCanvas = canvas['_objects'].length;
              for(var k in canvas['_objects']){
                console.log(canvas['_objects'][k]['alt']);
                if(canvas['_objects'][k]['alt']==name){
                  canvas['_objects'][k].remove();
                  var parent=$(id).parent();
                  $(parent).hide();
                }
              }
              console.log(canvas);
              filekey--;
              canvas.renderAll();
              $(".addNewFile").val('');
            }

            function readURL(input){
              if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                  src=e.target.result;
                  canvasOperations.loadFromUrl();
                };
                addNewFile(filename);
                reader.readAsDataURL(input.files[0]);
                filename++;
                filekey++;
              }
            }
            $(".del_fil").click();
          </script>

          <script>
            var id_campaign;
            var id_camp;
            $(document).ready(function() {
              $('#calendar').fullCalendar({
                header: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,listYear'
                },
                navLinks: true,
                selectable: true,
                selectHelper: true,
                displayEventTime: false,
                googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
                events: 'en.usa#holiday@group.v.calendar.google.com',
                dayClick: function(date, jsEvent, view) {
                 $('#date').val(date.format());
               },
               eventClick: function(event) {
                if (event.url) {
                  $(this).css('color', 'white');
                  $(this).css('text-decoration', 'none');
                  //console.log(date);
                  //$('#date').val(event.title)
                  return false;
                }
              },
              loading: function(bool) {
                $('#loading').toggle(bool);
              }
            });
            });
          </script>
<!--          <script type="text/javascript">
          CKEDITOR.plugins.add( 'tokens',
          {
          requires : ['richcombo'], //, 'styles' ],
          init : function( editor )
          {
          var config = editor.config,
          lang = editor.lang.format;
            var tags = []; //new Array();
            //this.add('value', 'drop_text', 'drop_label');
            tags[0]=["{$first_name}", "first_name", "first_name"];
            tags[1]=["{$last_name}", "last_name", "last_name"];
            editor.ui.addRichCombo( 'tokens',
            {
              label : "Insert data",
              className : 'cke_format',
              multiSelect : false,

              panel :
              {
               css : [ config.contentsCss, CKEDITOR.getUrl( editor.skinPath + 'editor.css' ) ],
               voiceLabel : lang.panelVoiceLabel
             },
             init : function()
             {
              this.startGroup( "" );
                     //this.add('value', 'drop_text', 'drop_label');
                     for (var this_tag in tags){
                      this.add(tags[this_tag][0], tags[this_tag][1]);
                    }
                  },
                  onClick : function( value )
                  {
                   editor.focus();
                   editor.fire( 'saveSnapshot' );
                   editor.insertHtml(value);
                   editor.fire( 'saveSnapshot' );
                 }
               });
          }
          });
        </script>-->
          <script>
          CKEDITOR.replace('editor_email',{
            //extraPlugins: 'tokens'
            toolbar :
          [
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
          { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
          { name: 'links', items: [ 'Link', 'Unlink'] },
          { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
          { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
          { name: 'tools', items: [ 'Maximize'] },
          ]
          });
          CKEDITOR.replace('text_editor',
            {
              toolbar :
              [
              { name: 'basicstyles', items : [ 'Bold','Italic' ] },
              { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
              { name: 'tools', items : [ 'Maximize','-' ] },
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
            ]});
            function modal_show(id){
              id_camp=id;
              $("#dangerModal").modal("show");
            }

            function change_input_data(id){
              var parent=$(id).parent();
              $(id).removeClass('form-control-danger');
              parent.removeClass('has-danger');
            }

            function hide_area(){
              $('.sk-circle').css('display','block');
              if(error==''){
                sessionStorage.removeItem('campaign_id');
                window.location='universal-dated-builder.php';
              }
              $('.sk-circle').css('display','none');
            }
            function change_input_data(id){
              var parent=$(id).parent();
              $(id).removeClass('form-control-danger');
              parent.removeClass('has-danger');
            }

            function save_new_company(){
              $('.sk-circle').css('display','block');
              var name_campaign=$('.name_campaign').val();
              var date=$('.date_holiday').val();
              if(name_campaign==''){
                error='empty_data';
                alert('Enter Campaign Name');
              }else if(date==''){
                error='empty_data';
                alert('Select Date');
              }else{
                error='';
                $.ajax({
                  url:"include/holiday_campaign/save_new_dated_company.php",
                  data:{name_campaign:name_campaign,date:date},
                  type:"POST",
                  success:function(data){
                    parsing_json(data);
                  }
                })
              }
              $('.sk-circle').css('display','none');
            }

            function notification(id,type_error){
              var parent=$(id).parent();
              $(parent).addClass('has-danger');
              $(id).addClass('form-control-danger');
              $(id).attr("data-content",type_error);
              $('.danger_btn').css("display","inline-block");
              setTimeout(function(){
                $('.danger_btn').css("display","none");
              },1500);
            }

            function parsing_json(data){
              var json=JSON.parse(data);
              if(json['success']==1){
                $('#calendar').css('display','none');
                id_campaign=json['campaign_id'];
                sessionStorage['campaign_id'] = id_campaign;
                $(".name_campaign").attr("disabled", true);
                $(".date_holiday").attr("disabled", true);
                $(".success_btn").css('display','inline-block');
                setTimeout(function(){
                  $(".success_btn").css('display','none');
                },1000);
                $('.save_and_start').css('display','none');
                $('#accordion').css('display','block');
                $(".success_btn").css('display','inline-block');
                setTimeout(function(){
                  $(".success_btn").css('display','none');
                },1000);
              }else{
                error='campaign_exist';
                notification($('.name_campaign'),'Campaign Exist');
                alert('Campaign Exist');
                throw new SyntaxError("error_data");
              }
            }

            function del_campaign(){
              $('.sk-circle').css('display','block');
              var id=id_camp;
              $.ajax({
                url:"include/holiday_campaign/delete_dated_campaign.php",
                data:{id:id},
                type:"POST",
                success:function(data){
                  window.location="universal-holiday-builder.php";
                }
              })
              $('.sk-circle').css('display','none');
            }

            function agree_check(id,tab,div)
            {
              $('.'+tab).css('display','none');
              $('.'+div).css('display','block');
            }

            function email_show(){
              $('.sk-circle').css('display','block');
              var editor_data = CKEDITOR.instances['editor_email'].getData();
              var delay_day_email=$(".delay_day_email").val();
              var repeat_email;
              if($(".repeat_email").prop("checked")){
                repeat_email=1;
              }else{
                repeat_email=0;
              }
              $.ajax({
                url:"include/holiday_campaign/add_email_to_holiday_campaign.php",
                data:{editor_data:editor_data,id_campaign:id_campaign,delay_day_email:delay_day_email,repeat_email:repeat_email},
                type:"POST",
                success:function(data){
                  $('.sk-circle').css('display','none');
                }
              })
              $('.final_save').css("display","block");
              $('.success_btn').css("display","inline-block");
              setTimeout(function(){
                $('.success_btn').css("display","none");
              },1500);
              $('#text_btn').tab('show');
              $('.sk-circle').css('display','none');
            }

            function save_text(){
              $('.sk-circle').css('display','block');
              var editor_text = CKEDITOR.instances['text_editor'].getData();
              var delay_day_text=$(".delay_day_text").val();
              var repeat_text;
              if($(".repeat_text").prop("checked")){
                repeat_text=1;
              }else{
                repeat_text=0;
              }
              //alert(repeat_text);
              $.ajax({
                url:"include/holiday_campaign/add_text_to_holiday_campaign.php",
                data:{editor_text:editor_text,id_campaign:id_campaign,delay_day_text:delay_day_text,repeat_text:repeat_text},
                type:"POST",
                success:function(data){
                  //alert(data);
                  $('.sk-circle').css('display','none');
                }
              })
              $('.final_save').css("display","block");
              $('.success_btn').css("display","inline-block");
              setTimeout(function(){
                $('.success_btn').css("display","none");
              },1500);
              $('.sk-circle').css('display','none');
            }

            function save_postcard(){
              var repeat_postcard;
              if($(".repeat_postcard").prop("checked")){
                repeat_postcard=1;
              }else{
                repeat_postcard=0;
              }
              var delay_day_postcard=$(".delay_day_postcard").val();
              $.ajax({
                url:"include/postcard/getPicture.php",
                data:{id_campaign:id_campaign,type_Campaign:type_Campaign,repeat_postcard:repeat_postcard,delay_day_postcard:delay_day_postcard},
                type:"POST",
                success:function(data){
                  insertPhotoIntoModal(JSON.parse(data));
                }
              })
            }

            function insertPhotoIntoModal(array){
              var out='';
              for(var k in array){
                out+="<img src='"+array[k]+"' alt='alt'>"
              }
              $(".photoDataToInsert").html(out);
            }

            document.getElementById('adm_holiday_email_days').onkeypress = function (e) {
              return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
            }
            document.getElementById('adm_holiday_text_days').onkeypress = function (e) {
              return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
            }
            document.getElementById('adm_holiday_postcard_days').onkeypress = function (e) {
              return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
            }

            function changeRepeat(){
              if(repeat_year==0){
                repeat_year=1;
              }else{
                repeat_year=0;
              }
            }
            function updatePostcardData(){
              var delay_day_postcard=$(".delay_day_postcard").val();
              $.ajax({
                url:"include/postcard/updatePostcardData.php",
                data:{id_campaign:id_campaign,repeat_year:repeat_year,delay_day_postcard:delay_day_postcard,type_Campaign:type_Campaign},
                type:"POST",
                success:function(data){
                }
              })
            }
            function insertPhotoIntoModal(array){
              var out='';
              for(var k in array){
                out+="<img src='"+array[k]+"' alt='alt'>"
              }
              $(".photoDataToInsert").html(out);
              $('.final_save').css("display","block");
            }
                    function closeSideBar(){
      $('body').addClass('sidebar-hidden');
    }
    function removeSideBar(){
      $('body').removeClass('sidebar-hidden');
    }
          </script>
        </body>

        </html>
