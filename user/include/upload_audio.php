<?php
require_once 'classes/Audio.php';
include 'connect_to_bd.php';
session_start();
$delay_day_rvmd='';
$repeat_rvmd='';
$token=$_SESSION['user_token'];
$file=$_FILES['upload'];
$id_campaign=$_POST['id'];
$bd=$_POST['bd'];
if(isset($_POST['delay_day_rvmd']) ){
	$delay_day_rvmd=$_POST['delay_day_rvmd'];
}
if(isset($_POST['repeat_rvmd'])){
	$repeat_rvmd=$_POST['repeat_rvmd'];
}
$name=$_POST['name_rvmd'];
$audio=new Audio();
$file_name=$md5=md5(rand(5,10)).mt_rand();
$clientId=Audio::getUserId($_SESSION['user_token']);
try
{
	$audio->get_id_client($token);
	$audio->get_dir_client($file_name);
	$audio->upload($_FILES["upload"]["tmp_name"]);
	$audio->insert_into_db($id_campaign,$file_name,$bd,$delay_day_rvmd,$clientId,$repeat_rvmd,$name);
	$audio->insert_into_rvmd_list($name);
	$success=array("success"=>"1","dir"=>$audio->directoryView);
	echo str_replace('\/','/',json_encode($success));
}catch(Exception $e)
{
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}
?>