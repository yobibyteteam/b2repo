<?php
include '../connect_to_bd.php';
include '../classes/LeadsRain.php';
require_once '../classes/Contact.php';
session_start();
$session_id=$_SESSION['user_token'];
$array1 = $_POST['array1'];
if(isset($_POST['array2'])){
	$array2 = $_POST['array2'];
}else{
	$array2='';
}
$contact_id=$_POST['contact_id'];
$type=$array1[12];

$contact=new Contact();
try{
	$contact->check_for_change($array1,$array2);
	$contact->get_current_user($session_id);
	$contact->change_main_data($contact_id);
	$contact->change_alt_data($contact_id);
	$text="User change contact data";
	$contact->add_to_activity_log($text);
	if($type=='Prospect'){
		Contact::deleteFromQuick($contact_id);
	}else{
		$contactData=Contact::getContactData($contact_id);
		$arrayData=Contact::getReviewLink($contactData['user_id']);
		//Contact::sendSms($contactData['mobile_phone'],$arrayData['link'],$arrayData['welcome_text']);
	}
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>