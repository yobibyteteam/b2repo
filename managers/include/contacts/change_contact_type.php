<?php
require_once '../classes/Contact.php';
include '../connect_to_bd.php';
session_start();
$type=$_POST['type'];
$id=$_POST['id'];
$contact=new Contact();
try{
	$contact->change_type($type,$id);
	if($type=='Customer'){
		$contactData=Contact::getContactData($id);
		$link=Contact::getReviewLink($contactData['user_id']);
		Contact::sendSms($contactData['mobile_phone'],$link);

	}else{
		Contact::deleteFromQuick($id);
	}
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}

?>