<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$token=$_SESSION['user_token'];
$query_model="SELECT * FROM token WHERE token='".$token."'";
$query=mysql_query($query_model);
$array_token=mysql_fetch_assoc($query);
$id=$array_token['client_id'];
$query_model2="SELECT * FROM activity_log WHERE user_id='".$id."' ORDER BY id DESC";
$query3=mysql_query($query_model2);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active ">Activity Log</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
         <br>
         <div class="card">
          <div class="card-header">
            <i class="fa fa-edit"></i> Activity Log
          </div>
          <div class="card-block">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Notification</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=1;
                while($res=mysql_fetch_array($query3)){

                 echo "<tr>";
                 echo "<td>".$i."</td>";
                 echo "<td>".$res['notification']."</td>";
                 echo "<td>".$res['date']."<br>".$res['time']."</td>";
                 echo "</tr>";
                 $i++;
               }
               ?>
             </tbody>
           </table>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</main>
</div>
<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="js/views/charts.js"></script>
<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!-- Plugins and scripts required by this views -->
<!-- Custom scripts required by this view -->
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
</body>
</html>
