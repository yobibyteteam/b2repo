<?php
include '../connect_to_bd.php';
require_once '../classes/Quick_appointment.php';
session_start();
$session_id=$_SESSION['user_token'];
$array = $_POST['array'];

$appointment=new Quick_appointment();


try{
	$appointment->checkAnotherCampaign($array);
	$managerId=$appointment->getManagerId($session_id);
	$appointment->getUserId($managerId);
	$appointment->add_new_appointment($array);
	$text="User add new appointment";
	$appointment->add_to_activity_log($text);
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>