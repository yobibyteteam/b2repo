    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">

                <li class="nav-item">
                    <a class="nav-link" href="index.php"><i class="icon-speedometer"></i> Dashboard
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>
                <li class="divider"></li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-user-follow"></i> Add User</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link dropdown-menu-nav" href="add-admin.php"><i class="icon-user"></i>Admin</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropdown-menu-nav" href="add-user.php"><i class="icon-user"></i>User</a>
                        </li>
                    </ul>
                </li>
                <li class="divider"></li>

                <li class="nav-item">
                    <a class="nav-link" href="universal-campaign-overview.php"><i class="icon-briefcase"></i>Universal Campaign Overview
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="universal-campaign-builder.php"><i class="icon-wrench"></i>Universal Campaign Builder
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>

                
                <!--<li class="nav-item">
                    <a class="nav-link" href="universal-review-builder.php"><i class="icon-eye"></i>Universal Review Builder
                       
                    </a>
                </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="universal-dated-builder.php"><i class="icon-calendar"></i>Universal Dated Builder
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="universal-holiday-builder.php"><i class="fa fa-calendar-plus-o"></i>Universal Holiday Builder
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="activity-log.php"><i class=" icon-chart"></i>Activity Log
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="user-profile-search.php"><i class="icon-magnifier"></i>User Profile Search
                        <!--<span class="badge badge-info">NEW</span> -->
                    </a>
                </li>
            </ul>
        </nav>
    </div>