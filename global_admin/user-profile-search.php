<?php 
include "include/session.php";
include 'include/connect_to_bd.php';
$query=mysql_query("SELECT * FROM user"); 
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
    body{
      position: relative;
    }
    .sk-circle{
      display: none;
      z-index: 10001;
      width: 40px;
      height: 40px;
      position: absolute;
      top: 0;
      bottom: 0;
      right: 0;
      left: 0;
      margin: auto;
    }
  </style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'componets/header.php';  ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">User Profile Search</li>
      </ol>
      <div class="container-fluid">

        <div class="animated fadeIn">
          <br>
          <div class="card">
            <div class="card-header">
              <i class="fa fa-edit"></i> Angular DataTables
            </div>
            <div class="card-block">
              <table class="table table-striped table-bordered datatable table-responsive" >
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <!--<th>Status</th>-->
                    <th style="width:0px;">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  while ($res=mysql_fetch_assoc($query)) {
                    echo "<tr>";
                    echo "<td>".$res['username']."</td>";
                    echo "<td>".$res['email']."</td>";
                    /*if($res['access_system']==0){
                     echo '<td><select style="background:#ff5454!important" onchange="select_type(this)"  name="'.$res['id'].'" class="badge badge-warning btn_user_table_warning select_type">
                     <option value="'.$res['access_system'].'">Inactive</option>
                     <option value="1">Active</option>
                   </td>
                   ';
                 }else{
                  echo '<td><select style="background:#67c2ef!important" onchange="select_type(this)"  name="'.$res['id'].'" class="badge badge-warning btn_user_table_warning select_type">
                  <option value="'.$res['access_system'].'">Active</option><option value="0">Inactive</option></td>
                  ';
                }*/
                echo "<td class=table_button><a style='margin-right:5px; width:45px !important' onclick='info_user(".$res['id'].")' class='overview_btn btn btn-success btn_table' name='".$res['id']."' href='#'>
                <i class='fa fa-search-plus'></i>
              </a><a onclick='modal_show(".$res['id'].")' class='btn btn-danger name='".$res['id']."' btn_table' href='#'>
              <i class='fa fa-trash-o'></i>
            </a></td>";
            echo "</tr>";
          }
          ?>
        </tbody>
      </table>
      <br>
      <button type="button" class="btn btn-success" onclick="window.location='add-user.php'">Create New User</button>
    </div>
  </div>
</div>

</div>
</main>
<div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this user?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="button" onclick="del_user()" data-dismiss="modal" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

</div>
</main>

</div>

<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!-- Plugins and scripts required by this views -->
<script src="js/libs/jquery.dataTables.min.js"></script>
<script src="js/libs/dataTables.bootstrap4.min.js"></script>
<!-- Custom scripts required by this view -->
<script src="js/views/tables.js"></script>
<script>
  var user_id;

  function modal_show(id)
  {
    user_id=id;
    $("#dangerModal").modal("show");
  }

  function del_user(){
    $('.sk-circle').css('display','block');
    var id=user_id;
    $.ajax({
      url:'include/user/delete_user.php',
      type:'POST',
      data:{id:id},
      success:function(){
        $('.sk-circle').css('display','none');
        window.location="user-profile-search.php";
      }
    })
  }
  function info_user(id){
    $('.sk-circle').css('display','block');
    var href='user-profile-overview.php?id='+id;
    window.location=href;
    $('.sk-circle').css('display','none');
  }
  $('.card-header').html("User Profile Search");

  function select_type(id){
    if($(id).val()=='0'){
      $(id).attr('style','background:#ff5454!important');
    }else{
      $(id).attr('style','background:#67c2ef!Important');
    }
    $('.sk-circle').css('display','block');
    var type=$(id).val();
    var id=$(id).attr('name');
    $.ajax({
      url:"include/user/changeStatus.php",
      data:{type:type,id:id},
      type:"POST",
      success:function(data){
      }
    })
    $('.sk-circle').css('display','none');
  }
</script>
</body>
</html>
