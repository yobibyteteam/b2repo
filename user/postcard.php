<html lang="en">
<head>
  <!-- Icons -->
  <link rel="shortcut icon" href="img/favicon.png">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/all.css" rel="stylesheet">
  <link href="css/colpick.css"  rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/component.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/chosen.css" type="text/css" >
  <link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
</head>
<body>
  <main>
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs float-right" role="tablist">
            <li class="nav-item">
              <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab">Front</a>
            </li>
            <li class="nav-item">
              <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab">Back</a>
            </li>
          </ul>
        </div>
        <div class="card-block p-0">
          <div class="tab-content">
            <div class="tab-pane active" id="front">
              <iframe style="width: 100%; height: 800px;" src="postcard-front.php" frameborder="0"></iframe>
            </div>
            <div class="tab-pane" id="back">
              <iframe src="postcard-back.php" style="width: 100%; height: 800px;" frameborder="0"></iframe> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/plugins.js"></script> 
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script src="js/libs/pace.min.js"></script>
  <script src="js/app.js"></script>


  <script type="text/javascript" src="js/html2canvas.js"></script>
  <script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
  <script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js" ></script> 
  <script type="text/javascript" src="js/colpick.js"></script>
  <script src="js/all.min.js" ></script>
  <script src="js/construktor.js" defer="defer"></script>
  <script type="text/javascript">
    var src = '';
  //var canvas = new fabric.Canvas('.component');
  var necessarily_data_array=[];
  var token;
  var filename=0;
  var filekey=1;

  function viewCoupon(){
    $("#readyCoupon").modal("show");
  }

  if($.cookie('top_position')!=null){
    top_block_add = $.cookie('top_position');
  }else{top_block_add = "250px";}
  top_position_add = top_block_add.substr(0,3)*1;
  $(".popup_close").click(function(){
    $(".preview_pop").hide();
  });


  $(".pod_menu_cat_td a").click(function(){
    id_tovar = $(this).attr("id_tovar"); 
    name_tovar = $(this).find("span").text();
    bg_overlayImage = $(this).attr("bg_overlayImage");
    bg_backgroundImage = $(this).attr("bg_backgroundImage");
    bg_kontur_img_fade = $(this).attr("bg_kontur_img");
    price_tovar = $(this).attr("price");
    $(".konstruktor_page .slider_price_all strong").text(price_tovar);
    $("[name=id_product]").val(id_tovar);
    new_url = "/konstruktor.htm?id_outline="+id_tovar;
    window.history.pushState({path:new_url},'',new_url);
    $.post("/jscripts/ajax/change_product.php", {id_tovar:id_tovar },
      function(data){
        obj = jQuery.parseJSON(data);    
        $(".info_product_desc").html(obj.text);    
      });  
    change_product(bg_overlayImage ,name_tovar,id_tovar, bg_backgroundImage);
  });

  var canvasOperations = {
    loadFromUrl : function(){
      fabric.Image.fromURL(src, function(image){
        canvas.add(image.set({
          id : 'canvas'+filename,
          alt : 'canvas'+filename,
          width : canvas.width / 2,
          height : canvas.height / 2,
          perPixelTargetFind : true,
          targetFindTolerance : 4
        }));
      });
    },
  }

  $(".show_menu_konstr").click(function(){
    $(".chuse_product_konstruktor").fadeIn(200);
  });
  $(".chuse_product_konstruktor_top").click(function(){
    $(".chuse_product_konstruktor").fadeOut(200); 
  })

  $( function() {
    $("#resize").resizable({autoHide:true,alsoResize:'#preview'});
  } );

  function del_input(id){
    var parent=$(id).parent();
    $(parent).remove();
  }

  function viewCoupon(){
    $(".component").css('border', '0px');
    $('.component').html2canvas({
      onrendered: function (canvas) {
        $(".imageData").attr('src',canvas.toDataURL("image/png"));
        $(".component").css('border', '3px solid #49708A');
      }
    });
  }

  function saveCoupon(){
    var file = $(".imageData").attr('src');
    var campaignName=$(".campaignName").val();
    var mobileNumber=$(".mobileNumber").val();
    if(mobileNumber==''){
      alert("Enter Mobile Number");
    }else{
      $.ajax({
        url: 'include/coupon/savePostcard.php',
        data: {file:file,campaignName:campaignName,mobileNumber:mobileNumber},
        type: 'POST',
        success:function(){}})
      window.location='coupon-builder.php';
    }
  }
  function addNewFile(filename){
    $(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');  
  }

  function clearAll(){
    canvas.clear();
    filename=0;
    filekey=1;
    $(".field").each(function(){
      $(this).hide();
    })
  }

  function delFile(id,idFile){
    var key=idFile+1;
    var name='canvas'+key;
    console.log(name);
    var lengthCanvas = canvas['_objects'].length;
    for(var k in canvas['_objects']){
      console.log(canvas['_objects'][k]['alt']);
      if(canvas['_objects'][k]['alt']==name){
        canvas['_objects'][k].remove();
        var parent=$(id).parent();
        $(parent).hide();
      }
    }
    console.log(canvas);
    filekey--;
    canvas.renderAll();
    $(".addNewFile").val('');
  }

  function readURL(input){
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        src=e.target.result;
        canvasOperations.loadFromUrl();
      };
      addNewFile(filename);
      reader.readAsDataURL(input.files[0]);
      filename++;
      filekey++;
    }
  }
  $(".del_fil").click();
</script>
<script src="components/user_notifications.js"></script>
</body>
</html>
