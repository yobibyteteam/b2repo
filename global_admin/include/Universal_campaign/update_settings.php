<?php
include '../connect_to_bd.php';
require_once '../class/Universal_campaign.php';
$id=$_POST['id_campaign'];
$name=$_POST['name'];
$type=$_POST['type'];

$un_campaign=new Universal_campaign();

try{
	$un_campaign->get_name_campaign($id);
	$un_campaign->update_type_campaign($id,$type);
	$un_campaign->update_name_campaign($id,$name);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>