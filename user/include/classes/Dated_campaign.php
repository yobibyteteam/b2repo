<?php
require_once 'LeadsRain.php';
class Dated_campaign
{
	var $name,$type_campaign, $error, $session_token, $type_client, $id_client, $campaign_id,$time_current,$date_current,$admin_name ,$dated ,$final_array,$id_campaign,$audio_data,$text_data,$email_data,$postcard,$activity_reminder;
	
	function set_data(&$name_campaign,&$type_campaign,&$dated,&$activity_reminder){
		$this->session_token = $_SESSION['user_token'];
		$this->name = $name_campaign;
		$this->type_campaign = $type_campaign;
		$this->dated = $dated;
		$this->name = htmlspecialchars($this->name);
		$this->dated = htmlspecialchars($this->dated);
		$this->name = mysql_real_escape_string($this->name);
		$this->dated = mysql_real_escape_string($this->dated);
		$this->activity_reminder=$activity_reminder;
	}

	function get_postcard_data($dir)
	{
		$query_model="SELECT * FROM postcard WHERE campaign_builder_id='".$this->id_campaign."' AND type='user' AND type_campaign='dated'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$this->postcard[]=$res;
				$this->postcard['dir']=$dir;
			}
		}
	}
	static public function getClientId($token){
		$query_model="SELECT client_id FROM token WHERE token='".$token."'";
		$query=mysql_query($query_model);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['client_id'];
	}

	static public function get_dir_admin($clientId){
		$query_model="SELECT dir_img FROM user WHERE id='".$clientId."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData['dir_img'];
	}

	function check_input_data(){
		$query_model="SELECT * FROM dated_builder WHERE name = '".$this->name."'"
		;
		$query = mysql_query($query_model);
		if(mysql_num_rows($query) != 0){
			$this->error = 'campaign_exist';
			throw new Exception("campaign_exist", 1);
		}
	}

	function get_id_client(){
		$query_model="SELECT * FROM token WHERE token = '".$this->session_token."'";
		$query = mysql_query($query_model);
		if(mysql_num_rows($query) == 0){
			throw new Exception("not_fount_token", 1);
		}else{
			$array_token = mysql_fetch_assoc($query);
			$this->type_client=$array_token['type'];
			$this->id_client=$array_token['client_id'];
		}
	}

	function add_to_bd($assignNum){
		$md5=md5(rand(5,15)).mt_rand();
		$query_model="INSERT INTO `dated_builder`(`client_id`, `type`, `name`,`type_campaign`,`dated`,`md5`,`activity_reminder`,`assign_num`) VALUES ('".$this->id_client."','".$this->type_client."','".$this->name."','".$this->type_campaign."','".$this->dated."','".$md5."','".$this->activity_reminder."','".$assignNum."')";
		mysql_query($query_model);
		$this->campaign_id=mysql_insert_id();
	}

	function get_admin_name(){
		$token=$_SESSION['user_token'];
		$query_model="SELECT * FROM token WHERE token='".$token."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_token=mysql_fetch_assoc($query);
			$id_admin=$array_token['client_id'];
			$query_model="SELECT * FROM user WHERE id='".$id_admin."'";
			$query=mysql_query($query_model);
			if(mysql_num_rows($query)!=0){
				$array_admin=mysql_fetch_assoc($query);
				$this->admin_name=$array_admin['first_name'];
			}
		}
	}

	function add_to_activity_log($text){
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		$array_token=mysql_fetch_array($query);
		$id_user=$array_token['client_id'];
		$time_current=date("h:i:s");
		$date_current=date("Y-m-d");
		$query_model="INSERT INTO activity_log(`notification`,`date`,`time`,`user_id`,`type`) VALUES('".$text."','".$date_current."','".$time_current."','".$id_user."','user')";
		mysql_query($query_model);
	}

	function delete_campaign($id){
		$query_model="DELETE FROM dated_builder_email WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM dated_builder_text WHERE campaign_builder_id='".$id."'";
		$arrayLeadsrain=$this->checkLeadsRainCampaign($id);
		if($arrayLeadsrain!=''){
			$leadsRain=new LeadsRain();
			$leadsRain->deleteList($arrayLeadsrain['leadsrain_list_id']);
			$leadsRain->deleteCampaign($arrayLeadsrain['leadsrain_campaign_id']);
		}
		mysql_query($query_model);
		$query_model="DELETE FROM dated_builder_rvmd WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM dated_builder WHERE id='".$id."'";
		mysql_query($query_model);
	}

	function checkLeadsRainCampaign($id){
		$arrayData='';
		$query_model="SELECT leadsrain_campaign_id,leadsrain_list_id FROM dated_builder_rvmd WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData;
	}

	function add_email($id,$text,$delay_day_email,$clientId,$repeat_email){
		//$delay_day_email=date( "Y-m-d", strtotime( "+".$delay_day_email." day" ));
		$query_model="SELECT * FROM dated_builder_email WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			$query_model="INSERT INTO `dated_builder_email`(`campaign_builder_id`,`text`,`delay_day`,`type`,`client_id`,`repeat_year`) VALUES('".$id."','".$text."','".$delay_day_email."','user','".$clientId."','".$repeat_email."')";
			mysql_query($query_model);
		}else{
			$query_model="UPDATE dated_builder_email SET `text`='".$text."' , `delay_day`='".$delay_day_email."',`repeat_year`='".$repeat_email."' WHERE campaign_builder_id='".$id."'";
			mysql_query($query_model);
		}
	}

	function add_text($id,$text,$delay_day_text,$clientId,$repeat_text){
		//$delay_day_text=date( "Y-m-d", strtotime( "+".$delay_day_text." day" ));
		$query_model="SELECT * FROM dated_builder_text WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			$query_model="INSERT INTO `dated_builder_text`(`campaign_builder_id`,`text`,`delay_day`,`type`,`client_id`,`repeat_year`) VALUES('".$id."','".$text."','".$delay_day_text."','user','".$clientId."','".$repeat_text."')";
			echo $query_model;
			mysql_query($query_model);
		}else{
			$query_model="UPDATE dated_builder_text SET `text`='".$text."' , `delay_day`='".$delay_day_text."',`repeat_year`='".$repeat_text."' WHERE campaign_builder_id='".$id."'";
			mysql_query($query_model);
		}

	}

	function get_data($name){
		$query_model="SELECT * FROM dated_builder WHERE name='".$name."'";
		$query=mysql_query($query_model);
		$array_campaign=mysql_fetch_assoc($query);
		$id=$array_campaign['id'];
		$query_model="SELECT * FROM dated_builder_text WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_text=mysql_fetch_assoc($query);
			$array_text=array("text"=>$array_text);
		}else{
			$array_text=array("text"=>"0");
		}
		$query_model="SELECT * FROM dated_builder_email WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_email=mysql_fetch_assoc($query);
			$array_email=array("email"=>$array_email);
		}else{
			$array_email=array("email"=>"0");
		}
		$query_model="SELECT * FROM dated_builder_rvmd WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_audio=mysql_fetch_assoc($query);
			$array_audio=array("audio"=>$array_audio);
		}else{
			$array_audio=array("audio"=>"0");
		}
		$this->array_final=array_merge($array_campaign,$array_text,$array_email,$array_audio);
	}
	function get_campaign_id_token($token){
		$query_model="SELECT * FROM dated_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_token=mysql_fetch_assoc($query);
		$this->id_campaign=$array_token['id'];
	}

	function get_email_data(){
		$query_model="SELECT * FROM dated_builder_email WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_email=mysql_fetch_assoc($query);
			if($array_email['text']!=''){
				$this->email_data=$array_email;
			}else{
				$this->email_data='empty';
			}
		}
	}

	function get_text_data(){
		$query_model="SELECT * FROM dated_builder_text WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_text=mysql_fetch_assoc($query);
			if($array_text['text']!=''){
				$this->text_data=$array_text;
			}else{
				$this->text_data='empty';
			}
		}
	}

	function get_audio_data(){
		$query_model="SELECT * FROM dated_builder_rvmd WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_audio=mysql_fetch_assoc($query);
			if($array_audio['filename']!=''){
				$this->audio_data=$array_audio;
			}else{
				$this->audio_data='empty';
			}
		}
	}

	function get_setting_data($token){
		$query_model="SELECT * FROM dated_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_token=mysql_fetch_assoc($query);
		$this->name=$array_token['name'];
		$this->type_campaign=$array_token['type_campaign'];
		$this->dated=$array_token['dated'];
	}

	function get_name_campaign($id){
		$query_model="SELECT * FROM dated_builder WHERE id='".$id."'";
		$query=mysql_query($query_model);
		$array_campaign=mysql_fetch_assoc($query);
		$this->name=$array_campaign['name'];
	}

	function update_type_campaign($id,$type){
		$query_model="UPDATE `dated_builder` SET `type_campaign`='".$type."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function update_dated_campaign($id,$dated){
		$query_model="UPDATE `dated_builder` SET `dated`='".$dated."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function update_name_campaign($id,$name){
		$query_model="SELECT * FROM dated_builder WHERE name='".$name."'";
		$query=mysql_query($query_model);
		if($this->name!=$name){
			if(mysql_num_rows($query)!=0){
				throw new Exception("Name_exist", 1);
			}else{
				$query_model="UPDATE `dated_builder` SET `name`='".$name."' WHERE `id`='".$id."'";
				mysql_query($query_model);
			}
		}
	}

	
	function update_text_data($id,$text,$delay_day_text){
		$query_model="UPDATE `dated_builder_text` SET `text`='".$text."',`delay_day`='".$delay_day_text."' WHERE `campaign_builder_id`='".$id."'";
		mysql_query($query_model);
	}

	function update_email_data($id,$text,$delay_day_email){
		$query_model="UPDATE `dated_builder_email` SET `text`='".$text."',`delay_day`='".$delay_day_email."' WHERE `campaign_builder_id`='".$id."'";
		mysql_query($query_model);
	}

	static public function getUserId($token){
		$queryModel="SELECT client_id FROM token WHERE token = '".$token."'";
		$queryToTokenBD = mysql_query($queryModel);
		$arrayToken = mysql_fetch_assoc($queryToTokenBD);
		return $arrayToken['client_id'];
	}

	static public function getReadyEmail($clientId){
		$queryToEmailBD=mysql_query("SELECT `text` FROM dated_builder_email WHERE type='global_admin' or client_id='".$clientId."'");
		if(mysql_num_rows($queryToEmailBD)!=0){
			while ($res=mysql_fetch_assoc($queryToEmailBD)) {
				$arrayData[]=$res['text'];
			}
		}else{
			$arrayData=[];
		}
		return $arrayData;
	}

	static public function getReadyText($clientId){
		$queryToTextBD=mysql_query("SELECT `text` FROM dated_builder_text WHERE type='global_admin' OR client_id='".$clientId."'");
		if(mysql_num_rows($queryToTextBD)!=0){
			while ($res=mysql_fetch_assoc($queryToTextBD)) {
				$arrayData[]=$res['text'];
			}
		}else{
			$arrayData=[];
		}
		return $arrayData;
	}

	static public function getReadyRVMD($clientId){
		$queryToRVMDBD=mysql_query("SELECT `filename`,`name` FROM rvmd_list WHERE user_id='".$clientId."'");
		if(mysql_num_rows($queryToRVMDBD)!=0){
			while ($res=mysql_fetch_assoc($queryToRVMDBD)) {
				$arrayData[]=$res;
			}
		}else{
			$arrayData=[];
		}
		return $arrayData;
	}

}

?>