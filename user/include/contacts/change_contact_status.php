<?php
require_once '../classes/Contact.php';
include '../connect_to_bd.php';
session_start();
$status=$_POST['status'];
$id=$_POST['id'];
$contact=new Contact();
try{
	$contact->change_status($status,$id);
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}

?>