<?php
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';

$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_text'];
$delay_day_text=$_POST['delay_day_text'];
$holiday_campaign=new Holiday_campaign();

try{
	$holiday_campaign->update_text_data($id_campaign,$editor_text,$delay_day_text);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>