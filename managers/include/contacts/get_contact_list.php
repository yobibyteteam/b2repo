<?php
require_once '../classes/Contact.php';
include '../connect_to_bd.php';
session_start();
$id=$_POST['id'];

$contact=new Contact();
try{
	$contact->get_list_contact($id);
	echo json_encode(array("success" => 1,"data"=>$contact->list_contact));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}

?>