<?php
include '../connect_to_bd.php';
require_once '../classes/Dated_campaign.php';

$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_data'];
$delay_day_email=$_POST['delay_day_email'];
$dated_campaign=new Dated_campaign();

try{
	$dated_campaign->update_email_data($id_campaign,$editor_text,$delay_day_email);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>