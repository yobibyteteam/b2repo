<?php
include '../connect_to_bd.php';
require_once '../class/Universal_campaign.php';

$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_text'];
$delay_day_text=$_POST['delay_day_text'];
$un_campaign=new Universal_campaign();

try{
	$un_campaign->add_text($id_campaign,$editor_text,$delay_day_text);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>