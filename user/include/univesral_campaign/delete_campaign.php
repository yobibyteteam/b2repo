<?php 
include '../connect_to_bd.php';
require_once '../classes/Universal_campaign.php';
session_start();

$id=$_POST['id'];
$text="User delete campaign";

$un_campaign=new Universal_campaign();
try
{
	$un_campaign->delete_campaign($id);
	$text="User delete universal campaign";
	$un_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>"1"));
}
catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>