<?php
require_once 'classes/Audio.php';
include 'connect_to_bd.php';
session_start();
if(isset($_FILES["audio-blob"])){
	$audio=$_FILES["audio-blob"];
	$file_name=$_POST["filename"];
	$token=$_SESSION['user_token'];
	$id_campaign=$_POST['id_campaign'];
	$bd=$_POST['bd'];
	$clientId=Audio::getUserId($_SESSION['user_token']);
	$audio=new Audio();
	$name=$_POST['name_rvmd'];
	try{
		$audio->get_id_client($token);
		$audio->get_dir_client($file_name);
		$audio->upload($_FILES["audio-blob"]["tmp_name"]);
		if(isset($_POST['repeat_rvmd']) && isset($_POST['delay_day_rvmd'])){
			$repeat_rvmd=$_POST['repeat_rvmd'];
			$delay_day_rvmd=$_POST['delay_day_rvmd'];
			$audio->insert_into_db($id_campaign,$file_name,$bd,$delay_day_rvmd,$clientId,$repeat_rvmd,$name);
		}else{
			$audio->insertToBdWithoutDelay($id_campaign,$file_name,$bd,$clientId);
		}
		$audio->insert_into_rvmd_list($name);
		$success=array("success"=>"1","dir"=>$audio->directoryView);
		echo str_replace('\/','/',json_encode($success));
	}catch(Exception $e)
	{
		echo json_encode(array("success" => "0","error" => $e->getMessage()));
	}



}
?>