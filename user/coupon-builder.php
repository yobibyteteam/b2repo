<?php
include "include/session.php";


function get_list_campaign()
{
	$token=$_SESSION['user_token'];
	$query_model="SELECT * FROM token WHERE token='".$token."'";
	$query=mysql_query($query_model);
	if(mysql_num_rows($query)!=0)
	{
		$array=mysql_fetch_assoc($query);
		$client_id=$array['client_id'];
		getUniversalCampaign($client_id);
		getDatedCampaign($client_id);
		getHolidayCampaign($client_id);

	}
}

function getUniversalCampaign($client_id){
	global $campaignId;
	$query_model="SELECT id,name FROM campaign_builder WHERE client_id='".$client_id."' AND type='user'";
	$query=mysql_query($query_model);
	if(mysql_num_rows($query)!=0){
		while($res=mysql_fetch_assoc($query)){
			echo '<option value="'.$res['id'].'/campaign">'.$res['name'].'(Universal)</option>';
		}
	}
}
function getHolidayCampaign($client_id){
	global $nameCampaign;
	$query_model="SELECT id,name FROM holiday_builder WHERE client_id='".$client_id."' AND type='user' ";
	$query=mysql_query($query_model);
	if(mysql_num_rows($query)!=0){
		while($res=mysql_fetch_assoc($query)){
			echo '<option  value="'.$res['id'].'/holiday">'.$res['name'].'(Holiday)</option>';
		}
	}
}
function getDatedCampaign($client_id){
	global $campaignId;
	$query_model="SELECT id,name FROM dated_builder WHERE client_id='".$client_id."' AND type='user'";
	$query=mysql_query($query_model);
	if(mysql_num_rows($query)!=0){
		while($res=mysql_fetch_assoc($query)){
			echo '<option value="'.$res['id'].'/dated">'.$res['name'].'(Dated)</option>';
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Clever - Bootstrap 4 Admin Template">
	<meta name="author" content="Łukasz Holeczek">
	<meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
	<meta property="og:site_name" content="www.mademycreative.com">
	<title>CRM System</title>
	<!-- Icons -->
	<link rel="shortcut icon" href="img/favicon.png">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/simple-line-icons.css" rel="stylesheet">
	<!-- Main styles for this application -->
	<link href="css/style.css" rel="stylesheet">
	<link href="css/all.css" rel="stylesheet">
	<link href="css/colpick.css"  rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/component.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/chosen.css" type="text/css" >
	<link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
	<style>
	.canvas-container{margin: 0;}
	.component{position: relative;
		height: 408px;
		border: 3px solid #49708A;
		width: 556px;
		overflow: hidden;
		background-color: grey;}
		#preview{display: none;}
		.add_img {display: none;}
		.target {
			border: 1px solid #CCC;
			padding: 5px;
			margin: 5px;
		}
		h2, h3 {color: #003d5d;}
		p {color:#AA00BB;}
		#more {
			font-family: Verdana;
			color: purple;
			background-color: #d8da3d;
		}

		.input-group-addon{
			color:inherit!important;
			background-color: inherit!important;
		}
		.form-control:disabled{
			opacity: 0.7;
			color:#000;
		}
		input{
			padding-left:5px!important;
			padding-right: 0.8rem!important;
		}
		input[type='number'] {
			-moz-appearance: textfield;
		}
		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}
		.modal-footer{
			padding: 10px;
			border-top: 2px solid #e1e6ef;
		}
	</style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
	<?php include 'components/header.php'; ?>
	<div class="app-body">
		<?php include "sidebar.php"; ?>
		<main class="main">
			<form method="POST" enctype="multipart/form-data" action="preview_coupon.php" id="myForm">
				<input type="hidden" name="img_val" id="img_val" value="" />
			</form>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="index.php">Home</a></li>
				<li class="breadcrumb-item active">Coupon Builder</li>
			</ol>
			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-header">
									<strong>Coupon Builder</strong>
									<a href="#" style="float:right;outline: none;" data-toggle="modal" data-target="#">Click Here for Video Tutorial</a>
								</div>
								<div class="card-block">
									<label> Select Campaign:</label>
									<select id="select2" name="select2" class="form-control campaignName">
										<option selected="" value="0">None</option>
										<?php get_list_campaign(); ?>
									</select><br>
									<label for="name_coupoon">Enter Mobile Number to Preview Coupon:</label>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1">+</span>
										<input id="name_coupon" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile input_data mobileNumber form-control"  placeholder="Mobile Number
										" aria-describedby="basic-addon1">
									</div>
									<br>
									<label for="price_coupon">Enter Price Of Coupon:</label>
									<div class="input-group">
										<span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd" aria-hidden="true"></i></span>
										<input id="price_coupon" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile input_data priceCoupon  form-control"  placeholder="Price Of Coupon
										" aria-describedby="basic-addon1">
									</div>
									<br>
									<div class="iframe_1">
										<div class="test" style="overflow-x: auto; -webkit-overflow-scrolling: touch;" >
											<div class="row" style="width: 780px;">
												<div class="col-md-3">
													<div class="control_el scroll_top">
														<table cellpadding="0" cellspacing="0" border="0" class="edit_text">
															<tr>
																<td>
																	<div class="butt_fonts fonts_color tool"  >
																		<div class="fonts_color_vn" id="color_text">
																			<span class="text">a</span>
																			<div class="fonts_color_bl" style="background: none repeat scroll 0% 0% rgb(51, 51, 51);"></div>
																			<div class="clear"></div>
																		</div>
																	</div>
																</td>
																<td>
																	<div class="butt_fonts fonts_b tool block_edit_text" action="bold">b</div>
																	<div class="butt_fonts fonts_i tool block_edit_text" action="italic">i</div>
																	<div class="butt_fonts fonts_u last tool block_edit_text" action="underline" >u</div>
																</td>
															</tr>
														</table>
													</div>
													<div class="center_text_block" style="width: 100%;margin:0 auto">
														<div class="add_text_block">
															<div class="panel_head">Editing Panel Front</div>
															<div class="clear"></div>
															<div class="block_show" style="overflow: hidden; display: block;">
																<div class="filds_all">
																	<div>
																		<textarea placeholder="Text" class="text_ar" style="resize: none; overflow-y: hidden; position: absolute; top: 0px; left: -9999px; height: 20px; width: 268px; line-height: normal; text-decoration: none solid rgb(0, 0, 0); letter-spacing: normal;" tabindex="-1">
																		</textarea>
																		<textarea name="fio_v" placeholder="Text" class="text_ar" style="resize: none; overflow-y: hidden;"></textarea>
																		<a onclick="del_input(this)" class="del_fil tool" cus="fio_v" title="Delete">
																		</a>
																	</div>
																</div>
																<button id="text_add" class=" btn btn-success add_fild tool">

																	<span class="add_fild_text">Add Text</span>

																</button>
																<input accept="image/jpeg,image/png" type='file' id='file' onchange="readURL(this);" class="btn btn-default addNewFile" style=" margin-top: 7px;opacity: 0;position: relative;z-index: 1;cursor: pointer;width: 75%;"
																/>
																<button id="img_add" style=" position: relative;bottom: 40px;width: 75%;" class="add_img_btn btn btn-primary"><b>Add Img</b></button>

																<p class="load_konstructor"></p>
																<br><br>
																<button style="position: relative;bottom: 50px;" class="btn btn-danger" onclick="clearAll()">Clear All</button>
															</div>
														</div>
													</div>

												</div>
												<div class="col-md-9">
													<div class="component"><img class="resize-image" id='preview' /><canvas id="canvas1" width="556" height="408"> </canvas>
														<div class="overlay">
															<div class="overlay-inner">
															</div>
														</div>
													</div>
													<button  style="margin-top: 15px;" data-toggle="modal" data-target="#readyCoupon"  class="previewCoupon  btn btn-primary" onclick="viewCoupon();">View</button>
												</div>
												<div class="chnage_templ_top">
													<select name="tema" id="tema" style="visibility: hidden;"></select>
												</div>
											</div>
										</div>
									</div>

									<div class="save_window popup">
										<div  class="save_window_top">
											<a class="close_window_login"></a>
										</div>
										<div  class="save_window_botton">
											<div class="save_window_botton_vn">
												<div class="res_save_maket"></div>
												<form action="" method="post" name="save_maket_form" class='form_save_maket'>
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td>Maket Name</td>
														</tr>
														<tr>
															<td>
																<input type="text" value="" name="name_maket">
																<div class="fild_error"><span id="er_name_maket" class="errors_disable"></span></div>
															</td>
														</tr>
													</table>
												</div>
												<div class="link_to_maket_block">
													<p><a href="" target="_blank" class="link_maket"></a></p>
												</div>
												<div class="save_window_botton_vn">
													<input type="button" value="Save" name="send_b" class="save_but">
												</div>
												<div class="res_save" style="display: none; text-align: center;"></div>
												<input type="hidden" name="type_product" value="">
											</form>
										</div>
									</div>
									<div class="block_sdvig upload_clipart_block">
										<p class="close_sdvig"><a></a></p>
										<div class="block_img_listing">
											<div class="chuse_template scrooly cliparts">
											</div>
										</div>
									</div>
								</div><br>
								<div class="card-block btn_save_block" style="display: none">
									<div class="text-center">
										<button type="button" onclick="saveCoupon()" class="btn btn-success" data-dismiss="modal">Send Coupon</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</main>


<div class="modal fade" id="readyCoupon" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="panel-group" id="accordion" style="text-align: center;">
				<img style="width: 100%;" class="imageData" src="#" alt="#">
				<input type="file" name="imgInput" class="photo" id="imgInput" style="display: none" />
				<button type="button" class="btn btn-primary" style="position: relative; margin-bottom: 10px;">Redeem </button>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success" onclick="buttonHide()" data-dismiss="modal">Save</button>
			</div>
		</div>
	</div>
</div>
</div>
<!--onclick="window.location='../redeemed_coupon.php'"-->


<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/plugins.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="js/app.js"></script>
<script type="text/javascript" src="js/html2canvas.js"></script>
<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js" ></script>
<script type="text/javascript" src="js/colpick.js"></script>
<script src="js/all.min.js" ></script>
<script src="js/construktor.js" defer="defer"></script>
<script type="text/javascript">
	var src = '';
	var necessarily_data_array=[];
	var token;
	var filename=0;
	var filekey=1;

	if($.cookie('top_position')!=null){
		top_block_add = $.cookie('top_position');
	}else{top_block_add = "250px";}
	top_position_add = top_block_add.substr(0,3)*1;
	$(".popup_close").click(function(){
		$(".preview_pop").hide();
	});

	var canvasOperations = {
		loadFromUrl : function(){
			fabric.Image.fromURL(src, function(image){
				canvas.add(image.set({
					id : 'canvas'+filename,
					alt : 'canvas'+filename,
					width : canvas.width / 2,
					height : canvas.height / 2,
					perPixelTargetFind : true,
					targetFindTolerance : 4
				}));
			});
		},
	}

	$(".show_menu_konstr").click(function(){
		$(".chuse_product_konstruktor").fadeIn(200);
	});
	$(".chuse_product_konstruktor_top").click(function(){
		$(".chuse_product_konstruktor").fadeOut(200);
	})

	$( function() {
		$("#resize").resizable({autoHide:true,alsoResize:'#preview'});
	} );

	function del_input(id){
		var parent=$(id).parent();
		$(parent).remove();
	}

	function buttonHide(){
		$(".previewCoupon").css("display","none");
		$(".btn_save_block").css("display","block");
	}

	function viewCoupon(){
		$(".component").css('border', '0px');
		$('.component').html2canvas({
			onrendered: function (canvas) {
				$(".imageData").attr('src',canvas.toDataURL("image/png"));
				$(".component").css('border', '3px solid #49708A');
			}
		});
		$("#readyCoupon").modal("show");
	}

	function saveCoupon(){
		var file = $(".imageData").attr('src');
		var campaignName=$(".campaignName").val();
		var mobileNumber=$(".mobileNumber").val();
		var priceCoupon=$(".priceCoupon").val();
		if(campaignName=='0'){
			alert("Select Campaign");
		}else if(mobileNumber==''){
			alert("Enter Mobile Number");
		}else if(priceCoupon==''){
			alert("Enter Price");
		}else{
			$.ajax({
				url: 'include/coupon/saveCoupon.php',
				data: {file:file,campaignName:campaignName,mobileNumber:mobileNumber,priceCoupon:priceCoupon},
				type: 'POST',
				success:function(){}})
			window.location='coupon-builder.php';
		}
	}
	function addNewFile(filename){
		$(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');
	}

	function clearAll(){
		canvas.clear();
		filename=0;
		filekey=1;
		$(".field").each(function(){
			$(this).hide();
		})
	}

	function delFile(id,idFile){
		var key=idFile+1;
		var name='canvas'+key;
		console.log(name);
		var lengthCanvas = canvas['_objects'].length;
		for(var k in canvas['_objects']){
			console.log(canvas['_objects'][k]['alt']);
			if(canvas['_objects'][k]['alt']==name){
				canvas['_objects'][k].remove();
				var parent=$(id).parent();
				$(parent).hide();
			}
		}
		console.log(canvas);
		filekey--;
		canvas.renderAll();
		$(".addNewFile").val('');
	}

	function readURL(input){
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				src=e.target.result;
				canvasOperations.loadFromUrl();
			};
			addNewFile(filename);
			reader.readAsDataURL(input.files[0]);
			filename++;
			filekey++;
		}
	}
	$(".del_fil").click();

	$(".checkMobile").keydown(function (e) {
// Allow: backspace, delete, tab, escape, enter and .
if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
// Allow: Ctrl/cmd+A
(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
// Allow: Ctrl/cmd+C
(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
// Allow: Ctrl/cmd+X
(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
// Allow: home, end, left, right
(e.keyCode >= 35 && e.keyCode <= 39)) {
// let it happen, don't do anything
return;
}
// Ensure that it is a number and stop the keypress
if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	e.preventDefault();
}
});
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
</body>

</html>
