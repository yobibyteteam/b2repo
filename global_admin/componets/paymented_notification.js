$(function () {
  var i = -1;
  var toastCount = 0;
  var $toastlast;


$('#show_payment_failed').click(function () {
         toastr.error('Payment failed. You need to check the payment method and repeat', 'Payment Error', {
            closeButton: true,
            progressBar: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 0,
            extendedTimeOut: 0,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"

        });
});
$('#show_payment_success').click(function () {
         toastr.success('Payment was successful', 'Payment', {
            closeButton: true,
            progressBar: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 0,
            extendedTimeOut: 0,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"

        });
});
})