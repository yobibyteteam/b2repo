<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever Bootstrap 4 Admin Template">
  <meta name="author" content="Lukasz Holeczek">
  <meta name="keyword" content="Clever Bootstrap 4 Admin Template">
  <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body class="app flex-row align-items-center" style="background-color: #f2f4f8!important;">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="clearfix">
          <h1 class="float-left display-3 mr-4">402</h1>
          <h4 class="pt-3">Oops! Payment failed</h4>
          <p class="text-muted">
            Please pay to enter the system</p>
          </div>
          <div class="input-prepend input-group">
            <p>Seems like the payment was not successful. Please, proceed with the payment and once it is done your access will be restored.<br>  
              Have questions? Contact your agent. Your agent’s name is <b>User</b>. Feel free to contact her at: <b>user</b> </p>   
            </div>
            <form action="/stripe.php" method="POST">
              <script
              src="https://checkout.stripe.com/checkout.js" class="stripe-button"
              data-key="pk_test_E8KGih5ZI16mjgG9RxSrehOg"
              data-amount="<?php echo $_POST['count'];?>"
              data-name="ContactTracs"
              data-description="Monthly payment"
              data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
              data-locale="auto"
              data-zip-code="true">
            </script>
          </form>
        </div>
      </div>
    </div>
    <!-- Bootstrap and necessary plugins -->
    <script src="js/libs/jquery.min.js"></script>
    <script src="js/libs/tether.min.js"></script>
    <script src="js/libs/bootstrap.min.js"></script>
  </body>
  </html>