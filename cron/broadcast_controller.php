<?php

class Broadcast {
	public $model;
	private $allBroadcastData,$allCampaign,$allEmailData,$allPostcardData,$allAddress,$allRvmdData,$arrayCampaignId;
	function __construct(){
		$this->model=new Cron_Broadcast();
		$this->action_getAllBroadCast();
		$this->action_getAllContacts();
		$this->action_getAllEmail();
		$this->action_getAllText();
		$this->action_getAllRvmd();
		$this->action_getAllPostcard();
	}
	function action_getAllBroadCast(){
		$this->allBroadcastData=$this->model->getAllBroadcast();
		//echo json_encode($this->allBroadcastData);
	}

	function action_getAllContacts(){
		foreach ($this->allBroadcastData as $key => $value) {
			$this->allCampaign[$value['id']]=$this->model->getAllCampain($value['id'],$value['parameter'],$value['parameter_value']);
			$this->arrayCampaignId[]=$value['id'];
		}
		//echo json_encode($this->allCampaign);
	}

	function action_deleteCampaign(){
		foreach ($this->arrayCampaignId as $key => $value) {
			$this->model->deleteCampaign($value);
		}
	}

	function action_getAllRvmd(){
		foreach ($this->allCampaign as $key => $value) {
			$this->allRvmdData[$key]=$this->model->getAllRvmd($key);
		}
		//echo json_encode($this->allRvmdData);
	}

	function action_getAllPostcard(){
		foreach ($this->allCampaign as $key => $value) {
			$this->allPostcardData[$key]=$this->model->getAllPostcard($key);
		}
		//echo json_encode($this->allPostcardData);
	}


	function action_sendPostcard(){
		$frontFile='';
		$backFile='';
		$clientId=0;
		foreach ($this->allCampaign as $ikey => $ivalue) {
			foreach ($this->allPostcardData as $jkey => $jvalue) {
				if($ikey==$jkey){
					if (array_key_exists('front', $jvalue)){
						$frontFile=$this->model->renameFile($jvalue['front']['filename']);
						$clientId=$jvalue['front']['client_id'];
					}
					if (array_key_exists('back', $jvalue)){
						$backFile=$this->model->renameFile($jvalue['back']['filename']);
						$clientId=$jvalue['back']['client_id'];
					}
					$dirImg=$this->model->getUserPhotoDir($clientId);
					foreach ($ivalue as $ykey => $yvalue) {
						$this->model->sendPostcard($dirImg,$frontFile,$backFile,$clientId,$yvalue,$jvalue['back']['delay_day_date']);	
					}
				}	
			}
		}
	}

	function action_getAllEmail(){
		foreach ($this->allCampaign as $key => $value) {
			foreach ($value as $ikey => $ivalue) {
				$this->allEmailData[$ikey]=$this->model->getAllEmail($ikey);	
			}
		}
		//echo json_encode($this->allEmailData);
	}

	function action_getAllText(){
		foreach ($this->allCampaign as $key => $value) {
			foreach ($value as $ikey => $ivalue) {
				$this->allTextData[$ikey]=$this->model->getAllText($ikey);	
			}
		}
		//echo json_encode($this->allTextData);
	}

	function addStatistic($id){
		$clientId=$this->model->getUserId($id);
		$this->model->addToStatistic($clientId,'campaign','broadcast');
	}

	function action_sendEmail(){
		foreach ($this->allCampaign as $key => $value) {
			foreach ($value as $ikey => $ivalue) {
				$this->addStatistic($key);
				$this->model->sendEmail($this->allEmailData[1]['text'],$ivalue['email'],$ivalue['user_id']);
			}
		}
	}

	function action_sendRvmd(){
		foreach ($this->allCampaign as $ikey => $ivalue) {
			foreach ($ivalue as $jkey => $jvalue) {
				$countPrice=$this->model->getCountPrice($jvalue['user_id'],'rvmd');
				$this->model->addToStatistic($jvalue['user_id'],'campaign','rvmd');
				$this->model->addBillingStat($jvalue['user_id'],$countPrice,'rvmd');
				$this->model->addMoreValue($jvalue['user_id'],'rvmd');
			}
		}
	}

	function action_sendText(){
		$text='';
		foreach ($this->allCampaign as $key => $value) {
			foreach ($value as $ikey => $ivalue) {
				if (array_key_exists('text', $this->allEmailData[1])) {
					$text=$this->allEmailData[1]['text'];
					$this->model->sendText($text,$ivalue['mobile_phone'],$ivalue['user_id'],$key);
				}
			}
		}
	}


}


?>