<?php
include '../connect_to_bd.php';
require_once '../classes/Universal_campaign.php';
session_start();
$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_text'];
$delay_day_text=$_POST['delay_day_text'];
$repeat_text=$_POST['repeat_text'];
$un_campaign=new Universal_campaign();
$clientId=Universal_campaign::getUserId($_SESSION['user_token']);
try{
	$un_campaign->add_text($id_campaign,$editor_text,$delay_day_text,$clientId,$repeat_text);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>