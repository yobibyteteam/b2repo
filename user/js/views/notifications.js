$(function () {
  var i = -1;
  var toastCount = 0;
  var $toastlast;

$('#show_feedback').click(function () {
        toastr.info('We have a new Feedback', 'Feedback Reports', {
            closeButton: true,
            progressBar: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 0,
            extendedTimeOut: 0,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"

        });
});
$('#show_completed_form').click(function () {
        toastr.info('Someone filled out the form. We have a new contact', 'Completed Form', {
            closeButton: true,
            progressBar: true,
            debug: false,
            newestOnTop: true,
            positionClass: "toast-top-right",
            preventDuplicates: false,
            onclick: null,
            showDuration: 300,
            hideDuration: 1000,
            timeOut: 0,
            extendedTimeOut: 0,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"

        });
});
})
