<?php
include 'include/session.php';
include 'include/connect_to_bd.php';
function getIncludeFunction($client_id){
  $query_model="SELECT include_function FROM user WHERE id='".$client_id."'";
  $query=mysql_query($query_model);
  $array_user=mysql_fetch_assoc($query);
  return $array_user['include_function'];
}
function getUniversalCampaign($client_id){
  $query_model="SELECT id,name FROM campaign_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/campaign">'.$res['name'].'(Universal)</option>';
    }
  }
}
function getHolidayCampaign($client_id){
  $query_model="SELECT id,name FROM holiday_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/holiday">'.$res['name'].'(Holiday)</option>';
    }
  }
}
function getDatedCampaign($client_id){
  $query_model="SELECT id,name FROM dated_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/dated">'.$res['name'].'(Dated)</option>';
    }
  }
}

function get_list_campaign(){
  $token=$_SESSION['user_token'];
  $query_model="SELECT * FROM token WHERE token='".$token."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    $array=mysql_fetch_assoc($query);
    $client_id=$array['client_id'];
    getUniversalCampaign($client_id);
    getDatedCampaign($client_id);
    //getHolidayCampaign($client_id);
  }
  $include_function=getIncludeFunction($client_id);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style type="text/css">
  input[type='number'] {
    -moz-appearance: textfield;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  .input-group-addon{
    color:inherit!important;
    background-color: inherit!important;
  }
</style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>

    <!-- Main content -->
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Add New Contact</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <strong>Add New Contact</strong>
                  <a href="#" style="float:right;outline: none;" data-toggle="modal" data-target="#">Click Here for Video Tutorial</a>
                </div>
                <div class="card-block">
                  <div class="form-group has-feedback">
                    <label for="select">Assigned Campaign</label>
                    <select id="select" name="select" class="main_array form-control">
                      <?php get_list_campaign(); ?>
                    </select>
                  </div>
                  <div class="form_input" id="sortable">
                    <div class="form-group">
                      <label>First name</label>
                      <input id="first_name" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data checkempty main_array form-control"  placeholder="Enter First Name">
                    </div>

                    <div class="form-group">
                      <label>Last Name</label>
                      <input id="last_name" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data checkempty main_array form-control"  placeholder="Enter Last Name">
                    </div>

                    <div class="form-group mobile_phone">
                      <label>Email</label>
                      <input onkeypress="change_input_data(this)"  data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control"  placeholder="Enter Email ">
                    </div>

                    <div class="form-group">
                      <label>Mobile Phone</label>
                      <!--<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="main_array input_data form-control"  placeholder="Enter Mobile phone" id="add_mobile_phone">-->
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">+</span>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="checkempty checkMobile main_array input_data form-control"  placeholder="Enter Mobile phone" id="add_mobile_phone" aria-describedby="basic-addon1">
                      </div>
                    </div>

                    <div class="form-group">
                      <label>Alternative Phone</label>
                      <!--<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="main_array input_data form-control"  placeholder="Enter Alternative Phone" id="add_alternative_phone">-->
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">+</span>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="checkMobile main_array input_data form-control"  placeholder="Enter Alternative Phone" id="add_alternative_phone" aria-describedby="basic-addon1">
                      </div>
                    </div>

                    <div class="form-group">
                      <label>Address</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control"  placeholder="Enter Address">
                    </div>
                    <div class="form-group">
                      <label>Address1</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control" placeholder="Enter Address1">
                    </div>
                    <div class="form-group">
                      <label>Country</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class=" checkempty countryData form-control" placeholder="Enter Country">
                    </div>
                    <div class="form-group">
                      <label>City</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control" placeholder="Enter City">
                    </div>
                    <div class="form-group">
                      <label>State</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control" placeholder="State State">
                    </div>
                    <div class="form-group">
                      <label>Postal Code</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile checkempty main_array input_data form-control" placeholder="Postal Code" id="add_postal_code">
                    </div>
                    <div class="form-group">
                      <label>Date of Birth</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control" placeholder="Enter Date of Birth" id="date_birth">
                    </div>
                    <div class="form-group">
                      <label>Religion</label>
                      <select id="select2" name="select2" class="main_array form-control">
                        <option value="Christianity">Christianity </option>
                        <option value="Buddhism">Buddhism</option>
                        <option value="Islam">Islam</option>
                        <option value="Judaism">Judaism</option>
                      </select>
                    </div>
                    <div class="form-group has-feedback">
                      <label for="select">Type:</label>
                      <select id="select_customer" name="select" class="main_array form-control">
                        <option value="Customer">Customer</option>
                        <option value="Prospect">Prospect</option>
                      </select><br>
                      <label for="select">Status:</label>
                      <select id="select_active" name="select" class="main_array form-control">
                        <option value="Active">Active</option>
                        <option value="Inactive">Inactive</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Spouse First Name</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter First Name">
                    </div>
                    <div class="form-group">
                      <label>Spouse Last Name</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group">
                      <label>Spouse Email</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                      <label>Spouse Mobile</label>
                      <!-- <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="main_array input_data form-control" placeholder="Enter Mobile" id="spouse_mobile_phone">-->

                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">+</span>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="checkMobile main_array input_data form-control" placeholder="Enter Mobile" id="spouse_mobile_phone" aria-describedby="basic-addon1">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Spouse Alternative</label>
                      <!--<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="main_array input_data form-control" placeholder="Enter Alternative" id="spouse_alternative_phone">-->
                      <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">+</span>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="checkMobile main_array input_data form-control" placeholder="Enter Alternative" id="spouse_alternative_phone" aria-describedby="basic-addon1">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Spouse Date of Birth</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Date of Birth" id="date_birth_sprouse">
                    </div>
                    <div class="form-group">

                      <label>Religion</label>
                      <select id="select2" name="select2" class="main_array form-control">
                        <option value="Christianity">Christianity </option>
                        <option value="Buddhism">Buddhism</option>
                        <option value="Islam">Islam</option>
                        <option value="Judaism">Judaism</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Child Name</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Child Name">
                    </div>
                    <div class="form-group">
                      <label>Child DOB</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Child DOB" id="date_child_birth">
                    </div>
                    <div class="form-group">
                      <label>Product/Service Purchased</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Product/Service Purchased">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="row" style="background-color: #e1e6ef;">
                        <div class="form-group col-sm-6">
                          <input style="margin-top: 20px" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="new_input_name form-control"  placeholder="Enter custom name">
                          <label class="checkbox-inline" for="inline-checkbox1">
                            <input style="margin-top: 10px; margin-right: 5px;" type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="">Date field
                          </label>

                        </div>
                        <div class="form-group col-sm-6">
                          <button style="margin-top: 20px" onclick="add_new_input(this)" class="btn btn-primary">Add Custom Field</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                  <!--<div class="form-group">
                    <form id="form1" style="display: none;"><br>
                    <input   data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" placeholder="Field Title" ><br>
                    <input  data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" placeholder="Enter text"><br>
                    <label class="checkbox-inline" for="inline-checkbox1">
                    <input  onclick="disp(document.getElementById('select_date'))" type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="">Date field
                    </label>
                    <input id="select_date" name="select_date" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" placeholder="Select date" style="display: none;"><br>
                    </form>
                    <button onclick="disp(document.getElementById('form1'))" class="btn btn-primary text-center">Add Custom Field</button>
                  </div>-->
                  <div class="row" style="margin-bottom: 15px">
                    <div class="col-sm-12 text-center">
                      <button onclick="add_new_contact()" style="padding: 0.5rem 2rem;" type="submit" class="save_contact btn btn-primary"> Save </button>
                      <button style="margin-left: 30px;display: none;padding: 0.5rem 2rem;" type="button" class="btn danger_btn btn-outline-danger">Error</button>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
    <div class="modal fade" id="ClickVideoAddNewContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-primary" >
        <div class="modal-video">
          <video width="500" height="300" controls="controls">
           <source src="../video/video1.ogv" type='video/ogg; codecs="theora, vorbis"'>
             <source src="../video/video1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
               <source src="../video/video1.webm" type='video/webm; codecs="vp8, vorbis"'>

               </video>
             </div>
           </div>
         </div>
       </div>
       <!-- Bootstrap and necessary plugins -->
       <script src="js/libs/jquery.min.js"></script>
       <script src="js/libs/tether.min.js"></script>
       <script src="js/libs/bootstrap.min.js"></script>
       <script src="js/libs/pace.min.js"></script>
       <script src="js/app.js"></script>
       <script src="ckeditor/ckeditor.js"></script>
       <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
       <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
       <script>
        $( function() {
          $( "#date_birth" ).datepicker({ dateFormat: 'mm/dd/yy' });
        });
        $( function() {
          $( "#date_birth_sprouse" ).datepicker({ dateFormat: 'mm/dd/yy' });
        } );
        $( function() {
          $( "#date_child_birth" ).datepicker({ dateFormat: 'mm/dd/yy' });
        } );
      </script>
      <script type="text/javascript">
        $('body').on('hidden.bs.modal', '.modal', function () {
          $('video').trigger('pause');
        });
      </script>
      <script>
        function disp(input) {
          if (form.style.display == "none") {
            form.style.display = "block";
          } else {
            form.style.display = "none";
          }
        }
      </script>
      <script>
        var data_array_1=[];
        var data_array_2=[];

        function add_new_contact(){
          try{
            //check_input_data();
            build_array_1();
            build_array_2();
            var country=$(".countryData").val();
            data_array_1.push(country);
            $('.save_contact').attr("disabled","true");
            send_ajax(data_array_1,data_array_2);
          }catch(e){
            console.log(e);
          }
        }

        function check_input_data(){
          $('.checkempty').each(function(){
            if($(this).val() == ''){
              var parent=$(this).parent();
              var label=parent.find('label');
              var destination = $(parent).offset().top;
              jQuery("html:not(:animated),body:not(:animated)").animate({
                scrollTop: destination-100
              }, 800);
              alert("Enter "+label.html());
              notification(this,'Empty Data');
              throw new SyntaxError("empty_data");
            }
          })
        }


        function notification(id,type_error){
          var parent=$(id).parent(".form-group");
          $(parent).addClass('has-danger');
          $(id).addClass('form-control-danger');
          $(id).attr("data-content",type_error);
          $('.danger_btn').css("display","inline-block");
          setTimeout(function(){
            $('.danger_btn').css("display","none");
          },1500);
        }
        function build_array_1(){
          data_array_1=[];
          $('.main_array').each(function(){
            data_array_1.push($(this).val());
          });
        }
        function build_array_2(){
          data_array_2=[];
          if($('.alt_input').val()!='' || $('.alt_input')!='undefined'){
            $('.alt_input').each(function(){
              var placeholder=$(this).attr("placeholder");
              data_array_2.push(placeholder,$(this).val());
            });
          }
        //alert(data_array_2);
      }

      function send_ajax(array1,array2)
      {
        $.ajax({
          url:"include/contacts/add_new_contact.php",
          data:{array1:array1,array2:array2},
          type:"POST",
          success:function(data)
          {
            $('.save_contact').removeAttr("disabled");
            console.log(data);
            parsing_data(data);
          }
        })
      }

      function parsing_data(arr){
        var json=JSON.parse(arr);
        if(json['success']==1){
          window.location = 'contacts.php';
        }else{
          $(".danger_btn").css('display','inline-block');
          setTimeout(function(){
            $(".danger_btn").css('display','none');
          },1000);
          alert(json['error']);
        }
      }

      function change_input_data(id){
        var parent=$(id).parent(".form-group");
        $(id).removeClass('form-control-danger');
        $(parent).removeClass('has-danger');
      }

      $(".checkMobile").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
             (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
             (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
             (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
             (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
               return;
             }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }
      });

    </script>
    <script>$("#inline-checkbox1").prop("checked")</script>
    <script>
      var necessarily_data_array=[];
      function del_input(id){
        var parent=$(id).parent();
        $(parent).remove();
      }

      function add_new_input(id){
        var out;
        var new_input_name;
        new_input_name=$('.new_input_name');
        if(new_input_name.val()=='')
        {
          notification(new_input_name,'Enter Name');
        } else if($("#inline-checkbox1").prop("checked"))
        {
          out=' <div class="form-group"><label>'+new_input_name.val()+'</label><button class="btn btn-outline-danger btn-sm" onclick="del_input(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button><input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="date" class="alt_input input_data form-control" placeholder="'+new_input_name.val()+'"></div>';
          $('.form_input').append(out);
          new_input_name.val('');

        }
        else{
          out=' <div class="form-group"><label>'+new_input_name.val()+'</label><button class="btn btn-outline-danger btn-sm" onclick="del_input(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button><input onkeypress="change_input_data(this)" type="text"  class="alt_input form-control" placeholder="'+new_input_name.val()+'"></div>';
          $('.form_input').append(out);
          new_input_name.val('');
        }
      }
    </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $(function() {
        $("#sortable").sortable();
        $("#sortable").disableSelection();
      } );
    </script>
    <script src="js/libs/toastr.js"></script>
    <script src="components/user_notifications.js"></script>

  </body>

  </html>
