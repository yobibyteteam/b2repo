<?php
include '../connect_to_bd.php';
require_once '../classes/Quick_appointment.php';
$id = $_POST['curr_id_appoint'];
session_start();
$appointment=new Quick_appointment();


try{
	$appointment->delete_appointment($id);
	$text="User delete appointment";
	$appointment->add_to_activity_log($text);
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>