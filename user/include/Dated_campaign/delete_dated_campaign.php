<?php
session_start();
include '../connect_to_bd.php';
require_once '../classes/Dated_campaign.php';
$id=$_POST['id'];

$dated_campaign=new Dated_campaign();

try{
	$text="User delete dated campaign";
	$dated_campaign->add_to_activity_log($text);
	$dated_campaign->delete_campaign($id);
	echo json_encode(array("success"=>"1"));
}
catch(Exception $e)
{
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}
?>