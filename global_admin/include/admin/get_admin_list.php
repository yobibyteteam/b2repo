<?php 
include '../connect_to_bd.php';
require_once '../class/Admin.php';

$admin=new Administrator();

try{
	$admin->getAdminList();
	echo json_encode(array("success"=>"1","num"=>$admin->countAdmin,"data"=>$admin->adminArrayList));
}
catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}



?>