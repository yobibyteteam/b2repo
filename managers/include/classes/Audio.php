<?php

/**
* 
*/
class Audio
{
    var $uploadDirectory, $admin_id , $admin_dir;


    function upload($audio)
    {
        move_uploaded_file($audio, $this->uploadDirectory);
    }

    function get_id_client($token)
    {
        $query_model="SELECT * FROM token WHERE token='".$token."'";
        $query=mysql_query($query_model);
        $array_token=mysql_fetch_assoc($query);
        $this->admin_id=$array_token['client_id'];
    }

    function get_dir_client($filename)
    {
        $query_model="SELECT * FROM user WHERE id='".$this->admin_id."'";
        $query=mysql_query($query_model);
        $array_admin=mysql_fetch_assoc($query);
        $this->admin_dir=$array_admin['md5'];
        $this->uploadDirectory = '../../uploads_audio/'.$this->admin_dir.'/'.$filename.'.wav';
        $this->directoryView='../uploads_audio/'.$this->admin_dir.'/'.$filename.'.wav';

    }

    function insert_into_db($id,$filename,$bd,$delay_day_rvmd,$clientId,$repeat_rvmd)
    {
        $filename=$filename.".wav";
        if($bd=='campaign_builder_rvmd'){
            $delay_day_rvmd2=date( "Y-m-d", strtotime( "+".$delay_day_rvmd." day" ));
            $query_model="INSERT INTO ".$bd."(`campaign_builder_id`,`filename`,`delay_day`,`type`,`client_id`,`repeat_year`) VALUES('".$id."','".$this->uploadDirectory."','".$delay_day_rvmd2."','user','".$clientId."','".$repeat_rvmd."')";
        }else{
            $query_model="INSERT INTO ".$bd."(`campaign_builder_id`,`filename`,`delay_day`,`type`,`client_id`,`repeat_year`) VALUES('".$id."','".$this->uploadDirectory."','".$delay_day_rvmd."','user','".$clientId."','".$repeat_rvmd."')";
        }
        mysql_query($query_model);
    }

    function insertToBdWithoutDelay($id,$filename)
    {
        $filename=$filename.".wav";
        $query_model="INSERT INTO `broadcast_rvmd`(`campaign_builder_id`, `filename`) VALUES ('".$id."','".$filename."')";
        mysql_query($query_model);
    }

    function delete_audio($id,$dir,$bd)
    {
        $query_model="SELECT * FROM campaign_builder WHERE id='".$id."'";
        $query=mysql_query($query_model);
        $array_campaign=mysql_fetch_assoc($query);
        if($array_campaign['type']=='user'){
            //unlink($dir);
        }
        $query_model="DELETE FROM ".$bd." WHERE campaign_builder_id='".$id."'";
        mysql_query($query_model);
    }

    function update_audio($dir,$id,$delay_day_rvmd,$clientId,$repeat_rvmd)
    {
        $delay_day_rvmd=date( "Y-m-d", strtotime( "+".$delay_day_rvmd." day" ));
        $query_model="SELECT * FROM campaign_builder_rvmd WHERE campaign_builder_id='".$id."'";
        $query=mysql_query($query_model);
        if(mysql_num_rows($query)==0){
            $query_model="INSERT INTO campaign_builder_rvmd(`filename`,`campaign_builder_id`,`delay_day`,`type`,`client_id`,`repeat_year`) VALUES('".$dir."','".$id."','".$delay_day_rvmd."','user','$clientId','".$repeat_rvmd."')";
            mysql_query($query_model);
        }else{
            $query_model="UPDATE campaign_builder_rvmd SET filename='".$dir."',`delay_day`='".$delay_day_rvmd."',`repeat_year`='".$repeat_rvmd."' WHERE campaign_builder_id='".$id."'";
            mysql_query($query_model);
        }
    }

    function update_dated_audio($dir,$id,$delay_day_rvmd,$clientId,$clientId,$repeat_rvmd)
    {
        $query_model="SELECT * FROM dated_builder_rvmd WHERE campaign_builder_id='".$id."'";
        $query=mysql_query($query_model);
        if(mysql_num_rows($query)==0){
            $query_model="INSERT INTO dated_builder_rvmd(`filename`,`campaign_builder_id`,`delay_day`,`type`,`client_id`,`repeat_year`) VALUES('".$dir."','".$id."','".$delay_day_rvmd."','user','".$clientId."','".$repeat_rvmd."')";
            mysql_query($query_model);
        }else{
            $query_model="UPDATE dated_builder_rvmd SET filename='".$dir."',`delay_day`='".$delay_day_rvmd."' WHERE campaign_builder_id='".$id."'";
            mysql_query($query_model);
        }
    } 

    static public function getUserId($token){
        $queryModel="SELECT client_id FROM token WHERE token = '".$token."'";
        $queryToTokenBD = mysql_query($queryModel);
        $arrayToken = mysql_fetch_assoc($queryToTokenBD);
        return $arrayToken['client_id'];
    }
}
?>