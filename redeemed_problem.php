<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
</head>
<body>
  <div class="card-block text-center">
    <img src="img/problem.jpg" style="" class="img"></img>
    <h6>
    The problem of use. <br> You redeemed this coupon!</h6>
  </div>


  <!-- Bootstrap and necessary plugins -->
  <script src="js/libs/jquery.min.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="js/libs/pace.min.js"></script>
  <script src="js/views/charts.js"></script>
  <!-- Plugins and scripts required by all views -->
  <script src="js/libs/Chart.min.js"></script>
  <!-- GenesisUI main scripts -->
  <script src="js/app.js"></script>
  <!-- Plugins and scripts required by this views -->
  <!-- Custom scripts required by this view -->
  <script src="js/libs/toastr.js"></script>
  <script src="components/user_notifications.js"></script>
</body>
</html>
