<?php
$token=$_POST['token'];
session_start();
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';

$holiday_campaign=new Holiday_campaign();

try{
	$clientId=Holiday_campaign::getClientId($_SESSION['user_token']);
	$dir=Holiday_campaign::get_dir_admin($clientId);
	$holiday_campaign->get_campaign_id_token($token);
	$holiday_campaign->get_email_data();
	$holiday_campaign->get_text_data();
	$holiday_campaign->get_postcard_data($dir);
	$holiday_campaign->get_setting_data($token);
	echo json_encode(array("success"=>1,"email"=>$holiday_campaign->email_data,"text"=>$holiday_campaign->text_data,"postcard"=>$holiday_campaign->postcard,"name"=>$holiday_campaign->name,"date"=>$holiday_campaign->date));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>