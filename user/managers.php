<?php
include "include/session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'components/header.php'; ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="user-profile.php">User Profile</a></li>
                <li class="breadcrumb-item active">Managers</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Add Managers</strong>
                                    <small>new</small>
                                </div>
                                <div class="card-block">
                                    <div class="form-group username_val has-feedback">
                                        <label class="form-form-control-label">Manager Email</label>
                                        <input placeholder="Enter Manager Email" type="text" class="form-control">
                                    </div>
                                    <div class="form-group username_val has-feedback">
                                        <label class="form-form-control-label">Manager Name</label>
                                        <input placeholder="Enter Manager Name" type="text" class="form-control">
                                    </div>
                                    <div class="form-group username_val has-feedback">
                                        <label class="form-form-control-label">Manager Passowrd</label>
                                        <input placeholder="Enter Manager Password" type="text" class="form-control">
                                    </div>
                                    <button class="btn btn-primary">Add Manager</button>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered datatable table-responsive">
                                <thead>
                                    <tr>
                                        <th>Email</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>user</td>
                                        <td>Pablo</td>
                                        <td class="table_button" style="width:60px!important;">
                                            <button type="button" class="btn btn-danger btn_user_table_danger" data-toggle="modal" data-target="#dangerModal">
                                                <i class="fa fa-trash-o "></i>
                                            </button>
                                        </td>
                                    </tr>                         
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <!-- Bootstrap and necessary plugins -->
    <script src="js/libs/jquery.min.js"></script>
    <script src="js/libs/tether.min.js"></script>
    <script src="js/libs/bootstrap.min.js"></script>
    <script src="js/libs/pace.min.js"></script>
    <script src="js/libs/Chart.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="js/dashboard.js"></script>
    <script src="js/views/charts.js"></script>
    <script src="js/views/widgets.js"></script>
    <script src="js/libs/toastr.js"></script>
    <script src="components/user_notifications.js"></script>
</body>

</html>