<?php
include "include/session.php";
include 'include/connect_to_bd.php';

$id=$_GET['token'];
$if=mysql_real_escape_string($id);
$query=mysql_query("SELECT * FROM review_builder WHERE md5='$id'");
$array=mysql_fetch_assoc($query);
$code[]="<head>";
$code[]="<link href='http://crm.yobibyte.in.ua/css/style.css' rel='stylesheet'>";
$code[]="<link href='http://crm.yobibyte.in.ua/css/rating.css' rel='stylesheet'>";
$code[]="<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>";
$code[]="</head>";
$code[]="<body>";
$code[]="<div class='container-fluid'>";
$code[]="<div id='".$array['md5']."' class='animated fadeIn form_question'></div>";
$code[]="</div>";
$code[]="<script src='https://code.jquery.com/jquery-3.2.1.min.js'
integrity='sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=''
crossorigin='anonymous'></script>";
$code[]="<script src='http://crm.yobibyte.in.ua/form/build_form.js'></script>";
$code[]="</body>";
$i=0;
foreach ($code as $key) {
  $code[$i]=htmlspecialchars($code[$i]);
  $i++;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  input[type='number'] {
    -moz-appearance: textfield;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  .starclick:before{
    color:#e3cf7a!important;
    content: "\f005"!important;
  }
  body{
    position: relative;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }
  .rating {
    unicode-bidi: bidi-override;
    direction: rtl;
    font-size: 30px;
  }
  .rating span.star,
  .rating span.star {
    font-family: FontAwesome;
    font-weight: normal;
    font-style: normal;
    display: inline-block;
  }
  .rating span.star:hover,
  .rating span.star:hover {
    cursor: pointer;
  }
  .rating span.star:before,
  .rating span.star:before {
    content: "\f006";
    padding-right: 5px;
    color: #999999;
  }
  .rating span.star:hover:before,
  .rating span.star:hover:before,
  .rating span.star:hover ~ span.star:before,
  .rating span.star:hover ~ span.star:before {
    content: "\f005";
    color: #e3cf7a;
  }
  .card{
    display:block;
  }
</style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <!-- Main content -->
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="review-builder.php">Review Builder</a></li>
        <li class="breadcrumb-item active">Overview Review</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn form_question">
          <?=$array['form']; ?>
        </div>
      </div>
    </main>
  </div>
  <div id="parent">
    <div id="child"></div>
  </div>
  <!-- Bootstrap and necessary plugins -->
  <script src="js/libs/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="js/libs/pace.min.js"></script>
  <!-- Plugins and scripts required by all views -->
  <script src="js/libs/Chart.min.js"></script>
  <!-- GenesisUI main scripts -->
  <script src="js/app.js"></script>
  <!-- Plugins and scripts required by this views -->
  <script src="../js/libs/toastr.min.js"></script>
  <script src="js/libs/gauge.min.js"></script>
  <script src="js/libs/moment.min.js"></script>
  <script src="js/libs/daterangepicker.js"></script>
  <!-- Custom scripts required by this view -->
  <script src="js/views/main.js"></script>
  <script>
    var star_val;
    var json;
    var token=$('.card').attr('id');
    $( function() {
      $( "#review_birthday_mounth" ).datepicker();
    } );
    //var out="<p><button class='copy_code btn btn-success add_item'>Copy Form</button></p>";
    //$('.btn_save_form').append(out);
    $('.copy_code').click(function(){
      $('.sk-circle').css('display','block');
      $('#copy_code').modal();
      $('.sk-circle').css('display','none');
    })
    //var out="<p><button class='btn btn-success' onclick='send()'>Send</button></p>";
    //$('.btn_section').append(out);

    function modal_friend(){
      $('.sk-circle').css('display','block');
      $('#invite_friend').modal();
      $('.sk-circle').css('display','none');
    }

    function inner_html(array){
      out='<br>';
      out+='<div class="col-md-12 text-center 1">';
      out+='<p><b>Thank You</b></p>';
      out+='</div>';
      out+='<div class="col-md-12 text-center">';
      if(array['facebook_link']!=''){
        //out+='<button type="button" class="btn btn-md fa fa-facebook text" style="margin-right: 10px;color:#3b5998"></button>';
        out+='<a style="margin-right: 10px;color:#3b5998" href="'+array['facebook_link']+'">Facebook</a>';
      }
      if(array['google_link']!=''){
        //out+='<button type="button" class="btn btn-md fa fa-google-plus text" style="margin-right: 10px;color:#bb4b39"></button>';
        out+='<a style="margin-right: 10px;color:#bb4b39" href="'+array['google_link']+'">Google+</a>';
      }
      if(array['yelp_link']!=''){
        //out+='<button type="button" class="btn btn-md fa fa-yelp text" style="margin-right: 10px;color:#cb2027"></button>';
        out+='<a style="margin-right: 10px;color:#cb2027" href="'+array['yelp_link']+'">Yelp</a>';
      }
      out+='</div>';
      out+='<br><p><b>Your input means a lot for us</b></p>';
      $('.card').html(out).fadeIn("slow");
    }
    function check_textarea()
    {
      $('textarea').each(function(){
        if($(this).val()==''){
          error="Empty Data";
          throw new SyntaxError("Empty Data");
        }
      });
    }

    function check_input()
    {
      $('.input_data').each(function(){
        if($(this).val()==''){
          error="Empty Data";
          throw new SyntaxError("Empty Data");
        }
      });
    }

    function check_select()
    {
      $('#hear_about').each(function(){
        if($(this).val()=='0'){
          error="Choose Answer";
          throw new SyntaxError("Empty Data");
        }
      });
    }
    function check_input_data()
    {
      var input_count=$('body').find('textarea');
      var hear_about=$('body').find('select')
      if(input_count!='undefined')
      {
        check_input();
      }
      if(hear_about!='undefined')
      {
        check_select();
      }
    }
    function send(){
      try{
        check_input_data();
        token=$('.card').attr('id');
        var out;
        out='<br><div class="col-md-12 text-center">';
        out+='Please rate your experience, 5 being the best:<br>';
        out+='<span class="rating"><span onclick="star(5)" class="star star5"></span><span onclick="star(4)" class="star star4"></span><span onclick="star(3)" class="star star3"></span><span onclick="star(2)" class="star star2"></span><span onclick="star(1)" class="star star1"></span></span><br><br>';
        out+='</div>';
        $('.card').append(out).fadeIn("slow");
        $('.card').html('');
        $('.card').append(out);
      }catch(e){
        alert(error);
      }
    }
    function star(id){
      star_val=id;
      var j;
      for (var i = 0; i <= id; i++) {
        $('.star'+i).addClass('starclick');
      }
      j=5-id+1;
      if(j>0){
        for (var i = id+1; i <=5; i++) {
          $('.star'+i).removeClass('starclick');
        }
      }
      if(star_val>3){
        $.ajax({
          url:"include/get_review_data.php",
          data:{token:token},
          type:"POST",
          success:function(data){
            //alert(data);
            inner_html(JSON.parse(data));
          }
        })
      }else{
        out='<br><div class="col-md-12 text-center">';
        out+='Thanks for your feedback! Sorry we did not meet your 5 star expectations! We promise to make it right next time! A company manager may reach out to you about this experience.<br><br>';
        out+='</div>';
        $('.card').html('');
        $('.card').append(out);
      }
    }
    add_attr_br();

    function add_attr_br()
    {
      var html_card=$(".card2").html();
      $(".card2").html("");
      $(".card2").append("<br><br>");
      $(".card2").append(html_card);
    }
    add_important_question();
    function add_important_question()
    {
      var hear_about=$(".hear_about_quest").html();
      $(".hear_about_quest").html(hear_about+"(<span style='color:red;vertical-align:middle'>*</span>)");
      //alert(hear_about);
      var card=$(".card2").html();
      //console.log(card);
      $(".card2").html('');
      var out='<br><h5>Feedback & Reputation Request Tool <br> Please Enter The Customer or Client`s Information Below</h5><br>';
      out+='<div class="col-md-8" style="margin:0 auto;"><p class="holiday question1">Full Name?(<span style="color:red;vertical-align:middle">*</span>)</p></div>';
      out+='<div class="col-md-8" style="margin:0 auto;"><input name="textarea-input" rows="5" class="build_input input_data form-control" placeholder="Enter Full Name"></input></div><br>';
      out+='<div class="col-md-8" style="margin:0 auto;"><p class="holiday question2">Email?(<span style="color:red;vertical-align:middle">*</span>)</p></div>';
      out+='<div class="col-md-8" style="margin:0 auto;"><input name="textarea-input" rows="5" class="build_input input_data form-control" placeholder="Enter Email"></input></div><br>';
      out+='<div class="col-md-8" style="margin:0 auto;"><p class="holiday question3">Customer Mobile Phone?(<span style="color:red;vertical-align:middle">*</span>)</p></div>';
      out+='<div class="checkMobile col-md-8" style="margin:0 auto;"><input type="number" name="textarea-input" rows="5" class="build_input input_data form-control" placeholder="Enter Mobile Phone" id="overview_review_phone"></input></div>';
      $(".card2").html(out);
      $(".card2").append(card);
    }
  </script>
  <script type="text/javascript">
  $(".checkMobile").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl/cmd+A
          (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+C
          (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+X
          (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
  </script>
  <script src="js/libs/toastr.js"></script>
  <script src="components/user_notifications.js"></script>
</body>

</html>
