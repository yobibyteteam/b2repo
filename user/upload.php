<?php
include "include/session.php";
include 'include/connect_to_bd.php';
function get_list_campaign()
{
  $token=$_SESSION['user_token'];
  $query_model="SELECT * FROM token WHERE token='".$token."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0)
  {
    $array=mysql_fetch_assoc($query);
    $client_id=$array['client_id'];
    $query_model="SELECT id,name FROM campaign_builder WHERE client_id='".$client_id."'";
    $query=mysql_query($query_model);
    if(mysql_num_rows($query)!=0){
      while($res=mysql_fetch_assoc($query)){
        echo '<option value="'.$res['id'].'">'.$res['name'].'</option>';
    }
}
}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'components/header.php'; ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active">Upload</li>
            </ol>
            <br>
            <div class="container-fluid">
                <div class="animated fadeIn">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="card">

                        <div class="card-header">

                          <strong>Upload</strong>
                          <a href="#" style="float:right;outline: none;" data-toggle="modal" data-target="#">Click Here for Video Tutorial</a>

                      </div>

                      <div class="card-block" id="sortable">
                          <div class="form-group">
                              <label for="company">Download .csv file to add contacts.Open this Microsoft Exel . Insert the data, save it  and load to add it.</label><br>
                              <a download href="example.csv">Download Example</a>
                              <br><br>
                              <label for="company">WARNING: We do NOT tolerate SPAM. Please read and agree to conditions before proceeding.</label>

                              <div class="checkbox">
                                <label>
                                    <input onchange="agree_check(this)" type="checkbox" id="agree" name="agree" value="agree">
                                    <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms" style="outline: none;">Terms and Conditions</a>
                                </label>
                            </div>

                        </div>
                        <div class="form-group has-feedback">

                            <select id="select_campaign" name="select_campaign" class="select_campaign form-control">
                              <option value="0">Select Campaign
                              </option>
                              <?php get_list_campaign(); ?>
                          </select>

                      </div>
                      <p><input type="file" name="file_csv" id="file_csv" accept="text/csv,application/csv">
                        <a href="#" data-toggle="modal" data-target="#Instructions" style="outline: none;"><p>Instructions to rename fields</p></a>
                        <div class="form-group">
                            <label for="company">IMPORTANT NOTICE: When upload complete it will trigger day one messages. If you just want to upload create campaign with no messages.</label>
                        </div>
                        <br>
                        <div class="form-group text-center">
                            <button disabled onclick="upload_file()" class="start_upload btn btn-primary">Upload and Start Campaign</button>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
</div>
</div>
</main>
<div class="modal fade" id="ClickVideoUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-primary" >
        <div class="modal-video">
            <video width="500" height="300" controls="controls">
                <!--<source src="../video/video1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>-->

                </video>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="Terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Terms and Conditions</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Terms and Conditions</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="Instructions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-primary" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Instructions to rename fields</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Instructions to rename fields</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- Bootstrap and necessary plugins -->
    <script src="js/libs/jquery.min.js"></script>
    <script src="js/libs/tether.min.js"></script>
    <script src="js/libs/bootstrap.min.js"></script>
    <script src="js/libs/pace.min.js"></script>


    <!-- Plugins and scripts required by all views -->
    <script src="js/libs/Chart.min.js"></script>


    <!-- GenesisUI main scripts -->

    <script src="js/app.js"></script>





    <!-- Plugins and scripts required by this views -->
    <script src="../js/libs/toastr.min.js"></script>
    <script src="js/libs/gauge.min.js"></script>
    <script src="js/libs/moment.min.js"></script>
    <script src="js/libs/daterangepicker.js"></script>


    <!-- Custom scripts required by this view -->
    <script src="js/views/main.js"></script>
    <script type="text/javascript">
        $('body').on('hidden.bs.modal', '.modal', function () {
            $('video').trigger('pause');
        });
    </script>

    <script>
        var val=true;
        function upload_file()
        {

            var select_campaign=$('.select_campaign').val();
            var file1 = $('#file_csv');
            var formData = new FormData();
            if(select_campaign!='0'){
                formData.append('upload1', file1.prop('files')[0]);
                formData.append('select_campaign', select_campaign);
                $.ajax({
                  url: 'include/upload_contacts.php',
                  data: formData,
                  processData: false,
                  contentType: false,
                  type: 'POST',
                  success:function(data){
                    parsing(data);
                }
            })
            }else{
                alert("Select Campaign");
            }

        }
        function parsing(arr){
            var json=JSON.parse(arr);
            if(json['success']==1){
                window.location='contacts.php';
            }else{
                alert(data);
            }
        }

        function agree_check(id)
        {
            var prop=$(id).prop('checked');
            if(prop){
                $('.start_upload').removeAttr('disabled');
            }else{
                $('.start_upload').attr('disabled','true');
            }
        }

    </script>
    <script src="js/libs/toastr.js"></script>
    <script src="components/user_notifications.js"></script>
</body>

</html>
