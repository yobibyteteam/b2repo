<?php
include "include/session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Clever - Bootstrap 4 Admin Template">
	<meta name="author" content="Łukasz Holeczek">
	<meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
	<link rel="shortcut icon" href="img/favicon.png">
  <!-- Include Editor style. -->
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_style.min.css' rel='stylesheet' type='text/css' />
  <script src="//cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
  <!-- Include JS file. -->
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/js/froala_editor.min.js'></script>
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
</head>
<style>
</style>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
   <?php include "sidebar.php"; ?>
   <!-- Main content -->
   <main class="main">
    <ol class="breadcrumb mb-0">
      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
      <li class="breadcrumb-item active">Universal Campaign Builder</li>
    </ol>
    <div class="container-fluid">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-sm-12"><br>
            <div class="card">
              <div class="card-header">
                <strong>Import Pre Done Campaign and Edit?</strong>
              </div><br>
              <div class="form-group has-feedback">
                <div class="col-md-12">
                  <select id="select1" name="select1" class="form-control">
                    <option value="0">Dentist Contact
                    </option>
                    <option value="1">Option #1</option>
                    <option value="2">Option #2</option>
                    <option value="3">Option #3</option>
                  </select>
                </div>
              </div>
              <div class="card-header">
                <strong>Create New Campaign from Scratch</strong>
              </div><br>
              <div class="form-group has-feedback">
                <label class="col-md-3 form-control-label" for="text-input">Name Campaign:</label>
                <div class="col-md-12">
                  <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Create Name">

                </div>
              </div>
              <div class="form-group has-feedback">
                <label class="col-md-3 form-control-label" for="select2">Type of Campaign: </label>
                <div class="col-md-12">
                  <select id="select2" name="select2" class="form-control">
                    <option value="1">Prospect</option>
                    <option value="2">Customer</option>
                  </select>
                </div>
              </div>
              <div class="form-group has-feedback">
                <label class="col-md-3 form-control-label" for="select2">Assigned Number: </label>
                <div class="col-md-12">
                  <select id="select2" name="select2" class="form-control">
                    <option value="1">Please select</option>
                    <option value="2">Option#1</option>
                    <option value="3">Option#2</option>
                    <option value="4">Option#3</option>
                  </select>
                </div>
              </div>
              <div class="panel-group" id="accordion">
                <div class="col-md-12 py-4 text-center">
                  <button onclick="show_area()" class="btn btn-primary">Save and Start</button>

                  <div class="wrapper_area" style="display: none;">
                    <div class="col-md-12 py-4 text-left ">
                      <!-- HTML код управляемого элемента -->
                      <button class="add_email_on btn btn-primary" data-parent="#accordion" data-toggle="collapse" data-target="#add_email_off">Add Email +</button><br> 
                      <a href="#">View Current Emails</a>
                      <!-- HTML код сворачиваемого элемента -->

                      <div id="add_email_off" class="collapse in"> 

                        <form >
                          <textarea name="editor1" id="editor1" rows="10" cols="80">
                          </textarea>
                        </form>
                        <div class="col-md-12 py-4 text-center">
                          <button class="btn btn-primary" data-toggle="collapse" data-target="#add_email_off">Save </button>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-12 py-4 text-left ">  
                      <!-- HTML код управляемого элемента -->
                      <button class="add_rvmd_on btn btn-primary" data-parent="#accordion" data-toggle="collapse" data-target="#add_rvmd_off">Add Rvmd +</button><br>  
                      <a href="#">View Current RVMD</a>
                      <!-- HTML код сворачиваемого элемента -->
                      <div id="add_rvmd_off" class="collapse in">
                        <p> test</p>
                        <div class="col-md-12 py-4 text-center">
                          <button class="btn btn-primary" data-toggle="collapse" data-target="#add_rvmd_off">Save </button>
                        </div>
                      </div>
                    </div> 

                    <div class="col-md-12 py-4 text-left"> 
                      <!-- HTML код управляемого элемента -->
                      <button class="add_email_on btn btn-primary" data-toggle="collapse" data-target="#add_text_off">Add Text +</button><br> 
                      <a href="#">View Current Text</a>
                      <!-- HTML код сворачиваемого элемента -->
                      <div id="add_text_off" class="collapse in">
                        <form >
                          <textarea name="editor2" id="editor2" rows="10" cols="80">
                          </textarea>
                        </form>
                        <div class="col-md-12 py-4 text-center">
                          <button class="btn btn-primary" data-toggle="collapse" data-target="#add_text_off">Save </button>
                        </div>
                      </div>
                    </div> 

                    <div class="col-md-12 py-4 text-left">
                      <!-- HTML код управляемого элемента -->
                      <button class="add_postcard_on btn btn-primary" data-toggle="collapse" data-target="#add_postcard_off">Add Postcard +</button><br>  
                      <a href="#">View Current Postcards</a>
                      <!-- HTML код сворачиваемого элемента -->
                      <div id="add_postcard_off" class="collapse in">
                        <div class="card email-app">
                          <nav>
                            <p class="btn btn-primary btn-block">Name card</p>

                            <ul class="nav">
                             <li class="nav-item">
                              <a class="nav-link" href="#"><i class="fa fa-file"></i> Open </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#"><i class="fa fa-star"></i> Text</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#"><i class="fa fa-image"></i> Image</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="#"><i class="fa fa-trash"></i> Delete</a>
                            </li>
                          </ul>
                        </nav>

                        <main>

                          <div class="col-md-12">
                            <div class="card">
                              <div class="card-header">
                                <ul class="nav nav-tabs float-right" role="tablist">
                                  <li class="nav-item">
                                    <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab">Front</a>
                                  </li>
                                  <li class="nav-item">
                                    <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab">Back</a>
                                  </li>
                                </ul>
                              </div>
                              <div class="card-block p-0">
                                <div class="tab-content">
                                  <div class="tab-pane active" id="front">
                                    <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Create Name">
                                  </div>
                                  <div class="tab-pane" id="back">
                                    test2
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </main>
                      </div>
                      <div class="col-md-12 py-4 text-center">
                        <button class="btn btn-primary" data-toggle="collapse" data-target="#add_postcard_off">Save </button>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 py-4 text-center">
                    <button class="btn btn-primary" onclick="hide_area()">Save Campaign </button>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>





    </main>


  </div>


  <!-- Bootstrap and necessary plugins -->
  <script src="js/libs/jquery.min.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="js/libs/pace.min.js"></script>
  <script src="js/libs/ckeditor.js"></script>
  <script src="js/libs/Chart.min.js"></script>
  <script src="ckeditor/ckeditor.js"></script>
  <script src="js/app.js"></script>


  <script>CKEDITOR.replace('editor1');</script>
  <script>CKEDITOR.replace('editor2',
  {
    toolbar :
    [
    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
    { name: 'tools', items : [ 'Maximize','-','About' ] }
    ]
  });</script>



  <script>
    function show_area(){
      $('.wrapper_area').css('display','block');
    }
    function hide_area(){
      $('.wrapper_area').css('display','none');
    }
  </script>
  <script src="js/libs/toastr.js"></script>
  <script src="components/user_notifications.js"></script>

</body>

</html>


