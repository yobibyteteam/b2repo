<?php
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';
$id=$_POST['id_campaign'];
$name=$_POST['name'];
$date=$_POST['date'];

$holiday_campaign=new Holiday_campaign();

try{
	$holiday_campaign->get_name_campaign($id);
	$holiday_campaign->update_name_campaign($id,$name);
	$holiday_campaign->update_date_campaign($id,$date);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>