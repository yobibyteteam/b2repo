<?php 
session_start();
include '../connect_to_bd.php';
require_once '../classes/Dated_campaign.php';

$name_campaign = $_POST['name_campaign'];
$type_campaign=$_POST['type_campaign'];
$dated=$_POST['dated'];
$text="Administrator add new dated campaign";
$activity_reminder=$_POST['activity_reminder'];
$dated_campaign = new Dated_campaign();
$assignNum=$_POST['assignNum'];
try{
	$dated_campaign->set_data($name_campaign,$type_campaign,$dated,$activity_reminder);
	$dated_campaign->check_input_data();
	$dated_campaign->get_id_client();
	$dated_campaign->add_to_bd($assignNum);
	$dated_campaign->get_admin_name();
	$dated_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>"1","campaign_id"=>$dated_campaign->campaign_id));
}catch(Exception $e){
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}



?>