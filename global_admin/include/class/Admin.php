<?php
/**
* Class Registration
*/
class Administrator
{
	public $countAdmin,$adminArrayList;
	private $login , $name , $password , $email ,$passwordOriginal;

	public static function getAdminName($token)
	{
		$queryToTokenDB=mysql_query("SELECT client_id FROM token WHERE token='$token'");
		if(mysql_num_rows($queryToTokenDB)!=0){
			$arrayToken=mysql_fetch_assoc($queryToTokenDB);
			$adminId=$arrayToken['client_id'];
			$queryToAdminDB=mysql_query("SELECT name FROM admin WHERE id='$adminId'");
			if(mysql_num_rows($queryToAdminDB)!=0){
				$arrayAdmin=mysql_fetch_assoc($queryToAdminDB);
				return $arrayAdmin['name'];
			}
		}
	}
	public function setData($login,$password,$email,$name){
		$this->login = $login;
		$this->password = md5($password);
		$this->email = $email;
		$this->name = $name;
		$this->passwordOriginal=$password;
		htmlspecialchars($this->login);
		htmlspecialchars($this->email);
		htmlspecialchars($this->name);
		htmlspecialchars($this->passwordOriginal);
		strip_tags($this->login);
		strip_tags($this->email);
		strip_tags($this->name);
		strip_tags($this->passwordOriginal);
		Administrator::checkData();
	}

	private function checkData(){
		$queryToAdminDB=mysql_query("SELECT * FROM `admin` WHERE email = '$this->email'");
		if(mysql_num_rows($this->queryToAdminDB) != 0){
			throw new Exception("user_exist", 1);
		}
	}

	public function addNewAdmin(){
		$md5=md5(rand(5,15)).mt_rand();
		mysql_query("INSERT INTO `admin`(`username`, `password`, `email`,`name`,`type`,`dir`) VALUES ('$this->login','$this->password','$this->email','$this->name','global_admin','$md5')");
		mkdir('../../../uploads_audio/'.$md5, 0777, true);
		mkdir('../../../upload_photo/'.$md5, 0777, true);
		Administrator::sendEmailNotification();
	}
	private function sendEmailNotification(){
		mail($this->email, "Registration", "Hi there! \n \n  Thank you for registering with ContactTRACS. \n \n To verify your email please click the following link and login using your email as the username and the following password: (".$this->passwordOriginal.") \n \n http://contacttracssystem.com \n \n You will be able to change your password inside your profile once you have logged in. Be sure to bookmark this site, this is where you will go to login and access your system at anytime! \n \n Welcome! \n \n The ContactTRACS Team ","From:crmsystem@admin.com");
	}

	public function getAdminList(){
		$queryToAdminDB=mysql_query("SELECT * FROM admin WHERE type='global_admin' ORDER BY id DESC");
		if(mysql_num_rows($queryToAdminDB)==0){
			throw new Exception("empty_data", 1);
		}else{
			while($result=mysql_fetch_assoc($queryToAdminDB)){
				$this->adminArrayList[]=array('id'=>$result['id'],'username'=>$result['username'],'email'=>$result['email']);
			}
			$this->countAdmin=(int)mysql_num_rows($queryToAdminDB);
		}
	}

}


/**
* Delete Administrator
*/
class DeleteAdministrator extends Administrator
{
	public static function deleteAdmin($adminId){
		mysql_query("DELETE FROM `admin` WHERE id='$adminId'");
	}
}

?>