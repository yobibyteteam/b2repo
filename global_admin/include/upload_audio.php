<?php
require_once 'class/Audio.php';
include 'connect_to_bd.php';
session_start();
$token=$_SESSION['user_token'];
$file=$_FILES['upload'];
$id_campaign=$_POST['id'];
$bd=$_POST['bd'];
$audio=new Audio();
$file_name=$md5=md5(rand(5,10)).mt_rand();
$delay_day_rvmd=$_POST['delay_day_rvmd'];
$repeat_rvmd=$_POST['repeat_rvmd'];

try
{
	$audio->get_id_client($token);
	$audio->get_dir_client($file_name);
	$audio->upload($_FILES["upload"]["tmp_name"]);
	$audio->insert_into_db($id_campaign,$file_name,$bd,$delay_day_rvmd,$repeat_rvmd);
	$success=array("success"=>"1","dir"=>$audio->directoryView);
	echo str_replace('\/','/',json_encode($success));
}catch(Exception $e)
{
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}
?>