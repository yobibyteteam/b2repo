<?php 
session_start();
include '../connect_to_bd.php';
require_once '../class/Dated_campaign.php';

$name_campaign = $_POST['name_campaign'];
$type_campaign=$_POST['type_campaign'];
$dated=$_POST['dated_campaign'];
$text="Administrator add new dated campaign";

$dated_campaign = new Dated_campaign();

try{
	$dated_campaign->set_data($name_campaign,$type_campaign,$dated);
	$dated_campaign->check_input_data();
	$dated_campaign->get_id_client();
	$dated_campaign->add_to_bd();
	$dated_campaign->get_admin_name();
	$dated_campaign->add_to_activity_log($text);
	$dated_campaign->get_campaign_id();
	echo json_encode(array("success"=>"1","campaign_id"=>$dated_campaign->campaign_id));
}catch(Exception $e){
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}



?>