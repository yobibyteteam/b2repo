<?php
require_once 'fpdf.php';
/**
* 
*/
class Postcard
{
	static public function generateName(){
		return mt_rand();
	}

	static public function getUserId($token){
		$queryModel="SELECT client_id FROM token WHERE token='".$token."'";
		$query=mysql_query($queryModel);
		$arrayToken=mysql_fetch_assoc($query);
		return $arrayToken['client_id'];
	}
	static public function getCampaignId($token){
		$queryModel="SELECT id FROM campaign_builder WHERE md5='".$token."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['id'];
	}

	static public function getUserDir($clientId){
		$queryModel="SELECT dir_img FROM user WHERE id='".$clientId."'";
		$query=mysql_query($queryModel);
		$arrayToken=mysql_fetch_assoc($query);
		return $arrayToken['dir_img'];
	}

	static public function addPostcardToBD($randomName,$typePostCard,$lastId){
		$randomName=$randomName.'.jpg';
		$queryModel="UPDATE `postcard` SET filename='".$randomName."' WHERE type_postcard='".$type_postcard."' AND id='".$lastId."'";
		echo $queryModel;
		//mysql_query($queryModel);
	}

	static public function deleteFile($dir,$filename){
		$str=(string)"../../../upload_photo/".(string)$dir."/".(string)$filename;
		unlink((string)$str);
	}

	static public function addToDB($newFileName,$campaignName,$clientId,$typeCampaign,$typePostCard){
		$newFileName=$newFileName.'.jpg';
		$queryModel="SELECT id FROM postcard WHERE campaign_builder_id='".$campaignName."' AND type_postcard='".$typePostCard."'";
		$query1=mysql_query($queryModel);
		if(mysql_num_rows($query1)==0){
			$queryModel="INSERT INTO `postcard`(`campaign_builder_id`, `type`, `client_id`, `type_campaign`,`type_postcard`,`filename`) VALUES ('".$campaignName."','user','".$clientId."','".$typeCampaign."','".$typePostCard."','".$newFileName."')";
			mysql_query($queryModel);
		}else{
			$userDir=Postcard::getUserDir($clientId);
			//Postcard::deleteFile($userDir,$newFileName);
			$arrayData=mysql_fetch_assoc($query1);
			$idPostcard=$arrayData['id'];
			$queryModel="UPDATE `postcard` SET `filename`='".$newFileName."' WHERE id='".$idPostcard."'";
			mysql_query($queryModel);
		}
	}

	static public function moveFileToDir($fileImg,$randomName,$clientDir){
		$newFileName=$randomName.'.jpg';
		$uploadDirectory='../../../upload_photo/'.$clientDir.'/'.$randomName.'.jpg';
		$pdfLink='../../../upload_photo/'.$clientDir.'/'.$randomName.'.pdf';
		copy($fileImg, $uploadDirectory);
		$pdf = new FPDF('L','pt','a5');
		$pdf->open();
		$pdf->AddPage();
		$pdf->Image($uploadDirectory,0,0);
		$pdf->Output($pdfLink,'F');
	}
	static public function getAllPhoto($id,$dir){
		$queryModel="SELECT filename FROM postcard WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayData[]='../upload_photo/'.$dir.'/'.$res['filename'];
			}
		}else{
			$arrayData=array();
		}
		return $arrayData;
	}
	static public function updateUniversalPostcardData($repeatYear,$delayDay,$idCampaign,$typeCampaign,$clinentId){
		$delayDay=date( "Y-m-d", strtotime( "+".$delayDay." day" ));
		$queryModel="UPDATE postcard SET repeat_year='".$repeatYear."',delay_day_date='".$delayDay."' WHERE campaign_builder_id='".$idCampaign."' AND type_campaign='".$typeCampaign."' AND client_id='".$clinentId."'";
		mysql_query($queryModel);
	}
	static public function updateDatedPostcardData($repeatYear,$delayDay,$idCampaign,$typeCampaign,$clinentId){
		//$delayDay=date( "Y-m-d", strtotime( "+".$delayDay." day" ));
		$queryModel="UPDATE postcard SET repeat_year='".$repeatYear."',delay_day_value='".$delayDay."' WHERE campaign_builder_id='".$idCampaign."' AND type_campaign='".$typeCampaign."' AND client_id='".$clinentId."'";
		mysql_query($queryModel);
	}
	static public function updateholidayPostcardData($repeatYear,$delayDay,$idCampaign,$typeCampaign,$clinentId){
		//$delayDay=date( "Y-m-d", strtotime( "+".$delayDay." day" ));
		$queryModel="UPDATE postcard SET repeat_year='".$repeatYear."',delay_day_value='".$delayDay."' WHERE campaign_builder_id='".$idCampaign."' AND type_campaign='".$typeCampaign."' AND client_id='".$clinentId."'";
		mysql_query($queryModel);
	}
}



?>