<?php


class LeadsRain{
	
	function __construct(){}

	function createCampaign($file){
		$ch = curl_init();
		$data = array('username' => 64225, 'api_key' => 'a44ace810b167b6dcd9e1a7f0304db39','campaign_name'=>mt_rand(),"sound_file_url"=>$file,"campaign_cid"=>mt_rand(),"local_call_time"=>"");

		curl_setopt($ch, CURLOPT_URL, "http://s2.leadsrain.com/rvm/api/campaign/add_api");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		$response = curl_exec($ch);
		$response=json_decode($response);
		//print_r($response);
		curl_close($ch);
		return $response->campaign_id;
		//return mt_rand();
	}

	function createList($campaignId){
		$ch = curl_init();
		$data = array('username' => 64225, 'api_key' => 'a44ace810b167b6dcd9e1a7f0304db39','campaign_id'=>$campaignId,"active"=>"Y","list_name"=>mt_rand());

		curl_setopt($ch, CURLOPT_URL, "http://s2.leadsrain.com/rvm/api/leadlist/add_api");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		$response = curl_exec($ch);
		$response=json_decode($response);
		curl_close($ch);
		//print_r($response);
		return $response->list_id;
		//return mt_rand();
	}

	function addContactToList($listId,$arrayContact){
		$ch = curl_init();
		$mobile="+".$arrayContact["phone_number"];
		$altPhone="+".$arrayContact["alt_phone"];
		$data = array("username" => 64225, "api_key" => "a44ace810b167b6dcd9e1a7f0304db39","list_id"=>$listId,"phone_number"=>$mobile,"title"=>"title","first_name"=>$arrayContact["first_name"],"middle_initial"=>"","last_name"=>$arrayContact["last_name"],"address1"=>$arrayContact["address1"],"address2"=>$arrayContact["address2"],"address3"=>"","city"=>$arrayContact["city"],"state"=>$arrayContact["state"],"province"=>"","postal_code"=>$arrayContact["postal_code"],"vendor_lead_code"=>mt_rand(),"phone_code"=>"","alt_phone"=>$altPhone,"security_phrase"=>mt_rand(),"email"=>$arrayContact["email"],"comments"=>"comments");
		;curl_setopt($ch, CURLOPT_URL, "http://s2.leadsrain.com/ringless/api/add_rvm_lead.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		$response = curl_exec($ch);
		$response=json_decode($response);
		if($response->lead_id==''){
			throw new Exception($response->message, 1);
		}
	}

	function deleteList($listId){
		$ch = curl_init();
		$data = array('username' => 64225, 'api_key' => 'a44ace810b167b6dcd9e1a7f0304db39','list_id'=>$listId);

		curl_setopt($ch, CURLOPT_URL, "http://s2.leadsrain.com/rvm/api/leadlist/delete_api");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		$response = curl_exec($ch);
		$response=json_decode($response);
		curl_close($ch);
	}

	function deleteCampaign($campaignId){
		$ch = curl_init();
		$data = array('username' => 64225, 'api_key' => 'a44ace810b167b6dcd9e1a7f0304db39','campaign_id'=>$campaignId);

		curl_setopt($ch, CURLOPT_URL, "http://s2.leadsrain.com/rvm/api/campaign/delete_api");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		$response = curl_exec($ch);
		$response=json_decode($response);
		curl_close($ch);
	}
}

?>