<?php
include "include/session.php";
include 'include/connect_to_bd.php';

function get_list_campaign(){
    $query=mysql_query("SELECT * FROM campaign_builder WHERE type='global_admin'");
    while ($res=mysql_fetch_assoc($query)) {
        echo '<tr>
        <td>'.$res['name'].'</td>
        <td>'.$res['campaign_type'].'</td>

        <td class="table_button" style="width:164px!important;">
            <a onclick=modal_show('.$res["id"].') class="btn btn-danger btn_table" href="#">
                <i class="fa fa-trash-o "></i>
            </a>
            <a class="btn btn-info btn_table" href="overview-campaign.php?token='.$res['md5'].'">
                <i class="fa fa-edit "></i>
            </a>
        </td>
    </tr>';
}
}
            //<a class="btn btn-success btn_table" href="#" data-toggle="modal" data-target="#myModal" ">
       // <i class="fa fa-search-plus "></i>
    //</a>
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        body{
          position: relative;
      }
      .sk-circle{
          display: none;
          z-index: 10001;
          width: 40px;
          height: 40px;
          position: absolute;
          top: 0;
          bottom: 0;
          right: 0;
          left: 0;
          margin: auto;
      }
  </style>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'componets/header.php';  ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">       
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Universal Campaign Overview</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="card">
                    <div class="card-header">
                        Current Campaigns
                    </div>
                    <div class="card-block">
                        <table class="table table-striped table-bordered datatable table-responsive">
                            <thead>
                                <tr>
                                    <th>Name of Campaign:</th>
                                    <th>Type:</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php get_list_campaign(); ?>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-success" onclick="window.location='universal-campaign-builder.php'">Create New Campaign</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>
<div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this campaign?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" onclick="del_campaign()" data-dismiss="modal" class="btn btn-primary">Yes</button>
            </div>
        </div>

    </div>
</div>
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>List of contacts in Campaign</h4>
            </div>
            <div class="modal-body">
                <table>
                    <?php // get_list($client_id); ?>
                </table>

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal" >Close</button>
            </div>
        </div>
    </div>
</div>

</div>


<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!-- Plugins and scripts required by this views -->
<script src="../js/libs/toastr.min.js"></script>
<script src="js/libs/gauge.min.js"></script>
<script src="js/libs/moment.min.js"></script>
<script src="js/libs/daterangepicker.js"></script>
<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>
<script>
    var id_campaign;
    function modal_show(id)
    {
        id_campaign=id;
        $("#dangerModal").modal("show");
    }

    function del_campaign(){
        $('.sk-circle').css('display','block');
        var id=id_campaign;
        $.ajax({
            url:"include/Universal_campaign/delete_campaign.php",
            data:{id:id},
            type:"POST",
            success:function(){
                window.location="universal-campaign-overview.php";
            }
        })
        $('.sk-circle').css('display','none');
    }
</script>
</body>
</html>