<?php

/**
* Universal Campaign
*/
class Universal_campaign
{
	var $name, $type, $error, $session_token, $type_client, $id_client, $campaign_id,$time_current,$date_current,$admin_name, $id_campaign , $data_output ,$email_data,$text_data,$audio_data ,$campaign_name,$campaign_type,$last_query,$postcard,$admin_dir;


	function set_data(&$name_campaign,&$type_campaign)
	{
		$this->session_token = $_SESSION['user_token'];
		$this->name = $name_campaign;
		$this->type = $type_campaign;
		$this->name = htmlspecialchars($this->name);
		$this->type = htmlspecialchars($this->type);
		$this->name = mysql_real_escape_string($this->name);
		$this->type = mysql_real_escape_string($this->type);
	}

	function check_input_data()
	{
		$query = mysql_query("SELECT * FROM campaign_builder WHERE name = '$this->name' AND type='global_admin'");
		if(mysql_num_rows($query) != 0){
			$this->error = 'campaign_exist';
			throw new Exception("campaign_exist", 1);
		}
	}

	function get_id_client()
	{
		$query = mysql_query("SELECT * FROM token WHERE token = '$this->session_token'");
		if(mysql_num_rows($query) == 0){
			throw new Exception("not_fount_token", 1);
		}else{
			$array_token = mysql_fetch_assoc($query);
			$this->type_client=$array_token['type'];
			$this->id_client=$array_token['client_id'];
		}
	}

	function get_campaign_id()
	{
		$query=mysql_query("SELECT * FROM `campaign_builder` WHERE `name`='$this->name'");
		if(mysql_num_rows($query)!=0){
			$array_campaign=mysql_fetch_assoc($query);
			$this->campaign_id = $array_campaign['id'];
		}
	}

	static public function getClientId($token){
		$query_model="SELECT client_id FROM token WHERE token='".$token."'";
		$query=mysql_query($query_model);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['client_id'];
	}

	function add_to_bd()
	{
		$md5=md5(rand(5,15)).mt_rand();
		mysql_query("INSERT INTO `campaign_builder`(`client_id`, `type`, `campaign_type`, `name`,`md5`) VALUES ('$this->id_client','$this->type_client','$this->type','$this->name','$md5')");
		$this->last_query=mysql_insert_id();
	}

	function get_admin_name()
	{
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		if(mysql_num_rows($query)!=0){
			$array_token=mysql_fetch_assoc($query);
			$id_admin=$array_token['client_id'];
			$query=mysql_query("SELECT * FROM admin WHERE id='$id_admin'");
			if(mysql_num_rows($query)!=0){
				$array_admin=mysql_fetch_assoc($query);
				$this->admin_name=$array_admin['name'];
			}
		}
	}

	function add_to_activity_log($text)
	{
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		$array_token=mysql_fetch_assoc($query);
		$id_admin=$array_token['client_id'];
		$time_current=date("h:i:s");
		$date_current=date("Y-m-d");
		$query_model="INSERT INTO activity_log(`notification`,`date`,`time`,`user_id`,`type`) VALUES('".$text."','".$date_current."','".$time_current."','".$id_admin."','global_admin')";
		mysql_query($query_model);
	}

	function add_email($id,$text,$delay_day_email,$repeat_email,$clientId)
	{
		$delay_day_email=date( "Y-m-d", strtotime( "+".$delay_day_email." day" ));
		$query_model="SELECT * FROM campaign_builder_email WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			$query_model="INSERT INTO `campaign_builder_email`(`campaign_builder_id`,`text`,`delay_day`,`type`,`repeat_year`,`client_id`) VALUES('".$id."','".$text."','".$delay_day_email."','global_admin','".$repeat_email."','".$clientId."')";
			mysql_query($query_model);
		}else{
			$query_model="UPDATE campaign_builder_email SET `text`='".$text."',`delay_day`='".$delay_day_email."',`repeat_year`='".$repeat_email."',`client_id`='".$clientId."' WHERE campaign_builder_id='".$id."'";
			mysql_query($query_model);
		}
	}

	function add_text($id,$text,$delay_day_text,$repeat_text,$clientId)
	{
		$delay_day_text=date( "Y-m-d", strtotime( "+".$delay_day_text." day" ));
		$query_model="SELECT * FROM campaign_builder_text WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			$query_model="INSERT INTO `campaign_builder_text`(`campaign_builder_id`,`text`,`delay_day`,`type`,`repeat_year`,`client_id`) VALUES('".$id."','".$text."','".$delay_day_text."','global_admin','".$repeat_text."','".$clientId."')";
			mysql_query($query_model);
		}else{
			$query_model="UPDATE campaign_builder_text SET `text`='".$text."',`delay_day`='".$delay_day_text."',`repeat_year`='".$repeat_text."',`client_id`='".$clientId."' WHERE campaign_builder_id='".$id."'";
			mysql_query($query_model);
		}
	}

	function delete_campaign($id)
	{
		$query_model="DELETE FROM campaign_builder_email WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM campaign_builder_text WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM campaign_builder_rvmd WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM campaign_builder WHERE id='".$id."'";
		mysql_query($query_model);
	}

	function delete_audio($id)
	{
		$query_model="SELECT * FROM campaign_builder_rvmd WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		$audio_array=mysql_fetch_assoc($query);
		$filename='../'.$audio_array['filename'];
		//unlink($filename);
	}

	function get_campaign_id_token($token)
	{
		$query_model="SELECT * FROM campaign_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_token=mysql_fetch_assoc($query);
		$this->id_campaign=$array_token['id'];
	}

	function get_email_data()
	{
		$query_model="SELECT * FROM campaign_builder_email WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_email=mysql_fetch_assoc($query);
			if($array_email['text']!=''){
				$this->email_data=$array_email;
			}else{
				$this->email_data='empty';
			}
		}
	}

	function get_text_data()
	{
		$query_model="SELECT * FROM campaign_builder_text WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_text=mysql_fetch_assoc($query);
			if($array_text['text']!=''){
				$this->text_data=$array_text;
			}else{
				$this->text_data='empty';
			}
		}
	}

	static public function get_dir_admin($clientId){
		$query_model="SELECT dir FROM admin WHERE id='".$clientId."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData['dir'];
	}

	function get_audio_data()
	{
		$query_model="SELECT * FROM campaign_builder_rvmd WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_audio=mysql_fetch_assoc($query);
			if($array_audio['filename']!=''){
				$this->audio_data=$array_audio;
			}else{
				$this->audio_data='empty';
			}
		}
	}
	function get_postcard_data($dir)
	{
		$query_model="SELECT * FROM postcard WHERE campaign_builder_id='".$this->id_campaign."' AND type='global_admin'  AND type_campaign='universal'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$this->postcard[]=$res;
				$this->postcard['dir']=$dir;
			}
		}
	}

	function update_text_data($id,$text,$delay_day_text)
	{

		$query_model="UPDATE `campaign_builder_text` SET `text`='".$text."',`delay_day`='".$delay_day_text."' WHERE `campaign_builder_id`='".$id."'";
		mysql_query($query_model);
	}

	function update_email_data($id,$text,$delay_day_email)
	{
		$query_model="UPDATE `campaign_builder_email` SET `text`='".$text."',`delay_day`='".$delay_day_email."' WHERE `campaign_builder_id`='".$id."'";
		mysql_query($query_model);
	}

	function get_type_name($token)
	{
		$query_model="SELECT name,campaign_type FROM campaign_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_token=mysql_fetch_assoc($query);
		$this->campaign_name=$array_token['name'];
		$this->campaign_type=$array_token['campaign_type'];
	}

	function update_type_campaign($id,$type)
	{
		$query_model="UPDATE `campaign_builder` SET `campaign_type`='".$type."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function get_name_campaign($id){
		$query_model="SELECT name FROM campaign_builder WHERE id='".$id."'";
		$query=mysql_query($query_model);
		$array_campaign=mysql_fetch_assoc($query);
		$this->campaign_name=$array_campaign['name'];
	}

	function update_name_campaign($id,$name)
	{
		$query_model="SELECT * FROM campaign_builder WHERE name='".$name."'";
		$query=mysql_query($query_model);
		if($this->campaign_name!=$name){
			if(mysql_num_rows($query)!=0){
				throw new Exception("Name_exist", 1);
			}else{
				$query_model="UPDATE `campaign_builder` SET `name`='".$name."' WHERE `id`='".$id."'";
				mysql_query($query_model);
			}
		}
	}

	static public function getReadyEmail(){
		$queryToEmailBD=mysql_query("SELECT `text` FROM campaign_builder_email WHERE type='global_admin'");
		if(mysql_num_rows($queryToEmailBD)!=0){
			while ($res=mysql_fetch_assoc($queryToEmailBD)) {
				$arrayData[]=$res['text'];
			}
		}else{
			$arrayData=[];
		}
		return $arrayData;
	}

	static public function getReadyText(){
		$queryToTextBD=mysql_query("SELECT `text` FROM campaign_builder_text WHERE type='global_admin'");
		if(mysql_num_rows($queryToTextBD)!=0){
			while ($res=mysql_fetch_assoc($queryToTextBD)) {
				$arrayData[]=$res['text'];
			}
		}else{
			$arrayData=[];
		}
		return $arrayData;
	}

	static public function getReadyRVMD(){
		$queryToRVMDBD=mysql_query("SELECT `filename` FROM campaign_builder_rvmd WHERE type='global_admin'");
		if(mysql_num_rows($queryToRVMDBD)!=0){
			while ($res=mysql_fetch_assoc($queryToRVMDBD)) {
				$arrayData[]=$res['filename'];
			}
		}else{
			$arrayData=[];
		}
		return $arrayData;
	}


}



?>