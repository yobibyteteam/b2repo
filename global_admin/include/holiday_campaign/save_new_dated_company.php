<?php 
session_start();
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';

$name_campaign = $_POST['name_campaign'];
$date=$_POST['date'];
$text="Administrator add new holiday campaign";

$holiday_campaign = new Holiday_campaign();

try{
	$holiday_campaign->set_data($name_campaign,$date);
	$holiday_campaign->check_input_data();
	$holiday_campaign->get_id_client();
	$holiday_campaign->add_to_bd();
	$holiday_campaign->add_to_activity_log($text);
	$holiday_campaign->get_campaign_id();
	echo json_encode(array("success"=>"1","campaign_id"=>$holiday_campaign->campaign_id));
}catch(Exception $e){
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}



?>