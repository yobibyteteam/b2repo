<?php

/**
* 
*/
class Holiday_campaign
{
	var $name, $delay_day,$date,$session_token,$type_client,$id_client,$campaign_id,$id_campaign,$email_data,$text_data,$postcard,$admin_dir;

	function set_data(&$name_campaign,&$date)
	{
		$this->session_token = $_SESSION['user_token'];
		$this->name = $name_campaign;
		$this->date = $date;
		$this->date = htmlspecialchars($this->date);
		$this->name = htmlspecialchars($this->name);
		$this->date = mysql_real_escape_string($this->date);
		$this->name = mysql_real_escape_string($this->name);
	}

	function check_input_data(){
		$query_model="SELECT * FROM holiday_builder WHERE name = '".$this->name."'";
		$query = mysql_query($query_model);
		if(mysql_num_rows($query) != 0){
			throw new Exception("campaign_exist", 1);
		}
	}

	static public function getClientId($token){
		$query_model="SELECT client_id FROM token WHERE token='".$token."'";
		$query=mysql_query($query_model);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['client_id'];
	}

	function get_id_client(){
		$query_model="SELECT * FROM token WHERE token = '".$this->session_token."'";
		$query = mysql_query($query_model);
		if(mysql_num_rows($query) == 0){
			throw new Exception("not_fount_token", 1);
		}else{
			$array_token = mysql_fetch_assoc($query);
			$this->type_client=$array_token['type'];
			$this->id_client=$array_token['client_id'];
		}
	}
	function get_campaign_id(){
		$query_model="SELECT * FROM `holiday_builder` WHERE `name`='".$this->name."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_campaign=mysql_fetch_assoc($query);
			$this->campaign_id = $array_campaign['id'];
		}
	}

	function add_to_bd(){
		$md5=md5(rand(5,15)).mt_rand();
		$query_model="INSERT INTO `holiday_builder`(`client_id`, `type`, `name`,`date`,`md5`) VALUES ('".$this->id_client."','".$this->type_client."','".$this->name."','".$this->date."','$md5')";
		mysql_query($query_model);
	}

	function add_to_activity_log($text)
	{
		$token=$_SESSION['user_token'];
		$query=mysql_query("SELECT * FROM token WHERE token='$token'");
		$array_token=mysql_fetch_assoc($query);
		$id_admin=$array_token['client_id'];
		$time_current=date("h:i:s");
		$date_current=date("Y-m-d");
		$query_model="INSERT INTO activity_log(`notification`,`date`,`time`,`user_id`,`type`) VALUES('".$text."','".$date_current."','".$time_current."','".$id_admin."','global_admin')";
		mysql_query($query_model);
	}
	function delete_campaign($id)
	{
		$query_model="DELETE FROM holiday_builder_email WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM holiday_builder_text WHERE campaign_builder_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM holiday_builder WHERE id='".$id."'";
		mysql_query($query_model);

	}

	function add_email($id,$text,$delay_day_email,$repeat_email,$clientId)
	{
		$query_model="SELECT * FROM holiday_builder_email WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			$query_model="INSERT INTO `holiday_builder_email`(`campaign_builder_id`,`text`,`delay_day`,`type`,`repeat_year`,`client_id`) VALUES('".$id."','".$text."','".$delay_day_email."','global_admin','".$repeat_email."','".$clientId."')";
			mysql_query($query_model);
		}else{
			$query_model="UPDATE holiday_builder_email SET `text`='".$text."',`delay_day`='".$delay_day_email."',`repeat_year`='".$repeat_email."' WHERE campaign_builder_id='".$id."'";
			mysql_query($query_model);
		}
	}

	function add_text($id,$text,$delay_day_text,$repeat_text,$clientId)
	{

		$query_model="SELECT * FROM holiday_builder_text WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			$query_model="INSERT INTO `holiday_builder_text`(`campaign_builder_id`,`text`,`delay_day`,`type`,`repeat_year`,`client_id`) VALUES('".$id."','".$text."','".$delay_day_text."','global_admin','".$repeat_text."','".$clientId."')";
			echo $query_model;
			mysql_query($query_model);
		}else{
			$query_model="UPDATE holiday_builder_text SET `text`='".$text."',`delay_day`='".$delay_day_text."',`repeat_year`='".$repeat_text."' WHERE campaign_builder_id='".$id."'";
			mysql_query($query_model);
		}
	}

	function get_campaign_id_token($token)
	{
		$query_model="SELECT * FROM holiday_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_token=mysql_fetch_assoc($query);
		$this->id_campaign=$array_token['id'];
	}

	function get_email_data()
	{
		$query_model="SELECT * FROM holiday_builder_email WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_email=mysql_fetch_assoc($query);
			if($array_email['text']!=''){
				$this->email_data=$array_email;
			}else{
				$this->email_data='empty';
			}
		}
	}

	function get_text_data()
	{
		$query_model="SELECT * FROM holiday_builder_text WHERE campaign_builder_id='".$this->id_campaign."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$array_text=mysql_fetch_assoc($query);
			$this->text_data=$array_text;
		}
	}

	function get_setting_data($token)
	{
		$query_model="SELECT * FROM holiday_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_token=mysql_fetch_assoc($query);
		$this->name=$array_token['name'];
		$this->date=$array_token['date'];
	}

	function update_delay_day_campaign($id,$delay_day)
	{
		$query_model="UPDATE `holiday_builder` SET `delay_day`='".$delay_day."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function update_date_campaign($id,$date)
	{
		$query_model="UPDATE `holiday_builder` SET `date`='".$date."' WHERE `id`='".$id."'";
		mysql_query($query_model);
	}

	function update_name_campaign($id,$name)
	{
		$query_model="SELECT * FROM holiday_builder WHERE name='".$name."'";
		$query=mysql_query($query_model);
		if($this->name!=$name){
			if(mysql_num_rows($query)!=0){
				throw new Exception("Name_exist", 1);
			}else{
				$query_model="UPDATE `holiday_builder` SET `name`='".$name."' WHERE `id`='".$id."'";
				mysql_query($query_model);
			}
		}
	}

	function update_text_data($id,$text,$delay_day_text)
	{
		$query_model="UPDATE `holiday_builder_text` SET `text`='".$text."',`delay_day`='".$delay_day_text."' WHERE `campaign_builder_id`='".$id."'";
		mysql_query($query_model);
	}

	function update_email_data($id,$text,$delay_day_email)
	{
		$query_model="UPDATE `holiday_builder_email` SET `text`='".$text."',`delay_day`='".$delay_day_email."' WHERE `campaign_builder_id`='".$id."'";
		mysql_query($query_model);
	}

	function get_name_campaign($id){
		$query_model="SELECT * FROM holiday_builder WHERE id='".$id."'";
		$query=mysql_query($query_model);
		$array_campaign=mysql_fetch_assoc($query);
		$this->name=$array_campaign['name'];
	}
	
	function get_postcard_data($dir)
	{
		$query_model="SELECT * FROM postcard WHERE campaign_builder_id='".$this->id_campaign."' AND type='global_admin'  AND type_campaign='holiday'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$this->postcard[]=$res;
				$this->postcard['dir']=$dir;
			}
		}
	}
	static public function get_dir_admin($clientId){
		$query_model="SELECT dir FROM admin WHERE id='".$clientId."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData['dir'];
	}

}



?>