<?php
include "include/session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        body{
          position: relative;
      }
      .sk-circle{
          display: none;
          z-index: 10001;
          width: 40px;
          height: 40px;
          position: absolute;
          top: 0;
          bottom: 0;
          right: 0;
          left: 0;
          margin: auto;
      }
  </style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'componets/header.php';  ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active">Add Admin</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <strong>Add Admin</strong>
                                    <small>new</small>
                                </div>
                                <div class="card-block">
                                    <div class="form-group username_val has-feedback">
                                        <label class="form-form-control-label" for="username_val">Username</label>
                                        <input data-toggle="popover" data-placement="bottom" onkeypress="change_input_data('username_val')" placeholder="Enter Username" type="text" class="form-control " id="username_val">
                                    </div>
                                    <div class="form-group email_val  has-feedback">
                                        <label class="form-form-control-label" for="email_val">Email</label>
                                        <input data-toggle="popover" data-placement="bottom" onkeypress="change_input_data('email_val')" placeholder="Enter Email" type="text" class="form-control " id="email_val">
                                    </div>
                                    <div class="form-group name_val   has-feedback">
                                        <label class="form-form-control-label" for="name_val">Name</label>
                                        <input data-toggle="popover" data-placement="bottom" onkeypress="change_input_data('name_val')" placeholder="Enter Name" type="text" class="form-control " id="name_val">
                                    </div>

                                    <div class="form-group password_val has-feedback">
                                        <label class="form-form-control-label" for="password_val">Password</label>
                                        <input data-toggle="popover" data-placement="bottom" onkeypress="change_input_data('password_val')" placeholder="Enter Password" type="text" class="form-control " id="password_val">
                                    </div>

                                    <button onclick="add_new_admin()" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Add </button>
                                    <button onclick="clear_data()" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                                    <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn btn-outline-success">Success</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    Existing Admins
                                </div>
                                <div class="card-block">
                                    <table class="table table-bordered table-striped table-condensed table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody class="admin_list">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this admin?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" onclick="delete_admin()" data-dismiss="modal" class="btn btn-primary">Yes</button>
                </div>
            </div>
        </div>
    </div>
</main>
</div>

<!-- Bootstrap and necessary plugins -->
<!--<script src="js/libs/jquery.min.js"></script>-->
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!-- Plugins and scripts required by this views -->
<script src="../js/libs/toastr.min.js"></script>
<script src="js/libs/gauge.min.js"></script>
<script src="js/libs/moment.min.js"></script>
<script src="js/libs/daterangepicker.js"></script>
<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>
<script>
    get_admin_list();
    var json;
    var out;
    var i;
    var num;
    var id_admin;
    function get_admin_list(){
        $('.sk-circle').css('display','block');
        $.ajax({
            url:"include/admin/get_admin_list.php",
            type:"POST",
            success:function(data2){
                output_admin_list(data2);
                $('.sk-circle').css('display','none');
                //alert(data2);
            }
        })
    }

    function modal_show(id)
    {
        id_admin=id;
        $("#dangerModal").modal("show");
    }

    function output_admin_list(arr){
        //$(".admin_list").html('');
        json=JSON.parse(arr);
        num=json['data'].length;
        if(json['success']==1){
            console.log(json);
            console.log(num);
            for (i = 0; i < num; i++) {
                out+="<tr>";
                out+="<td>"+json['data'][i]['username']+"</td>";
                out+="<td>"+json['data'][i]['email']+"</td>";
                out+="<td style=width:55px!important;><a onclick='modal_show("+json['data'][i]['id']+")' class='btn btn-danger' href='#delete_admin'><i class='fa fa-trash-o'></i></a></td>";
                out+="</tr>";
            }
        }
        $(".admin_list").html(out);
    }

    function delete_admin(){
        $('.sk-circle').css('display','block');
        var id=id_admin;
        $.ajax({
            url:"include/admin/delete_admin.php",
            type:"POST",
            data:{id:id},
            success:function(data){
                //alert(data);
                $('.sk-circle').css('display','none');
                window.location="add-admin.php";
            }
        })
    }

    /*  *********   Add New Admin BEGIN  ********* */
    function add_new_admin(){
        var username_val=$("#username_val").val();
        var email_val=$("#email_val").val();
        var name_val=$("#name_val").val();
        var password_val=$("#password_val").val();
        try{
            check_data_val(username_val,email_val,name_val,password_val);
            send_ajax_query(username_val,email_val,name_val,password_val);
            //get_admin_list();
            window.location="add-admin.php";
        }catch(e){
            //console.log(e);
        }
    }
    function send_ajax_query(username,email,name,password){
        $('.sk-circle').css('display','block');
        $.ajax({
            url:"include/admin/registration_admin.php",
            type:"POST",
            data:{email:email,password:password,username:username,name:name},
            success:function(data){
                //alert(data);
                //console.log(data);
                parsing_json(data);
                $('.sk-circle').css('display','none');

            }
        })
    }
    function parsing_json(data){
        var json=JSON.parse(data);
        if(json['success']==1){
            clear_data();
            $(".success_btn").css('display','inline-block');
            setTimeout(function(){
                $(".success_btn").css('display','none');
            },1000);
        }else{
            if(json['error']!=''){
                notification('email_val','User Exist');
                throw new SyntaxError("error_data");
            }
        }
    }
    function change_input_data(id){
        $('#'+id).removeClass('form-control-danger');
        $('.'+id).removeClass('has-danger');
    }
    function notification(id,type_error){
        $('#'+id).addClass('form-control-danger');
        $('.'+id).addClass('has-danger');
        $("#"+id).attr("data-content",type_error);
        $('#'+id).popover('show');
        setTimeout(function(){
            $('#'+id).popover('dispose');
        },2000);
    }
    function check_data_val(username,email,name,password){
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;

        if(username==''){
            notification('username_val','Enter Username');
            throw new SyntaxError("empty_data");
        }

        if(parseInt(username.substr(0, 1))){
            notification('username_val','Login must start with a letter');
            throw new SyntaxError("error");
        }

        if(/^[a-zA-Z1-9]+$/.test(username) === false){
            notification('username_val','In the login there must be only Latin letters');
            throw new SyntaxError("error");
        }

        if(username.length < 6 || username.length > 20){
            notification('username_val','The login must be between 6 and 20 characters');
            throw new SyntaxError("error");
        }

        if(email==''){
            notification('email_val','Enter Username');
            throw new SyntaxError("empty_data");
        }
        if(!pattern.test(email)){
            notification('email_val','Example: nick@mail.com');
            throw new SyntaxError("empty_data");
        }
        if(name==''){
            notification('name_val','Enter Username');
            throw new SyntaxError("empty_data");
        }
        if(password==''){
            notification('password_val','Enter Username');
            throw new SyntaxError("empty_data");
        }
        if(password.length < 6 || password.length > 20){
            notification('password_val','The password must be between 6 and 20 characters');
            throw new SyntaxError("empty_data");
        }
    }

    function clear_data(){
        $("#username_val").val('');
        $("#email_val").val('');
        $("#name_val").val('');
        $("#password_val").val('');
    }
    /*  *********   Add New Admin END  ********* */
</script>
</body>
</html>