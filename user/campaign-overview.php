<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$token=$_SESSION['user_token'];
$query_to_token_bd=mysql_query("SELECT * FROM token WHERE token='$token'");
$array_token=mysql_fetch_assoc($query_to_token_bd);
$id_user=$array_token['client_id'];
$campaign_id=0;
$count_contacts=0;
function get_list_campaign(&$id_user){
    $query=mysql_query("SELECT * FROM campaign_builder WHERE type='user' AND client_id='$id_user'");
    while ($res=mysql_fetch_assoc($query)) {
        $num=get_count_contact($res['id']);
        echo '<tr>
        <td>'.$res['name'].'</td>
        <td>'.$res['campaign_type'].'</td>
        <td>'.$num.'</td>
        <td>Active</td>
        <td class="table_button" style="width:157px!important;">';
        if($res['type']!='global_admin'){
            echo '<a onclick=show_modal('.$res["id"].') class="btn btn-danger btn_table" href="#">
            <i class="fa fa-trash-o "></i>

            </a>';
            echo '<a class="btn btn-info btn_table" href="overview-universal-campaign.php?token='.$res['md5'].'">
            <i class="fa fa-edit "></i>
            </a>';
            echo '<a class="btn btn-success btn_table" href="#" onclick="view_contact('.$res['id'].');">
            <i class="fa fa-search-plus "></i>
            </a>';

            echo '</td>
            </tr>';
        }
    }
}
function get_count_contact($id)
{
    $query_model="SELECT * FROM contacts WHERE assigned_campaign='".$id."'";
    $query=mysql_query($query_model);
    $num= mysql_num_rows($query);
    return $num;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'components/header.php'; ?>
    <div class="app-body">
        
        <?php include "sidebar.php"; ?>
        <main class="main">       
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Campaign Overview</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="card">
                    <div class="card-header">
                        Current Campaigns
                    </div>
                    <div class="card-block">
                        <table class="table table-striped table-bordered datatable table-responsive">
                            <thead>
                                <tr>
                                    <th>Name of Campaign:</th>
                                    <th>Type:</th>
                                    <th>Contacts</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php get_list_campaign($id_user); ?>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-success" onclick="window.location='campaign-builder.php'">Create New Campaign</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main>
<div id="myModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Contact List</h4>
                <h6 style="float: right;"><button class="btn " id="link_to_download" onclick="download_list()">Download List</button></h6>
                
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered datatable table-responsive">
                    <thead>
                        <tr>
                            <th>Id:</th>
                            <th>First Name:</th>
                            <th>Date Added:</th>
                            <th>Email:</th>
                            <th>Mobile Phone:</th>
                        </tr>
                    </thead>
                    <tbody class="list_contact">

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal" >Close</button>
            </div>
        </div>
    </div>
</div>

</div>

<div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-danger" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this campaign?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" onclick="del_campaign()" data-dismiss="modal" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>



<!-- GenesisUI main scripts -->

<script src="js/app.js"></script>





<!-- Plugins and scripts required by this views -->
<script src="../js/libs/toastr.min.js"></script>
<script src="js/libs/gauge.min.js"></script>
<script src="js/libs/moment.min.js"></script>
<script src="js/libs/daterangepicker.js"></script>


<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>
<script>
    var id_campaign;
    var current_id;
    function del_campaign(){
        var id=id_campaign;
        $('.sk-circle').css('display','block');
        $.ajax({
            url:"include/univesral_campaign/delete_campaign.php",
            data:{id:id},
            type:"POST",
            success:function(data){
                $('.sk-circle').css('display','none');
                //console.log(data);
                window.location="campaign-overview.php";
            }
        })
    }

    function download_list()
    {
        window.location= 'generate_list.php?id='+current_id;
    }

    function show_modal(id)
    {
        id_campaign=id;
        $("#dangerModal").modal("show"); 
    }

    function view_contact(id)
    {
        current_id=id;
        //$("#link_to_download").attr("href",'#');
        $.ajax({
            url:"include/contacts/get_contact_list.php",
            data:{id:id},
            type:"POST",
            success:function(data){
                $(".list_contact").html('');
                var json=JSON.parse(data);
                if(json['success']==0){
                    $("#link_to_download").attr("disabled",'disabled');
                    $("#link_to_download").removeClass("btn-success");
                }else{
                 $("#link_to_download").addClass("btn-success");
                 $("#link_to_download").removeAttr("disabled");
             }
             var length=json["data"]['count'];
             var out;
             for (var i = 1; i < length+1; i++) {
                out+="<tr>";
                out+="<th>"+i+"</th>";
                out+="<th>"+json['data'][i]['first_name']+"</th>";
                out+="<th>"+json['data'][i]['date_added']+"</th>";
                out+="<th>"+json['data'][i]['email']+"</th>";
                out+="<th>+"+json['data'][i]['mobile_phone']+"</th>";
                out+="</tr>"
            }
            $(".list_contact").html(out);
        }
    })
        $("#myModal").modal();
    }
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>

</body>

</html>