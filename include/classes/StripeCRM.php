<?php
namespace StripeCRM
{
	require_once "libs/stripe/init.php";
	use Stripe\Stripe;
	use Stripe\Invoice;
	use Stripe\InvoiceItem;
	use Stripe\Customer;

	class StripeCRM
	{
		private static $db_obj;

		public function __construct()
		{			
			try
			{
				mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
				Stripe::setApiKey('sk_test_6hIZtAi4vAgv2XXXoMBPgZxv');
				self::$db_obj = new \mysqli('localhost', 'root', '', 'crm');
				$this->create_inv_table();
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}

		private function create_inv_table()
		{
			try
			{
				$query = "CREATE TABLE IF NOT EXISTS invoice (
					`id` INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
					`user_id` INT(5) NOT NULL,
					`inv_id` VARCHAR(255) NULL UNIQUE,
					`cus_id` VARCHAR(255) NULL UNIQUE,
					`card_id` VARCHAR(255) NULL UNIQUE,
					`is_paid` ENUM('0', '1') NOT NULL,
					FOREIGN KEY(user_id) REFERENCES user(id)
				) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci";
				self::$db_obj->query($query);
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}				
		}

		private function check($user_id, $statement)
		{
			try 
			{
				switch ($statement) {
					case 'is_paid':
						$query = "SELECT is_paid FROM invoice WHERE `user_id` = '{$user_id}'";
						$result = self::$db_obj->query($query);
						$is_paid = $result->fetch_row();
						$result->close();
						if(!$is_paid[0])
						{
							return false;
						}
						else
						{
							return true;
						}
						break;	
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}		
		}

		private function get_price($user_id)
		{
			try
			{			
				$query = "SELECT base_price, rvm_price, postcard_price, text_price FROM user WHERE `id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$res_price = $result->fetch_row();
				$result->close();
				$price['base'] = $res_price[0];
				$price['rvm'] = $res_price[1];
				$price['postcard'] = $res_price[2];
				$price['text'] = $res_price[3];

				return $price;
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}

		private function get_cus_id($user_id)
		{
			try
			{
				$query = "SELECT cus_id FROM invoice WHERE `user_id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$cus_id = $result->fetch_row();
				$result->close();
				$cus_id = $cus_id[0];
				return $cus_id;
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}

		private function get_inv_id($user_id)
		{
			try
			{
				$query = "SELECT inv_id FROM invoice WHERE `user_id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$inv_id = $result->fetch_row();
				$result->close();
				$inv_id = $inv_id[0];
				return $inv_id;
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}

		private function get_card_id($user_id)
		{
			try
			{
				$query = "SELECT card_id FROM invoice WHERE `user_id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$card_id = $result->fetch_row();
				$result->close();
				$card_id = $card_id[0];
				return $card_id;
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}


		private function get_count($user_id)
		{
			try
			{
				$query = "SELECT type_postcard, type_rvm, type_text, type_email FROM payment WHERE `user_id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$res_count = $result->fetch_row();
				$result->close();
				$count['postcard'] = $res_count[0];
				$count['rvm'] = $res_count[1];
				$count['text'] = $res_count[2];
				$count['base'] = $res_count[3];

				return $count;
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}			
		}

		public function get_access($user_id)
		{
			try
			{
				$query = "SELECT access_system FROM user WHERE `id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$access = $result->fetch_row();
				return $access[0];
			}
			catch(\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}
		}

		private function calc_total_price($user_id)
		{
			try
			{
				$cus_id = $this->get_cus_id($user_id);
				if (isset($cus_id))
				{
					$keys = array('base', 'rvm', 'postcard', 'text');
					$price = $this->get_price($user_id);
					$count = $this->get_count($user_id);
					for($i = 0; $i < count($keys); $i++)
					{
						$total_count[$keys[$i]] = $count[$keys[$i]] * $price[$keys[$i]];

						InvoiceItem::create(array(
						'customer' => "{$cus_id}",
						'description' => "{$keys[$i]}",
						'amount' => $total_count[$keys[$i]],
						'currency' => 'usd'
						));
					}
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}			
		}

		public function set_invoice($user_id)
		{
			try
			{
				if(isset($user_id))
				{				
					$this->calc_total_price($user_id);
					$cus_id = $this->get_cus_id($user_id);

					if(isset($cus_id) && !$this->check($user_id, 'is_paid'))
					{
						$invoice = Invoice::create(array('customer' => "{$cus_id}"));
						$query = "UPDATE invoice SET `inv_id` = '{$invoice['id']}', `is_paid` = '0' WHERE `user_id` = '{$user_id}'";
						self::$db_obj->query($query);
					}
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}				
		}

		public function get_invoice($user_id)
		{
			try
			{
				$inv_id = $this->get_inv_id($user_id);
				if(isset($inv_id))
				{
					$invoice = Invoice::retrieve(array('id' => "{$inv_id}"));
					return $invoice;
				}	
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}				
		}

		public function get_calculations($user_id)
		{
			try
			{
				$cus_id = $this->get_cus_id($user_id);
				if (isset($cus_id))
				{
					$keys = array('base', 'rvm', 'postcard', 'text');
					$price = $this->get_price($user_id);
					$count = $this->get_count($user_id);
					for($i = 0; $i < count($keys); $i++)
					{
						$total_count[$keys[$i]] = $count[$keys[$i]] * $price[$keys[$i]];
						$total += $total_count[$keys[$i]];
					}
					$total_count['total'] = $total;
					return $total_count;
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}			
		}

		public function get_amount($user_id)
		{
			try
			{
				if (isset($user_id))
				{
					$keys = array('base', 'rvm', 'postcard', 'text');
					$price = $this->get_price($user_id);
					$count = $this->get_count($user_id);
					for($i = 0; $i < count($keys); $i++)
					{
						$total_count[$keys[$i]] = $count[$keys[$i]] * $price[$keys[$i]];
						$total += $total_count[$keys[$i]];
					}
					return $total;
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}					
		}

		public function is_paid($user_id)
		{
			try
			{
				$query = "SELECT is_paid FROM invoice WHERE `user_id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
				$is_paid = $result->fetch_row();
				$result->close();
				$is_paid = $is_paid[0];
				return $is_paid;
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}			
		}

		public function check_payment($user_id)
		{
			try
			{
				$inv = $this->get_invoice($user_id);
				if(isset($inv))
				{
					return $inv['is_paid'];
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}				
		}

		public function clear_payment($user_id)
		{
			try
			{
				$query = "UPDATE user SET `is_paid` = '0' WHERE `user_id` = '{$user_id}'";
				$result = self::$db_obj->query($query);
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}				
		}

		public function pay_invoice($user_id)
		{
			try
			{
				$inv_id = $this->get_inv_id($user_id);
				if(isset($inv_id))
				{
					$invoice = Invoice::retrieve(array('id' => "{$inv_id}"));
					if(!$this->check($user_id, 'is_paid'))
					{
						$invoice->pay();
						$query = "UPDATE invoice SET `is_paid` = '1' WHERE `user_id` = '{$user_id}'";
						$result = self::$db_obj->query($query);
						$query = "UPDATE user SET `is_paid` = '1' WHERE `user_id` = '{$user_id}'";
						$result = self::$db_obj->query($query);
						$query = "UPDATE user SET `access_system` = '0' WHERE `user_id` = '{$user_id}'";
						$result = self::$db_obj->query($query);
					}
				}
				else
				{
					print("Wrong user_id or invoice not exists");
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}

		public function create_cus($user_id, $email = NULL)
		{
			try
			{
				$cus_id = $this->get_cus_id($user_id);

				if(isset($email) && !isset($cus_id))
				{			
					$cus = Customer::create(array(
						'description' => "Customer for {$email}",
						'email' => "{$email}"
						));
					$query = "INSERT INTO invoice(`user_id`, `cus_id`) VALUES('{$user_id}', '{$cus['id']}')";
					self::$db_obj->query($query);
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}		
		}

		public function create_card($user_id, $card)
		{
			try
			{
				$data['source'] = $card;
				$cus_id = $this->get_cus_id($user_id);
				$card_id = $this->get_card_id($user_id);

				if(isset($cus_id) && !empty($data['source']) && !isset($card_id))
				{
					$cus = Customer::retrieve($cus_id);
					$cus->sources->create($data);
					$query = "UPDATE invoice SET `card_id` = '{$cus['id']}' WHERE `user_id` = '{$user_id}'";
					self::$db_obj->query($query);
				}
			}
			catch (\Exception $e)
			{
				print('Some troubles : '.$e->getMessage());
			}	
		}
	}
}
?>