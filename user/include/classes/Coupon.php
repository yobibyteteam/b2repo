<?php

require_once '../../../cron/config.php';
require_once '../../../Twilio/autoload.php'; 
use Twilio\Rest\Client;
/**
* 
*/
class Coupon
{
	static public function generateName(){
		return mt_rand().time();
	}

	public function getUserId($token){
		$queryModel="SELECT client_id FROM token WHERE token='".$token."'";
		$query=mysql_query($queryModel);
		$arrayToken=mysql_fetch_assoc($query);
		return $arrayToken['client_id'];
	}

	public function getUserDir($clientId){
		$queryModel="SELECT dir_img FROM user WHERE id='".$clientId."'";
		$query=mysql_query($queryModel);
		$arrayToken=mysql_fetch_assoc($query);
		return $arrayToken['dir_img'];
	}

	public function addToDB($newFileName,$campaigniId,$campaigniType,$mobileNumber,$clientId,$price){
		$md5=md5(time().mt_rand());
		$newFileName=$newFileName.'.jpg';
		$queryModel="INSERT INTO `coupon`(`campaign_id`,`campaign_type` ,`mobile_number`, `filename`, `client_id`,`md5`,`price`) VALUES ('".$campaigniId."','".$campaigniType."','".$mobileNumber."','".$newFileName."','".$clientId."','".$md5."','".$price."')";
		mysql_query($queryModel);
		return $md5;
	}

	public function moveFileToDir($fileImg,$randomName,$clientDir){
		$newFileName=$randomName.'.jpg';
		$uploadDirectory='../../../upload_photo/'.$clientDir.'/'.$randomName.'.jpg';
		copy($fileImg, $uploadDirectory);
	}

	public function getAllContact($id,$type){
		$arrayData=[];
		$queryModel="SELECT id,md5_hash,mobile_phone,user_id FROM contacts WHERE assigned_campaign='".$id."' AND assign_campaign_type='".$type."' AND status='Active'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayData[$res['id']]=array("user_id"=>$res['user_id'],"md5_hash"=>$res['md5_hash'],"mobile_phone"=>$res['mobile_phone']);
			}
			return $arrayData;
		}
	}

	public function seriesSend($array,$hash,$price){
		foreach ($array as $key => $value) {
			$this->sendSMS($value['mobile_phone'],$value['md5_hash'],$hash,$price,$value['user_id']);
		}
	}

	public function sendSMS($mobile,$hashContact,$hashCoupon,$cost,$id){
		$text='https://tracs.me/coupon/coupon.php?token='.$hashCoupon.'&contact='.$hashContact.'';
		$mobile='+'.$mobile;
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		$message = $client->messages->create(
			$mobile, 
			array(
				'from' => '+17202509876',
				'body' => $text
			)
		);
		if($message->status=='queued'){
			$this->addToStat($hashCoupon,$hashContact,$cost,$id);
		}
	}

	public function addToStat($couponHash,$contactHash,$cost,$id){
		$queryModel="INSERT INTO `coupon_used`(`coupon_hash`, `contact_hash`,`cost`,`user_id`) VALUES ('".$couponHash."','".$contactHash."','".$cost."','".$id."')";
		mysql_query($queryModel);
	}

	public function sendPreviewSms($mobile,$hashCoupon){
		$mobile='+'.$mobile;
		$text='https://tracs.me/coupon/coupon.php?token='.$hashCoupon.'';
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		$message = $client->messages->create(
			$mobile, 
			array(
				'from' => '+17202509876',
				'body' => $text
			)
		);
	}
}



?>