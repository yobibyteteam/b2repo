<?php
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';
session_start();
$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_data'];

$holiday_campaign=new Holiday_campaign();
$clientId=Holiday_campaign::getClientId($_SESSION['user_token']);
try{
	$holiday_campaign->add_email($id_campaign,$editor_text,$clientId);
	echo json_encode(array("success"=>"1"));
}
catch(Exception $e)
{
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}
?>