<?php
$token=$_POST['token'];
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';

$holiday_campaign=new Holiday_campaign();

try{
	$holiday_campaign->get_campaign_id_token($token);
	$holiday_campaign->get_email_data();
	$holiday_campaign->get_text_data();
	$holiday_campaign->get_setting_data($token);
	echo json_encode(array("success"=>1,"email"=>$holiday_campaign->email_data,"text"=>$holiday_campaign->text_data,"name"=>$holiday_campaign->name,"date"=>$holiday_campaign->date,"delay_day"=>$holiday_campaign->delay_day));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>