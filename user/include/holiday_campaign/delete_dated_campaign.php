<?php
session_start();
include '../connect_to_bd.php';
require_once '../class/Holiday_campaign.php';
$id=$_POST['id'];

$holiday_campaign=new Holiday_campaign();

try{
	$holiday_campaign->delete_campaign($id);
	$text="User delete holiday campaign";
	$un_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>1));
}
catch(Exception $e)
{
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}
?>