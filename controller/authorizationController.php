<?php
session_start();
include '../include/connect_to_bd.php';
require_once '../include/classes/authorization.php';
require_once "../include/classes/StripeCRM.php";

$userEmail=$_POST['username'];
$userPass=$_POST['password'];
$keepMeSign=$_POST['agree'];

$authorization = new Authorization($userEmail,$userPass,$keepMeSign);
try{
	$typeClient=$authorization->identifyTypeClient();
	$authorization->addTokenToBD($typeClient);
	$_SESSION['user_token']=$authorization->token;

	echo json_encode(array("success"=>"1","user_type"=>$typeClient,"user_token"=>$authorization->token,"client_id"=>$authorization->arrayClient['id']));
}catch(Exception $e){
	$type=$e->getMessage();
	if($type=='access'){
		echo json_encode(array("success"=>"0","error"=>$e->getMessage(),"count"=>$authorization->count));
	}else{
		echo json_encode(array("success"=>"0","error"=>$e->getMessage()));
	}
}

?>