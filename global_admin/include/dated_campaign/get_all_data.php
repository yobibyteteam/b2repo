<?php
$token=$_POST['token'];
session_start();
include '../connect_to_bd.php';
require_once '../class/Dated_campaign.php';

$dated_campaign=new Dated_campaign();

try{
	$clientId=Dated_campaign::getClientId($_SESSION['user_token']);
	$dir=Dated_campaign::get_dir_admin($clientId);
	$dated_campaign->get_campaign_id_token($token);
	$dated_campaign->get_email_data();
	$dated_campaign->get_text_data();
	$dated_campaign->get_audio_data();
	$dated_campaign->get_postcard_data($dir);
	$dated_campaign->get_setting_data($token);
	echo json_encode(array("success"=>1,"email"=>$dated_campaign->email_data,"text"=>$dated_campaign->text_data,"audio"=>$dated_campaign->audio_data,"postcard"=>$dated_campaign->postcard,"name"=>$dated_campaign->name,"type"=>$dated_campaign->type_campaign,"dated"=>$dated_campaign->dated));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>