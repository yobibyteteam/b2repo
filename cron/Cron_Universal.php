<?php 
require_once 'Twilio/autoload.php'; 
require_once 'Mailgun/vendor/autoload.php';
use Mailgun\Mailgun;
use Twilio\Rest\Client;
class Cron_Universal extends Cron{
	public $arrayId=[];
	function __construct(){}

	protected function getAllCampaign(){
		$arrayData=[];
		$queryModel="SELECT id,type FROM campaign_builder WHERE type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData[]=array($res['type'],$res['id']);
			}
		}
		return $arrayData;
	}

	public function updateSuccessedValue(){
		if($this->arrayId){

			foreach ($this->arrayId as $key => $value) {
				$queryModel="UPDATE `contacts` SET `success_sended_universal`='1' WHERE id='".$value."'";
				mysql_query($queryModel);
			}
		}
	}
	public function getAllPostcard($key){
		$arrayData=[];
		$queryModel="SELECT * FROM postcard WHERE campaign_builder_id='".$key."' AND type_campaign='universal' AND type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['type_postcard']=='back'){
					$arrayData['back']=$res;
				}else{
					$arrayData['front']=$res;
				}
			}
		}
		return $arrayData;
	}

	public function getUserPhotoDir($id){
		$queryModel="SELECT dir_img FROM user WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['dir_img'];
	}

	public function sendPostcard($dirImg,$frontFile='',$backFile='',$clientId,$arrayAddress,$delayDay){
		$date=date("Y-m-d");
		$frontLink='https://tracs.me/upload_photo/'.$dirImg.'/'.$frontFile;
		$backLink='https://tracs.me/upload_photo/'.$dirImg.'/'.$backFile;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://rest.clicksend.com/v3/post/postcards/send");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{
			\"file_urls\": [
				\"".$frontLink."\",
				\"".$backLink."\"
			],
			\"recipients\": [
				{
					\"address_name\": \"".$arrayAddress[0]['first_name']." ".$arrayAddress[0]['last_name']."\",
					\"address_line_1\": \"".$arrayAddress[0]['address']."\",
					\"address_line_2\": \"\",
					\"address_city\": \"".$arrayAddress[0]['city']."\",
					\"address_state\": \"".$arrayAddress[0]['state']."\",
					\"address_postal_code\": \"".$arrayAddress[0]['postal_code']."\",
					\"address_country\": \"".$arrayAddress[0]['country']."\",
					\"return_address_id\": 1,
					\"custom_string\": \"\"
				}
			]
		}");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Content-Type: application/json",
			"Authorization: Basic Y29udGFjdFRSQUNTOjBGOTc5RTg5LTAyQzUtNzMxNy02NjVDLUZDNEJEOUI0MURBNg=="
		));
		if($date==$delayDay){
			$response = curl_exec($ch);
			print_r($response);
			$response=json_decode($response);
			if($response->http_code=='200'){
				$countPrice=Cron_Universal::getCountPrice($clientId,'postcard');
				Cron_Universal::addToStatistic($clientId,'campaign','postcard');
				Cron_Universal::addBillingStat($clientId,$countPrice,'postcard');
				Cron_Universal::addMoreValue($clientId,'postcard');
			}
		}
	}

	public function getAllAddr($key){
		$arrayData=[];
		$queryModel="SELECT first_name,last_name,address,country,city,state,postal_code FROM contacts WHERE assigned_campaign='".$key."' AND status='Active' AND assign_campaign_type='campaign' AND success_sended_universal='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData[]=$res;
			}
		}
		return $arrayData;
	}

	protected function getAllMobileOfContact($idCampaign){
		$arrayData='';
		$arrayId=[];
		$queryModel="SELECT mobile_phone,alt_phone,spouse_mobile,spouse_alternate,id FROM contacts WHERE assigned_campaign='".$idCampaign."' AND status='Active' AND assign_campaign_type='campaign' AND success_sended_universal='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['mobile_phone']!='' && $res['alt_phone']!=''){
					$arrayData[]='+'.$res['mobile_phone'];
					$this->arrayId[]=$res['id'];
				}elseif($res['mobile_phone']=='' && $res['alt_phone']!=''){
					$arrayData[]='+'.$res['alt_phone'];
					$this->arrayId[]=$res['id'];
				}

				if($res['spouse_mobile']!='' && $res['spouse_alternate']!=''){
					$arrayData[]='+'.$res['spouse_mobile'];
					$this->arrayId[]=$res['id'];
				}elseif($res['spouse_mobile']=='' && $res['spouse_alternate']!=''){
					$arrayData[]='+'.$res['spouse_alternate'];
					$this->arrayId[]=$res['id'];
				}
			}
		}
		//Cron_Universal::updateSuccessedValue($arrayId);
		return $arrayData;
	}

	protected function getAllContact($idCampaign){
		$arrayData='';
		$arrayId=[];
		$queryModel="SELECT email,spouse_email,id FROM contacts WHERE assigned_campaign='".$idCampaign."' AND status='Active' AND assign_campaign_type='campaign' AND success_sended_universal='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['email']!=''){
					$arrayData[]=$res['email'];
					$this->arrayId[]=$res['id'];
				}
				if($res['spouse_email']!=''){
					$this->arrayData[]=$res['spouse_email'];
				}
				if (array_key_exists($res['id'], $this->arrayId)) {
					$this->arrayId[]=$res['id'];
				}
			}
		}
		//Cron_Universal::updateSuccessedValue($arrayId);
		return $arrayData;
	}

	protected function getUserIdFromContactId($id){
		$queryModel="SELECT user_id FROM contacts WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['user_id'];
	}

	protected function sendRvmdData($key){
		$userId=$this->getUserIdFromContactId($key);
		$countPrice=$this->getCountPrice($userId,'rvmd');
		$this->addToStatistic($userId,'camapign','rvmd');
		$this->addBillingStat($userId,$countPrice,'rvmd');
		$this->addMoreValue($userId,'rvmd');
	}

	protected function getAllEmail($idCampaign){
		$arrayData='';
		$date=date("Y-m-d");
		$queryModel="SELECT `text`,`delay_day` FROM campaign_builder_email WHERE campaign_builder_id='".$idCampaign."' AND delay_day='".$date."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['text']!=''){
					$arrayData[]=$res['text'];
				}
			}
		}
		return $arrayData;
	}

	protected function getAllText($idCampaign){
		$arrayData='';
		//$date=date("Y-m-d");
		$queryModel="SELECT `text` FROM campaign_builder_text WHERE campaign_builder_id='".$idCampaign."' and type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['text']!=''){
					$arrayData[]=$res['text'];
				}
			}
		}
		return $arrayData;
	}

	protected function updateTextData($email,$text){
		$queryModel="SELECT first_name,last_name,assign_campaign_type,user_id,mobile_phone,email,address,address1,city,state,postal_code FROM contacts WHERE email='{$email}' OR spouse_email='{$email}'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
			foreach ($arrayData as $key => $value) {
				$type='{$'.$key.'}';
				$request=strpos($text,$type);
				if($request){
					$text=str_replace($type, $value, $text);
				}
			}
		}
		return array('text'=>$text,'user_id'=>$arrayData['user_id'],'type'=>$arrayData['assign_campaign_type']);
	}

	protected function addToStatistic($userId,$typeCampaign,$type){
		$date=date("Y-m-d");
		$queryModel="INSERT INTO `statistics`(`user_id`, `type_campaign`, `date`,`type`) VALUES ('".$userId."','".$typeCampaign."','".$date."','".$type."')";
		mysql_query($queryModel);
	}

	protected function getCountPrice($id,$type){
		switch ($type) {
			case 'email':
			$type='base_price';
			break;
			case 'text':
			$type='text_price';
			break;
			case 'postcard':
			$type='postcard_price';
			break;
			case 'rvmd':
			$type='rvm_price';
			break;
		}
		$sql="SELECT ".$type." FROM user WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData[$type];
	}

	protected function addBillingStat($id,$count,$type){
		$sql='INSERT INTO `billing_stat`( `user_id`, `count`,`type`) VALUES ("'.$id.'","'.$count.'","'.$type.'")';
		mysql_query($sql);
	}

	protected function addMoreValue($id,$type){
		switch ($type) {
			case 'email':
			$type='type_email';
			break;
			
			case 'text':
			$type='type_text';
			break;
			case 'postcard':
			$type='type_postcard';
			break;
			case 'rvmd':
			$type='type_rvm';
			break;
		}
		$sql='UPDATE `payment` SET '.$type.'='.$type.'+1 WHERE user_id='.$id.'';
		mysql_query($sql);
	}

	protected function sendEmailData($email,$text){
		$mgClient = new Mailgun(KEY);
		$domain = DOMAIN;
		$data=Cron_Universal::updateTextData($email[0],$text[0]);
		$countPrice=Cron_Universal::getCountPrice($data['user_id'],'email');
		foreach ($email as $key => $value){
			$result = $mgClient->sendMessage("$domain",
				array('from'    =>  '<crm@tracs.me>',
					'to'      => ''.$value.'',
					'html'    => $data['text']));
			$typeAnt=$result->http_response_code;
			if($typeAnt=200){
				Cron_Universal::addToStatistic($data['user_id'],$data['type'],'email');
				Cron_Universal::addBillingStat($data['user_id'],$countPrice,'email');
				Cron_Universal::addMoreValue($data['user_id'],'email');
			}
		}
	}

	protected function getUserId($value){
		$value = mb_substr($value, 1);
		$queryModel="SELECT user_id,assign_campaign_type FROM contacts WHERE mobile_phone='".$value."' OR spouse_mobile='".$value."' OR spouse_alternate='".$value."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
			return array("user_id"=>$arrayData['user_id'],"type"=>$arrayData['assign_campaign_type']);
		}
	}
	private function getMobileCampaign($id){
		$sql="SELECT assign_num FROM campaign_builder WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['assign_num'];
	}

	protected function sendTextData($text,$mobile,$key){
		$from=Cron_Universal::getMobileCampaign($key);
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		foreach ($mobile as $key => $value){
			$message = $client->messages->create(
				$value, 
				array(
					'from' => $from,
					'body' => strip_tags($text[0])
				)
			);

			if($message->status=='queued'){
				$userArray=Cron_Universal::getUserId($value);
				$countPrice=Cron_Universal::getCountPrice($userArray['user_id'],'text');
				Cron_Universal::addToStatistic($userArray['user_id'],$userArray['type'],'text');
				Cron_Universal::addBillingStat($userArray['user_id'],$countPrice,'text');
				Cron_Universal::addMoreValue($userArray['user_id'],'text');
			}

		}

	}
}


?>