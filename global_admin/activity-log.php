<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$query=mysql_query("SELECT * FROM activity_log WHERE type='global_admin' ORDER BY id DESC");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
    body{
      position: relative;
    }
    .sk-circle{
      display: none;
      z-index: 10001;
      width: 40px;
      height: 40px;
      position: absolute;
      top: 0;
      bottom: 0;
      right: 0;
      left: 0;
      margin: auto;
    }
  </style>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'componets/header.php';  ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">User Profile Search</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <br>
          <div class="card">
            <div class="card-header">
              <i class="fa fa-edit"></i> Angular DataTables
            </div>
            <div class="card-block">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Notification</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $i=1;
                  while ($res=mysql_fetch_array($query)) {
                   echo "<tr>";
                   echo "<td>".$i."</td>";
                   echo "<td>".$res['notification']."</td>";
                   echo "<td>".$res['date']."<br>".$res['time']."</td>";
                   echo "</tr>";
                   $i++;
                 }
                 ?>
               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>
   </main>
 </tbody>
</table>
</div>
</div>
</div>
</div>
</main>
</div>



<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!-- Plugins and scripts required by this views -->
<!-- Custom scripts required by this view -->
<script src="js/views/tables.js"></script>
<script>
  $('.card-header').html("Activity Log");
</script>
</body>
</html>
