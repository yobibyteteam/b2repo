<?php 

class Cron
{
	private $campaign;
	protected $arrayCampaignData,$arrayContactData=[],$arrayEmailData=[],$arrayAllDateContact=[],$arrayAllDelayDayContact=[],$arrayTextData=[],$arrayMobileContactData=[],$arrayAllDelayDayContactText=[],$arrayId,$arrayPostcardLink=[],$arrayAddress=[],$arrayPostcard=[];
	
	function __construct($type){
		require_once 'Cron_'.$type.'.php';
		$className='Cron_'.$type;
		$this->campaign=new $className;
		$this->getAllCampaignList();
		$this->getAllContactOfUser();
		switch ($type) {
			case 'Universal':
			$this->getPostcardLink();
			$this->getAllEmailData();
			$this->getAllTextData();
			$this->sendRvmd();
			$this->getAllAddress();
			$this->getAllMobileContactOfUser();
			$this->sendPostcard();
			$this->updateSuccessed();
			break;
			case 'Dated':
	         $this->getAllDelayDayContact();//
	         $this->getAllEmailData();
	         $this->getAllTextData();
	         $this->getAllMobileContactOfUser();
	         $this->getAllDelayDayContactText();
	         $this->getAllDateContact();//
	         $this->getPostcardLink();
	         $this->getAllAddress();
	         $this->separateEmailData();//
	         $this->separateTextData();//
	         $this->separatePostcardData();
	         $this->sendRvmd();
	         $this->sendPostcard();
	         $this->updateSuccessed();
	         break;
	         case 'Admin_Holiday':
	         $this->getAllEmailData();
	         $this->getAllTextData();
	         $this->getAllMobileContactOfUser();
	         $this->sendAdminHolidayEmail();
	         $this->sendAdminHolidayText();
	         $this->updateSuccessed();

	         break;
	         case 'Holiday':
	         $this->getAllDelayDayContact();
	         $this->getAllEmailData();
	         $this->getAllTextData();
	         $this->getAllMobileContactOfUser();
	         $this->getAllDelayDayContactText();
	         $this->getAllDateContact();//
	         $this->getPostcardLink();
	         $this->getAllAddress();
	         $this->separatePostcardData();
	         $this->separateEmailData();//
	         $this->separateTextData();//
	         $this->sendPostcard();
	         $this->updateSuccessed();
	         break;
	     }
	     $this->sendEmail();
	     $this->sendText();
	 }

	 public function updateSuccessed(){
	 	$this->campaign->updateSuccessedValue();
	 }

	public function getAllCampaignList(){//Получаем все компании 
		$this->arrayCampaignData=$this->campaign->getAllCampaign();
		//echo json_encode($this->arrayCampaignData);
	}
	public function sendRvmd(){
		foreach ($this->arrayContactData as $ikey => $ivalue) {
			foreach ($this->campaign->arrayId as $jkey => $jvalue) {
				$this->campaign->sendRvmdData($jvalue);
			}
		}
	}

	public function getAllContactOfUser(){//Получаем Все Эмаилы Контакта
		foreach ($this->arrayCampaignData as $key) {
			$value=$this->campaign->getAllContact($key[1]);
			if($value!=null){
				$this->arrayContactData[$key[1]]=$value;
			}
		}
		//echo json_encode($this->arrayContactData);
	}

	public function getPostcardLink(){
		foreach ($this->arrayContactData as $key => $value) {
			$this->arrayPostcardLink[$key]=$this->campaign->getAllPostcard($key);
		}
		//echo json_encode($this->arrayPostcardLink);
	}

	public function getAllAddress(){
		foreach ($this->arrayCampaignData as $key=>$value) {
			$this->arrayAddress[$value[1]]=$this->campaign->getAllAddr($value[1]);
		}
		//echo json_encode($this->arrayAddress);
	}

	public function renameFile($fileName){
		$newFile=substr($fileName, 0, -4);
		return $newFile.'.pdf';
	}

	public function getUserDir($clientId){
		return $this->campaign->getUserPhotoDir($clientId);
	}

	public function sendPostcard(){
		$frontFile='';
		$backFile='';
		$clientId=0;
		foreach ($this->arrayAddress as $ikey => $ivalue) {
			foreach ($this->arrayPostcardLink as $jkey => $jvalue) {
				if($jkey==$ikey){
					if (array_key_exists('front', $jvalue)){
						$frontFile=$this->campaign->renameFile($jvalue['front']['filename']);
						$clientId=$jvalue['front']['client_id'];
					}
					if (array_key_exists('back', $jvalue)){
						$backFile=$this->campaign->renameFile($jvalue['back']['filename']);
						$clientId=$jvalue['back']['client_id'];
					}
					$dirImg=$this->getUserDir($clientId);
					$this->campaign->sendPostcard($dirImg,$frontFile,$backFile,$clientId,$ivalue,$jvalue['back']['delay_day_date']);
				}
			}
		}
	}

    //Получаем Все Мобильные Номера  Контакта
	public function getAllMobileContactOfUser(){
		foreach ($this->arrayCampaignData as $key) {
			$value=$this->campaign->getAllMobileOfContact($key[1]);
			if($value!=null){
				$this->arrayMobileContactData[$key[1]]=$value;
			}
		}
			//echo json_encode($this->arrayMobileContactData);
	}

//Получаем все делай-дэй значения
	public function getAllDelayDayContactText(){
		foreach ($this->arrayCampaignData as $key) {
			$value=$this->campaign->getDelayDayContactText($key[1]);
			if($value!=null){
				$this->arrayAllDelayDayContactText[$key[1]]=$value;
			}
		}
				//echo json_encode($this->arrayAllDelayDayContactText);
	}

	public function getAllDelayDayContact(){//Получаем все делай-дэй значения
		foreach ($this->arrayCampaignData as $key) {
			$value=$this->campaign->getDelayDayContact($key[1]);
			if($value!=null){
				$this->arrayAllDelayDayContact[$key[1]]=$value;
			}
		}
		//echo json_encode($this->arrayAllDelayDayContact);
	}

	public function getAllEmailData(){ // Получаем все Эмаил Тексты Компании
		foreach ($this->arrayCampaignData as $key=>$valuek) {
			$value=$this->campaign->getAllEmail($valuek[1]);
			if($value!=null){
				$this->arrayEmailData[$valuek[1]]=$value;
			}
		}
		//echo json_encode($this->arrayEmailData);
	}

	protected function getAllTextData(){//Получаем все смс тексты
		foreach ($this->arrayCampaignData as $key=>$valuek) {
			$value=$this->campaign->getAllText($valuek[1]);
			if($value!=null){
				$this->arrayTextData[$valuek[1]]=$value;
			}
		}
		//echo json_encode($this->arrayTextData);
	}

	public function getAllDateContact(){//Получение списка Date
		foreach ($this->arrayCampaignData as $key=>$valuek) {
			$dop='';
			if (array_key_exists('2', $valuek)){
				$dop=$valuek[2];
			}
			$value=$this->campaign->getAllDateOfContact($valuek[1],$dop);
			if($value!=null){
				$this->arrayAllDateContact[$valuek[1]]=$value;
			}
		}
		//echo json_encode($this->arrayAllDateContact);
	}

	public function separateEmailData(){//Определяем дату с учетом делай дэй
		$this->arrayEmailData=$this->campaign->separateEmail($this->arrayAllDateContact,$this->arrayAllDelayDayContact,$this->arrayEmailData);
		//echo json_encode($this->arrayEmailData);
	}

	public function separatePostcardData(){
		$this->arrayPostcardLink=$this->campaign->separatePostcard($this->arrayAddress,$this->arrayPostcardLink,$this->arrayAllDateContact);
		//echo json_encode($this->arrayPostcardLink);
	}

	public function separateTextData(){
		$this->arrayTextData=$this->campaign->separateText($this->arrayAllDateContact,$this->arrayAllDelayDayContactText,$this->arrayTextData);
		//echo json_encode($this->arrayTextData);
	}

	public function sendEmail(){//Отправка Эмаил
		foreach ($this->arrayContactData as $ikey => $ivalue) {
			foreach ($this->arrayEmailData as $jkey => $jvalue) {
				if($ikey==$jkey){
					echo 1;
					$this->campaign->sendEmailData($ivalue,$jvalue);
				}
			}
		}
	}
//Отправка SMS
	public function sendText(){
		foreach ($this->arrayTextData as $ikey => $ivalue) {
			foreach ($this->arrayMobileContactData as $jkey => $jvalue) {
				if($ikey==$jkey){
					$this->campaign->sendTextData($ivalue,$jvalue,$jkey);
				}
			}
		}
	}
		//Отправка Admin Holiday Эмаил
	public function sendAdminHolidayEmail(){
		$date=date("Y-m-d");
		foreach ($this->arrayContactData as $ikey => $ivalue) {
			foreach ($this->arrayEmailData as $jkey => $jvalue) {
				if($ikey==$jkey && $this->arrayCampaignData[0][0]==$date){
					$this->campaign->sendEmailData($ivalue,$jvalue);
				}
			}
		}
	}
	//Отправка Admin Holiday Text
	public function sendAdminHolidayText(){
		$date=date("Y-m-d");
		foreach ($this->arrayTextData as $ikey => $ivalue) {
			foreach ($this->arrayMobileContactData as $jkey => $jvalue) {
				if($ikey==$jkey && $this->arrayCampaignData[0][0]==$date){
					$this->campaign->sendTextData($ivalue,$jvalue);
				}
			}
		}
	}
}

?>