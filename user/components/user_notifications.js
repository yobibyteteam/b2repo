
$.ajax({
    url:"include/getNotification.php",
    type:"POST",
    success:function(data){
        var json=JSON.parse(data);
        for(var k in json){
            addNewNotification(json[k]['text'],json[k]['notes']);
            //console.log(json[k]['text']);
        }
    }
})
function addNewNotification(text,notes){
    toastr.info(text, notes, {
        closeButton: true,
        progressBar: true,
        debug: false,
        newestOnTop: true,
        positionClass: "toast-top-right",
        preventDuplicates: false,
        onclick: null,
        showDuration: 300,
        hideDuration: 1000,
        timeOut: 3000,
        extendedTimeOut: 5000,
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"

    });

}
