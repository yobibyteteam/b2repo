                          <html lang="en">
                          <head>
                          	<!-- Icons -->
                          	<link rel="shortcut icon" href="img/favicon.png">
                          	<link href="css/font-awesome.min.css" rel="stylesheet">
                          	<link href="css/simple-line-icons.css" rel="stylesheet">
                          	<!-- Main styles for this application -->
                          	<link href="css/style.css" rel="stylesheet">
                          	<link href="css/all.css" rel="stylesheet">
                          	<link href="css/colpick.css"  rel="stylesheet" type="text/css" />
                          	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
                          	<link rel="stylesheet" type="text/css" href="css/demo.css" />
                          	<link rel="stylesheet" type="text/css" href="css/component.css" />
                          	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
                          	<link rel="stylesheet" href="css/chosen.css" type="text/css" >
                          	<link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
                          </head>
                          <style>
                          	body{background-color: white!important;}
                          	.component{position: relative;
                          		height: 408px;
                          		border: 3px solid #49708A;
                          		width: 576;
                          		overflow: hidden;
                          		}
                          		.canvas-container{
                          			margin: 0;

                          		}
                          	
                          </style>
                          <body>
                          	<div class="test" >
                          		<div class="row">
                          			<div class="col-md-3">
                          				<div class="control_el scroll_top">
                          					<table cellpadding="0" cellspacing="0" border="0" class="edit_text">
                          						<tr>
                          							<td>
                          								<div class="butt_fonts fonts_color tool"  >
                          									<div class="fonts_color_vn" id="color_text">
                          										<span class="text">a</span>
                          										<div class="fonts_color_bl" style="background: none repeat scroll 0% 0% rgb(51, 51, 51);"></div>
                          										<div class="clear"></div>
                          									</div>   
                          								</div>
                          							</td>
                          							<td>
                          								<div class="butt_fonts fonts_b tool block_edit_text" action="bold">b</div>
                          								<div class="butt_fonts fonts_i tool block_edit_text" action="italic">i</div>
                          								<div class="butt_fonts fonts_u last tool block_edit_text" action="underline" >u</div>
                          							</td>
                          						</tr>
                          					</table>
                          				</div>
                          				<div class="center_text_block" style="width: 100%;margin:0 auto">
                          					<div class="add_text_block">
                          						<div class="panel_head">Editing Panel Front</div>
                          						<div class="clear"></div>
                          						<div class="block_show" style="overflow: hidden; display: block;">  
                          							<div class="filds_all">
                          								<div>
                          									<textarea placeholder="Text" class="text_ar" style="resize: none; overflow-y: hidden; position: absolute; top: 0px; left: -9999px; height: 20px; width: 268px; line-height: normal; text-decoration: none solid rgb(0, 0, 0); letter-spacing: normal;" tabindex="-1">
                          									</textarea>
                          									<textarea name="fio_v" placeholder="Text" class="text_ar" style="resize: none; overflow-y: hidden;"></textarea>
                          									<a onclick="del_input(this)" class="del_fil tool" cus="fio_v" title="Delete">
                          									</a>
                          								</div>
                          							</div>  
                          							<button id="text_add" class=" btn btn-success add_fild tool">

                          								<span class="add_fild_text">Add Text</span>

                          							</button>
                          							<input accept="image/jpeg,image/png" type='file' id='file' onchange="readURL(this);" class="btn btn-default addNewFile" style=" margin-top: 7px;opacity: 0;position: relative;z-index: 1;cursor: pointer;width: 75%;"
                          							/>
                          							<button id="img_add" style=" position: relative;bottom: 40px;width: 75%;" class="add_img_btn btn btn-primary"><b>Add Img</b></button>

                          							<p class="load_konstructor"></p>
                          							<br><br>
                          							<button style="position: relative;bottom: 50px;" class="btn btn-danger" onclick="clearAll()">Clear All</button>
                          						</div>
                          					</div>
                          				</div>

                          			</div>
                          			<div class="col-md-9">
                          				<div class="component"><img class="resize-image" id='preview' /><canvas id="canvas1" width="576" height="408"> </canvas>
                          					<div class="overlay">
                          						<div class="overlay-inner">
                          						</div>
                          					</div>
                          				</div>
                          				<button style="margin-top: 15px;" data-toggle="modal" data-target="#readyCoupon"  class="btn btn-primary" onclick="viewCoupon();">View</button>
                          			</div>
                          			<div class="chnage_templ_top"> 
                          				<select name="tema" id="tema" style="visibility: hidden;"></select>   
                          			</div> 
                          		</div>
                          		<div class="modal fade" id="readyCoupon" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
                          			<div class="modal-dialog modal-lg" role="document">
                          				<div class="modal-content">

                          					<div class="modal-body">
                          						<div class="panel-group" id="accordion" style="text-align: center;">
                          							<img style="width: 100%;" class="imageData" src="#" alt="#">
                          							<input type="file" name="imgInput" class="photo" id="imgInput" style="display: none" />
                          						</div>
                          					</div>
                          					<div class="modal-footer">
                                    <button type="button" class="btn btn-primary" style="position: absolute;bottom: 17%;right: 40%;">Redeem  </button>
                          						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          						<button type="button" onclick="saveCoupon()" class="btn btn-success" data-dismiss="modal">Save</button>
                          					</div>
                          				</div>
                          			</div>
                          		</div> 
                          	</div>
                          	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          	<script src="js/libs/tether.min.js"></script>
                          	<script src="js/libs/bootstrap.min.js"></script>
                          	<script src="js/jquery.min.js"></script>
                          	<script src="js/plugins.js"></script> 
                          	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

                          	<script src="js/libs/pace.min.js"></script>
                          	<script src="js/app.js"></script>


                          	<script type="text/javascript" src="js/html2canvas.js"></script>
                          	<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
                          	<script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js" ></script> 
                          	<script type="text/javascript" src="js/colpick.js"></script>
                          	<script src="js/all.min.js" ></script>
                          	<script src="js/construktor.js" defer="defer"></script>
                          	<script type="text/javascript">
                          		var src = '';
  //var canvas = new fabric.Canvas('.component');
  var necessarily_data_array=[];
  var token;
  var filename=0;
  var filekey=1;

  function viewCoupon(){
  	$("#readyCoupon").modal("show");
  }

  if($.cookie('top_position')!=null){
  	top_block_add = $.cookie('top_position');
  }else{top_block_add = "250px";}
  top_position_add = top_block_add.substr(0,3)*1;
  $(".popup_close").click(function(){
  	$(".preview_pop").hide();
  });


  $(".pod_menu_cat_td a").click(function(){
  	id_tovar = $(this).attr("id_tovar"); 
  	name_tovar = $(this).find("span").text();
  	bg_overlayImage = $(this).attr("bg_overlayImage");
  	bg_backgroundImage = $(this).attr("bg_backgroundImage");
  	bg_kontur_img_fade = $(this).attr("bg_kontur_img");
  	price_tovar = $(this).attr("price");
  	$(".konstruktor_page .slider_price_all strong").text(price_tovar);
  	$("[name=id_product]").val(id_tovar);
  	new_url = "/konstruktor.htm?id_outline="+id_tovar;
  	window.history.pushState({path:new_url},'',new_url);
  	$.post("/jscripts/ajax/change_product.php", {id_tovar:id_tovar },
  		function(data){
  			obj = jQuery.parseJSON(data);    
  			$(".info_product_desc").html(obj.text);    
  		});  
  	change_product(bg_overlayImage ,name_tovar,id_tovar, bg_backgroundImage);
  });

  var canvasOperations = {
  	loadFromUrl : function(){
  		fabric.Image.fromURL(src, function(image){
  			canvas.add(image.set({
  				id : 'canvas'+filename,
  				alt : 'canvas'+filename,
  				width : canvas.width / 2,
  				height : canvas.height / 2,
  				perPixelTargetFind : true,
  				targetFindTolerance : 4
  			}));
  		});
  	},
  }

  $(".show_menu_konstr").click(function(){
  	$(".chuse_product_konstruktor").fadeIn(200);
  });
  $(".chuse_product_konstruktor_top").click(function(){
  	$(".chuse_product_konstruktor").fadeOut(200); 
  })

  $( function() {
  	$("#resize").resizable({autoHide:true,alsoResize:'#preview'});
  } );

  function del_input(id){
  	var parent=$(id).parent();
  	$(parent).remove();
  }

  function viewCoupon(){
  	$(".component").css('border', '0px');
  	$('.component').html2canvas({
  		onrendered: function (canvas) {
  			$(".imageData").attr('src',canvas.toDataURL("image/png"));
  			$(".component").css('border', '3px solid #49708A');
  		}
  	});
  }

  function saveCoupon(){
  	var file = $(".imageData").attr('src');
  	var campaignName=$(".campaignName").val();
  	var mobileNumber=$(".mobileNumber").val();
  	if(mobileNumber==''){
  		alert("Enter Mobile Number");
  	}else{
  		$.ajax({
  			url: 'include/coupon/savePostcard.php',
  			data: {file:file,campaignName:campaignName,mobileNumber:mobileNumber},
  			type: 'POST',
  			success:function(){}})
  		window.location='coupon-builder.php';
  	}
  }
  function addNewFile(filename){
  	$(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');  
  }

  function clearAll(){
  	canvas.clear();
  	filename=0;
  	filekey=1;
  	$(".field").each(function(){
  		$(this).hide();
  	})
  }

  function delFile(id,idFile){
  	var key=idFile+1;
  	var name='canvas'+key;
  	console.log(name);
  	var lengthCanvas = canvas['_objects'].length;
  	for(var k in canvas['_objects']){
  		console.log(canvas['_objects'][k]['alt']);
  		if(canvas['_objects'][k]['alt']==name){
  			canvas['_objects'][k].remove();
  			var parent=$(id).parent();
  			$(parent).hide();
  		}
  	}
  	console.log(canvas);
  	filekey--;
  	canvas.renderAll();
  	$(".addNewFile").val('');
  }

  function readURL(input){
  	if (input.files && input.files[0]) {
  		var reader = new FileReader();
  		reader.onload = function (e) {
  			src=e.target.result;
  			canvasOperations.loadFromUrl();
  		};
  		addNewFile(filename);
  		reader.readAsDataURL(input.files[0]);
  		filename++;
  		filekey++;
  	}
  }
  $(".del_fil").click();
</script>

</body>
</html>