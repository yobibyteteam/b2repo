<?php
require_once '../classes/Contact.php';
include '../connect_to_bd.php';
session_start();
$id_input=$_POST['id_input'];
$contact=new Contact();
try{
	$contact->delete_custom_field($id_input);
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}

?>