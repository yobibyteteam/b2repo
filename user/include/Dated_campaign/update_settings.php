<?php
include '../connect_to_bd.php';
require_once '../classes/Dated_campaign.php';
$id=$_POST['id_campaign'];
$name=$_POST['name'];
$type=$_POST['type'];
$dated=$_POST['dated_val'];

$un_campaign=new Dated_campaign();

try{
	$un_campaign->get_name_campaign($id);
	$un_campaign->update_type_campaign($id,$type);
	$un_campaign->update_name_campaign($id,$name);
	$un_campaign->update_dated_campaign($id,$dated);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>