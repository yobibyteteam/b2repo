<?php
/**
* Upload data
*/
class Upload
{
	var $count , $csv_data , $error ,$file_name ,$select_campaign ,$user_id;
	
	function set_data($select_campaign)
	{
		$dir_file='../../csv_files/'.$this->file_name;
		$this->select_campaign=$select_campaign;
		$this->csv_data=array_map('str_getcsv', file($dir_file));
		$this->count=count($this->csv_data);
	}

	function explode_data()
	{
	//echo json_encode($this->csv_data);
		for($i=1;$i<$this->count;$i++)
		{
			$subcats= explode(";", $this->csv_data[$i][0]);
	//echo json_encode($subcats);
			Upload::add_contact($subcats);
		}

	}

	function get_user_id()
	{
		$token=$_SESSION['user_token'];
		$query_model="SELECT * FROM token WHERE token='".$token."'";
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)==0){
			throw new Exception("not_found_token", 1);
		}else{
			$array_token=mysql_fetch_array($query);
			$this->user_id=$array_token['client_id'];
		}
	}

	function add_contact($array)
	{
	//echo json_encode($array);
		$query_model="SELECT * FROM contacts WHERE email ='".$array[2]."' ";
		$query=mysql_query($query_model);
	//var_dump($array[15]);
		if(mysql_num_rows($query)==0){
			$date_current=date("Y-m-d");
			$md5=md5($date_current).mt_rand();
			$query_model="INSERT INTO `contacts`( `first_name`, `last_name`, `email`, `mobile_phone`, `alt_phone`, `address`, `address1`, `city`, `state`, `postal_code`, `date_of_birth`, `religion`, `type`, `status`, `assigned_campaign`, `spouse_first_name`, `spouse_last_name`, `spouse_email`, `spouse_mobile`, `spouse_alternate`, `spouse_date_of_birth`, `spouse_religion`, `child_name`, `child_dob`, `product_service_purchased`, `user_id` , `data_register`,`md5_hash`) VALUES ('".$array[0]."','".$array[1]."','".$array[2]."','".$array[3]."','".$array[4]."','".$array[5]."','".$array[6]."','".$array[7]."','".$array[8]."','".$array[9]."','".date("Y-m-d", strtotime($array[10]))."','".$array[11]."','Prospect','Active','".$this->select_campaign."','".$array[12]."','".$array[13]."','".$array[14]."','".$array[15]."','".$array[16]."','".date("Y-m-d", strtotime($array[17]))."','".$array[18]."','".$array[19]."','".date("Y-m-d", strtotime($array[20]))."','".$array[21]."','".$this->user_id."','".$date_current."','".$md5."')";
			mysql_query($query_model);
	//echo json_encode($array);
		}
	}

	function delete_file()
	{
		$filename="../../csv_files/".$this->file_name;
		unlink($filename);
	}


	function load_image($file_image)
	{
		$this->file_name=Upload::str2url(rand(5,15).time().basename($file_image['name']));
		$uploaddir='../../csv_files/';
		$uploadfile=$uploaddir.$this->file_name;
		copy($file_image['tmp_name'], $uploadfile);
	}

	function rus2translit($string)
	{
		$converter = array(
			'а' => 'a',   'б' => 'b',   'в' => 'v',
			'г' => 'g',   'д' => 'd',   'е' => 'e',
			'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
			'и' => 'i',   'й' => 'y',   'к' => 'k',
			'л' => 'l',   'м' => 'm',   'н' => 'n',
			'о' => 'o',   'п' => 'p',   'р' => 'r',
			'с' => 's',   'т' => 't',   'у' => 'u',
			'ф' => 'f',   'х' => 'h',   'ц' => 'c',
			'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
			'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
			'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

			'А' => 'A',   'Б' => 'B',   'В' => 'V',
			'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
			'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
			'И' => 'I',   'Й' => 'Y',   'К' => 'K',
			'Л' => 'L',   'М' => 'M',   'Н' => 'N',
			'О' => 'O',   'П' => 'P',   'Р' => 'R',
			'С' => 'S',   'Т' => 'T',   'У' => 'U',
			'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
			'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
			'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
			'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
			);
		return strtr($string, $converter);
	}

	function str2url($str)
	{
		$str = Upload::rus2translit($str);
		$str = strtolower($str);
		$str = preg_replace('[^-a-z0-9_]', '-', $str);
		$str=preg_replace('/\s/','',$str);
		$str = trim($str, "-");
		return  $str;
	}

}
?>