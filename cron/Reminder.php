<?php
require_once 'Twilio/autoload.php'; 
use Twilio\Rest\Client;
class Reminder{
	private $currentDate , $arrayReminders ,$arrayPhone ,$arrayDopPhone;
	
	public function __construct(){
		$this->currentDate = date("Y-m-d");
		Reminder::getAllReminder();
	}

	private function getAllReminder(){
		$queryModel = "SELECT * FROM reminder";
		$query = mysql_query($queryModel);
		if(mysql_num_rows($query) != 0){
			while($res = mysql_fetch_assoc($query)) {
				$arrayClientId[]=array($res['id'],$res['user_id']);
				$this->arrayReminders[] = $res;
			}
		}
		//echo json_encode($this->arrayReminders);
		Reminder::getAllPhoneNumber($arrayClientId);
	}

	public function remindMe(){
		Reminder::checkTime();
	}

	private function diffTime($time,$delayMin){
		$time2 = strtotime($time) - $delayMin * 60; 
		return date('H:i', $time2);
	}

	private function checkTime(){
		foreach ($this->arrayReminders as $key => $value) {
			$diffTime=Reminder::diffTime($value['time'],$value['delay_min']);
			$currentTime=date('H:i');
			if($diffTime==$currentTime && $this->currentDate==$value['date']){
				Reminder::sendSmsOnPhone($value);
				Reminder::sendSmsOnDopPhone($value);
				Reminder::deleteRemind($value['id']);
			}
		}
	}

	private function sendSmsOnPhone($idUser){
		foreach ($this->arrayPhone as $key => $value) {
			if($idUser['id']==$key){
				Reminder::sendingSms($value[1]);
			}
		}
	}

	private function sendSmsOnDopPhone($idUser){
		foreach ($this->arrayDopPhone as $key => $value) {
			if($idUser['id']==$key){
				foreach ($value[1] as $key => $kvalue) {
					Reminder::sendingSms($kvalue);	
				}
			}
		}
	}

	private function sendingSms($mobile){
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		$message = $client->messages->create(
			'+'.$mobile, 
			array(
				'from' => '+17202509876',
				'body' => 'Remind Success'
				)
			);

	}

	private function deleteRemind($idRemind){
		$queryModel="DELETE FROM reminder WHERE id='".$idRemind."'";
		mysql_query($queryModel);
	}

	private function getAllPhoneNumber($array){
		foreach ($array as $key => $value) {
			$this->arrayPhone[$value[0]]=array($value[1],Reminder::getClientPhoneNumber($value[1]));
			$this->arrayDopPhone[$value[0]]=array($value[1],Reminder::getDopPhoneNumber($value[1]));
		}
	}

	private function getClientPhoneNumber($clientId){
		$arrayData=[];
		$queryModel = "SELECT on_call_phone_number FROM user WHERE id='".$clientId."'";
		$query = mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		if($arrayData['on_call_phone_number']!=''){
			return $arrayData['on_call_phone_number'];
		}
	}

	private function getDopPhoneNumber($clientId){
		$arrayData=[];
		$queryModel = "SELECT phone FROM user_dop_phone WHERE user_id='".$clientId."'";
		$query = mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['phone']!=''){
					$arrayData[]=$res['phone'];
				}	
			}
		}
		return $arrayData;
	}
}

?>