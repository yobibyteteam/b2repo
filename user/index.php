<?php
include "include/session.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'components/header.php'; ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">      
            <ol class="breadcrumb mb-0">
              <li class="breadcrumb-item active ">Home / </li>
          </ol>
          <div class="container-fluid">
            <div id="activity" style="min-width: 310px; height: 400px; margin: 0 auto">
            </div><br>
            <div class="row">
                <div class="col-md-6 text-center">
                    <div class="card-box">
                        <div class="card-header card-header-inverse card-header-primary">
                          <div class="font-weight-bold mb-3">
                              Profit from Coupons </div>
                              <div class="chart-wrapper mb-3">
                                <canvas id="chart-56" style="height: 200px;" class="chart chart-line"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div style="width: 350px; height: 250px;">
                        <canvas style="width: 350px; height: 250px; position:relative; left: -21px; " id="canvas-5"></canvas>
                    </div>
                </div>
                <!--<button type="button" class="btn btn-primary" id="show_feedback">Show Toast</button>
                <button type="button" class="btn btn-danger" id="show_completed_form">Clear Toasts</button>-->
            </div>
        </div>
    </div>
</div>
</main>
</div>
<!-- Bootstrap and necessary plugins -->
<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
<script src="js/views/charts.js"></script>
<script src="js/views/widgets.js"></script>
<script src="js/views/main.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="js/libs/toastr.min.js"></script>
<script src="js/libs/Chart.js"></script>
<script src="js/app.js"></script>
<script src="js/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="js/dashboard.js"></script>

<!-- Notifications JS START -->
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
<!-- Notifications JS END -->
</body>
</html>