$(function (){
  'use strict';





  var doughnutData = {
    labels: [
      'RVM ',
      'Text ',
      'PostCard',
      'Email '
    ],
    datasets: [{
      data: [40, 20, 10, 30],
      backgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56',
        'green'
      ],
      hoverBackgroundColor: [
        '#FF6384',
        '#36A2EB',
        '#FFCE56',
        'green'
      ]
    }]
  };
  var ctx = document.getElementById('canvas-3');
  var chart = new Chart(ctx, {
    type: 'doughnut',
    data: doughnutData,
    options: {
      responsive: true
    }
  }); 
});


