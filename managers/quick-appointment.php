<?php 
include "include/session.php";
include 'include/connect_to_bd.php';

function getClientId($token){
  $query_model="SELECT client_id FROM token WHERE token='".$token."'";
  $query=mysql_query($query_model);
  $array_data=mysql_fetch_assoc($query);
  return $array_data['client_id'];
}

function get_list_conacts(){
  $clientId=getUserId(getClientId($_SESSION['user_token']));
  $query_model="SELECT * FROM contacts WHERE user_id='".$clientId."'";
  $query=mysql_query($query_model);
  $i=0;
  while($res=mysql_fetch_assoc($query)){
    if($i==0){
      echo "'".$res['first_name'].'(ID:'.$res['id'].")'";
    }
    else{
      echo ",'".$res['first_name'].'(ID:'.$res['id'].")'";
    }
    $i++;
  }  
}

function getUserId($managerId){
  $queryModel="SELECT user_id FROM managers WHERE id='".$managerId."'";
  $query=mysql_query($queryModel);
  $arrayData=mysql_fetch_assoc($query);
  return $arrayData['user_id'];
}

function get_list_appointment(){
  $clientId=getUserId(getClientId($_SESSION['user_token']));
  $query_model="SELECT * FROM quick_appointment WHERE user_id='".$clientId."'";
  $query=mysql_query($query_model);
  $i=0;
  while($res=mysql_fetch_assoc($query)){
    echo '<tr>
    <td>'.$res['username'].'</td>
    <td>'.$res['date'].'</td>
    <td>'.$res['time'].' PM</td>
    <td>'.$res['notes'].'</td>
    <td class="table_button" style="width:113px!important;">
      <button onclick="write_curr_id('.$res['id'].')" type="button" class="btn btn-danger btn_user_table_danger" data-toggle="modal" data-target="#dangerModal">
        <i class="fa fa-trash-o "></i>
      </button>

    </td>
  </tr>';
}  
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <title>CRM System</title>

  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
    .twitter-typeahead{
      display: block!important;
    }
    .tt-menu{
      width: 100%;
      background-color: white;
      border:2px solid #e1e6ef;
      padding: 5px;
    }
    .blur{
      -webkit-filter: blur(2px);
      -moz-filter: blur(2px);
      -o-filter: blur(2px);
      -ms-filter: blur(2px);
      filter: blur(2px);
    }
    .search_form{
      -webkit-filter: blur(0px);
      -moz-filter: blur(20px);
      -o-filter: blur(0px);
      -ms-filter: blur(0px);
      filter: blur(0px);
    }
    .timeEntry-control{
      display: none!important;
    }
  </style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
    <a class="navbar-brand" href="#"></a>
    <ul class="nav navbar-nav d-md-down-none b-r-1">
      <li class="nav-item">
        <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
      </li>

    </ul>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item dropdown" style="margin-right: 30px!important">
        <a class="nav-link nav-pill avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img src="img/avatars/10.png" class="img-avatar" alt="admin@bootstrapmaster.com">
        </a>
        <div class="dropdown-menu dropdown-menu-right">

          <div class="dropdown-header text-center">
            <strong>Account</strong>
          </div>
          <a class="dropdown-item" href="include/logout_user.php"><i class="fa fa-lock"></i> Logout</a>
        </div>
      </li>
    </ul>
  </header>

  <div class="app-body">
    <?php include "sidebar.php"; ?>

    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="user-profile.php">Home</a></li>
        <li class="breadcrumb-item active">Quick Appointment</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="card">
            <div class="card-header">
              <strong>Quick Appointment</strong>
            </div>

            
            <div class="card-block">
              <div class="form-group">

                <div  class="form-group has-feedback">
                  <label class="form-control-label" for="text-input">Contacts search:</label>
                  <div id="the-basics">
                    <input onkeypress="change_input_data(this)" class="typehead_check input_data form-control typeahead" data-toggle="popover" data-placement="bottom" type="text" placeholder="User search">
                  </div>
                </div>
                <div class="form-group has-feedback">
                  <label class="form-control-label" for="text-input">Appointment Date:</label>
                  <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control" id="dated" placeholder="Select Date">
                </div>
                <div class="form-group has-feedback">
                  <label class="form-control-label" for="text-input">Time:</label>
                  <input name="ffgfdgfd" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Enter Time" id="defaultEntry">
                </div>
                <div class="form-group has-feedback">
                  <label class="form-control-label" for="text-input">Notes:</label>
                  <input  onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder=" Enter Notes">
                </div>


              </div>
              <div class=" py-4 text-center">
                <button onclick="add_new_appointment()" class="btn btn-primary"> Add Appointment</button>
                <button style="margin-left: 30px;display: none;padding: 0.5rem 2rem;" type="button" class="btn danger_btn btn-outline-danger">Error</button>
              </div>
            </div>

            <div class="card-header">
              <i class="fa fa-edit"></i> Appointment
              <div class="card-actions">

              </div>
            </div>
            <div class="card-block">
              <table class="table table-striped table-bordered datatable table-responsive">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Notes</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php get_list_appointment(); ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
</main>
<div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button onclick="delete_appointment()" type="button" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
</div>
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="js/libs/jquery.plugin.js"></script>
<script src="js/libs/jquery.timeentry.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!-- Plugins and scripts required by this views -->
<script src="js/libs/jquery.dataTables.js"></script>
<script src="js/libs/dataTables.bootstrap4.js"></script>
<!-- Custom scripts required by this view -->
<script src="js/views/tables.js"></script>
<script>
  $( function() {
    $( "#dated" ).datepicker({ dateFormat: 'mm/dd/yy' });
  } );
</script>
<script>
  $(function () {
    $('#defaultEntry').timeEntry();
  });
</script>
<script>
  var curr_id_appoint;

  function write_curr_id($id){
    curr_id_appoint=$id;
  }

  function delete_appointment(){
   $.ajax({
    url:'include/quick_appointment/delete_appointment.php',
    type:'POST',
    data:{curr_id_appoint:curr_id_appoint},
    success:function(){
      window.location="quick-appointment.php";
    }
  })
 }
</script>
<script>
 var array_data=[];

 function add_new_appointment()
 {
  try{
    check_input_data();
    build_array();
    set_ajax_query(array_data);
  }catch(e){
    console.log(e);
  }
}

function build_array(){
  array_data=[];
  $('.input_data').each(function(){
    array_data.push($(this).val());
  })
}

function set_ajax_query(array){
  $.ajax({
    url:"include/quick_appointment/add_new_quick_appointment.php",
    data:{array:array},
    type:"POST",
    success:function(data){
      //alert(data);
      parsing_data(data);
    }
  })
}

function check_input_data()
{
  $('.input_data').each(function(){
    if($(this).val() == ''){
      notification(this,'Empty Data');
      throw new SyntaxError("empty_data");
    }
  })
}

function change_input_data(id){
  var parent=$(id).parent();
  $(id).removeClass('form-control-danger');
  $(parent).removeClass('has-danger');
}

function notification(id,type_error){
  var parent=$(id).parent();
  $(parent).addClass('has-danger');
  $(id).addClass('form-control-danger');
  $(id).attr("data-content",type_error);
  $(id).popover('show');
  $('.danger_btn').css("display","inline-block");
  setTimeout(function(){
    $(id).popover('dispose');
    $('.danger_btn').css("display","none");
  },1500);
}

function parsing_data(arr){
  var json=JSON.parse(arr);
  if(json['success']==1){
    window.location = 'quick-appointment.php';
  }else{
    if(json['error']=='error_type'){
      alert("Сontact is in the company");
    }else{
      $(".danger_btn").css('display','inline-block');
      setTimeout(function(){
        $(".danger_btn").css('display','none');
      },1000);
      alert(json['error']);
    }
  }
}

</script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script>
  var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
      var matches, substrRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        // the typeahead jQuery plugin expects suggestions to a
        // JavaScript object, refer to typeahead docs for more info
        matches.push({ value: str });
      }
    });

    cb(matches);
  };
};

var states = [<?php get_list_conacts(); ?>
];

$('#the-basics .typeahead').typeahead({
  hint: false,
  highlight: true,
  minLength: 1
},
{
  name: 'states',
  displayKey: 'value',
  source: substringMatcher(states)
});
</script>
</body>

</html>