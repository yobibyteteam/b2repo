<?php
require_once 'Twilio/autoload.php'; 
use Twilio\Rest\Client;
require_once 'Mailgun/vendor/autoload.php';
use Mailgun\Mailgun;
require_once 'user/include/classes/LeadsRain.php';
class Cron_Broadcast extends Broadcast{

	function __construct(){}

	function getAllBroadcast(){
		$arrayData=[];
		$sql="SELECT * FROM broadcast";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['delay_day']==date("Y-m-d")){
					$arrayData[]=$res;
				}
			}
			return $arrayData;
		}
	}

	function getAllRvmd($key){
		$arrayData=[];
		$sql="SELECT `filename` FROM broadcast_rvmd WHERE campaign_builder_id='".$key."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData;
	}

	function getContactsFromCampaign($id,$param,$param_val){
		$arrayData=[];
		switch ($param) {
			case 'Religion':
			$param=strtolower($param);
			break;
			case 'Birthday':
			$param='date_of_birth';
			$param_val=date("Y-m-d",strtotime($param_val));
			break;
		}
		$sql="SELECT email,mobile_phone,user_id,first_name,last_name,address,country,city,state,postal_code FROM contacts WHERE assigned_campaign='".$id."' AND assign_campaign_type='campaign' AND ".$param."='".$param_val."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayData[]=$res;
			}
			return $arrayData;
		}
	}
	public function getUserPhotoDir($id){
		$queryModel="SELECT dir_img FROM user WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['dir_img'];
	}

	public function renameFile($fileName){
		$newFile=substr($fileName, 0, -4);
		return $newFile.'.pdf';
	}

	public function sendPostcard($dirImg,$frontFile='',$backFile='',$clientId,$arrayAddress,$delayDay){
		$frontLink='https://tracs.me/upload_photo/'.$dirImg.'/'.$frontFile;
		$backLink='https://tracs.me/upload_photo/'.$dirImg.'/'.$backFile;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://rest.clicksend.com/v3/post/postcards/send");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{
			\"file_urls\": [
				\"".$frontLink."\",
				\"".$backLink."\"
			],
			\"recipients\": [
				{
					\"address_name\": \"".$arrayAddress['first_name']." ".$arrayAddress['last_name']."\",
					\"address_line_1\": \"".$arrayAddress['address']."\",
					\"address_line_2\": \"\",
					\"address_city\": \"".$arrayAddress['city']."\",
					\"address_state\": \"".$arrayAddress['state']."\",
					\"address_postal_code\": \"".$arrayAddress['postal_code']."\",
					\"address_country\": \"".$arrayAddress['country']."\",
					\"return_address_id\": 1,
					\"custom_string\": \"\"
				}
			]
		}");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			"Content-Type: application/json",
			"Authorization: Basic Y29udGFjdFRSQUNTOjBGOTc5RTg5LTAyQzUtNzMxNy02NjVDLUZDNEJEOUI0MURBNg=="
		));
		$response = curl_exec($ch);
		$response=json_decode($response);
		if($response->http_code=='200'){
			$countPrice=$this->getCountPrice($clientId,'postcard');
			$this->addToStatistic($clientId,'campaign','postcard');
			$this->addBillingStat($clientId,$countPrice,'postcard');
			$this->addMoreValue($clientId,'postcard');
		}
	}
	protected function addMoreValue($id,$type){
		switch ($type) {
			case 'email':
			$type='type_email';
			break;
			
			case 'text':
			$type='type_text';
			break;

			case 'postcard':
			$type='type_postcard';
			break;

			case 'rvmd':
			$type="type_rvm";
			break;
		}
		$sql='UPDATE `payment` SET '.$type.'='.$type.'+1 WHERE user_id='.$id.'';
		mysql_query($sql);
	}

	function getAllPostcard($key){
		$arrayData=[];
		$queryModel="SELECT * FROM postcard WHERE campaign_builder_id='".$key."' AND type_campaign='broadcast' AND type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['type_postcard']=='back'){
					$arrayData['back']=$res;
				}else{
					$arrayData['front']=$res;
				}
			}
		}
		return $arrayData;
	}

	function getAllContacts($param,$param_val){
		switch ($param) {
			case 'Religion':
			$param=strtolower($param);
			break;
			case 'Birthday':
			$param='date_of_birth';
			$param_val=date("Y-m-d",strtotime($param_val));
			break;
		}
		$sql="SELECT email,mobile_phone,user_id,first_name,last_name,address,country,city,state,postal_code FROM contacts WHERE  assign_campaign_type='campaign' AND ".$param."='".$param_val."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayData[]=$res;
			}
			return $arrayData;
		}	
	}

	function getAllCampain($id,$param,$param_val){
		$arrayData=[];
		$sql="SELECT * FROM broadcast_campaign WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['campaign']=='0'){
					$arrayData=$this->getAllContacts($param,$param_val);
				}else{
					$arrayData=$this->getContactsFromCampaign($res['campaign'],$param,$param_val);
				}
			}
		}
		return $arrayData;
	}

	function getAllEmail($id){
		$arrayData=[];
		$sql="SELECT `text` FROM broadcast_email WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData;
	}

	function getAllText($id){
		$arrayData=[];
		$sql="SELECT `text` FROM broadcast_text WHERE campaign_builder_id='".$id."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData;
	}
	public function addToStatistic($userId,$typeCampaign,$type){
		$date=date("Y-m-d");
		$queryModel="INSERT INTO `statistics`(`user_id`, `type_campaign`, `date`,`type`) VALUES ('".$userId."','".$typeCampaign."','".$date."','".$type."')";
		mysql_query($queryModel);
	}

	function checkLeadsRainCampaign($id){
		$arrayData='';
		$query_model="SELECT leadsrain_campaign_id,leadsrain_list_id FROM broadcast_rvmd WHERE campaign_builder_id='".$id."'";
		echo $query_model;
		$query=mysql_query($query_model);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
		}
		return $arrayData;
	}

	function deleteCampaign($id){
		$sql="DELETE FROM broadcast WHERE id='".$id."'";
		mysql_query($sql);
		$sql="DELETE FROM broadcast_campaign WHERE campaign_builder_id='".$id."'";
		mysql_query($sql);
		$sql="DELETE FROM broadcast_email WHERE campaign_builder_id='".$id."'";
		mysql_query($sql);
		$arrayLeadsrain=$this->checkLeadsRainCampaign($id);
		if($arrayLeadsrain!=''){
			$leadsRain=new LeadsRain();
			$leadsRain->deleteList($arrayLeadsrain['leadsrain_list_id']);
			$leadsRain->deleteCampaign($arrayLeadsrain['leadsrain_campaign_id']);
		}
		$sql="DELETE FROM broadcast_rvmd WHERE campaign_builder_id='".$id."'";
		mysql_query($sql);
		$sql="DELETE FROM broadcast_text WHERE campaign_builder_id='".$id."'";
		mysql_query($sql);
		$sql="DELETE FROM broadcast_text WHERE campaign_builder_id='".$id."' AND type_campaign='broadcast'";
		mysql_query($sql);
	}

	protected function getCountPrice($id,$type){
		switch ($type) {
			case 'email':
			$type='base_price';
			break;
			case 'text':
			$type='text_price';
			break;
			case 'postcard':
			$type="postcard_price";
			break;
			case 'rvmd':
			$type="rvm_price";
			break;
		}
		$sql="SELECT ".$type." FROM user WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData[$type];
	}

	protected function addBillingStat($id,$count,$type){
		$sql='INSERT INTO `billing_stat`( `user_id`, `count`,`type`) VALUES ("'.$id.'","'.$count.'","'.$type.'")';
		mysql_query($sql);
	}

	protected function updateTextData($email,$text){
		$queryModel="SELECT first_name,last_name,assign_campaign_type,user_id,mobile_phone,email,address,address1,city,state,postal_code FROM contacts WHERE email='{$email}' OR spouse_email='{$email}'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			$arrayData=mysql_fetch_assoc($query);
			foreach ($arrayData as $key => $value) {
				$type='{$'.$key.'}';
				$request=strpos($text,$type);
				if($request){
					$text=str_replace($type, $value, $text);
				}
			}
		}
		return array('text'=>$text,'user_id'=>$arrayData['user_id'],'type'=>$arrayData['assign_campaign_type']);
	}


	function sendEmail($text,$email,$id){
		$mgClient = new Mailgun(KEY);
		$domain = DOMAIN;
		$text=$this->updateTextData($email,$text);
		$result = $mgClient->sendMessage("$domain",
			array('from'    =>  '<crm@tracs.me>',
				'to'      => 'Mailgun Devs '.$email.'',
				'subject' => 'Hello',
				'html'    => $text));
		$typeAnt=$result->http_response_code;
		if($typeAnt=200){
			$countPrice=$this->getCountPrice($id,'email');
			$this->addToStatistic($id,'campaign','email');
			$this->addBillingStat($id,$countPrice,'email');
			$this->addMoreValue($id,'email');
		}
	}

	private function getMobileCampaign($id){
		$sql="SELECT assign_num FROM broadcast WHERE id='".$id."'";
		$query=mysql_query($sql);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['assign_num'];
	}


	function sendText($text='',$mobile,$id,$key){
		$from=$this->getMobileCampaign($key);
		$mobile='+'.$mobile;
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		$message = $client->messages->create(
			$mobile, 
			array(
				'from' => $from,
				'body' => $text
			)
		);
		if($message->status=='queued'){
			$countPrice=$this->getCountPrice($id,'text');
			$this->addToStatistic($id,'campaign','text');
			$this->addBillingStat($id,$countPrice,'text');
			$this->addMoreValue($id,'text');
		}
	}

	function getUserId($id){
		$sql="SELECT client_id FROM broadcast WHERE id='".$id."'";
		$query=mysql_query($sql);
		if(mysql_num_rows($query)!=0){
			$clientId=mysql_fetch_assoc($query);
		}
		return $clientId['client_id'];
	}
}

?>