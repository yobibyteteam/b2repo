<?php
include "include/session.php";
include 'include/connect_to_bd.php';
if(!isset($_GET['token'])){
  header("Location: campaign-overview.php");
}else{
  $token=$_GET['token'];
  $query_model="SELECT * FROM campaign_builder WHERE md5='".$token."'";
  $query=mysql_query($query_model);
  $array_token=mysql_fetch_assoc($query);
  $id_campaign=$array_token['id'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <!-- Include Editor style. -->
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_style.min.css' rel='stylesheet' type='text/css' />
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  body{
    position: relative;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }
  .tableExist{
    font-weight: 700;
    font-size: 16px;
    color:red;
    margin-bottom: 0px!important;
  }
  .photoDataToInsert img{
    width: 50%;
  }
  .iframe_1_2,.iframe_2_2{
    display: block;
    width: 678px;
    height: 480px;
    margin: 0 auto 50px auto;
  }
  .iframe_1_2 img,.iframe_2_2 img{
    width: 100%;
    height: 100%;
  }
  @media (max-width: 414px){
    #span{
      display: none;
    }
    .side{
      display: initial;
    }
  }
</style>

</head>
<style>
</style>
<body name="<?php echo $id_campaign; ?>" id="<?php echo $token; ?>" class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden" >
  <div class="idCampaign" name="<?php echo $id_campaign; ?>"></div>
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
  <?php include 'components/header.php'; ?>
  <div class="app-body">
   <?php include "sidebar.php"; ?>
   <!-- Main content -->
   <main class="main">
    <ol class="breadcrumb mb-0">
      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
      <li class="breadcrumb-item active">Overview Universal Campaign</li>
    </ol>
    <div class="container-fluid">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-sm-12"><br>
            <div class="card">
             <div style="padding-top:24px;">
              <div class="panel-group" id="accordion" >
                <div class="col-md-12 mb-4">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" onclick="getlist('email');" id="email_btn" data-toggle="tab" href="#email_center" role="tab" aria-controls="home"><span id="span">Email Center</span> <i class="fa fa-envelope-o "></i></a>
                    </li>
                    <li class="nav-item">
                      <a  onclick="getlist('rvmd');" id="voice_btn" class="nav-link" data-toggle="tab" href="#voice_center" role="tab" aria-controls="profile"><span id="span">Voice Center</span>  <i class="fa fa-microphone"></i></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="text_btn" onclick="getlist('text');" data-toggle="tab" href="#text_center" role="tab" aria-controls="messages"><span id="span">Text Center</span> <i class="fa fa-file-text-o"></i></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" onclick="closeSideBar();" id="postcard_btn" data-toggle="tab" href="#postcard_center" role="tab" aria-controls="messages"><span id="span">Postcard Center</span> <i class="fa fa-newspaper-o"></i></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="settings_btn" onclick="getlist();" data-toggle="tab" href="#settings" role="tab" aria-controls="settings"><span id="span">Campaign Settings</span> <i class="fa fa-wrench"></i></a>
                    </li>
                  </ul>

                  <div class="tab-content">
                    <div class="tab-pane active" id="email_center" role="tabpanel" style="">

                      <div class="form-group close_tab1">
                        <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which emails bounce. A full report is available to you
                        under logs on your dashboard.</p>
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab1','variable1')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>
                      </div>
                      <div class=" col-md-12 variable1" style="display: none">
                        <div class="form-group">
                          <input type="text" class="form-control" id="company" placeholder="Enter email subject">
                        </div>
                        <a href="#ready_email" onclick="readyDataShow('email')" >View Current Emails</a><br>
                        <textarea name="editor_email" id="editor_email" rows="10" cols="80">
                        </textarea>
                        <br>
                        <div class="form-group has-feedback">
                          <span>Include:</span>
                          <button onclick="signature('email')" style="padding: 5px; margin: 2px;" class="btn btn-primary">Email Signature</button>
                          <button onclick="signature('footer')" style="padding: 5px; margin: 2px;" class="btn btn-primary">Email Footer</button>
                          <button style="padding: 5px; margin: 2px;" class="btn btn-primary">Picture</button>
                          <button style="padding: 5px; margin: 2px;" class="btn btn-primary">Logo</button>
                          <div style="margin-top: 10px;" class="radio">
                            <span style="margin-left: 3px;"> Send in<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_email" value="0" type="number"  name="text" style="width: 60px; height: 30px; margin:0px 5px;" id="delay_day_email">days </span>
                            <br>
                            <span><input type="checkbox" class="repeat_email" style="margin:10px 3px;">Repeat every year</span>
                          </div>
                        </div>
                        <table class="table table-striped table-bordered datatableEmail table-responsive" id="calendar_info" border=1>
                        </table>
                        <button class="btn btn-primary" onclick="addNewDays()">Loar More</button><br>
                        <div class="col-md-12 py-4 text-center">
                          <button data-toggle="tab" role="tab" href="#" onclick="email_show()" class="btn btn-primary" >Save </button>
                          <button style="display: none;" class="success_btn btn btn-success">Success</button>
                        </div>
                      </div>

                    </div>
                    <div class="tab-pane" id="voice_center" role="tabpanel">
                      <div class="form-group close_tab2">
                        <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which numbers do not accept the message and only
                          charged for delivered messages. A full report is available to you under logs on your dashboard.
                        </p>
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab2','variable2')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>

                      </div>
                      <div class="col-md-12 form-group variable2" style="display: none">
                        <a href="#ready_rvmd" onclick="readyDataShow('rvmd')">View Current RVMD</a>
                        <h1>
                        </h1>
                        <label for="name_rvmd">Record Name:</label>
                        <input type="text" class="form-control name_rvmd" placeholder="Enter record name"><br>
                        <input type="file" onchange="load()" id="import_audio" class="import_audio" accept="audio/wav">
                        <section class="experiment">
                          <p style="text-align: center;">
                            <video id="preview" controls style="display: none;border: 1px solid rgb(15, 158, 238); height: 240px; max-width: 100%; vertical-align: top; width: 320px;"></video>
                          </p>
                          <button class="btn btn-primary" id="record">Record</button>
                          <button class="btn btn-danger" id="stop" disabled>Stop</button><br>
                          <div id="container" ></div>
                        </section><br>
                        <div class="audio_list">
                        </div>
                        <br>
                        <div class="form-group has-feedback">
                          <div class="radio">
                            <span style="margin-left: 3px;"> Send in<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_rvmd" value="0" type="number" name="text" style="width: 60px; height: 30px; margin:0px 5px;" id="delay_day_voice">days </span>
                            <br>
                            <span><input type="checkbox" class="repeat_rvmd" style="margin:10px 3px;">Repeat every year</span>
                          </div>
                        </div>
                        <table class="table table-striped table-bordered datatableEmail table-responsive" id="calendar_info" border=1>
                        </table><br>
                        <button class="btn btn-primary" onclick="addNewDays()">Load More</button>
                        <div class="col-md-12 py-4 text-center">
                          <button onclick="save_voice()" class="btn btn-primary">Save </button>
                          <button style="display: none;" class="success_btn btn btn-success">Success</button>
                        </div>
                      </div>
                    </div>


                    <div class="tab-pane" id="text_center" role="tabpanel">
                      <div class="form-group close_tab3">
                        <label for="company"><p>Note: Note all messages are not guaranteed delivery. You will be notified of which texts bounce. A full report is available to you
                        under logs on your dashboard.</p>
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab3','variable3')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>
                      </div>

                      <div class="col-md-12 form-group variable3" style="display: none">
                       <a href="#ready_text" onclick="readyDataShow('text')">View Current Text</a>
                       <textarea name="text_editor" id="text_editor" rows="10" cols="80">
                       </textarea>
                       <br>
                       <div class="form-group has-feedback">
                        <div class="radio">
                          <span style="margin-left: 3px;"> Send in<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_text" value="0" type="number" name="text" style="width: 60px; height: 30px; margin:0px 5px;" id="delay_day_text">days </span>
                          <br>
                          <span><input type="checkbox" class="repeat_text" style="margin:10px 3px;">Repeat every year</span>
                        </div>
                      </div>
                      <table class="table table-striped table-bordered datatableEmail table-responsive" id="calendar_info" border=1>
                      </table>
                      <button class="btn btn-primary" onclick="addNewDays()">Load More</button><br>
                      <div class="col-md-12 py-4 text-center">
                        <button onclick="save_text()" class="btn btn-primary">Save </button>
                        <button style="display: none;" class="success_btn btn btn-success">Success</button>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="postcard_center" role="tabpanel">
                    <div class="form-group close_tab4">
                      <label for="company"><p>Note: Note all messages are guaranteed delivery. You are charged for all postcards sent. If you receive a card back you must update the address or remove address from contacts information to stop any further attempts.</p>
                        <p>You must agree to terms and conditions before proceeding:</p></label>

                        <div class="checkbox">
                          <label>
                            <input onchange="agree_check(this,'close_tab4','variable4')" type="checkbox" id="agree" name="agree" value="agree">
                            <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                          </label>
                        </div>
                      </div>

                      <div class="col-md-12 form-group variable4" style="display: none">

                       <main>
                        <div class="col-md-12" style="padding: 0;">
                          <div class="card" style="border:0; padding: 0;">
                            <div class="card-block p-0">
                              <div class="tab-content">
                                <div class="tab-pane active" id="front" style="padding: 0;overflow: auto;">
                                  <div class="iframe_1" style="overflow:auto;" style="margin-top: 5px;">
                                    <iframe scrolling="no" style=" width: 1100px; height: 650px; border: none" src="postcard-front.php" frameborder="0"></iframe>
                                  </div>
                                  <div class="iframe_1_2" style="display: none">
                                    <img class="photo_front" alt="">
                                    <div class="text-center">
                                      <button class="btn btn-success" onclick="updatePostcardFront()">Update Postcard Front</button>
                                    </div>
                                  </div>
                                </div>
                                <div class="tab-pane" id="back" style="padding:0;overflow: auto;style="margin-top: 5px;"">
                                  <div class="iframe_2" style="overflow: auto;" >
                                    <iframe scrolling="no" src="postcard-back.php" style="width: 1100px; height: 650px;" frameborder="0"></iframe>
                                  </div>
                                  <div class="iframe_2_2" style="display: none">
                                    <img class="photo_back" alt="">
                                    <div class="text-center">
                                      <button class="btn btn-success" onclick="updatePostcardBack()">Update Postcard Back</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <ul class="nav side" role="tablist" style="text-align:center; position:relative;">
                              <li class="nav-item">
                                <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab"><img src="../img/front.jpg" style="width:100px; height:100px;"><br>Front side</a>
                              </li>
                              <li class="nav-item">
                                <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab"><img src="../img/back.jpg" style="width:100px; height:100px;"><br>Back side</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </main>

                      <div class="form-group has-feedback">
                        <div class="radio">
                          <span style="margin-left: 3px;"> Send in<input onchange="change_input_data(this)" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day_postcard" value="0" type="number" name="text" style="width: 60px; height: 30px; margin:0px 5px;" id="delay_day_postcard">days </span>
                          <br>
                          <span><input type="checkbox" onchange="changeRepeat(this)" class="repeat_postcard" style="margin:10px 3px;">Repeat every year</span>
                        </div>
                      </div>
                      <div class="col-md-12 py-4 text-center">
                        <button onclick="save_postcard()" data-toggle="modal" data-target="#photoDataToInsert" class="btn btn-primary">View and Save</button>
                        <button style="display: none;" class="success_btn btn btn-success">Success</button>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane" id="settings" role="tabpanel">
                    <div class="form-group has-feedback">
                      <label class=" form-control-label" for="text-input">Name Campaign:</label>
                      <div>
                        <input type="text" onkeypress="change_input_data(this)" id="name_campaign" data-toggle="popover" data-placement="bottom" name="name_campaign" class="form-control name_campaign" placeholder="Create Name">
                      </div>
                    </div>
                    <div class="form-group has-feedback">
                      <label class=" form-control-label" for="select2">Type of Campaign: </label>
                      <div >
                        <select id="select_type" name="select_type" class="select_type form-control">
                          <option value="Prospect">Prospect</option>
                          <option value="Customer">Customer</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group has-feedback" style="display: none">
                      <label class=" form-control-label" for="select2">Assigned Number: </label>
                      <div >
                        <select id="select_assign_num" name="select_assign_num" class="select_assign_num form-control">
                          <option value="1">Number1</option>
                          <option value="2">Number2</option>
                        </select>
                      </div><br><br>
                    </div>
                    <div class="col-md-12 form-group ">
                     <div class="col-md-12 py-4 text-center">
                      <button onclick="update_setting()" class="btn btn-primary">Save </button>
                      <button style="display: none;" class="success_btn btn btn-success">Success</button>
                    </div>
                  </div>
                </div>




              </div>
              <br>
              <div  class="final_save ">
                <button onclick="window.location='campaign-overview.php'" class="btn btn-primary">Back</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<div class="modal fade" id="readyCoupon" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <div class="panel-group" id="accordion" style="text-align: center;">
          <img style="width: 100%;" class="imageData" src="#" alt="#">
          <input type="file" name="imgInput" class="photo" id="imgInput" style="display: none" />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" onclick="saveCoupon()" class="btn btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="readyDataToInsert" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
  <div class="modal-dialog modal-default" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="panel-group accordion_data" id="accordion" style="text-align: center;">

        </div>
      </div>
      <div class="modal-footer">
        <!--<button type="button" onclick="updatePostcardData()" class="btn btn-success" data-dismiss="modal">Save</button>-->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="photoDataToInsert" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="panel-group photoDataToInsert"  style="text-align: center;">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


</div>
<script src="js/libs/jquery.min.js"></script>
<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Include JS file. -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/js/froala_editor.min.js'></script>
<script src="js/libs/tether.js"></script>
<script src="js/libs/bootstrap.min.js"></script>

<script src="ckeditor/ckeditor.js"></script>
<script src="js/app.js"></script>

<!-- Include JS file. -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/js/froala_editor.min.js'></script>

<script type="text/javascript" src="js/html2canvas.js"></script>
<script type="text/javascript" src="js/jquery.plugin.html2canvas.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.1.pack.js" ></script>
<script>
  function viewCoupon(){
    $(".component").css('border', '0px');
    $('.component').html2canvas({
      onrendered: function (canvas) {
        $(".imageData").attr('src',canvas.toDataURL("image/png"));
        $(".component").css('border', '3px solid #49708A');
      }
    });
  }

  function signature(type){
    $.ajax({
      url:"include/getSignature.php",
      data:{type:type},
      type:"POST",
      success:function(data){
        //alert(data);
        var json=JSON.parse(data);
        var fData=CKEDITOR.instances['editor_email'].getData();
        CKEDITOR.instances['editor_email'].setData(fData+json);
      }
    })
  }
</script>
<script>
  document.getElementById('delay_day_email').onkeypress = function (e) {
    return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
  }
  document.getElementById('delay_day_voice').onkeypress = function (e) {
    return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
  }
  document.getElementById('delay_day_text').onkeypress = function (e) {
    return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
  }
  document.getElementById('delay_day_postcard').onkeypress = function (e) {
    return !(/[А-Яа-яA-Za-z---+-=]/.test(String.fromCharCode(e.charCode)));
  }
  var id_campaign=$("body").attr("name");
  var type_Campaign="universal";

  var name_file;
  var dir;
  var  error='';
  get_all_data();

  function PostBlob(audioBlob, videoBlob, fileName) {
    var formData = new FormData();
    var bd="campaign_builder_rvmd";
    formData.append('filename', fileName);
    formData.append('audio-blob', audioBlob);
    formData.append('video-blob', videoBlob);
    formData.append('bd',bd );
    var delay_day_rvmd=$(".delay_day_rvmd").val();
    formData.append('delay_day_rvmd', delay_day_rvmd);
    var repeat_rvmd;
    if($(".repeat_rvmd").prop("checked")){
      repeat_rvmd=1;
    }else{
      repeat_rvmd=0;
    }
    formData.append('repeat_rvmd',repeat_rvmd );
    var campaign_id=id_campaign;
    formData.append('id_campaign',campaign_id);
    var name_rvmd=$(".name_rvmd").val();
    formData.append('name_rvmd', name_rvmd);
    xhr('include/upload_audio.php', formData, function(ffmpeg_output) {
      preview.src = '../uploads_audio/' + fileName + '-merged.webm';
      preview.play();
      preview.muted = false;
      name_file=fileName + '.webm';
    });
  }
  var record = document.getElementById('record');
  var stop = document.getElementById('stop');
  var audio = document.querySelector('audio');
  var recordVideo = document.getElementById('record-video');
  var preview = document.getElementById('preview');
  var container = document.getElementById('container');
  var input_file=document.getElementById('import_audio');
  var recordAudio, recordVideo;
  record.onclick = function() {
    record.disabled = true;
    input_file.disabled=true;
    !window.stream && navigator.getUserMedia({
      audio: true,
      video: true
    }, function(stream) {
      window.stream = stream;
      onstream();
    }, function(error) {
      alert(JSON.stringify(error, null, '\t'));
    });
    window.stream && onstream();
    function onstream() {
      preview.src = window.URL.createObjectURL(stream);
      preview.play();
      preview.muted = true;
      recordAudio = RecordRTC(stream, {
        type: 'audio',
        recorderType: StereoAudioRecorder,
            // bufferSize: 16384,
            onAudioProcessStarted: function() {
              recordVideo.startRecording();
            }
          });
      var videoOnlyStream = new MediaStream();
      videoOnlyStream.addTrack(stream.getVideoTracks()[0]);
      recordVideo = RecordRTC(videoOnlyStream, {
        type: 'video',
            // recorderType: MediaStreamRecorder || WhammyRecorder
          });
      recordAudio.startRecording();
      stop.disabled = false;
    }
  };
  var fileName;
  stop.onclick = function() {
    record.disabled = true;
    stop.disabled = true;
    preview.src = '';
    preview.poster = 'ajax-loader.gif';
    fileName = Math.round(Math.random() * 99999999) + 99999999;
    recordAudio.stopRecording(function() {
      recordVideo.stopRecording(function() {
        PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
      });
    });
  };
  function xhr(url, data, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState == 4 && request.status == 200) {
        callback(request.responseText);
      }
    };
    $('.sk-circle').css('display','block');
    $.ajax({
      url:"include/record_audio.php",
      data: data,
      processData: false,
      contentType: false,
      type: 'POST',
      success:function(data){
        $('.sk-circle').css('display','none');
        parsing_audio_json(data);
      }
    })
  }


  CKEDITOR.plugins.add( 'tokens',
  {
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
    var config = editor.config,
    lang = editor.lang.format;
      var tags = []; //new Array();
      //this.add('value', 'drop_text', 'drop_label');
      tags[0]=["{$first_name}", "first_name", "first_name"];
      tags[1]=["{$last_name}", "last_name", "last_name"];
      tags[2]=["{$mobile_phone}", "mobile_phone", "mobile_phone"];
      tags[3]=["{$email}", "email", "email"];
      tags[4]=["{$address}", "address", "address"];
      tags[5]=["{$address1}", "address1", "address1"];
      tags[6]=["{$city}", "city", "city"];
      tags[7]=["{$state}", "state", "state"];
      tags[8]=["{$postal_code}", "postal_code", "postal_code"];
      editor.ui.addRichCombo( 'tokens',
      {
        label : "Insert data",
        className : 'cke_format',
        multiSelect : false,

        panel :
        {
         css : [ config.contentsCss, CKEDITOR.getUrl( editor.skinPath + 'editor.css' ) ],
         voiceLabel : lang.panelVoiceLabel
       },
       init : function()
       {
        this.startGroup( "" );
               //this.add('value', 'drop_text', 'drop_label');
               for (var this_tag in tags){
                this.add(tags[this_tag][0], tags[this_tag][1]);
              }
            },
            onClick : function( value )
            {
             editor.focus();
             editor.fire( 'saveSnapshot' );
             editor.insertHtml(value);
             editor.fire( 'saveSnapshot' );
           }
         });
    }
  });

  CKEDITOR.replace('editor_email',{
    extraPlugins: 'tokens',
    toolbar :
    [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
    { name: 'links', items: [ 'Link', 'Unlink'] },
    { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize','tokens' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools', items: [ 'Maximize'] },
    ]
  });
  CKEDITOR.replace('text_editor',
  {
    toolbar :
    [
    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
    { name: 'tools', items : [ 'Maximize','-' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
    ]});
  function parsing_audio_json(array)
  {
    var json=JSON.parse(array);
    var dir="../"+json['dir'];
    var out='<audio controls id="'+dir+'">';
    out+='<source src="'+json['dir']+'" type="audio/ogg; codecs=vorbis">';
    out+='<source src="'+json['dir']+'" type="audio/mpeg">';
    out+='Тег audio не поддерживается вашим браузером. ';
    out+='</audio><a style="margin-left:10px" href="#del" onclick="delete_audio()">Delete</a>';
    $('.sk-circle').css('display','none');
    $('.audio_list').append(out);
  }


  function load()
  {
    $('.sk-circle').css('display','block');
    var input_file=document.getElementById('import_audio');
    var record = document.getElementById('record');
    input_file.disabled=true;
    record.disabled=true;
    var id=id_campaign;
    var data=new FormData();
    var file = $('.import_audio');
    data.append('upload', file.prop('files')[0]);
    data.append('id', id);
    data.append('bd', 'campaign_builder_rvmd');
    var name_rvmd=$(".name_rvmd").val();
    data.append('name_rvmd', name_rvmd);
    var delay_day_rvmd=$(".delay_day_rvmd").val();
    data.append('delay_day_rvmd', delay_day_rvmd);
    var repeat_rvmd;
    if($(".repeat_rvmd").prop("checked")){
      repeat_rvmd=1;
    }else{
      repeat_rvmd=0;
    }
    data.append('repeat_rvmd',repeat_rvmd );
    $('.sk-circle').css('display','block');
    $.ajax({
      url:"include/upload_audio.php",
      data: data,
      processData: false,
      contentType: false,
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
        parsing_audio_json(data);
        file.val('');
        $('#record').attr('disabled');
        $('#input_file').attr('disabled');
      }
    })
    $('.sk-circle').css('display','none');
  }



  function delete_audio(){
    $('.sk-circle').css('display','block');
    $('.sk-circle').css('display','block');
    var id=id_campaign;
    var dir=$('audio').attr('id');
    var bd="campaign_builder_rvmd";
    var input_file=document.getElementById('import_audio');
    $.ajax({
      url:"include/delete_audio.php",
      data:{dir:dir,id:id,bd:bd},
      type:"POST",
      success:function(){
        $('.sk-circle').css('display','none');
        $('.audio_list').html('');}})
    $('.sk-circle').css('display','none');
    $('#record').removeAttr('disabled');
    input_file.disabled=false;

  }

  function save_text(){
    $('.sk-circle').css('display','block');
    var editor_text = CKEDITOR.instances['text_editor'].getData();
    var delay_day_text=$(".delay_day_text").val();
    var repeat_text;
    if($(".repeat_text").prop("checked")){
      repeat_text=1;
    }else{
      repeat_text=0;
    }
    $.ajax({
      url:"include/univesral_campaign/add_text_to_campaign.php",
      data:{editor_text:editor_text,id_campaign:id_campaign,delay_day_text:delay_day_text,repeat_text:repeat_text},
      type:"POST",
      success:function(){
        $('.sk-circle').css('display','none');
      }})
    successAdded();
    $('.sk-circle').css('display','none');
  }

  function successAdded(){
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },500);
  }

  function email_show(){
    $('.sk-circle').css('display','block');
    var editor_data = CKEDITOR.instances['editor_email'].getData();
    var delay_day_email=$(".delay_day_email").val();
    var repeat_email;
    if($(".repeat_email").prop("checked")){
      repeat_email=1;
    }else{
      repeat_email=0;
    }
    $.ajax({
      url:"include/univesral_campaign/add_email_to_campaign.php",
      data:{editor_data:editor_data,id_campaign:id_campaign,delay_day_email:delay_day_email,repeat_email:repeat_email},
      type:"POST",
      success:function(){
        $('.sk-circle').css('display','none');
      }})
    successAdded();
    $('.sk-circle').css('display','none');
  }

  function save_voice(){
    successAdded();
    update_audio();
  }
  function update_audio()
  {
    $('.sk-circle').css('display','block');
    if($('audio').attr("name")=='default'){
      dir_img="../"+$('audio').attr("id");
    }else{
      dir_img=$('audio').attr('id');
    }
    var repeat_rvmd;
    if($(".repeat_rvmd").prop("checked")){
      repeat_rvmd=1;
    }else{
      repeat_rvmd=0;
    }
    var name_rvmd=$(".name_rvmd").val();
    var delay_day_rvmd=$(".delay_day_rvmd").val();
    $.ajax({
      url:"include/update_universal_audio.php",
      data: {dir_img:dir_img,id_campaign:id_campaign,delay_day_rvmd:delay_day_rvmd,repeat_rvmd:repeat_rvmd,name_rvmd:name_rvmd},
      type: 'POST',
      success:function(){
        $('.sk-circle').css('display','none');
      }})
  }

  function insertPhotoIntoModal(array){
    var out='';
    for(var k in array){
      out+="<img src='"+array[k]+"' alt='alt'>"
    }
    $(".photoDataToInsert").html(out);
  }

  function hide_area(){
    $('.sk-circle').css('display','none');
  }

  function notification(id,type_error){
    var parent=$(id).parent();
    $(parent).addClass('has-danger');
    $(id).addClass('form-control-danger');
    $(id).attr("data-content",type_error);
    $(id).popover('show');
    $('.danger_btn').css("display","inline-block");
    setTimeout(function(){
      $(id).popover('dispose');
      $('.danger_btn').css("display","none");
    },1500);
  }

  function change_input_data(id){
    var parent=$(id).parent();
    $(id).removeClass('form-control-danger');
    parent.removeClass('has-danger');
  }



  function agree_check(id,tab,div){
    $('.'+tab).css('display','none');
    $('.'+div).css('display','block');
  }

  function get_all_data()
  {$('.sk-circle').css('display','block');
  var token=$('body').attr('id');
  $.ajax({
    url:"include/univesral_campaign/get_all_data.php",
    data:{token:token},
    type:"POST",
    success:function(data){
      $('.sk-circle').css('display','none');
          //alert(data);
          parsing_campaign_data(data);
        }
      })
}

function parsing_campaign_data(array){
  var json=JSON.parse(array);
      //alert(array);
      var date2;
      var date1=new Date();
      var timeDiff;
      var diffDays;
      if(json['success']==1){
        if(json['email']!=null){
          $('.variable1').css('display','block');
          $('.close_tab1').css('display','none');
          CKEDITOR.instances['editor_email'].setData(json['email']['text']);
          date2 = new Date(json['email']['delay_day']);
          timeDiff = Math.abs(date2.getTime() - date1.getTime());
          diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          $(".delay_day_email").val(diffDays);
          var repeat_email=json['email']['repeat_year'];
          if(repeat_email==1){
            $(".repeat_email").attr("checked","true");
          }
        }
        if(json['text']!=null){
          $('.variable3').css('display','block');
          $('.close_tab3').css('display','none');
          CKEDITOR.instances['text_editor'].setData(json['text']['text']);
          date2 = new Date(json['text']['delay_day']);
          timeDiff = Math.abs(date2.getTime() - date1.getTime());
          diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          $(".delay_day_text").val(diffDays);
          var repeat_text=json['text']['repeat_year'];
          if(repeat_text==1){
            $(".repeat_text").attr("checked","true");
          }
        }
        if(json['audio']!=null){
          $('.variable2').css('display','block');
          $('.close_tab2').css('display','none');
          var record = document.getElementById('record');
          var input_file=document.getElementById('import_audio');
          record.disabled=true;
          input_file.disabled=true;
          date2 = new Date(json['audio']['delay_day']);
          timeDiff = Math.abs(date2.getTime() - date1.getTime());
          diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
          $(".delay_day_rvmd").val(diffDays);
          var repeat_rvmd=json['audio']['repeat_year'];
          if(repeat_rvmd==1){
            $(".repeat_rvmd").attr("checked","true");
          }
          var dir=json['audio']['filename'];
          var audio='<audio controls id="'+dir+'">';
          audio+='<source src="'+json['audio']['filename'].substring(3)+'" type="audio/ogg; codecs=vorbis">';
          audio+='<source src="'+json['audio']['filename'].substring(3)+'" type="audio/mpeg">';
          audio+='</audio><a style="margin-left:10px" href="#del" onclick="delete_audio()">Delete</a>';
          $('.audio_list').html(audio);
          $(".name_rvmd").val(json['audio']['name_rvmd']);
        }
        if(json['postcard']!=null){
          var dir=json['postcard']['dir'];
          $('.variable4').css('display','block');
          $('.close_tab4').css('display','none');
          for(var k=0;k<2;k++){
            date2 = new Date(json['postcard'][k]['delay_day_date']);
            timeDiff = Math.abs(date2.getTime() - date1.getTime());
            diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

            $(".delay_day_postcard").val(diffDays);
            var repeat_postcard=json['postcard'][k]['repeat_year'];
            if(repeat_postcard==1){
              $(".repeat_postcard").attr("checked","true");
            }
            if(json['postcard'][k]['type_postcard']=="front"){
              $(".iframe_1").css('display','none');
              $(".iframe_1_2").css('display','block');
              $(".photo_front").attr("src",'../upload_photo/'+dir+'/'+json['postcard'][k]['filename']);
            }else{
              $(".iframe_2").css('display','none');
              $(".iframe_2_2").css('display','block');
              $(".photo_back").attr("src",'../upload_photo/'+dir+'/'+json['postcard'][k]['filename']);
            }
          }
        }
        $('.name_campaign').val(json['name']);
        $('.select_type').val(json['type']);
        $('.select_assign_num').val(json['assign_num']);
      }
    }

    function update_setting()
    {
      $('.sk-circle').css('display','block');
      var name= $('.name_campaign').val();
      var type=$('.select_type').val();
      var assign_num=$('.select_assign_num').val();
      $.ajax({
        url:"include/univesral_campaign/update_settings.php",
        data:{name:name,type:type,id_campaign:id_campaign,assign_num:assign_num},
        type:"POST",
        success:function(data){
          $('.sk-circle').css('display','none');
          var json=JSON.parse(data);
          if(json['success']==1){
            $('.success_btn').css("display","inline-block");
            setTimeout(function(){
              $('.success_btn').css("display","none");
            },500);
          }else{alert(json['error']);}
        }
      })
    }

    function readyDataShow(type_data){
      $('.sk-circle').css('display','block');
      type_editor=type_data;
      $.ajax({
        url:"include/univesral_campaign/get_ready_data.php",
        data:{type_data:type_data},
        type:"POST",
        success:function(data){
            //alert(data);
            $('.sk-circle').css('display','none');
            parsingReadyData(data);
          }
        })
    }

    function parsingReadyData(array){
      var json=JSON.parse(array);
      switch (json['type']) {
        case 'text':
        appendModalData(json);
        $("#readyDataToInsert").modal("show");
        break;
        case 'email':
        appendModalData(json);
        $("#readyDataToInsert").modal("show");
        break;
        case 'rvmd':
        appendModalDataRVMD(json);
        $("#readyDataToInsert").modal("show");
        break;
      }
    }

    function appendModalDataRVMD(array){
      $(".accordion_data").html('');
      var out='';
      var dir='';
      var j=1;
      for(var k in array['data']){
        out='';
        dir=array['data'][k]['filename'];
        var name=array['data'][k]['name'];
        out+='<div class="form-group has-feedback">';
        out+='<label class=" form-control-label">Name of file: '+name+'</label><div>';
        out+='<audio controls name="audio'+j+'" id="'+dir+'">';
        out+='<source src="'+dir.substr(3)+'" type="audio/ogg; codecs=vorbis">';
        out+='<source src="'+dir.substr(3)+'" type="audio/mpeg">';
        out+='</audio><a style="margin-left:10px" href="#select" onclick="addReadyRVMD('+j+')">Select</a><br></div></div>';
        $(".accordion_data").append(out);
        j++;
      }
    }

    function appendModalData(array){
      $(".accordion_data").html('');
      var out='';
      var j=1;
      for(var k in array['data']){
        out+="<div class='panel panel-default'><div class='panel-heading'><p class='panel-title' style='margin-bottom:0;'><a data-toggle='collapse' data-parent='#accordion' href='#collapse"+j+"'>Variation "+j+"</a></p></div><div id='collapse"+j+"' class='panel-collapse collapse in'><div class='panel-body collapse"+j+"'><textarea name='editor_test"+j+"' disabled id='editor_test"+j+"' rows='10' cols='80'>"+array['data'][k]+"</textarea></div><a href='#add_data' onclick='addToEditor("+j+")'>Select</a></div></div>";
        j++;
      }
      $(".accordion_data").append(out);
      for(var i=1;i<j;i++){
        CKEDITOR.replace('editor_test'+i);
      }
    }

    function addToEditor(id){
      var editor_test = CKEDITOR.instances['editor_test'+id].getData();
      switch(type_editor){
        case 'text':
        CKEDITOR.instances['text_editor'].setData(editor_test);
        break;
        case 'email':
        CKEDITOR.instances['editor_email'].setData(editor_test);
        break;
      }
      $("#readyDataToInsert").modal("hide");
      $(".accordion_data").html('');
    }

    function addReadyRVMD(id){
      $('.audio_list').html('');
      var dir=$('audio[name=audio'+id+']').attr('id');
      var out='<br><audio controls id="'+dir+'">';
      out+='<source src="'+dir.substr(3)+'" type="audio/ogg; codecs=vorbis">';
      out+='<source src="'+dir.substr(3)+'" type="audio/mpeg">';
      out+='</audio><a style="margin-left:10px" href="#del" onclick="delete_audio()">Delete</a>';
      $('.audio_list').html(out);
      $(".accordion_data").html('');
      $("#readyDataToInsert").modal("hide");
      $(".import_audio").attr("disabled", true);
      $("#record").attr("disabled", true);
      $("#stop").attr("disabled", true);
    }
  </script>

  <script>
    var periodDays = [];
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    var date = today.getDate();
    var ikey=0;
    var out;
    var j=30;
    var typeGlobal='email';

    function buildArrayDate(j=30){
      periodDays = [];
      for(i=0; i<=j; i++){
        var day=new Date(year, month, date + (i));
        var dd = day.getDate();
        var mm = day.getMonth()+1;
        var yyyy = day.getFullYear();
        if(dd<10) {
          dd='0'+dd
        }
        if(mm<10) {
          mm='0'+mm
        }
        curr_day = yyyy+'-'+mm+'-'+dd;
        periodDays.push(curr_day);
      }
    }

    getTable(30,"email");

    function getTable(count,type){
      $(".datatableEmail").html('');
      $.ajax({
        url:"include/table_center/getTable.php",
        data:{count:count,type:type},
        type:"POST",
        success:function(data){
        //alert(data);
        buildArrayDate(count);
        viewAllDate(JSON.parse(data));
      }
    })
    }

    function getExistDate(str,array){
      var existStr=0;
      for(var l in array){
        if(array[l]==str){
          existStr++;
        }
      }
      return existStr;
    }

    function viewAllDate(array){
      ikey=0;
      $(".datatableEmail").html('');
      for(var k in periodDays){
        var existDate = getExistDate(periodDays[k],array);
        if (existDate!=0){
          out='<td><b>Day '+ikey+'</b><br>'+periodDays[k]+'<br><p class="tableExist">'+existDate+' Exist</p></td>';
        }else{
          out='<td><b>Day '+ikey+'</b><br>'+periodDays[k]+'</td>';
        }
        if(ikey%7==0){
          $(".datatableEmail").append('<tr class="odd">');
        }
        $(".datatableEmail").append(out);
        ikey++;
      }
    }

    function addNewDays(){
      j=j+30;
      getTable(j,typeGlobal);
    }

    function getlist(type){
      $('body').removeClass('sidebar-hidden');
      j=30;
      typeGlobal=type;
      getTable(j,typeGlobal);
    }


    function save_postcard(){
      $('.sk-circle').css('display','block');
      var repeat_postcard;
      if($(".repeat_postcard").prop("checked")){
        repeat_postcard=1;
      }else{
        repeat_postcard=0;
      }
      var delay_day_postcard=$(".delay_day_postcard").val();
      $.ajax({
        url:"include/postcard/getPicture.php",
        data:{id_campaign:id_campaign,type_Campaign:type_Campaign,repeat_postcard:repeat_postcard,delay_day_postcard:delay_day_postcard},
        type:"POST",
        success:function(data){
          $('.sk-circle').css('display','none');
          insertPhotoIntoModal(JSON.parse(data));
        }
      })
    }
    function insertPhotoIntoModal(array){
      var out='';
      for(var k in array){
        out+="<img src='"+array[k]+"' alt='alt'>"
      }
      $(".photoDataToInsert").html(out);
    }

    function updatePostcardFront(){
      $(".iframe_1").css('display','block');
      $(".iframe_1_2").css('display','none');
    }

    function updatePostcardBack(){
      $(".iframe_2").css('display','block');
      $(".iframe_2_2").css('display','none');
    }

    function updatePostcardData(){
      /*
      var delay_day_postcard=$(".delay_day_postcard").val();
      $.ajax({
        url:"include/postcard/updatePostcardData.php",
        data:{id_campaign:id_campaign,repeat_year:repeat_year,delay_day_postcard:delay_day_postcard,type_Campaign:type_Campaign},
        type:"POST",
        success:function(data){
          //alert(data);
        }
      })*/
      save_postcard();
    }
    function closeSideBar(){
      $('body').addClass('sidebar-hidden');
    }
  </script>
  <script src="js/libs/toastr.js"></script>
  <script src="components/user_notifications.js"></script>
</body>

</html>
