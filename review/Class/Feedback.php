<?php
require_once '../Twilio/autoload.php';
use Twilio\Rest\Client;
require_once '../cron/config.php';
/**
* 
*/
class Feedback
{
	var $id_last_query,$user_id;

	function get_user_id($token)
	{
		$query_model="SELECT * FROM review_builder WHERE md5='".$token."'";
		$query=mysql_query($query_model);
		$array_review=mysql_fetch_array($query);
		$this->user_id=$array_review['user_id'];
		$this->addToNotification($this->user_id);
	}

	function addToNotification($clientId){
		$queryModel="INSERT INTO notifications(`user_id`,`text`,`notes`) VALUES('".$clientId."','We have a new Feedback','Feedback Reports')";
		mysql_query($queryModel);
	}
	
	function add_new_feedback($array1,$array2)
	{
		$date=date("Y-m-d");
		$query_model="INSERT INTO `feedback`(`date`,`user_id`,`view`) VALUES ('".$date."','".$this->user_id."','0')";
		$query=mysql_query($query_model);
		$this->id_last_query=mysql_insert_id();
		$count=count($array1);
		for ($i=0; $i <$count ; $i++) { 
			$query_model="INSERT INTO `feedback_reports`(`feedback_id`, `question`, `answer`) VALUES ('".$this->id_last_query."','".$array1[$i]."','".$array2[$i]."')";
			$query=mysql_query($query_model);
		}
	}

	function update_rate($id,$val,$mobile,$name)
	{
		$query_model="UPDATE `feedback` SET `star`='".$val."' WHERE id='".$id."'";
		$query=mysql_query($query_model);
		if($val<3){
			$userId=$this->getUserIdFromFeed($id);
			$userMobile=$this->getUserMobile($userId);
			$this->sendText($userMobile,$mobile,$name);
		}
	}

	function getUserIdFromFeed($id){
		$queryModel="SELECT user_id FROM feedback WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['user_id'];
	}

	function getUserMobile($id){
		$queryModel="SELECT mobile_phone FROM user WHERE id='".$id."'";
		$query=mysql_query($queryModel);
		$arrayData=mysql_fetch_assoc($query);
		return $arrayData['mobile_phone'];
	}

	function sendText($mobileUser,$mobileClient,$name){
		$firstSymbol=mb_substr($mobileUser,0,1);
		if($firstSymbol!='+'){
			$mobileUser='+'.$mobileUser;
		}
		$mobileClient='+'.$mobileClient;
		$text=ucfirst($name).' added negative feedback.Contact by phone :'.$mobileClient;
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		$message = $client->messages->create(
			$mobileUser, 
			array(
				'from' => '+17202509876',
				'body' => $text
			)
		);
	}

	function delete_feedback($id)
	{
		$query_model="DELETE FROM feedback_reports WHERE feedback_id='".$id."'";
		mysql_query($query_model);
		$query_model="DELETE FROM feedback WHERE id='".$id."'";
		mysql_query($query_model);

	}
}
?>