<?php
include '../connect_to_bd.php';
require_once '../class/Universal_campaign.php';

$id=$_POST['id'];
$text="Administrator delete campaign";
$un_campaign=new Universal_campaign();
try{
	$un_campaign->delete_audio($id);
	$un_campaign->delete_campaign($id);
	$un_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>