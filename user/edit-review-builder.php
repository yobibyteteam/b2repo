<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$id=$_GET['token'];
$if=mysql_real_escape_string($id);
$query=mysql_query("SELECT * FROM review_builder WHERE md5='$id'");
$array=mysql_fetch_assoc($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  .starclick:before{
    color:#e3cf7a!important;
    content: "\f005"!important;
  }
  body{
    position: relative;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }
  .rating {
    unicode-bidi: bidi-override;
    direction: rtl;
    font-size: 30px;
  }
  .rating span.star,
  .rating span.star {
    font-family: FontAwesome;
    font-weight: normal;
    font-style: normal;
    display: inline-block;
  }
  .rating span.star:hover,
  .rating span.star:hover {
    cursor: pointer;
  }
  .rating span.star:before,
  .rating span.star:before {
    content: "\f006";
    padding-right: 5px;
    color: #999999;
  }
  .rating span.star:hover:before,
  .rating span.star:hover:before,
  .rating span.star:hover ~ span.star:before,
  .rating span.star:hover ~ span.star:before {
    content: "\f005";
    color: #e3cf7a;
  }
  .card{
    display:block;
  }
  input{
   margin: 5px;
 }
</style>

</head>

<body id="<?=$array['md5']?>" class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="review-builder.php">Review Builder</a></li>
        <li class="breadcrumb-item active">Overview Review</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn form_question">
          <div class="html_form">
            <?=htmlspecialchars_decode($array['html_form'])?>
          </div>
        </div>
      </div>
    </main>
  </div>
  <div id="parent">
    <div id="child"></div>
  </div>
  <!-- Bootstrap and necessary plugins -->
  <script src="js/libs/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="js/libs/pace.min.js"></script>
  <!-- Plugins and scripts required by all views -->
  <script src="js/libs/Chart.min.js"></script>
  <!-- GenesisUI main scripts -->
  <script src="js/app.js"></script>
  <!-- Plugins and scripts required by this views -->
  <script src="../js/libs/toastr.min.js"></script>
  <script src="js/libs/gauge.min.js"></script>
  <script src="js/libs/moment.min.js"></script>
  <script src="js/libs/daterangepicker.js"></script>
  <!-- Custom scripts required by this view -->
  <script src="js/views/main.js"></script>
  li
  <script>
    var star_val;
    var json;
    var token=$('body').attr('id');
    var i=0;
    var count_answer=getCountQuestions();
    var j=i-1;
    var data_answer=[];
    var data_quest=[];
    var name_form;
    var data=[];
    var last_answer;
    var ii=0;
    var review_id;
    var count=['Two','Three','Four','Five'];
    updateValueTextarea(count_answer);

    function updateAnswerValue(){
      var antValue=0;
      $(".questions").each(function(){
        antValue++;
      })

    }

    function updateValueTextarea(){
      $("textarea").each(function(){
        $(this).html($(this).attr("value"));
      })
    }

    function getCountQuestions(){
      var array=[];
      array[1]=3;
      array[2]=3;
      array[3]=3;
      array[4]=3;
      array[5]=3;
      $(".questions").each(function(){
       i++;
     })
      for (var j = 1; j < i+1; j++) {
       $(".answer"+j).each(function(){
        array[j]++;
      })
     }
     return array;
   }

   function modal_show(id){
    review_id=id;
      //$("#dangerModal").modal("show");
    }
    function add_new_question(id){
      $('.sk-circle').css('display','block');
      var out;
      i++;
      if(i>=2 && i<5){
        $(id).css('display','inline-block');
      }else{
       $(id).css('display','none');
     }
     $('.delete_item').css('display','inline-block');
     out='';
     out+="<div class='form-group has-feedback question"+i+"'>";
     out+="<label class='form-control-label' for='text-input'>Question "+count[j]+":</label>";
     out+="<div>";
     out+="<textarea onkeyup='append_attr_value(this)'  onkeypress='change_input_data(this)' id='textarea-input"+i+"' name='textarea-input' rows='5' class='textarea_input textarea_input_check form-control' placeholder='Content'></textarea>";
     out+="</div><br>";
     out+="<div>";
     out+="<label>";
     out+="<input checked='checked' id='"+i+"' type='radio' onclick='hide_drop_menu("+i+",this)'  name='inline-checkbox"+i+"' value='textarea'>Text Area";
     out+="</label>";
     out+="<label>";
     out+="<input type='radio' id='"+i+"'  onclick='display_answer("+i+",this)'  name='inline-checkbox"+i+"' value='dropdown'>Drop Down Answers";
     out+="</label>";
     out+="<label>";
     out+="<input type='radio' id='"+i+"' onclick='hide_drop_menu("+i+",this)' name='inline-checkbox"+i+"' value='date'>Date";
     out+="</label>";
     out+="<div style='display:none' class='answer_wrapper"+i+"'>";
     out+="<div class='row'>";
     out+="<div class='col-sm-12 answer_input"+i+"'>";
     out+="<input onkeyup='add_attr_value(this)' onkeypress='change_input_data(this)' style='margin-left: 0!important' type='text' id='text-input' name='text-input' class='input_answer1 form-control' placeholder='Answer 1'>";
     out+="<input onkeyup='add_attr_value(this)' onkeypress='change_input_data(this)' style='margin-left: 0!important' type='text' id='text-input' name='text-input' class='input_answer2 form-control' placeholder='Answer 2'>";
     out+="</div>";
     out+="</div>";
     out+="<div class='row'>";
     out+="<div class='col-md-12 text-left'>";
     out+="<button onclick='add_new_answer("+i+")' class='btn btn-secondary add_answer'>Add Answer</button>";
     out+="<button style='display: none;'' onclick='delete_answer("+i+")'' class='btn btn-secondary delete_answer"+i+"'>Delete Last Answer</button>";
     out+="</div>";
     out+="</div>";
     out+="</div>";
     out+="</div>";
     out+="</div>";
     $('.question').append(out);
     j++;
     $('.sk-circle').css('display','none');
   }

   function add_new_answer(id){
    var out;
    console.log(id);
    var key=count_answer[id];
    count_num=count_answer[id];
    out="<input onkeyup='add_attr_value(this)' onkeypress='change_input_data(this)' style='margin-left: 0!important' onclick='delete_answer("+id+key+")' type='text' id='text-input' name='text-input' class='input_answer"+id+key+" form-control' placeholder='Answer "+count_num+"'>";
    $('.answer_input'+id).append(out);
    if(key>2){
      $('.add_answer').css('display','inline-block');
    }else{
     $('.add_answer').css('display','none');
   }
   $('.delete_answer'+id).css('display','inline-block');
   count_answer[id]++;
 }

 function delete_last_question(id){
  $(".question"+i).remove();
  i--;
  j--;
  $('.add_item').css('display','inline-block');
  if(i<2){
    $('.delete_item').css('display','none');
  }else{
    $('.delete_item').css('display','inline-block');
  }
}

function save_form(){
  try{
    get_nameform();
    welcome_text();
    get_question();
    check_answer();
    get_answer();
    //check_share_data();
    ajax_query(name_form,data);
  }catch(e){
    console.log(e);
  }
}

function check_answer(){
  $(".input_answer").each(function(){
    if($(this).val()==''){
      var parent=$(this).parent();
      $(this).addClass('form-control-danger');
      parent.addClass('has-danger');
      throw new SyntaxError("empty_data");
    }
  })
}

function ajax_query(name,data){
  $('.sk-circle').css('display','block');
  var success_text=$('.success_text').val();
  var facebook_id=$('.facebook_id').val();
  var yelp_id=$('.yelp_id').val();
  var google_id=$('.google_id').val();
  var share_text=$('.share_text').val();
  var welcome_text=$('.welcome_text').val();
  var html_form=$('.html_form').html();
  var postMd5=token;
  $.ajax({
    url:'include/updateReview.php',
    data:{name:name,data:data,success_text:success_text,facebook_id:facebook_id,yelp_id:yelp_id,google_id:google_id,share_text:share_text,welcome_text:welcome_text,html_form:html_form,count:i,postMd5:postMd5},
    type:'POST',
    success:function(data){
      console.log(data);
      $('.sk-circle').css('display','none');
      if(data==1){
        window.location='review-builder.php';
        $(".question2").remove();
        $(".question3").remove();
        $(".question4").remove();
        $(".question5").remove();
        $('.name_form').val('');
        $('.textarea_input').val('');
        j=0;
        i=1;
        $('.add_answer').css('display','inline-block');
        $('.delete_item').css('display','none');
        $("#inline-checkbox11").prop("checked", true);
        $(".answer_wrapper1").css("display","none");
      }else{
        $(".danger_btn").css('display','inline-block');
        setTimeout(function(){
          $(".danger_btn").css('display','none');
        },1000);
        var parent=$(".name_form").parent();
        $('.name_form').addClass('form-control-danger');
        parent.addClass('has-danger');
        $('.name_form').attr("data-content",'Form Exist');
        $('.name_form').popover('show');
        setTimeout(function(){
          $('.name_form').popover('dispose');
        },1500);
      }
    }

  })
}

function welcome_text(){
  if($('.welcome_text').val()==''){
    var parent=$(".welcome_text").parent();
    $('.welcome_text').addClass('form-control-danger');
    parent.addClass('has-danger');
    throw new SyntaxError("empty_data");
  }
}

function get_nameform(){
  if($('.name_form').val()==''){
    var parent=$(".name_form").parent();
    $('.name_form').addClass('form-control-danger');
    parent.addClass('has-danger');
    $(".danger_btn").css('display','inline-block');
    setTimeout(function(){
      $(".danger_btn").css('display','none');
    },1000);
    throw new SyntaxError("empty_data");
  }
  name_form=$('.name_form').val();
    //console.log(name_form);
  }
  function get_question(){
    last_answer=0;
    $(".textarea_input_check").each(function(){
      if($(this).val()==''){
        var parent=$(this).parent();
        $(this).addClass('form-control-danger');
        parent.addClass('has-danger');
        throw new SyntaxError("empty_data");
      }
    })
    $(".textarea_input").each(function(){
      data_quest[last_answer]=$(this).val();
      last_answer++;
    })
    //console.log(data_quest);
  }
  function get_answer(){
    ii=0;
    $("input[type='radio']:checked").each(function(){
      var attr=$(this).attr('id');
      if($(this).val()=='dropdown'){
        data_answer=[];
        var child = $('.answer_input'+attr).find('input');
        for(var i=0;i<child.length;i++){
          data_answer[i]=child[i]['value'];
        }
      }else{
        data_answer=$(this).val();
      }
      build_array(data_answer);
        //console.log(data);
      });
  }
  function build_array(arr){
   data[ii]={data_quest:data_quest[ii],answer:arr};
   ii++
   console.log(data);
 }


 function display_answer(id,id_radio){
  $('.answer_wrapper'+id).css('display','block');
  $(id_radio).prop("checked", true);
  $(id_radio).attr('checked', 'checked');
}

function hide_drop_menu(id,id_radio){
  $('.answer_wrapper'+id).css('display','none');
  $(id_radio).prop("checked", true);
  $(id_radio).attr('checked', 'checked');
  console.log(id_radio);
}
function delete_answer(id){
  count_answer[id]--;
  var key=count_answer[id];
  $('.input_answer'+id+count_answer[id]).remove();
  $('.add_answer').css('display','inline-block');
  if(key<4){
    $('.delete_answer'+id).css('display','none');
  }else{
    $('.delete_answer'+id).css('display','inline-block');
  }
}
function change_input_data(id){
  var val=$(id).val();
  $(id).attr('value',val);
  var parent=$(id).parent();
  $(id).removeClass('form-control-danger');
  parent.removeClass('has-danger');
}
function check_share_data(){
  $(".share_input").each(function(){
    if($(this).val()==''){
      var parent=$(this).parent();
      $(this).addClass('form-control-danger');
      parent.addClass('has-danger');
      throw new SyntaxError("empty_data");
    }
  })
}
function add_attr_value(id){
  var val=$(id).val();
  $(id).attr('value',val);
}

function append_attr_value(id){
  var val=$(id).val();
  $(id).append(val);
}
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
</body>
</html>
