<?php


class Centers
{
	protected $count,$finalArray,$clientId;
	static protected function getAdminHolidayCampaignDate(){
		$queryModel="SELECT `date` FROM holiday_builder WHERE type='global_admin'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayAdminDate[]=$res['date'];
			}
		}else{
			$arrayAdminDate=array();
		}
		return $arrayAdminDate;
	}

	static public  function getClientId($token){
		$queryModel='SELECT client_id FROM token WHERE token = "'.$token.'"';
		$query=mysql_query($queryModel);
		$arrayToken=mysql_fetch_assoc($query);
		return $arrayToken['client_id'];
	}
}


class EmailCenter extends Centers
{
	public function __construct($count,$clientId){
		$this->count=$count;
		$this->clientId=$clientId;
		EmailCenter::getAllUniversalCampaignDate();
		EmailCenter::getAllHolidayCampaignDate();
		echo json_encode($this->finalArray);
	}

	private function getAllUniversalCampaignDate(){
		$queryModel="SELECT delay_day FROM campaign_builder_email WHERE type='user' AND client_id='".$this->clientId."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayEmailDate[]=$res['delay_day'];
			}
		}else{
			$arrayEmailDate=array();
		}
		$this->finalArray=array_merge($arrayEmailDate);
	}

	private function getAllDatedCampaignDate(){
	}

	private function getUserHolidayaCampaignDate(){
		$queryModel="SELECT `delay_day` FROM holiday_builder_email WHERE type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayUserDate[]=$res['delay_day'];
			}
		}else{
			$arrayUserDate=array();
		}
		return $arrayUserDate;
	}

	private function getAllHolidayCampaignDate(){
		$arrayAdminDate=Centers::getAdminHolidayCampaignDate();
		$arrayUserDate=EmailCenter::getUserHolidayaCampaignDate();
		if(count($arrayAdminDate)!=0){
			foreach ($arrayAdminDate as $ikey => $ivalue) {
				foreach ($arrayUserDate as $jkey =>$jvalue) {
					$date = new DateTime($ivalue);
					if($jvalue<0){
						$kk=$jvalue.' day';
					}else{
						$kk='+'.$jvalue.' day';
					}
					$date->modify($kk);
					array_push($this->finalArray, $date->format('Y-m-d'));
				}
			}
		}
	}
}

class TextCenter extends Centers
{

	public function __construct($count,$clientId){
		$this->count=$count;
		$this->clientId=$clientId;
		TextCenter::getAllUniversalCampaignDate();
		TextCenter::getAllHolidayCampaignDate();
		echo json_encode($this->finalArray);
	}

	private function getAllUniversalCampaignDate(){
		$queryModel="SELECT delay_day FROM campaign_builder_text WHERE type='user' AND client_id='".$this->clientId."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayEmailDate[]=$res['delay_day'];
			}
		}else{
			$arrayEmailDate=array();
		}
		$this->finalArray=array_merge($arrayEmailDate);
	}

	private function getAllDatedCampaignDate(){
	}


	private function getUserHolidayaCampaignDate(){
		$queryModel="SELECT `delay_day` FROM holiday_builder_text WHERE type='user'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayUserDate[]=$res['delay_day'];
			}
		}else{
			$arrayUserDate=array();
		}
		return $arrayUserDate;
	}

	private function getAllHolidayCampaignDate(){
		$arrayAdminDate=Centers::getAdminHolidayCampaignDate();
		$arrayUserDate=TextCenter::getUserHolidayaCampaignDate();
		if(count($arrayAdminDate)!=0){
			foreach ($arrayAdminDate as $ikey => $ivalue) {
				foreach ($arrayUserDate as $jkey =>$jvalue) {
					$date = new DateTime($ivalue);
					if($jvalue<0){
						$kk=$jvalue.' day';
					}else{
						$kk='+'.$jvalue.' day';
					}
					$date->modify($kk);
					array_push($this->finalArray, $date->format('Y-m-d'));
				}
			}
		}
	}
}

class RVMDCenter extends Centers
{
	public function __construct($count,$clientId){
		$this->count=$count;
		$this->clientId=$clientId;
		RVMDCenter::getAllUniversalCampaignDate();
		echo json_encode($this->finalArray);
	}

	private function getAllUniversalCampaignDate(){
		$queryModel="SELECT delay_day FROM campaign_builder_rvmd WHERE type='user' AND client_id='".$this->clientId."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				$arrayEmailDate[]=$res['delay_day'];
			}
		}else{
			$arrayEmailDate=array();
		}
		$this->finalArray=array_merge($arrayEmailDate);
	}

	private function getAllDatedCampaignDate(){
	}




}


?>