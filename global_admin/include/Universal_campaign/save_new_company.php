<?php 
session_start();
include '../connect_to_bd.php';
include_once '../class/Universal_campaign.php';
$name_campaign = $_POST['name_campaign'];
$type_campaign = $_POST['type_campaign'];

$text="Administrator add new campaign";
$un_campaign = new Universal_campaign();
try{
	$un_campaign->set_data($name_campaign,$type_campaign);
	$un_campaign->get_id_client();
	$un_campaign->check_input_data();
	$un_campaign->add_to_bd();
	$un_campaign->get_admin_name();
	$un_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>"1","campaign_id"=>$un_campaign->last_query));
}catch(Exception $e){
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}



?>