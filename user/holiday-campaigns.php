<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$userId=getUserId($_SESSION['user_token']);
function getUserId($token){
  $queryModel="SELECT client_id FROM token WHERE token='".$token."'";
  $query=mysql_query($queryModel);
  $arrayData=mysql_fetch_assoc($query);
  return $arrayData['client_id'];
}
function get_list_campaign(){
  $query_model="SELECT * FROM holiday_builder WHERE type='global_admin'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'">'.$res['name'].'</option>';
    }
  }

}
function getAllPhoneNumbers($userId){
  $sql="SELECT * FROM phone_numbers WHERE user_id='".$userId."' ORDER BY timestamp DESC";
  $query=mysql_query($sql);
  if(mysql_num_fields($query)!=0){
    while ($res=mysql_fetch_assoc($query)) {
      echo '<option value="'.$res['number'].'">'.$res['number'].'</option>';
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <style>
  fieldset{display: none;}
  .tableExist{
    font-weight: 700;
    font-size: 16px;
    color:red;
    margin-bottom: 0px!important;
  }
  .photoDataToInsert img{
    width: 50%;
  }
  @media (max-width: 414px){
    .side{
      display: initial;
    }
  }
</style>
<title>CRM System</title>
<!-- Icons -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/simple-line-icons.css" rel="stylesheet">
<!-- Main styles for this application -->
<link href="css/style.css" rel="stylesheet">
<link href="css/all.css" rel="stylesheet">
<link href="css/colpick.css"  rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/chosen.css" type="text/css" >
<link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Holiday Campaigns</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <strong>Activate Holiday Campaign</strong>
                  <a href="#" style="float:right;outline: none;" data-toggle="modal" data-target="#">Click Here for Video Tutorial</a>
                </div>
                <div class="card-block">
                  <div >
                    <p style="margin:0;" class="holiday">IMPORTANT: The holiday campaign includes email, text and postcards only. No voice messages are included. If you want to send a voice message we recommend using the broadcast feature. You can, as an alternate, use the broadcast feature for all holidays but this will have to be done manually on each holiday. You may activate multiple holiday campaigns for different campaigns and have each include different messages.</p></div>
                  </div>
                  <div class="card-header">
                    <strong>Include</strong>
                  </div>
                  <div class="card-block">
                    <div class="form-group has-feedback">
                      <label class=" form-control-label" for="text-input">Name Campaign:</label>
                      <div>
                        <input type="text" id="name_campaign" data-toggle="popover" data-placement="bottom" name="name_campaign" class="form-control name_campaign" placeholder="Create Name">

                      </div>
                    </div>
                    <div class="form-group has-feedback">
                      <label class=" form-control-label" for="select2">Assigned Number: </label>
                      <div >
                        <select id="select_assign_num" name="select_assign_num" class="select_assign_num form-control">
                          <option value="0" default>None</option>
                          <?php getAllPhoneNumbers($userId); ?>
                        </select>
                      </div>
                    </div>
                    <div style="margin-top: 15px;
                    margin-bottom: 15px;">
                    <label class=" form-control-label" for="text-input">Choose Campaign: </label>
                    <select id="select_campaign" name="select_campaign" class="select_campaign form-control">

                      <?php get_list_campaign(); ?>
                    </select>
                  </div>
                  <div style="margin-top: 10px;
                  margin-bottom: 15px;">
                  <label class=" form-control-label" for="select2">Activity Reminder: </label>
                  <select class="activity_reminder form-control">
                    <option value="7" default>7</option>
                    <option value="14">14</option>
                    <option value="21">21</option>
                    <option value="28">28</option>
                  </select>
                </div>
                <div class="form-group has-feedback">
                  <div class="radio ">
                    <span style="margin-left: 3px;">Send<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" class="delay_day" type="number" id="holiday_days" name="text" value="0" style="width: 60px; height: 30px; margin:0px 5px;" > days before date</span>
                    <br>
                    <span><input type="checkbox" class="repeat_year" style="margin:10px 3px;">Repeat every year</span>
                  </div>
                </div>
                <div class="col-md-12 text-center ">
                  <button class="btn btn-primary save_form"  onclick="save_campaign()">Save</button>
                </div>
                <form style="display: none" class="activate_form">

                  <div class="radio">
                    <label for="radio1">
                      <input style="margin-right: 3px;" type="radio" id="radio1" name="itemtype" class="email_radio" value="email" onclick="toggleSet(this,'email')">Email
                    </label>
                  </div>
                  <div class="radio">
                    <label for="radio2">
                      <input style="margin-right: 3px;" type="radio" id="radio2" name="itemtype" value="text" onclick="toggleSet(this,'text')">Text
                    </label>
                  </div>
                  <div class="radio">
                    <label for="radio3">
                      <input style="margin-right: 3px;" type="radio" id="radio3" name="itemtype" value="postcard" onclick="toggleSet(this,'postcard')">PostCard
                    </label>
                  </div>

                  <fieldset style="border:0;" id="email" class="item">
                    <div class="form-group">
                      <input type="text" class="form-control" id="company" placeholder="Enter email subject">
                    </div>
                    <textarea name="editor_email" id="editor_email" rows="10" cols="80">
                    </textarea>
                    <br>
                    <span>Include:</span>
                    <button onclick="signature('email')" style="padding: 5px;" class="btn btn-primary">Email Signature</button>
                    <button onclick="signature('footer')" style="padding: 5px;" class="btn btn-primary">Email Footer</button>
                    <button style="padding: 5px;" class="btn btn-primary">Picture</button>
                    <button style="padding: 5px;" class="btn btn-primary">Logo</button>
                    <br>
                    <table style="margin-top: 10px" class="table table-striped table-bordered datatableEmail table-responsive" id="calendar_info" border=1>
                    </table>
                    <button class="btn btn-primary" onclick="addNewDays()">Load More</button>
                  </fieldset>
                  <fieldset style="border:0;" id="text" class="item">
                   <textarea name="text_editor" id="text_editor" rows="10" cols="80">
                   </textarea><br>
                   <table class="table table-striped table-bordered datatableEmail table-responsive" id="calendar_info" border=1>
                   </table>
                   <button class="btn btn-primary" onclick="addNewDays()">Load More</button>
                 </fieldset>
                 <fieldset style="border:0;" id="postcard" class="item">
                  <main>
                    <div class="col-md-12" style="padding: 0;">
                      <div class="card" style="padding: 0;border:0;">
                        <div class="card-block p-0">
                          <div class="tab-content">
                            <div class="tab-pane active" id="front" style="padding: 0; overflow: auto;">
                              <iframe scrolling="no" style="width: 1100px; height: 650px;" src="postcard-front.php" frameborder="0"></iframe>
                            </div>
                            <div class="tab-pane" id="back" style="padding: 0; overflow: auto;">
                              <iframe  scrolling="no" src="postcard-back.php" style="width: 1100px; height: 650px;" frameborder="0"></iframe>
                            </div>
                          </div>
                        </div>
                        <ul class="nav side" role="tablist" style="text-align:center; position:relative;">
                          <li class="nav-item">
                            <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab"><img src="../img/front.jpg" style="width:100px; height:100px;"><br>Front side</a>
                          </li>
                          <li class="nav-item">
                            <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab"><img src="../img/back.jpg" style="width:100px; height:100px;"><br>Back side</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </main>
                </fieldset>
              </form><br><br>
              <div class="col-md-12 text-center">
                <button class="btn btn-primary activate" onclick="activateCampaign();" style="display: none" onclick="">Activate</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<div class="modal fade" id="ClickVideoHoliday" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-primary" >
    <div class="modal-video">
      <video width="500" height="300" controls="controls">
       <source src="../video/video1.ogv" type='video/ogg; codecs="theora, vorbis"'>
         <source src="../video/video1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
           <source src="../video/video1.webm" type='video/webm; codecs="vp8, vorbis"'>

           </video>
         </div>
       </div>
     </div>

   </main>

   <div class="modal fade" id="photoDataToInsert" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="panel-group photoDataToInsert"  style="text-align: center;">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


</div>
</div>




<!-- Bootstrap and necessary plugins -->
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>


<!-- GenesisUI main scripts -->

<script src="js/app.js"></script>





<!-- Plugins and scripts required by this views -->
<script src="../js/libs/toastr.min.js"></script>
<script src="js/libs/moment.min.js"></script>
<script src="js/libs/daterangepicker.js"></script>
<script src="ckeditor/ckeditor.js"></script>


<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>
<script type="text/javascript">
  document.getElementById('holiday_days').onkeypress = function (e) {
    return !(/[А-Яа-яA-Za-z---+-= ]/.test(String.fromCharCode(e.charCode)));
  }
  var src = '';
  //var canvas = new fabric.Canvas('.component');
  var necessarily_data_array=[];
  var token;
  var filename=0;
  var filekey=1;

  function viewCoupon(){
    $("#readyCoupon").modal("show");
  }

  function signature(type){
    $.ajax({
      url:"include/getSignature.php",
      data:{type:type},
      type:"POST",
      success:function(data){
        var json=JSON.parse(data);
        var fData=CKEDITOR.instances['editor_email'].getData();
        CKEDITOR.instances['editor_email'].setData(fData+json);
      }
    })
  }

  if($.cookie('top_position')!=null){
    top_block_add = $.cookie('top_position');
  }else{top_block_add = "250px";}
  top_position_add = top_block_add.substr(0,3)*1;
  $(".popup_close").click(function(){
    $(".preview_pop").hide();
  });


  $(".pod_menu_cat_td a").click(function(){
    id_tovar = $(this).attr("id_tovar");
    name_tovar = $(this).find("span").text();
    bg_overlayImage = $(this).attr("bg_overlayImage");
    bg_backgroundImage = $(this).attr("bg_backgroundImage");
    bg_kontur_img_fade = $(this).attr("bg_kontur_img");
    price_tovar = $(this).attr("price");
    $(".konstruktor_page .slider_price_all strong").text(price_tovar);
    $("[name=id_product]").val(id_tovar);
    new_url = "/konstruktor.htm?id_outline="+id_tovar;
    window.history.pushState({path:new_url},'',new_url);
    $.post("/jscripts/ajax/change_product.php", {id_tovar:id_tovar },
      function(data){
        obj = jQuery.parseJSON(data);
        $(".info_product_desc").html(obj.text);
      });
    change_product(bg_overlayImage ,name_tovar,id_tovar, bg_backgroundImage);
  });

  var canvasOperations = {
    loadFromUrl : function(){
      fabric.Image.fromURL(src, function(image){
        canvas.add(image.set({
          id : 'canvas'+filename,
          alt : 'canvas'+filename,
          width : canvas.width / 2,
          height : canvas.height / 2,
          perPixelTargetFind : true,
          targetFindTolerance : 4
        }));
      });
    },
  }

  $(".show_menu_konstr").click(function(){
    $(".chuse_product_konstruktor").fadeIn(200);
  });
  $(".chuse_product_konstruktor_top").click(function(){
    $(".chuse_product_konstruktor").fadeOut(200);
  })

  $( function() {
    $("#resize").resizable({autoHide:true,alsoResize:'#preview'});
  } );

  function del_input(id){
    var parent=$(id).parent();
    $(parent).remove();
  }

  function viewCoupon(){
    $(".component").css('border', '0px');
    $('.component').html2canvas({
      onrendered: function (canvas) {
        $(".imageData").attr('src',canvas.toDataURL("image/png"));
        $(".component").css('border', '3px solid #49708A');
      }
    });
  }

  function saveCoupon(){
    var file = $(".imageData").attr('src');
    var campaignName=$(".campaignName").val();
    var mobileNumber=$(".mobileNumber").val();
    if(mobileNumber==''){
      alert("Enter Mobile Number");
    }else{
      $.ajax({
        url: 'include/coupon/savePostcard.php',
        data: {file:file,campaignName:campaignName,mobileNumber:mobileNumber},
        type: 'POST',
        success:function(){}})
      window.location='coupon-builder.php';
    }
  }
  function addNewFile(filename){
    $(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');
  }

  function clearAll(){
    canvas.clear();
    filename=0;
    filekey=1;
    $(".field").each(function(){
      $(this).hide();
    })
  }

  function delFile(id,idFile){
    var key=idFile+1;
    var name='canvas'+key;
    console.log(name);
    var lengthCanvas = canvas['_objects'].length;
    for(var k in canvas['_objects']){
      console.log(canvas['_objects'][k]['alt']);
      if(canvas['_objects'][k]['alt']==name){
        canvas['_objects'][k].remove();
        var parent=$(id).parent();
        $(parent).hide();
      }
    }
    console.log(canvas);
    filekey--;
    canvas.renderAll();
    $(".addNewFile").val('');
  }

  function readURL(input){
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        src=e.target.result;
        canvasOperations.loadFromUrl();
      };
      addNewFile(filename);
      reader.readAsDataURL(input.files[0]);
      filename++;
      filekey++;
    }
  }
  $(".del_fil").click();
</script>
<script type="text/javascript">
  CKEDITOR.plugins.add( 'tokens',
  {
 requires : ['richcombo'], //, 'styles' ],
 init : function( editor )
 {
  var config = editor.config,
  lang = editor.lang.format;
    var tags = []; //new Array();
    //this.add('value', 'drop_text', 'drop_label');
    tags[0]=["{$first_name}", "first_name", "first_name"];
    tags[1]=["{$last_name}", "last_name", "last_name"];
    tags[2]=["{$mobile_phone}", "mobile_phone", "mobile_phone"];
    tags[3]=["{$email}", "email", "email"];
    tags[4]=["{$address}", "address", "address"];
    tags[5]=["{$address1}", "address1", "address1"];
    tags[6]=["{$city}", "city", "city"];
    tags[7]=["{$state}", "state", "state"];
    tags[8]=["{$postal_code}", "postal_code", "postal_code"];
    editor.ui.addRichCombo( 'tokens',
    {
      label : "Insert data",
      className : 'cke_format',
      multiSelect : false,

      panel :
      {
       css : [ config.contentsCss, CKEDITOR.getUrl( editor.skinPath + 'editor.css' ) ],
       voiceLabel : lang.panelVoiceLabel
     },
     init : function()
     {
      this.startGroup( "" );
             //this.add('value', 'drop_text', 'drop_label');
             for (var this_tag in tags){
              this.add(tags[this_tag][0], tags[this_tag][1]);
            }
          },
          onClick : function( value )
          {
           editor.focus();
           editor.fire( 'saveSnapshot' );
           editor.insertHtml(value);
           editor.fire( 'saveSnapshot' );
         }
       });
  }
});

</script>
<script type="text/javascript">
  CKEDITOR.replace('editor_email',{
    extraPlugins: 'tokens',
    toolbar :
    [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
    { name: 'links', items: [ 'Link', 'Unlink'] },
    { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize','tokens' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools', items: [ 'Maximize'] },
    ]
  });
  CKEDITOR.replace('text_editor',
  {
    toolbar :
    [
    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
    { name: 'tools', items : [ 'Maximize','-' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
    ]});


  var type_variable='';
  var error;
  var data;
  $('body').on('hidden.bs.modal', '.modal', function () {
    $('video').trigger('pause');
  });
  function removeSideBar(){$('body').removeClass('sidebar-hidden');}
  function closeSideBar (){$('body').addClass('sidebar-hidden');}
  function toggleSet(rad,type){
    if(type=='postcard'){
      closeSideBar();
    }
    else{
      removeSideBar();
    };

    getlist(type);
    type_variable=$(rad).val();
    var type = rad.value;
    for(var k=0,elm;elm=rad.form.elements[k];k++)
      if(elm.className=='item')
        elm.style.display = elm.id==type? 'block':'';
    }
    function save_campaign()
    {
      try{
        //check_data();
        //build_data();
        add_new_campaign();

      }catch(e)
      {
        alert(error);
      }
    }
    var repeat_year;
    function add_new_campaign()
    {
      $('.sk-circle').css('display','block');
      var delay_day=$('.delay_day').val();
      var activity_reminder=$(".activity_reminder").val();
      var select_campaign=$('.select_campaign').val();
      var name_campaign=$(".name_campaign").val();
      var assignNumber=$(".select_assign_num").val();
      if(name_campaign==''){
        error="Enter Name Campaign";
        throw new SyntaxError("empty_data");
      }else if(assignNumber=='0'){
        error="Enter assigned number";
        throw new SyntaxError("empty_data");
      }else
      if(select_campaign=='0'){
        error="Select Campaign";
        throw new SyntaxError("empty_data");
      }else if($(".repeat_year").prop("checked")){
        repeat_year=1;
      }else{
        repeat_year=0;
      }
      $.ajax({
        url:"include/holiday_campaign/save_new_holiday_company.php",
        data:{type_variable:type_variable,select_campaign:select_campaign,data:data,delay_day:delay_day,repeat_year:repeat_year,activity_reminder:activity_reminder,name_campaign:name_campaign,assignNumber:assignNumber},
        type:"POST",
        success:function(data){
          $('.sk-circle').css('display','none');
          //alert(data);
          var json=JSON.parse(data);
          if(json['success']==1){
            $(".email_radio").click();
            $(".activate").css("display","block");
            $(".activate_form").css("display","block");
            $(".save_form").attr('disabled','true');
            id_campaign=json['id'];
            //save_postcard();
          }else{
            alert(json['error']);
          }
        }
      })
    }

    function check_data()
    {
      /*
      if(type_variable==''){
        error="Choose data";
        throw new SyntaxError("empty_data");
      }
      */
      if($('.select_campaign').val()==0){
        error="Choose campaign";
        throw new SyntaxError("empty_data");
      }
    }

    function build_data()
    {
      if(type_variable=='email'){
        data=CKEDITOR.instances['editor_email'].getData();
      }else if(type_variable=='text'){
        data=CKEDITOR.instances['text_editor'].getData();
      }else{
        data='rvmd';
      }
    }




    var periodDays = [];
    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth();
    var date = today.getDate();
    var ikey=0;
    var out;
    var j=30;
    var typeGlobal='email';

    function buildArrayDate(j=30){
      periodDays = [];
      for(i=0; i<=j; i++){
        var day=new Date(year, month, date + (i));
        var dd = day.getDate();
        var mm = day.getMonth()+1;
        var yyyy = day.getFullYear();
        if(dd<10) {
          dd='0'+dd
        }
        if(mm<10) {
          mm='0'+mm
        }
        curr_day = yyyy+'-'+mm+'-'+dd;
        periodDays.push(curr_day);
      }
    }

    getTable(30,"email");

    function getTable(count,type){
      $(".datatableEmail").html('');
      $.ajax({
        url:"include/table_center/getTable.php",
        data:{count:count,type:type},
        type:"POST",
        success:function(data){
          //alert(data);
          buildArrayDate(count);
          viewAllDate(JSON.parse(data));
        }
      })
    }

    function getExistDate(str,array){
      var existStr=0;
      for(var l in array){
        if(array[l]==str){
          existStr++;
        }
      }
      return existStr;
    }

    function viewAllDate(array){
      ikey=0;
      $(".datatableEmail").html('');
      for(var k in periodDays){
        var existDate = getExistDate(periodDays[k],array);
        if (existDate!=0){
          out='<td><b>Day '+ikey+'</b><br>'+periodDays[k]+'<br><p class="tableExist">'+existDate+' Exist</p></td>';
        }else{
          out='<td><b>Day '+ikey+'</b><br>'+periodDays[k]+'</td>';
        }
        if(ikey%7==0){
          $(".datatableEmail").append('<tr class="odd">');
        }
        $(".datatableEmail").append(out);
        ikey++;
      }
    }

    function addNewDays(){
      j=j+30;
      getTable(j,typeGlobal);
    }

    function getlist(type){
      j=30;
      typeGlobal=type;
      getTable(j,typeGlobal);
    }

    $(function(){
      $('form').submit(function() {
        return false;
      });
    });
    var type_Campaign='holiday';

    function save_postcard(){
      $('.sk-circle').css('display','block');
      type_variable='postcard';
      var repeat_postcard;
      if($(".repeat_year").prop("checked")){
        repeat_postcard=1;
      }else{
        repeat_postcard=0;
      }
      var delay_day_postcard=$(".delay_day").val();
      $.ajax({
        url:"include/postcard/getPicture.php",
        data:{id_campaign:id_campaign,type_Campaign:type_Campaign,repeat_postcard:repeat_postcard,delay_day_postcard:delay_day_postcard},
        type:"POST",
        success:function(data){
          $('.sk-circle').css('display','none');
          //alert(data);
          insertPhotoIntoModal(JSON.parse(data));
        }
      })
    }
    function insertPhotoIntoModal(array){
      var out='';
      for(var k in array){
        out+="<img src='"+array[k]+"' alt='alt'>"
      }
      $(".photoDataToInsert").html(out);

    }

    function activateCampaign(){
      build_data();
      $('.sk-circle').css('display','block');
      //var select_campaign=$('.select_campaign').val();
      var delay_day=$('.delay_day').val();
      if(type_variable!='postcard'){
        $.ajax({
          url:'include/holiday_campaign/saveCampaign.php',
          data:{type_variable:type_variable,id_campaign:id_campaign,data:data,delay_day:delay_day,repeat_year:repeat_year},
          type:"POST",
          success:function(data){window.location='holiday-campaigns.php';
          //alert(data);
        }
      })
      }else{
        window.location='holiday-campaigns.php';
      }
    }


  </script>
  <script src="js/libs/toastr.js"></script>
  <script src="components/user_notifications.js"></script>
</body>

</html>
