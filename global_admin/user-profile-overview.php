<?php
include '../include/connect_to_bd.php';
include "include/session.php";
if(!isset($_GET['id'])){
  header("Location: user-profile-search.php");
}else{
  $id=$_GET['id'];
  $query_to_user=mysql_query("SELECT * FROM user WHERE id='$id'");
  $aray_user=mysql_fetch_assoc($query_to_user);
  if($aray_user['picture']!=''){
    $picture='../upload_photo/'.$aray_user['dir_img'].'/'.$aray_user['picture'];
  }else{
    $picture="";
  }
  if($aray_user['logo']!=''){
    $logo='../upload_photo/'.$aray_user['dir_img'].'/'.$aray_user['logo'];
  }else{
    $logo="";
  }
  function get_admin_list($id){
    $sql=mysql_query("SELECT * FROM admin");
    while($result=mysql_fetch_assoc($sql)){
      if($result['id']==$id){
        echo "<option selected value='".$result['id']."'>".$result['username']."</option>";
      }else{
        echo "<option  value='".$result['id']."'>".$result['username']."</option>";
      }
    }
  }
}
$include_function=$aray_user['include_function'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  #cke_2_contents{
    height: 100px!important;
  }
  #cke_1_contents{
    height: 100px!important;
  }
  input[type='number'] {
    -moz-appearance: textfield;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  .fiieldest{
    margin-top: 15px;
    display: inline-block;
    margin-right: 25px;
  }
  .input-file-row-1:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
  }
  .input-group-addon{
    color:inherit!important;
    background-color: inherit!important;
  }
  .form-control:disabled{
    opacity: 0.7;
    color:#000;
  }

  .input-file-row-1{
    display: inline-block;
    position: relative;
    width: 100px;
  }

  html[xmlns] .input-file-row-1{
    display: block;
  }

  * html .input-file-row-1 {
    height: 1%;
  }

  .upload-file-container {
    height: 137px;
    overflow: hidden;
    background: url(http://i.imgur.com/AeUEdJb.png) top center no-repeat;
    margin-left: 23px;
  }

  .upload-file-container:first-child {
    margin-left: 0;
  }

  .upload-file-container > img {
    opacity: 1;
    width: 100px;
    height: 93px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
  }

  .upload-file-container-text{
    font-size: 12px;
    color: #20a8d8;;
    line-height: 17px;
    text-align: center;
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100px;
    height: 35px;
  }
  .upload-file-container-text > span{
    border-bottom: 1px solid #20a8d8;;
    cursor: pointer;
  }
  .upload-file-container input  {
    position: absolute;
    left: 0;
    bottom: 0;
    font-size: 1px;
    opacity: 0;
    filter: alpha(opacity=0);
    margin: 0;
    padding: 0;
    border: none;
    width: 70px;
    height: 50px;
    cursor: pointer;
  }
  body{
    position: relative;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }

</style>

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'componets/header.php';  ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <!-- Main content -->
    <main class="main" <?php echo "id='".$id=$_GET['id']."'"; ?> >
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="user-profile-search.php">User Profile Search</a></li>
        <li class="breadcrumb-item active">User Profile Overview</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Add User</strong>
                  <small>new</small>
                  <button style="position: relative;float: right;padding: 0.5rem 2rem;
                  top: 4px;" onclick="save_new_user()" type="submit" class="btn btn-primary"><i class="fa fa-save" style="margin-right: 10px"> </i>  Save </button>
                </div>
                <div class="card-block" id="sortable">
                  <div class="form-group">
                    <label for="company">On Call Phone Number</label>
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">+</span>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" <?php echo "value='".$aray_user['on_call_phone_number']."'" ?> type="number" class="checkMobile input_data_val input_data form-control"  placeholder="Enter On Call Phone Number" id="phone_number">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="last_name">First Name</label>
                    <input id="last_name" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" <?php echo "value='".$aray_user['first_name']."'" ?> placeholder="Enter First Name">
                  </div>

                  <div class="form-group">
                    <label for="vat">Last Name</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" <?php echo "value='".$aray_user['last_name']."'" ?> class="input_data_val input_data form-control"  placeholder="Enter Last Name">
                  </div>
                  <div class="form-group email">
                    <label for="street">E-mail</label>
                    <input onkeypress="change_input_data(this)" id="email" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" <?php echo "value='".$aray_user['email']."'" ?>  placeholder="Enter E-mail">
                  </div>
                  <div class="form-group mobile_phone">
                    <label for="street">Mobile Phone</label>
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">+</span>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" class="checkMobile input_data_val input_data form-control" <?php echo "value='".$aray_user['mobile_phone']."'" ?> placeholder="Enter Mobile Phone" id="mobile_phone">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="street">Alternative Phone</label>
                    <!-- <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" <?php echo "value='".$aray_user['alt_phone']."'" ?> class="checkMobile input_data_val input_data form-control"  placeholder="Enter Alternative Phone" id="alternative_phone">-->
                    <div class="input-group">
                      <span class="input-group-addon" id="basic-addon1">+</span>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="number" <?php echo "value='".$aray_user['alt_phone']."'" ?> class="checkMobile input_data_val input_data form-control"  placeholder="Enter Alternative Phone" id="alternative_phone">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="country">Street Address</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" id="country" <?php echo "value='".$aray_user['street_address']."'" ?> placeholder="Enter Street Address">
                  </div>

                  <div class="form-group">
                    <label for="street">Suite</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" <?php echo "value='".$aray_user['suite']."'" ?> class="input_data_val input_data form-control"  placeholder="Enter Suite">
                  </div>

                  <div class="form-group">
                    <label for="country">State</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" id="country" <?php echo "value='".$aray_user['state']."'" ?> placeholder="State name">
                  </div>
                  <div class="row">

                    <div class="form-group col-sm-8">
                      <label for="city">City</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" id="city" <?php echo "value='".$aray_user['city']."'" ?> placeholder="Enter your city">
                    </div>

                    <div class="form-group col-sm-4">
                      <label for="postal-code">Postal Code</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile input_data_val input_data form-control" id="postal_code" <?php echo "value='".$aray_user['postal_code']."'" ?> placeholder="Postal Code">
                    </div>

                  </div>
                  <!--/.row-->

                  <div class="form-group">
                    <label for="country">Birthday Mounth</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" id="overview_dated" <?php echo "value='".$aray_user['birthday_mounth']."'" ?> placeholder="Enter Birthday Mounth">
                  </div>
                  <div class="form-group">
                    <label for="country">Industry</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" <?php echo "value='".$aray_user['industry']."'" ?> class="input_data_val input_data form-control"  placeholder="Enter Industry">
                  </div>

                  <div class="form-group">
                    <label for="country">Username</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" id="country" <?php echo "value='".$aray_user['username']."'" ?> placeholder="Enter Username">
                  </div>

                  <div class="form-group">
                    <label for="country">New Password</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="password" class=" input_data form-control"  id="pass" placeholder="Enter Password">
                  </div>
                  <div class="form-group">
                    <div class="form-group">
                      <label for="country">Confirm Password</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="password" class=" input_data form-control"  id="conf_pass"  placeholder="Enter Confirm Password">
                    </div>
                    <div class="form-group">

                      <fieldset class="fiieldest">
                        <div class="input-file-row-1">
                          <div class="upload-file-container">
                            <img id="image1" <?php echo "src='".$picture."'" ?> alt="" />
                            <div class="upload-file-container-text">
                              <span>Upload Picture</span>
                              <input type="file" name="imgInput1" class="photo" id="imgInput1" />
                            </div>
                          </div>
                        </div>
                      </fieldset>

                      <fieldset class="fiieldest">
                        <div class="input-file-row-1">
                          <div class="upload-file-container">
                            <img id="image2" <?php echo "src='".$logo."'" ?> alt="" />
                            <div class="upload-file-container-text">
                              <span>Upload Logo</span>
                              <input type="file" name="imgInput2" class="photo" id="imgInput2" />
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </div>
                    <div style="display: inline-block;width: 120px!important">ContactTRACS</div>
                    <label class="switch switch-icon switch-pill switch-primary">
                      <input onclick="check_contacttracs(this)" type="checkbox" value="contact" <?php if($include_function=='contact'){echo "checked";} ?> class="check_function check_contacttracs switch-input" >
                      <span class="switch-label" data-on="&#xf00c" data-off="&#xf00d"></span>
                      <span class="switch-handle"></span>
                    </label>
                    <br>
                    <div style="display: inline-block;width: 120px!important">ProspectTRACS</div>
                    <label class="switch switch-icon switch-pill switch-primary">
                      <input onchange="check_prospecttracs(this)" type="checkbox" value="prospect" <?php if($include_function=='prospect'){echo "checked";} ?> class="check_prospecttracs switch-input check_function" >
                      <span class="switch-label" data-on="&#xf00c" data-off="&#xf00d"></span>
                      <span class="switch-handle"></span>
                    </label>
                    <br>
                    <div style="display: inline-block;width: 120px!important">All features</div>
                    <label class="switch switch-icon switch-pill switch-primary">
                      <input <?php if($include_function=='all'){echo "checked";} ?> onchange="check_allfeatures(this)" type="checkbox" value="all"  class="check_allfeatures switch-input check_function" >
                      <span class="switch-label" data-on="&#xf00c" data-off="&#xf00d"></span>
                      <span class="switch-handle"></span>
                    </label><br><br>
                    <div class="form-group has-feedback">
                     <label for="country">Assigned agent </label>
                     <select id="select" name="select" class="input_data form-control">
                       <?php
                       get_admin_list($aray_user['admin_id']);
                       ?>
                     </select>

                   </div>

                   <div class="form-group" style="display: none">
                    <label for="country">Tagline 1</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" <?php echo "value='".$aray_user['tagline1']."'" ?> id="country" placeholder="Enter Tagline 1">
                  </div>
                  <div class="form-group" style="display: none">
                    <label for="country">Tagline 2</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" <?php echo "value='".$aray_user['tagline2']."'" ?> id="country" placeholder="Enter Tagline 2">
                  </div>
                  <div class="form-group" style="display: none">
                    <label for="country">Tagline 3</label>
                    <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val input_data form-control" <?php echo "value='".$aray_user['tagline3']."'" ?> id="country" placeholder="Enter Tagline 3">
                  </div>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <strong>Email Signature</strong>
              </div>
              <div class="card-block">
                <div class="form-group">
                  <textarea onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val form-control" name="editor_email_signature" id="editor_email_signature" rows="10" cols="80">
                    <?php echo $aray_user['email_tagline']; ?>
                  </textarea>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <strong>​Postcard Signature</strong>
              </div>
              <div class="card-block">
                <div class="form-group">
                  <textarea onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val form-control" name="editor_postcard_signature" id="editor_postcard_signature" rows="10" cols="80">
                    <?php echo $aray_user['postcard_tagline']; ?>
                  </textarea>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header">
                <strong>Billing Preferences</strong>
              </div>
              <div class="card-block">
                <div class="form-group">
                  <label for="country">Base Price</label>
                  <!--<input disabled <?php echo "value='".$aray_user['base_price']."'" ?>  type="number" class="form-control" >-->
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd" aria-hidden="true"></i></span>
                    <input  <?php echo "value='".$aray_user['base_price']."'" ?>  type="number" class="form-control input_data" aria-describedby="basic-addon1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="country">PostCard Price</label>
                  <!-- <input disabled <?php echo "value='".$aray_user['postcard_price']."'" ?>  type="number" class="form-control" >-->
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd" aria-hidden="true"></i></span>
                    <input  <?php echo "value='".$aray_user['postcard_price']."'" ?>  type="number" class="form-control input_data"  aria-describedby="basic-addon1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="country">RVM Price</label>
                  <!--<input disabled <?php echo "value='".$aray_user['rvm_price']."'" ?> type="number" class="form-control"  >-->
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd" aria-hidden="true"></i></span>
                    <input  <?php echo "value='".$aray_user['rvm_price']."'" ?> type="number" class="form-control input_data"  aria-describedby="basic-addon1">
                  </div>
                </div>
                <div class="form-group">
                  <label  for="country">Text Price</label>
                  <!--<input disabled type="number" <?php echo "value='".$aray_user['text_price']."'" ?> class="form-control"  >-->
                  <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-usd" aria-hidden="true"></i></span>
                    <input  type="number" <?php echo "value='".$aray_user['text_price']."'" ?> class="form-control input_data"    aria-describedby="basic-addon1">
                  </div>
                </div>
              </div>
              <div class="form-group" style="text-align: center;">
                <button style="padding: 0.5rem 2rem;" onclick="save_new_user()" type="submit" class="btn btn-primary"><i class="fa fa-save" style="margin-right: 10px"> </i>  Save </button>
                <button style="margin-left: 30px;display: none;padding: 0.5rem 2rem;" type="button" class="btn success_btn btn-outline-success">Success</button>
                <button style="margin-left: 30px;display: none;padding: 0.5rem 2rem;" type="button" class="btn danger_btn btn-outline-danger">Error</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  </div>

  <!-- Bootstrap and necessary plugins -->
  <script src="js/libs/jquery.min.js"></script>
  <script src="js/libs/tether.min.js"></script>
  <script src="js/libs/bootstrap.min.js"></script>
  <script src="ckeditor/ckeditor.js"></script>
  <script src="js/libs/pace.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $( function() {
      $( "#overview_dated" ).datepicker();
    } );
  </script>
  <!-- GenesisUI main scripts -->
  <script src="js/app.js"></script>
  <!-- Plugins and scripts required by this views -->
  <script src="../js/libs/toastr.min.js"></script>
  <script src="js/libs/gauge.min.js"></script>
  <script src="js/libs/moment.min.js"></script>



  <!-- Custom scripts required by this view -->
  <script src="js/views/main.js"></script>
  <script>
    CKEDITOR.replace('editor_email_signature',{
      toolbar :
    [
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
    { name: 'links', items: [ 'Link', 'Unlink'] },
    { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
      { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize'] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools', items: [ 'Maximize'] },
    ]
    });
    CKEDITOR.replace('editor_postcard_signature',{
      toolbar :
    [
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
    { name: 'links', items: [ 'Link', 'Unlink'] },
    { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
      { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize'] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools', items: [ 'Maximize'] },
    ]
    });
    $(".checkMobile").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
             (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
             (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
             (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
             (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
               return;
             }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
        }
      });

    var data_array=[];
    var user_id;
    var include_function='all';
    var id=$('.main').attr('id');
    function save_new_user(){
      try{
        checkInput();
        check_function();
        check_two_tracs();
        build_array();
        send_ajax_query();
      }catch(e){
        console.log(e);
      }
    }
    function checkInput(){
      $('.input_data_val').each(function(){
        if($(this).val() == ''){
          notification(this,'Empty Data');
          throw new SyntaxError("empty_data");
        }
      });
    }

    function build_array(){
      var i=1;
      data_array=[];
      $('.input_data').each(function(){
        data_array.push($(this).val());
      });
      var editor_data1 = CKEDITOR.instances['editor_email_signature'].getData();
      var editor_data2 = CKEDITOR.instances['editor_postcard_signature'].getData();
      data_array.push(editor_data1);
      data_array.push(editor_data2);
      console.log(data_array);
    }
    function send_ajax_query(){
      $('.sk-circle').css('display','block');
      $.ajax({
        url:"include/user/change_user_data.php",
        type:"POST",
        data:{data_array:data_array,id:id,include_function:include_function},
        success:function(data){
          //alert(data);
          parsing_json(data);
          $('.sk-circle').css('display','none');
        }
      })
    }
    function sendphoto(id){
      var file1 = $('#imgInput1');
      var formData = new FormData();
      formData.append('upload1', file1.prop('files')[0]);
      formData.append('type_photo', 'picture');
      formData.append('user_id', id);
      if(file1.prop('files')[0]!='undefined'){
        $.ajax({
          url: 'include/user/send_image.php',
          data: formData,
          processData: false,
          contentType: false,
          type: 'POST',
          success:function(data){
            //console.log(data);
            //alert(data);
          }
        })
      }
      var file1 = $('#imgInput2');
      var formData = new FormData();
      formData.append('upload1', file1.prop('files')[0]);
      formData.append('type_photo', 'logo');
      formData.append('user_id', id);
      if(file1.prop('files')[0]!='undefined'){
        $.ajax({
          url: 'include/user/send_image.php',
          data: formData,
          processData: false,
          contentType: false,
          type: 'POST',
          success:function(data){
            //console.log(data);
            //alert(data);
          }
        })
      }
    }

    function parsing_json(arr){
      var json=JSON.parse(arr);
      sendphoto(id);
      if(json['success']=='1'){
        window.location='user-profile-search.php';
      }else{
        if(json['error']=='email'){
          notification('#email','Email Exist');
        }else{
         notification('#conf_pass','Incorrect Password');
       }
     }
   }

   function change_input_data(id){
    var parent=$(id).parent(".form-group");
    $(id).removeClass('form-control-danger');
    $(parent).removeClass('has-danger');
  }

  function notification(id,type_error){
    var parent=$(id).parent(".form-group");
    $(parent).addClass('has-danger');
    $(id).addClass('form-control-danger');
    $(id).attr("data-content",type_error);
    $('.danger_btn').css("display","inline-block");
    setTimeout(function(){
      $('.danger_btn').css("display","none");
    },1500);
  }
</script>
<script>
  function readURL(input,id) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#image'+id).attr('src', e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imgInput1").change(function(){
    readURL(this,'1');
    $(".upload-file-container > #image1").css('opacity','1');
  });
  $("#imgInput2").change(function(){
    readURL(this,'2');
    $(".upload-file-container > #image2").css('opacity','1');
  });

  function check_contacttracs(id)
  {
    var prop=$(id).prop('checked');
    if(prop){
      $('.check_allfeatures').prop('checked' , false);
      $('.check_prospecttracs').prop('checked' , false);
      include_function='contact';
    }else{
      $('.check_contacttracs').prop('checked' , false);
      $('.check_prospecttracs').prop('checked' , false);
    }
  }

  function check_two_tracs()
  {
    var prop1=$('.check_contacttracs').prop('checked');
    var prop2=$('.check_prospecttracs').prop('checked');
    if(prop1 && prop2){
      include_function='all';
    }
  }

  function check_function()
  {
    var val='';
    $(".check_function").each(function(){
     if($(this).prop('checked')){
      val='1';
    }
  })
    if(val==''){
      alert("Check Include Function");
      throw new SyntaxError("empty_data");
    }
  }

  function check_prospecttracs(id)
  {
    var prop=$(id).prop('checked');
    if(prop){
      $('.check_allfeatures').prop('checked' , false);
      $('.check_contacttracs').prop('checked' , false);
      include_function='prospect';
    }else{
      $('.check_contacttracs').prop('checked' , false);
      $('.check_prospecttracs').prop('checked' , false);
    }
  }

  function check_allfeatures(id)
  {
    var prop=$(id).prop('checked');
    if(prop){
      $('.check_contacttracs').prop('checked' , false);
      $('.check_prospecttracs').prop('checked' , false);
      include_function='all';
    }
  }
</script>

</body>

</html>
