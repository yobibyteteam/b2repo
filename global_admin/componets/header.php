    <header class="app-header navbar">
      <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
      <a class="navbar-brand" href="index.php"></a>
      <ul class="nav navbar-nav d-md-down-none b-r-1">
        <li class="nav-item">
          <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>
      </ul>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown" style="margin-right: 30px!important">
          <a class="nav-link nav-pill avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="img/avatars/9.png" class="img-avatar" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="include/logout_user.php"><i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
    </header>