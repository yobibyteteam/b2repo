<?php
include '../connect_to_bd.php';
require_once '../classes/Broadcast.php';

$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_text'];

$broadcast=new Broadcast();

try{
	$broadcast->add_text($id_campaign,$editor_text);
	echo json_encode(array("success"=>1));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>