<?php

include 'connect_to_bd.php';
require_once 'classes/User.php';
session_start();
$input_data=$_POST['data_array'];
$id=$_POST['id'];

$text="Manager change data";

$user=new User();
try{
	$user->set_data_for_change($input_data,$id);
	$user->check_pass_for_change();
	$user->check_email_for_change();
	$user->query_for_change();
	$user->add_to_activity_log($text);
	echo json_encode(array("success"=>"1","user_id"=>$id));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>