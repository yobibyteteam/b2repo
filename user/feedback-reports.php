<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$list='';
$query_model="SELECT * FROM token WHERE token='".$_SESSION['user_token']."'";
$query=mysql_query($query_model);
$array_token=mysql_fetch_assoc($query);
$query_model="UPDATE feedback SET view='1' WHERE view='0'";
mysql_query($query_model);

function get_list($id){
  global $list;
  $query_model="SELECT * FROM feedback WHERE user_id='".$id."' ORDER BY id DESC";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)==0){
    $list='
    <div class="col-md-12">
    <div class="card">
    <div class="card-block text-center">
    <h5>Feedback List is Empty</h5>
    </div>
    </div>
    </div>';
  }else{
    $i=1;
    while($res=mysql_fetch_assoc($query))
    {
      $format_date=date('d.m.Y',strtotime($res['date']));
      $question=build_list_qestion($res['id']);
      $list='
      <div class="col-md-6" style="flex:100%; max-width:100%">
      <div class="card">
      <div class="card-header">
      <div class="row">
      <div class="col-md-4">
      <label style="margin-bottom:0px;" for="name_broadcast"><b>Date:</b> <span style="margin-left: 3px;">'.$format_date.'</span> </label><br></div>
      <div class="col-md-7" style="flex:100%; max-width:100%">
      <label style="margin-bottom:0px;" for="name_broadcast"> <b> Overall Rating:</b></label>
      <span class="rating" id="'.$res['star'].'">
      <span onclick="star(5)" class="star star5"></span><span onclick="star(4)" class="star star4"></span><span onclick="star(3)" class="star star3"></span><span onclick="star(2)" class="star star2"></span><span onclick="star(1)" class="star star1"></span>
      </span></div></div>
      <div class="card-actions">
      <a class="feedback'.$i.'_on btn-minimize" data-parent="#accordion" data-toggle="collapse" data-target="#feedback'.$i.'_off" style="width: 35px;" href=""><i class="icon-arrow-down"></i></a>
      <a onclick="modal_show('.$res['id'].')" style="width: 35px;" href="#" class="btn-close"><i class="icon-close"></i></a>
      </div>
      </div>
      <div id="feedback'.$i.'_off" class="collapse in">
      <div class="card-block">
      <div class="row">'.$question.'
      </div>
      </div>
      </div>
      </div>
      </div>';
      $i++;
      echo $list;
    }
  }
}

function build_list_qestion($id)
{
  $query_model="SELECT * FROM feedback_reports WHERE feedback_id='".$id."'";
  $query=mysql_query($query_model);
  $array_question='';
  while ($res=mysql_fetch_assoc($query)) {
        //$substr=mb_substr($res['question'],2);
    if(($res['answer']!='') && ($res['answer']!='0')){
      $array_question.='
      <div class="col-md-12">
      <label for="name_broadcast"><b> Question:</b> <span style="margin-left: 3px; color: green;">'.$res['question'].'</span> </label><br>
      <label for="name_broadcast"><b>Answer:</b> <span style="margin-left: 3px; color: red;">'.$res['answer'].'</span> </label><br><hr>
      </div>';
    }
  }
  return $array_question;
}
?>
<!--
 * GenesisUI - Bootstrap 4 Admin Template
 * @version v1.8.4
 * @link https://genesisui.com
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license Commercial
-->
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <!-- Include Editor style. -->
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
  <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/css/froala_style.min.css' rel='stylesheet' type='text/css' />
  <script src="//cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
  <!-- Include JS file. -->
  <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.2/js/froala_editor.min.js'></script>
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
</head>
<style>
.starclick:before{
  color:#e3cf7a!important;
  content: "\f005"!important;
}
body{
  position: relative;
}
.sk-circle{
  display: none;
  z-index: 10001;
  width: 40px;
  height: 40px;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  margin: auto;
}
.rating {
  unicode-bidi: bidi-override;
  direction: rtl;
  font-size: 15px;
}
.rating span.star,
.rating span.star {
  font-family: FontAwesome;
  font-weight: normal;
  font-style: normal;
  display: inline-block;
}
.rating span.star:hover,
.rating span.star:hover {
  cursor: pointer;
}
.rating span.star:before,
.rating span.star:before {
  content: "\f006";
  padding-right: 5px;
  color: #999999;
}
.rating span.star:hover:before,
.rating span.star:hover:before,
.rating span.star:hover ~ span.star:before,
.rating span.star:hover ~ span.star:before {
  content: "\f005";
  color: #e3cf7a;
}
</style>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
   <?php include "sidebar.php"; ?>
   <main class="main">
    <ol class="breadcrumb mb-0">
      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
      <li class="breadcrumb-item active">Feedback Reports</li>
    </ol>
    <div class="container-fluid">
      <div class="animated fadeIn">
        <div class="row">
          <?php get_list($array_token['client_id']);  ?>
        </div>
        <div class="modal fade" id="ClickVideoCampaignBuilder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-primary" >
            <div class="modal-video">
              <iframe id="if" width="560" height="315" src="https://www.youtube.com/embed/BPNTC7uZYrI" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </main>
      <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-danger" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Delete</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete this?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
              <button type="button" onclick="delete_feedback()" data-dismiss="modal" class="btn btn-primary">Yes</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Bootstrap and necessary plugins -->
      <script src="js/libs/jquery.min.js"></script>
      <script src="js/libs/tether.min.js"></script>
      <script src="js/libs/bootstrap.min.js"></script>
      <script src="js/libs/pace.min.js"></script>
      <script src="js/libs/Chart.min.js"></script>
      <script src="js/app.js"></script>



      <script>CKEDITOR.replace('editor1');</script>
      <script>CKEDITOR.replace('editor2',
      {
        toolbar :
        [
        { name: 'basicstyles', items : [ 'Bold','Italic' ] },
        { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
        { name: 'tools', items : [ 'Maximize','-','About' ] }
        ]
      });</script>



      <script>
        var feedback_id;
        set_star_val();
        $('#show_link').click(function(){
          $('#block').show("normal");
        })

        $('#hide_link').click(function(){
          $('#block').hide("normal");
        })

        function show_area(){
          $('.wrapper_area').css('display','block');
        }

        function hide_area(){
          window.location='campaign-overview.php';
          $('.wrapper_area').css('display','none');
        }

        function set_star_val()
        {
          var star=$('body').find('.rating');
          star.each(function(){
            var id=$(this).attr('id');
            for (var i = 0; i <= id; i++) {
              var children=$(this).children('.star'+i);
              $(children).addClass('starclick');
            }
          })
        }

        function modal_show(id)
        {
          feedback_id=id;
          $("#dangerModal").modal("show");
        }

        function delete_feedback()
        {
          var id=feedback_id;
          $.ajax({
            url:"../review/delete_feedback.php",
            data:{id:id},
            type:"POST",
            success:function(data){
              //alert(data);
            }
          })
        }
      </script>
      <script src="js/libs/toastr.js"></script>
      <script src="components/user_notifications.js"></script>
    </body>
    </html>