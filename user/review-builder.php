<?php 
include "include/session.php";
include 'include/connect_to_bd.php';
$token=$_SESSION['user_token'];
$query_to_token_bd=mysql_query("SELECT * FROM token WHERE token='$token'");
$array_token=mysql_fetch_assoc($query_to_token_bd);
$id_user=$array_token['client_id'];
function get_list_review(&$id_user){
  $query_to_review_db=mysql_query("SELECT * FROM review_builder WHERE type_account='global_admin'");
  while ($result=mysql_fetch_assoc($query_to_review_db)) {
    echo '<option value="'.$result['name'].'">'.$result['name'].'</option>';
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  body{
    position: relative;
  }
  .sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
  }
  input{
   margin: 5px;
 }
     /* Star Rating
     =================================================================== */
     .rating {
      unicode-bidi: bidi-override;
      direction: rtl;
      font-size: 30px;
    }
    .rating span.star,
    .rating span.star {
      font-family: FontAwesome;
      font-weight: normal;
      font-style: normal;
      display: inline-block;
    }
    .rating span.star:hover,
    .rating span.star:hover {
      cursor: pointer;
    }
    .rating span.star:before,
    .rating span.star:before {
      content: "\f006";
      padding-right: 5px;
      color: #999999;
    }
    .rating span.star:hover:before,
    .rating span.star:hover:before,
    .rating span.star:hover ~ span.star:before,
    .rating span.star:hover ~ span.star:before {
      content: "\f005";
      color: #e3cf7a;
    }
  </style>

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <!-- Main content -->
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Review Builder</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  Current Forms
                </div>
                <div class="card-block">
                  <table class="table table-striped table-bordered datatable table-responsive">
                    <thead>
                      <tr>
                        <!--<th>Name of Form:</th>-->
                        <th>Link</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $count=0;
                      $query=mysql_query("SELECT * FROM review_builder WHERE user_id='$id_user' ");
                      while($res=mysql_fetch_array($query)){
                        $count++;
                        echo "<tr>";
                        //echo "<td>".$res['name']."</td>";
                        echo "<td><a href='https://tracs.me/review/overview.php?token=".$res['md5']."'>Link to review</a></td>";
                        echo "<td class='table_button' >";
                        if($res['type_account']!='global_admin'){
                          echo "<a href='#' name='".$res['id']."' data-toggle='modal' data-target='#dangerModal' onclick='modal_show(this)' class='btn btn-danger btn_table'><i class='fa fa-trash-o'></i>
                          </a>";
                        }
                        echo  '<a class="btn btn-info btn_table" href="edit-review-builder.php?token='.$res['md5'].'">
                        <i class="fa fa-edit "></i>
                        </a>';
                        echo "<a href='overview-review-builder.php?token=".$res['md5']."' class='btn btn-success btn_table' href='#''>
                        <i class='fa fa-search-plus'></i>";
                        echo "</td>";
                        echo "</tr>";
                      } 
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <?php if($count==0){echo '
          <div class="html_form">
          <div class="row">
          <div class="col-sm-12">
          <div class="card">
          <div class="card-header">
          <strong>Review Builder</strong>
          </div>
          <div class="card-block">
          <div class="form_import">
          <h6 class=" form-control-label" for="text-input">Type Image :</h6>
          <div>
          <!--<input data-toggle="popover" onkeyup="change_input_data(this)"data-placement="bottom" type="text"  name="text-input" class="name_form form-control" placeholder="Name Form">-->
          <select class="name_form form-control">
          <option value="logo" default>Logo</option>
          <option value="picture">Picture</option>
          </select>
          </div><br>
          <label class=" form-control-label" for="text-input">Welcome text:</label>
          <div >
          <textarea onkeyup="change_input_data(this)" id="textarea-input" name="textarea-input" rows="5" class="textarea_input_check welcome_text form-control" placeholder="Welcome Text..."></textarea>
          </div><br>
          <!--<label class=" form-control-label" for="text-input">Landing page text:</label>
          <div >
          <textarea onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data_val form-control"  name="landing_page_text" rows="10" cols="80">
          </textarea>
          </div><br>-->
          <div class="question">
          <div class="form-group has-feedback questions question1">
          <label class=" form-control-label" for="text-input">Question One:</label>
          <div>
          <textarea disabled="" onkeyup="change_input_data(this)" id="textarea-input" name="textarea-input" rows="5" class="textarea_input  form-control" placeholder="How did you hear about us" value="How did you hear about us">How did you hear about us</textarea>
          </div><br>

          <div class=" checkbox_radio1">
          <label>
          <input  checked="" type="radio" id="1" name="inline-checkbox1" value="dropdown">Drop Down Answers
          </label>

          <div class="answer_wrapper1">

          <div class="row">
          <div class="col-sm-12 answer_input1">
          <input onkeyup="change_input_data(this)" disabled value="Friend"  style="margin-left: 0!important" type="text" id="text-input" name="text-input" class="input_answer input_answer1 form-control" placeholder="Answer 1
          ">
          <input onkeyup="change_input_data(this)"style="margin-left: 0!important" type="text" id="text-input" name="text-input" class="input_answer input_answer2 form-control" placeholder="Answer 2">
          </div>
          </div>
          <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 text-left">
          <button onclick="add_new_answer(1)" class="btn btn-secondary add_answer">Add Answer</button>
          <button style="display: none;" onclick="delete_answer(1)" class="btn btn-secondary delete_answer1">Delete Last Answer</button>
          </div>
          </div>
          </div>

          </div>

          </div>
          </div>
          </div>
          <div class="col-md py-4 text-center">
          <button onclick="add_new_question(this)" class="btn btn-primary add_item">Add Question </button>
          <button onclick="delete_last_question()" class="btn btn-danger delete_item" style="display: none;">Delete Last Question</button>
          </div>
          </div>
          </div>
          </div>
          </div>
          <div class="card">
          <div class="card-header">
          Social Review
          </div>
          <div class="card-block">
          <div style="display: none">
          <h6>Allow users leave their reviews on your social accounts</h6>
          <textarea onkeypress="change_input_data(this)" rows="5" class="success_text share_input form-control" value=" " placeholder="Your call to action message for users"> </textarea>
          <br></div>
          <h6>Links to you business online</h6>
          <div>
          <input type="text" onkeyup="change_input_data(this)"  name="text-input" class="facebook_id form-control share_input" placeholder="Facebook"><br>
          </div>
          <div>
          <input  type="text" onkeyup="change_input_data(this)"  name="text-input" class="yelp_id form-control share_input" placeholder="Yelp"><br>
          </div>
          <div>
          <input  type="text" onkeyup="change_input_data(this)"  name="text-input" class="google_id form-control share_input" placeholder="Google+">
          </div>                          
          </div>
          </div>
          <div class="card">
          <div class="card-header">
          Sharing
          </div>
          <div class="card-block">
          <h6>Add a message for users to share in social networks</h6>
          <textarea onkeyup="change_input_data(this)"  rows="5" class="share_text share_input form-control" placeholder="Write message here"></textarea>                  
          </div>
          <div class="card-block">
          <div class="col-md py-4 text-left">
          <button onclick="save_form()" class="btn btn-success save_form">Save Form</button>
          <button style="margin-left: 30px;display:none ;" type="button" class="btn danger_btn btn-outline-danger">Error</button>
          </div>
          </div>
          </div>

          </div>
          </div>
          <?';}?>
        </main>
        <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-danger" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete this?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <button type="button" onclick="delete_form()" data-dismiss="modal" class="btn btn-primary">Yes</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    <!--
    <h6 class="col-md-3 form-control-label" for="text-input">Google ID:</h6>
    <div class="col-md-10">
        <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Email Address">
    </div><br>
    <h6 class="col-md-3 form-control-label" for="text-input">Facebook ID: </h6>
    <div class="col-md-10">
        <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Email Address">
    </div><br>
    <h6 class="col-md-3 form-control-label" for="text-input">Yelp ID: </h6>
    <div class="col-md-10">
        <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Email Address">
      </div><br> -->



      <!-- Bootstrap and necessary plugins -->
      <script src="js/libs/jquery.min.js"></script>
      <script src="js/libs/tether.min.js"></script>
      <script src="js/libs/bootstrap.min.js"></script>
      <script src="js/libs/pace.min.js"></script>


      <!-- Plugins and scripts required by all views -->
      <script src="js/libs/Chart.min.js"></script>

      <!-- GenesisUI main scripts -->

      <script src="js/app.js"></script>

      <!-- Plugins and scripts required by this views -->
      <script src="../js/libs/toastr.min.js"></script>
      <script src="js/libs/gauge.min.js"></script>
      <script src="js/libs/moment.min.js"></script>
      <script src="js/libs/daterangepicker.js"></script>

      <!-- Custom scripts required by this view -->
      <script src="js/views/main.js"></script>
      <script src="ckeditor/ckeditor.js"></script>
      <script>
        var i=1;
        var j=0;
        var data_answer=[];
        var data_quest=[];
        var count_answer=[];
        var name_form;
        var data=[];
        var last_answer;
        var ii=0;
        var review_id;
        count_answer[1]=3;
        count_answer[2]=3;
        count_answer[3]=3;
        count_answer[4]=3;
        count_answer[5]=3;
        var count=['Two','Three','Four','Five'];

        function modal_show(id){
          review_id=id;
      //$("#dangerModal").modal("show");
    }
    function add_new_question(id){
      $('.sk-circle').css('display','block');
      var out;
      i++;
      if(i>=2 && i<5){
        $(id).css('display','inline-block');
      }else{
       $(id).css('display','none');
     }
     $('.delete_item').css('display','inline-block');
     out='';
     out+="<div class='form-group has-feedback questions question"+i+"'>";
     out+="<label class='form-control-label' for='text-input'>Question "+count[j]+":</label>";
     out+="<div>";
     out+="<textarea onkeyup='change_input_data(this)' id='textarea-input"+i+"' name='textarea-input' rows='5' class='textarea_input textarea_input_check form-control' placeholder='Content'></textarea>";
     out+="</div><br>";
     out+="<div>";
     out+="<label>";
     out+="<input checked='checked' id='"+i+"' type='radio' onclick='hide_drop_menu("+i+",this)'  name='inline-checkbox"+i+"' value='textarea'>Text Area";
     out+="</label>";
     out+="<label>";
     out+="<input type='radio' id='"+i+"'  onclick='display_answer("+i+",this)'  name='inline-checkbox"+i+"' value='dropdown'>Drop Down Answers";
     out+="</label>";
     out+="<label>";
     out+="<input type='radio' id='"+i+"' onclick='hide_drop_menu("+i+",this)' name='inline-checkbox"+i+"' value='date'>Date";
     out+="</label>";
     out+="<div style='display:none' class='answer_wrapper"+i+"'>";
     out+="<div class='row'>";
     out+="<div class='col-sm-12 answer_input"+i+"'>";
     out+="<input onkeyup='change_input_data(this)' style='margin-left: 0!important' type='text' id='text-input' name='text-input' class='input_answer1 form-control' placeholder='Answer 1'>";
     out+="<input onkeyup='change_input_data(this)' style='margin-left: 0!important' type='text' id='text-input' name='text-input' class='input_answer2 form-control' placeholder='Answer 2'>";
     out+="</div>";
     out+="</div>";
     out+="<div class='row'>";
     out+="<div class='col-md-12 text-left'>";
     out+="<button onclick='add_new_answer("+i+")' class='btn btn-secondary add_answer'>Add Answer</button>";
     out+="<button style='display: none;'' onclick='delete_answer("+i+")'' class='btn btn-secondary delete_answer"+i+"'>Delete Last Answer</button>";
     out+="</div>";
     out+="</div>";
     out+="</div>";
     out+="</div>";
     out+="</div>";
     $('.question').append(out);
     j++;
     $('.sk-circle').css('display','none');
   }

   function add_new_answer(id){
    //console.log(id);
    console.log(count_answer[id]);
    var out;
    var key=count_answer[id];
    count_num=count_answer[id];
    out="<input onkeyup='add_attr_value(this)' onkeypress='change_input_data(this)' style='margin-left: 0!important' onclick='delete_answer("+id+key+")' type='text' id='text-input' name='text-input' class='answer"+id+" input_answer"+id+key+" form-control' placeholder='Answer "+count_num+"'>";
    $('.answer_input'+id).append(out);
    if(key>2){
      $('.add_answer').css('display','inline-block');
    }else{
     $('.add_answer').css('display','none');
   }
   $('.delete_answer'+id).css('display','inline-block');
   count_answer[id]++;
 }

 function delete_last_question(id){
  $(".question"+i).remove();
  i--;
  j--;
  $('.add_item').css('display','inline-block');
  if(i<2){
    $('.delete_item').css('display','none');
  }else{
    $('.delete_item').css('display','inline-block');
  }
}

function save_form(){
  try{
    get_nameform();
    welcome_text();
    get_question();
    check_answer();
    get_answer();
    //check_share_data();
    ajax_query(name_form,data);
  }catch(e){
    console.log(e);
  }
}

function check_answer(){
  $(".input_answer").each(function(){
    if($(this).val()==''){
      var parent=$(this).parent();
      $(this).addClass('form-control-danger');
      parent.addClass('has-danger');
      throw new SyntaxError("empty_data");
    }
  })
}

function ajax_query(name,data){
  $('.sk-circle').css('display','block');
  var success_text=$('.success_text').val();
  var facebook_id=$('.facebook_id').val();
  var yelp_id=$('.yelp_id').val();
  var google_id=$('.google_id').val();
  var share_text=$('.share_text').val();
  var welcome_text=$('.welcome_text').val();
  var html_form=$('.html_form').html();
  console.log(html_form);
  $.ajax({
    url:'include/add_new_review_builder.php',
    data:{name:name,data:data,success_text:success_text,facebook_id:facebook_id,yelp_id:yelp_id,google_id:google_id,share_text:share_text,welcome_text:welcome_text,html_form:html_form,count:i},
    type:'POST',
    success:function(data){
      //alert(data);
      $('.sk-circle').css('display','none');
      if(data==1){
        window.location='review-builder.php';
        $(".question2").remove();
        $(".question3").remove();
        $(".question4").remove();
        $(".question5").remove();
        $('.name_form').val('');
        $('.textarea_input').val('');
        j=0;
        i=1;
        $('.add_answer').css('display','inline-block');
        $('.delete_item').css('display','none');
        $("#inline-checkbox11").prop("checked", true);
        $(".answer_wrapper1").css("display","none");
      }else{
        $(".danger_btn").css('display','inline-block');
        setTimeout(function(){
          $(".danger_btn").css('display','none');
        },1000);
        var parent=$(".name_form").parent();
        $('.name_form').addClass('form-control-danger');
        parent.addClass('has-danger');
        $('.name_form').attr("data-content",'Form Exist');
        $('.name_form').popover('show');
        setTimeout(function(){
          $('.name_form').popover('dispose');
        },1500);
      }
    }

  })
}

function welcome_text(){
  if($('.welcome_text').val()==''){
    var parent=$(".welcome_text").parent();
    $('.welcome_text').addClass('form-control-danger');
    parent.addClass('has-danger');
    throw new SyntaxError("empty_data");
  }
}

function delete_form(){
  var id=review_id;
  $('.sk-circle').css('display','block');
  var name=$(id).attr('name');
  $.ajax({
    url:"include/delete_form.php",
    data:{name:name},
    type:"POST",
    success:function(){
      $('.sk-circle').css('display','none');
      window.location='review-builder.php';
    }
  })
}

function get_nameform(){
  if($('.name_form').val()==''){
    var parent=$(".name_form").parent();
    $('.name_form').addClass('form-control-danger');
    parent.addClass('has-danger');
    $(".danger_btn").css('display','inline-block');
    setTimeout(function(){
      $(".danger_btn").css('display','none');
    },1000);
    throw new SyntaxError("empty_data");
  }
  name_form=$('.name_form').val();
    //console.log(name_form);
  }
  function get_question(){
    last_answer=0;
    $(".textarea_input_check").each(function(){
      if($(this).val()==''){
        var parent=$(this).parent();
        $(this).addClass('form-control-danger');
        parent.addClass('has-danger');
        throw new SyntaxError("empty_data");
      }
    })
    $(".textarea_input").each(function(){
      data_quest[last_answer]=$(this).val();
      last_answer++;
    })
    //console.log(data_quest);
  }
  function get_answer(){
    ii=0;
    $("input[type='radio']:checked").each(function(){
      var attr=$(this).attr('id');
      if($(this).val()=='dropdown'){
        data_answer=[];
        var child = $('.answer_input'+attr).find('input');
        for(var i=0;i<child.length;i++){
          data_answer[i]=child[i]['value'];
        }
      }else{
        data_answer=$(this).val();
      }
      build_array(data_answer);
        //console.log(data);
      });
  }
  function build_array(arr){
   data[ii]={data_quest:data_quest[ii],answer:arr};
   ii++
   
 }


 function display_answer(id,id_radio){
  $('.answer_wrapper'+id).css('display','block');
  $(id_radio).prop("checked", true);
  $(id_radio).attr('checked', 'checked');
}

function hide_drop_menu(id,id_radio){
  $('.answer_wrapper'+id).css('display','none');
  $(id_radio).prop("checked", true);
  $(id_radio).attr('checked', 'checked');
  console.log(id_radio);
}
function delete_answer(id){
  count_answer[id]--;
    //console.log(id);
    //console.log(count_answer[id]);
    var key=count_answer[id];
    $('.input_answer'+id+count_answer[id]).remove();
    $('.add_answer').css('display','inline-block');
    if(key<4){
      $('.delete_answer'+id).css('display','none');
    }else{
      $('.delete_answer'+id).css('display','inline-block');
    }
  }
  function change_input_data(id){
    var val=$(id).val();
    $(id).attr('value',val);
    var parent=$(id).parent();
    $(id).removeClass('form-control-danger');
    parent.removeClass('has-danger');
  }
  function check_share_data(){
    $(".share_input").each(function(){
      if($(this).val()==''){
        var parent=$(this).parent();
        $(this).addClass('form-control-danger');
        parent.addClass('has-danger');
        throw new SyntaxError("empty_data");
      }
    })
  }

  function default_form(){
    var out;
    $('.form_import').html('');
    out='<h6 class="col-md-12 form-control-label" for="text-input">Name Form:</h6><div class="col-md-12"><input data-toggle="popover" onkeyup="add_attr_value(this)" data-placement="bottom" onkeypress="change_input_data(this)" type="text" id="text-input" name="text-input" class="name_form form-control" placeholder="Name Form"></div><br><label class="col-md-3 form-control-label" for="text-input">Welcome text:</label><div class="col-md-12"><textarea onkeyup="add_attr_value(this)"  onkeypress="change_input_data(this)" id="textarea-input" name="textarea-input" rows="5" class="textarea_input_check welcome_text form-control" placeholder="Welcome Text..."></textarea></div><br><div class="question"><div class="form-group has-feedback question1"><label class="col-md-3 form-control-label" for="text-input">Question One:</label><div class="col-md-12"><textarea disabled="" onkeypress="change_input_data(this)" id="textarea-input" name="textarea-input" rows="5" class="textarea_input  form-control" placeholder="How did you hear about us?" value="How did you hear about us?">How did you hear about us?</textarea></div><br><div class="col-md-12 checkbox_radio1"><label><input  checked="" type="radio" id="1" name="inline-checkbox1" value="dropdown">Drop Down Answers</label><div class="answer_wrapper1"><div class="row"><div class="col-sm-12 answer_input1"><input onkeyup="add_attr_value(this)" disabled value="Friend" onkeypress="change_input_data(this)" style="margin-left: 0!important" type="text" id="text-input" name="text-input" class="input_answer input_answer1 form-control" placeholder="Answer 1"><input onkeyup="add_attr_value(this)" onkeypress="change_input_data(this)" style="margin-left: 0!important" type="text" id="text-input" name="text-input" class="input_answer input_answer2 form-control" placeholder="Answer 2"></div></div><div class="row"><div class="col-xs-12 col-sm-12 col-md-12 text-left"><button onclick="add_new_answer(1)" class="btn btn-secondary add_answer">Add Answer</button><button style="display: none;" onclick="delete_answer(1)" class="btn btn-secondary delete_answer1">Delete Last Answer</button></div></div></div></div></div></div>';
    i=1;
    j=0;
    $('.delete_item').css('display','none');
    $('.form_import').append(out);

  }

  function import_review(){
    $('.sk-circle').css('display','block');
    var name=$('.select_import_review').val();
    if(name=='none'){
      default_form();
    }else{
      $.ajax({
        url:"include/import_review_data.php",
        data:{name:name},
        type:"POST",
        success:function(data){
          $('.sk-circle').css('display','none');
          append_data(data);
          //alert(data);
          //console.log(data);
        }
      })
    }
    $('.sk-circle').css('display','none');
  }
  function append_data(data){
    $('.sk-circle').css('display','none');
    var json=JSON.parse(data);
    i=json['count'];
    j=i-1;
    //console.log(json['form']);
    id_campaign=json['id'];
    sessionStorage['campaign_id'] = id_campaign;
    $('.form_import').html('');
    if(i>1){
      $('.delete_item').css('display','inline-block');
    }
    $('.form_import').append(json['form']);
    $('.welcome_text').val(json['welcome_text']);
    $('.share_text').val(json['share_text']);
    $('.facebook_id').val(json['facebook_link']);
    $('.yelp_id').val(json['yelp_link']);
    $('.google_id').val(json['google_link']);
    $('.success_text').val(json['success_text']);
    var name_form=$(".name_form").val();
    $(".name_form").val(name_form+'1');

  }

  function add_attr_value(id){
    var val=$(id).val();
    $(id).attr('value',val);
    //alert($(id).attr('value'));
  }

  function append_attr_value(id){
    //$(id).text('');
    var val=$(id).val();
    $(id).append(val);
  }
  CKEDITOR.replace('landing_page_text',{
   toolbar :
   [
   { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
   { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
   { name: 'links', items: [ 'Link', 'Unlink'] },
   { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
   { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize','TextColor', 'BGColor','Maximize'] }
   ]
 });
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
</body>

</html>