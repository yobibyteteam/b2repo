<?php 

require_once 'Twilio/autoload.php'; 
use Twilio\Rest\Client;
require_once 'Mailgun/vendor/autoload.php';
use Mailgun\Mailgun;
class Cron_Admin_Holiday extends Cron
{
	protected $arrayId=[];
	function __construct(){}

	protected function getAllCampaign(){
		$queryModel="SELECT id,`date` FROM holiday_builder WHERE type='global_admin'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				$arrayData[]=array($res['date'],$res['id']);
			}
		}else{
			throw new Exception("Empty Holiday Campaign", 1);
		}
		return $arrayData;
	}

	protected function getAllContact($idCampaign){
		$arrayData='';
		$arrayId='';
		$queryModel="SELECT email,spouse_email,id FROM contacts WHERE success_sended_holiday_admin='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['email']!=''){
					$arrayData[]=$res['email'];
					$this->arrayId[]=$res['id'];
				}
				if($res['spouse_email']!=''){
					$arrayData[]=$res['spouse_email'];
					$this->arrayId[]=$res['id'];
				}
			}
		}
		//Cron_Admin_Holiday::updateSuccessedValue($arrayId);
		return $arrayData;
	}

	protected function getAllEmail($idCampaign){
		$arrayData='';
		//$date=date("Y-m-d");
		$queryModel="SELECT `text` FROM holiday_builder_email WHERE campaign_builder_id='".$idCampaign."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['text']!=''){
					$arrayData[]=$res['text'];
				}
			}
		}
		return $arrayData;
	}

	protected function updateSuccessedValue(){
		if($this->arrayId){
			foreach ($this->arrayId as $key => $value) {
				$queryModel="UPDATE `contacts` SET `success_sended_holiday_admin`='1' WHERE id='".$value."'";
				mysql_query($queryModel);
			}
		}

	}

	protected function getAllMobileOfContact($idCampaign){
		$arrayData='';
		$arrayId=[];
		$queryModel="SELECT mobile_phone,alt_phone,spouse_mobile,spouse_alternate,id FROM contacts WHERE success_sended_holiday_admin='0'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while($res=mysql_fetch_assoc($query)){
				if($res['mobile_phone']!='' && $res['alt_phone']!=''){
					$arrayData[]='+'.$res['mobile_phone'];
					$this->arrayId[]=$res['id'];
				}elseif($res['mobile_phone']=='' && $res['alt_phone']!=''){
					$arrayData[]='+'.$res['alt_phone'];
					$this->arrayId[]=$res['id'];
				}

				if($res['spouse_mobile']!='' && $res['spouse_alternate']!=''){
					$arrayData[]='+'.$res['spouse_mobile'];
					$this->arrayId[]=$res['id'];
				}elseif($res['spouse_mobile']=='' && $res['spouse_alternate']!=''){
					$arrayData[]='+'.$res['spouse_alternate'];
					$this->arrayId[]=$res['id'];
				}
			}
		}
		//Cron_Admin_Holiday::updateSuccessedValue($arrayId);
		return $arrayData;
	}
	protected function getAllText($idCampaign){
		$arrayData='';
		//$date=date("Y-m-d");
		$queryModel="SELECT `text` FROM holiday_builder_text WHERE campaign_builder_id='".$idCampaign."'";
		$query=mysql_query($queryModel);
		if(mysql_num_rows($query)!=0){
			while ($res=mysql_fetch_assoc($query)) {
				if($res['text']!=''){
					$arrayData[]=$res['text'];
				}
			}
		}
		return $arrayData;
	}

	protected function addToStatistic($userId,$typeCampaign,$type){
		$date=date("Y-m-d");
		$queryModel="INSERT INTO `statistics`(`user_id`, `type_campaign`, `date`,`type`) VALUES ('".$userId."','".$typeCampaign."','".$date."','".$type."')";
		mysql_query($queryModel);
	}

	protected function sendEmailData($email,$text){
		//echo json_encode($text);
		$mgClient = new Mailgun(KEY);
		$domain = DOMAIN;
		foreach ($email as $key => $value){
			$result = $mgClient->sendMessage("$domain",
				array('from'    =>  '<crm@tracs.me>',
					'to'      => 'Mailgun Devs '.$value.'',
					'subject' => 'Hello',
					'html'    => $text[0]));
					//mail('calmadeveloper@gmail.com', '123', 'message');*/
		}
	}

	protected function sendTextData($text,$mobile){
		$client = new Client(APIKEY,APISECRET,ACCOUNTSID);
		foreach ($mobile as $key => $value){
			$message = $client->messages->create(
				$value, 
				array(
					'from' => '+17202509876',
					'body' => $text[0]
					)
				);
		}
	}
}
?>