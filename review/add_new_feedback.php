<?php
require_once 'Class/Feedback.php';
include '../include/connect_to_bd.php';
$answer=$_POST['array1'];
$question=$_POST['array2'];
$token=$_POST['token'];


$feedback=new Feedback();
try{
	$feedback->get_user_id($token);
	$feedback->add_new_feedback($question,$answer);
	echo json_encode(array("success" => 1,"id"=>$feedback->id_last_query));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
