<?php
/**
* Class Authorization
*/
use StripeCRM\StripeCRM as stripe;

class Authorization
{
	public $token , $arrayClient ,$count;
	protected $email , $password , $keepMeSign, $queryToUser , $queryToAdmin, $queryToManager;

	function __construct(&$email , &$password , &$keepMeSign)
	{
		$this->email = strip_tags($email);
		$this->email = mysql_real_escape_string($this->email);
		$this->password = md5($password);	
		$this->keepMeSign = $keepMeSign;
	}

	 /*
	  Function identify type Client
	 */		
	  public function identifyTypeClient(){
	  	$this->queryToUser=mysql_query("SELECT email,id,password,access_system FROM user WHERE email = '$this->email'");
	  	$this->queryToAdmin=mysql_query("SELECT * FROM admin WHERE email = '$this->email'");
	  	$this->queryToManager=mysql_query("SELECT * FROM managers WHERE email = '$this->email'");
	  	if(mysql_num_rows($this->queryToUser) == 0 && mysql_num_rows($this->queryToAdmin) == 0 && mysql_num_rows($this->queryToManager) == 0){throw new Exception("not_found", 1);
	  }elseif (mysql_num_rows($this->queryToUser)>0) {
	  	$type = 'user';
	  	$this->arrayClient = mysql_fetch_assoc($this->queryToUser);
	  	Authorization::checkAccess();
	  }elseif(mysql_num_rows($this->queryToManager)>0){
	  	$type = 'manager';
	  	$this->arrayClient = mysql_fetch_assoc($this->queryToManager);
	  }else{
	  	$type = 'global_admin';
	  	$this->arrayClient = mysql_fetch_assoc($this->queryToAdmin);
	  }
	  Authorization::checkEmail();
	  Authorization::checkPassword();
	  Authorization::generateToken();
	  return $type;
	}

	private function checkAccess(){
		if($this->arrayClient['access_system']=='0'){
			$stripe = new stripe();
			$this->count=$stripe->get_amount($this->arrayClient['id']);
			throw new Exception("access", 1);
		}
	}

	 /*
	  Function Checks Two Email
	 */
	  private function checkEmail(){
	  	if($this->arrayClient['email'] != $this->email){throw new Exception("email", 1);}
	  }

	 /*
	  Function Checks Two Password
	 */
	  private function checkPassword(){
	  	if($this->arrayClient['password'] != $this->password){throw new Exception("pass",1);}
	  }

     /*
      Function Generate New Token
     */
      private function generateToken(){$this->token = md5(rand(0, PHP_INT_MAX));}

     /*
      Function Add Token To DataBase
     */
      public function addTokenToBD($typeClient){
      	if($this->keepMeSign==false){
      		$timestamp = date( "Y-m-d", strtotime( "+1 day" ));
      	}else{$timestamp = date( "Y-m-d", strtotime( "+7 day" ));}
      	$clientId=$this->arrayClient['id'];
      	$queryModel="INSERT INTO `token`(`token`, `valid_date`, `client_id`, `type`) VALUES ('".$this->token."','".$timestamp."','".$clientId."','".$typeClient."')";
      	mysql_query($queryModel);
      }
  }
  ?>