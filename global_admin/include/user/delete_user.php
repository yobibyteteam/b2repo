<?php
include '../connect_to_bd.php';
require_once '../class/User.php';
session_start();

$id=$_POST['id'];
$text="Administrator delete user";

$user=new User();
try{
	$user->delete_dir_user($id);
	$user->delete_user($id);
	$user->add_to_activity_log($text);
	echo json_encode(array("success"=>"1"));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>