<?php
$token=$_POST['token'];
include '../connect_to_bd.php';
require_once '../classes/Universal_campaign.php';
session_start();
$un_campaign=new Universal_campaign();

try{
	$clientId=Universal_campaign::getClientId($_SESSION['user_token']);
	$dir=Universal_campaign::get_dir_admin($clientId);
	$un_campaign->get_campaign_id_token($token);
	$un_campaign->get_email_data();
	$un_campaign->get_text_data();
	$un_campaign->get_audio_data();
	$un_campaign->get_postcard_data($dir);
	$un_campaign->get_setting_data($token);
	echo json_encode(array("success"=>1,"email"=>$un_campaign->email_data,"text"=>$un_campaign->text_data,"audio"=>$un_campaign->audio_data,"name"=>$un_campaign->name,"postcard"=>$un_campaign->postcard,"type"=>$un_campaign->type_campaign,"assign_num"=>$un_campaign->assign_num));
}catch(Exception $e)
{
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}
?>