<?php 
session_start();
include '../connect_to_bd.php';
require_once '../classes/Holiday_campaign.php';
$clientId=Holiday_campaign::getClientId($_SESSION['user_token']);
if(isset($_SESSION['user_token'])){
	$token=$_SESSION['user_token'];
}
$activity_reminder=$_POST['activity_reminder'];
//$type=$_POST['type_variable'];
//$data=$_POST['data'];
$name_campaign=$_POST['name_campaign'];
$select_campaign=$_POST['select_campaign'];
$delay_day = $_POST['delay_day'];
//$repeat_year=$_POST['repeat_year'];
$text="Administrator add new holiday campaign";
$assignNumber=$_POST['assignNumber'];
$holiday_campaign = new Holiday_campaign();
try{
	$holiday_campaign->get_id_client($token);
	$holiday_campaign->add_to_bd($select_campaign,$delay_day,$activity_reminder,$name_campaign,$assignNumber);
	$holiday_campaign->add_to_activity_log($text);
	echo json_encode(array("success"=>"1","id"=>$holiday_campaign->last_id));
}catch(Exception $e){
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}



?>