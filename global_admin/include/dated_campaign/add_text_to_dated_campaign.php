<?php
include '../connect_to_bd.php';
require_once '../class/Dated_campaign.php';
session_start();
$id_campaign=$_POST['id_campaign'];
$editor_text=$_POST['editor_text'];
$delay_day_text=$_POST['delay_day_text'];
$repeat_text=$_POST['repeat_text'];
$dated_campaign=new Dated_campaign();
$clientId=Dated_campaign::getClientId($_SESSION['user_token']);
try{
	$dated_campaign->add_text($id_campaign,$editor_text,$delay_day_text,$repeat_text,$clientId);
	echo json_encode(array("success"=>"1"));
}
catch(Exception $e)
{
	echo json_encode(array("success" => "0","error" => $e->getMessage()));
}
?>