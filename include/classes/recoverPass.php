<?php 
/**
* Class Recovery password
*/
class RecoverPass
{
	private $email , $arrayClient , $generatePass;
	function __construct($email){
		$this->email = $email;
		htmlspecialchars($this->email);
		strip_tags($this->email);
	}

    /*
     Function Get Client
     return string
    */
     public function getClientData(){
     	$queryModel="SELECT * FROM `user` WHERE `email`='".$this->email."'";
     	$selectClientdata=mysql_query($queryModel);
     	if(mysql_num_rows($selectClientdata)==0){throw new Exception("not_found", 1);
     	}else{
     		$this->arrayClient=mysql_fetch_assoc($selectClientdata);
     		RecoverPass::generatePass();
     		return $this->arrayClient['type'];
     	}
     }

    /*
     Function Generate Password
    */
     private function generatePass(){
     	$simv = array ("92", "83", "7", "66", "45", "4", "36", "22", "1", "0", 
     		"k", "l", "m", "n", "o", "p", "q", "1r", "3s", "a", "b", "c", "d", "5e", "f", "g", "h", "i", "j6", "t", "u", "v9", "w", "x5", "6y", "z5");
     	for ($k = 0; $k < 8; $k++){
     		shuffle($simv);
     		$this->generatePass = $this->generatePass.$simv[$k];
     	}
     }

    /*
     Function adding new password to db
    */
     public function addNewPassToBD($typeClient){
     	$passMD5=md5($this->generatePass);
     	$queryModel="UPDATE ".$typeClient." SET `password`='".$passMD5."' WHERE `email`='".$this->email."'";
     	mysql_query($queryModel);
     	RecoverPass::sendEmailNotification();
     }
    /*
     Function sending email notification
    */
     private function sendEmailNotification(){
     	mail($this->email, "Password Recovery", "Hello, ".$this->arrayClient['username']." . \n Your new password: $this->generatePass","From:crmsystem@admin.com");
     }
 }

 ?>