<?php
include '../connect_to_bd.php';
require_once '../class/User.php';
session_start();

$input_data=$_POST['data_array'];
$text='Administrator added a new user ';
$user=new User();
$text='Administrator added a new administrator';
$include_function=$_POST['include_function'];


try{
	$user->set_data($input_data,$include_function);
	$user->check_input_data();
	$user->set_query();
	$user->get_admin_name();
	$user->add_to_activity_log($text);
	$user->send_notification_email();
	mkdir('../../../upload_photo/'.$user->dir, 0777, true);
	echo json_encode(array("success"=>1,"user_id"=>$user->id));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}


?>