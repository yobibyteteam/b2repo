<?php 
include "include/session.php";
include 'include/connect_to_bd.php';
$session_id=$_SESSION['user_token'];
$session_id=mysql_real_escape_string($session_id);
$query=mysql_query("SELECT client_id FROM token WHERE token='$session_id'");
if(mysql_num_rows($query)!=0){
    $array_token=mysql_fetch_assoc($query);
    $client_id=$array_token['client_id'];
}
$adminId=getAgentId($client_id);
function getAgentId($client_id){
    $query=mysql_query("SELECT admin_id FROM user WHERE id='".$client_id."'");
    $arrayData=mysql_fetch_assoc($query);
    return $arrayData['admin_id'];
}
function get_agent_data($id){
    $query=mysql_query("SELECT * FROM admin WHERE id='".$id."'");
    $arrayAdmin=mysql_fetch_assoc($query);
    echo "Welcome to support! Have questions, comments, or concerns? You have been assigned a personal support agent that will be able to help you with whatever you need. <br><br>

    Your agent’s name is <b>".$arrayAdmin['name'].".</b>If you prefer to email you can reach her at: <b class='email_agent'>".$arrayAdmin['email']." </b><br><br>

    Remember, our staff is only human much like you! Your agent will get back with you during normal business hours. You may also fill out the support ticket below and any member from our team will also get back with you shortly! <br><br>

    The ContactTRACS Team";
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Clever - Bootstrap 4 Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>CRM System</title>
    <!-- Icons -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application -->
    <link href="css/style.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
    <?php include 'components/header.php'; ?>
    <div class="app-body">
        <?php include "sidebar.php"; ?>
        <main class="main">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active">Support</li>
            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-header">
                          <strong>Support</strong>
                      </div>
                      <div class="card-block" id="sortable">
                          <div class="form-group">
                            <label for="company"><p>
                                <?php get_agent_data($adminId); ?>
                            </label><br><br>
                            <label class="form-control-label" for="text-input">
                            Topic</label>
                            <input type="text" id="text-input" name="text-input" class="topic input_data form-control" placeholder="Please enter topic">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="textarea-input">Message</label>

                            <textarea id="textarea-input" name="textarea-input" rows="9" class="message input_data form-control" placeholder="Please enter your message"></textarea>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" onclick="send_email()">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Bootstrap and necessary plugins -->
    <script src="js/libs/jquery.min.js"></script>
    <script src="js/libs/tether.min.js"></script>
    <script src="js/libs/bootstrap.min.js"></script>
    <script src="js/libs/pace.min.js"></script>
    <!-- Plugins and scripts required by all views -->
    <script src="js/libs/Chart.min.js"></script>
    <!-- GenesisUI main scripts -->
    <script src="js/app.js"></script>
    <!-- Plugins and scripts required by this views -->
    <script src="../js/libs/toastr.min.js"></script>
    <script src="js/libs/gauge.min.js"></script>
    <script src="js/libs/moment.min.js"></script>
    <script src="js/libs/daterangepicker.js"></script>
    <!-- Custom scripts required by this view -->
    <script src="js/views/main.js"></script>

    <script>
        var topic;
        var message;
        var error;
        var email=$(".email_agent").val();

        function send_email()
        {
            try{
                check_data();
                ajax_query();
            }catch(e){
                alert(error);
            }
        }

        function check_data()
        {
            $(".input_data").each(function(){
                if($(this).val()==''){
                    error="Enter All Data";
                    throw new SyntaxError("empty_data");
                }
            })
        }

        function ajax_query()
        {
            topic=$(".topic").val();
            message=$(".message").val();
            $.ajax({
                url:"include/send_email.php",
                data:{email:email,topic:topic,message:message},
                type:"POST",
                success:function(data)
                {
                    alert("Success!");
                    window.location="support.php";
                }
            })
        }
    </script>
    <script src="js/libs/toastr.js"></script>
    <script src="components/user_notifications.js"></script>
</body>

</html>