<?php
include "include/session.php";
include 'include/connect_to_bd.php';
$token=$_SESSION['user_token'];
$query_model="SELECT * FROM token WHERE token='".$token."'";
$query=mysql_query($query_model);
$array=mysql_fetch_assoc($query);
$client_id=$array['client_id'];
function getAllPhoneNumbers($userId){
  $sql="SELECT * FROM phone_numbers WHERE user_id='".$userId."' ORDER BY timestamp DESC";
  $query=mysql_query($sql);
  if(mysql_num_fields($query)!=0){
    while ($res=mysql_fetch_assoc($query)) {
      echo '<option value="'.$res['number'].'">'.$res['number'].'</option>';
    }
  }
}
function get_list($client_id)
{
  $query_model="SELECT * FROM campaign_builder WHERE client_id='".$client_id."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    $i=1;
    while ($res=mysql_fetch_assoc($query)) {
      echo '
      <th style="padding-right: 25px;font-weight: normal;">
      <div class="checkbox">';
      if($i==1){
        echo '<label style="padding-right:10px">
        <input style="margin:5px" onchange="update_array()" type="checkbox" id="checkbox1" name="checkbox1" class="first_campaign select_campaign" value="'.$res['id'].'">'.$res['name'].'
        </label>';
      }else{
        echo '
        <label style="padding-right:10px">
        <input style="margin:5px" onchange="update_array()" type="checkbox" id="checkbox1" name="checkbox1" class="select_campaign" value="'.$res['id'].'">'.$res['name'].'
        </label>';
      }
      echo '  </div>';
      if($i%3==0){
        echo '</th><th style="padding-right: 25px;font-weight: normal;">';
      }
      $i++;
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <title>CRM System</title>
  <!-- Bootstrap core CSS -->
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/all.css" rel="stylesheet">
  <link href="css/colpick.css"  rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/component.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/chosen.css" type="text/css" >
  <link href="css/style_konstruktor.css"  rel="stylesheet" type="text/css" />
  <style>
  fieldset{display: none;}
  .photoDataToInsert img{
    width: 100%;
  }
  @media (max-width: 414px){
    #span{
      display: none;
    }
    .side{
      display: initial;
    }
  }
</style>
<script type="text/javascript">
  function toggleSet(rad)
  {
    var type = rad.value;
    for(var k=0,elm;elm=rad.form.elements[k];k++)
      if(elm.className=='item')
        elm.style.display = elm.id==type? 'block':'';
    }
  </script>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php include 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <!-- Main content -->
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Broadcast Builder</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Broadcast Builder</strong>
                  <a href="#" style="float:right;outline: none;" data-toggle="modal" data-target="#">Click Here for Video Tutorial</a>
                </div>
                <div class="card-block">
                  <div class="form-group has-feedback">
                    <div>
                      <label for="name_broadcast">Name Broadcast:</label>
                      <input id="name_broadcast" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data form-control"  placeholder="Broadcast Name">

                    </div>
                  </div>
                  <div class="form-group has-feedback">
                    <label class=" form-control-label" for="select2">Assigned Number: </label>
                    <div >
                      <select id="select_assign_num" name="select_assign_num" class="select_assign_num form-control">
                        <option value="0" default>None</option>
                        <?php getAllPhoneNumbers($client_id); ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group has-feedback">
                    <div>
                      <form >
                        <div class="radio">
                          <label for="radio1">
                            <input checked style="margin-right: 3px;" type="radio" id="radio1" name="itemtype" value="0" onclick="toggleSet(this)" class="default_delay">Send Immediately
                          </label>
                        </div>
                        <div class="radio">
                          <label for="radio2">
                            <input style="margin-right: 3px;" type="radio" id="radio2" name="itemtype" value="text" class="custom_delay" onclick="toggleSet(this)">Delayed Broadcast
                          </label>
                        </div>
                        <fieldset id="email" class="item">
                        </fieldset>
                        <fieldset id="text" class="item">
                          <span style="margin-left: 3px;"><input data-toggle="popover" data-placement="bottom" class="checkMobile delay_day" type="number" id="delay_day" name="number" class="delay_num" style="width: 60px; height: 30px; margin:0px 5px;">How many days to delay</span><br>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                  <div class="form-group has-feedback">
                    <div>
                      <!-- HTML код управляемого элемента -->
                      <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal">Select Campaign</button>
                      <!-- HTML код сворачиваемого элемента -->
                    </div>
                  </div>
                  <div class="form-group has-feedback">
                    <label class=" form-control-label" for="select2">Special parameter. Select field to search and parameter. If none leave blank </label>
                    <div>
                      <select id="select2" name="select2" class="form-control select_option">
                        <option value="none">None</option>
                        <option value="Religion">Religion</option>
                        <option value="Birthday">Birthday</option>
                      </select>
                      <select type="text" id="select_2" name="select_2" class="select_option_value form-control" placeholder="Enter dated"  style="display:none; margin-top: 10px;">
                        <option value="Christianity">Christianity</option>
                        <option value="Islam">Islam</option>
                        <option value="Buddhism">Buddhism</option>
                      </select>
                      <input type="text" id="select_2_date" name="select_2_date" class=" select_2_date form-control" placeholder="Enter date" style="display:none; margin-top: 10px;" />
                    </div><br>
                    <div class="col-md-12 py-4 text-center">
                      <button onclick="save_new_broadcast()" class="save_and_start btn btn-primary">Save and Start</button>
                      <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn  btn-outline-success">Success</button>
                    </div>
                    <div class="panel-group " id="accordion" style="display: none ">
                      <ul class="nav nav-tabs" style="border-bottom: 0!important" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="email_btn" data-toggle="tab" href="#email_center" role="tab" aria-controls="email_center"><span id="span">Email Center</span> <i class="fa fa-envelope-o "></i></a>
                        </li>
                        <li  class="nav-item">
                          <a  id="voice_btn" class="nav-link" data-toggle="tab" href="#voice_center" role="tab" aria-controls="voice_center"><span id="span">Voice Center</span>  <i class="fa fa-microphone"></i></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="text_btn" data-toggle="tab" href="#text_center" role="tab" onclick="removeSideBar();" aria-controls="text_center"><span id="span">Text Center</span> <i class="fa fa-file-text-o"></i></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="postcard_btn" data-toggle="tab" href="#postcard_center" role="tab" onclick="closeSideBar();" aria-controls="postcard_center"><span id="span">Postcard Center</span> <i class="fa fa-newspaper-o"></i></a>
                        </li>
                      </ul>

                      <div class="tab-content" style="border: 1px solid #e1e6ef!important;">
                        <div class="tab-pane active" id="email_center" role="tabpanel" style="">

                          <div class="form-group close_tab1">
                            <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which emails bounce. A full report is available to you
                            under logs on your dashboard.</p>
                            <p>You must agree to terms and conditions before proceeding:</p></label>

                            <div class="checkbox">
                              <label>
                                <input onchange="agree_check(this,'close_tab1','variable1')" type="checkbox" id="agree" name="agree" value="agree">
                                <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                              </label>
                            </div>
                          </div>
                          <div class=" col-md-12 variable1" style="display: none">
                            <div class="form-group">
                              <input type="text" class="form-control" id="company" placeholder="Enter email subject">
                            </div>
                            <!--<a href="#">View Current Emails</a>-->
                            <textarea name="editor_email" id="editor_email" rows="10" cols="80">
                            </textarea>
                            <br>
                            <span>Include:</span>
                            <button onclick="signature('email')" style="padding: 5px;" class="btn btn-primary">Email Signature</button>
                            <button onclick="signature('footer')" style="padding: 5px;" class="btn btn-primary">Email Footer</button>
                            <button style="padding: 5px;" class="btn btn-primary">Picture</button>
                            <button style="padding: 5px;" class="btn btn-primary">Logo</button>
                            <div style="margin-top: 10px" class="col-md-12 py-4 text-center">
                              <button data-toggle="tab" role="tab" href="#voice_center" onclick="email_show()" class="btn btn-primary" >Save </button>
                              <button style="margin-left: 30px;display: none;" type="button" class="btn success_btn btn-outline-success">Success</button>
                            </div>
                          </div>

                        </div>
                        <div class="tab-pane" id="voice_center" role="tabpanel">
                          <div class="form-group close_tab2">
                            <label for="company"><p>Note: Note all messages are guaranteed delivery. You will be notified of which numbers do not accept the message and only
                              charged for delivered messages. A full report is available to you under logs on your dashboard.
                            </p>
                            <p>You must agree to terms and conditions before proceeding:</p></label>

                            <div class="checkbox">
                              <label>
                                <input onchange="agree_check(this,'close_tab2','variable2')" type="checkbox" id="agree" name="agree" value="agree">
                                <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                              </label>
                            </div>

                          </div>
                          <div class="col-md-12 form-group variable2" style="display: none">
                            <!--<a href="#">View Current RVMD</a>-->
                            <h1>
                            </h1>
                            <input type="file" onchange="load()" id="import_audio" class="import_audio" accept="audio/wav">
                            <section class="experiment">
                              <p style="text-align: center;">
                                <video id="preview" controls style="display: none;border: 1px solid rgb(15, 158, 238); height: 240px; max-width: 100%; vertical-align: top; width: 320px;"></video>
                              </p>
                              <button onclick="plus_one()" class="btn btn-primary" id="record">Record</button>
                              <button id="stop" class="btn btn-danger" disabled>Stop</button><br>
                              <div id="container" ></div>
                            </section>
                            <div class="audio_list">
                            </div>
                            <div class="col-md-12 py-4 text-center">
                              <button onclick="save_voice()" class="btn btn-primary">Save </button>
                            </div>
                          </div>
                        </div>


                        <div class="tab-pane" id="text_center" role="tabpanel">
                          <div class="form-group close_tab3">
                            <label for="company"><p>Note: Note all messages are not guaranteed delivery. You will be notified of which texts bounce. A full report is available to you
                            under logs on your dashboard.</p>
                            <p>You must agree to terms and conditions before proceeding:</p></label>

                            <div class="checkbox">
                              <label>
                                <input onchange="agree_check(this,'close_tab3','variable3')" type="checkbox" id="agree" name="agree" value="agree">
                                <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                              </label>
                            </div>
                          </div>

                          <div class="col-md-12 form-group variable3" style="display: none">
                            <!--<a href="#">View Current Text</a>-->
                            <textarea name="text_editor" id="text_editor" rows="10" cols="80">
                            </textarea>
                            <div class="col-md-12 py-4 text-center">
                              <button onclick="save_text()" class="btn btn-primary">Save </button>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="postcard_center" role="tabpanel">
                          <div class="form-group close_tab4">
                            <label for="company"><p>Note: Note all messages are guaranteed delivery. You are charged for all postcards sent. If you receive a card back you must update the address or remove address from contacts information to stop any further attempts.</p>
                              <p>You must agree to terms and conditions before proceeding:</p></label>

                              <div class="checkbox">
                                <label>
                                  <input onchange="agree_check(this,'close_tab4','variable4')" type="checkbox" id="agree" name="agree" value="agree">
                                  <span style="padding-right:5px">I have read and agree to</span><a href="#" data-toggle="modal" data-target="#Terms">Terms and Conditions</a>
                                </label>
                              </div>
                            </div>

                            <div class="col-md-12 form-group variable4" style="display: none">
                             <!-- <a href="#">View Current Postcards</a>-->



                             <main>
                              <div class="col-md-12" style="padding: 0;">
                                <div class="card" style="padding: 0;border:0;">
                                  <div class="card-block p-0">
                                    <div class="tab-content">
                                      <div class="tab-pane active" id="front" style="padding: 0; overflow: auto;">
                                        <iframe scrolling="no" style="width: 1090px; height: 650px;" src="postcard-front.php" frameborder="0"></iframe>
                                      </div>
                                      <div class="tab-pane" id="back" style="padding: 0; overflow: auto;">
                                        <iframe  scrolling="no" src="postcard-back.php" style="width: 1090px; height: 650px;" frameborder="0"></iframe>
                                      </div>
                                    </div>
                                  </div>
                                  <ul class="nav side" role="tablist" style="text-align:center; position:relative;">
                                    <li class="nav-item">
                                      <a tab="" class="nav-link active" data-toggle="tab" href="#front" role="tab"><img src="../img/front.jpg" style="width:100px; height:100px;"><br>Front side</a>
                                    </li>
                                    <li class="nav-item">
                                      <a tab="" class="nav-link" data-toggle="tab" href="#back" role="tab"><img src="../img/back.jpg" style="width:100px; height:100px;"><br>Back side</a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </main>

                            <div class="col-md-12 py-4 text-center">
                              <button onclick="save_postcard()" data-toggle="modal" data-target="#photoDataToInsert" class="btn btn-primary">Save and View</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div style="display: none;text-align: center;"  class="final_save text-center">
                        <button onclick="window.location='broadcast-builder.php'" class="btn btn-primary">Schedule and Deliver</button>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </div>
      <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <label><input style="margin: 5px;" type="checkbox" id="all_campaign" name="all_campaign" checked onchange="select_all(this)" class="all_campaign" value="all_campaign">All Campaign</label>
              <h4>List of Campaign</h4>
            </div>
            <div class="modal-body">

              <table>
                <?php get_list($client_id); ?>
              </table>

            </div>
            <div class="modal-footer">
              <button class="btn btn-primary" type="button" data-dismiss="modal" >Select</button>
              <button class="btn btn-default" type="button" onclick="unchecked_all()" data-dismiss="modal">Close</button></div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="ClickVideoBroadcast" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-primary" >
            <div class="modal-video" >
              <video width="500" height="300" controls="controls">
               <source src="../video/video1.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>

               </video>
             </div>
             <!-- /.modal-content -->
           </div>
           <!-- /.modal-dialog -->
         </div>
       </div>

       <div class="modal fade" id="photoDataToInsert" tabindex="-1" role="dialog" aria-labelledby="readyDataToInsert" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="panel-group photoDataToInsert"  style="text-align: center;">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Bootstrap and necessary plugins -->
      <script src="js/libs/jquery.min.js"></script>
      <script src="js/libs/tether.min.js"></script>
      <script src="js/libs/bootstrap.min.js"></script>
      <script src="js/libs/pace.min.js"></script>
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script>
        $( function() {
          $( "#select_2_date" ).datepicker();
        } );
      </script>
      <script src="js/app.js"></script>
      <script src="ckeditor/ckeditor.js"></script>
      <script src="js/ui-elements.js"></script>
      <script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
      <script type="text/javascript">
        $('body').on('hidden.bs.modal', '.modal', function () {
          $('video').trigger('pause');
        });
      </script>
      <script type="text/javascript">
        var src = '';
  //var canvas = new fabric.Canvas('.component');
  var necessarily_data_array=[];
  var token;
  var filename=0;
  var filekey=1;

  function viewCoupon(){
    $("#readyCoupon").modal("show");
  }
  function signature(type){
    $.ajax({
      url:"include/getSignature.php",
      data:{type:type},
      type:"POST",
      success:function(data){
        var json=JSON.parse(data);
        var fData=CKEDITOR.instances['editor_email'].getData();
        CKEDITOR.instances['editor_email'].setData(fData+json);
      }
    })
  }
  if($.cookie('top_position')!=null){
    top_block_add = $.cookie('top_position');
  }else{top_block_add = "250px";}
  top_position_add = top_block_add.substr(0,3)*1;
  $(".popup_close").click(function(){
    $(".preview_pop").hide();
  });


  $(".pod_menu_cat_td a").click(function(){
    id_tovar = $(this).attr("id_tovar");
    name_tovar = $(this).find("span").text();
    bg_overlayImage = $(this).attr("bg_overlayImage");
    bg_backgroundImage = $(this).attr("bg_backgroundImage");
    bg_kontur_img_fade = $(this).attr("bg_kontur_img");
    price_tovar = $(this).attr("price");
    $(".konstruktor_page .slider_price_all strong").text(price_tovar);
    $("[name=id_product]").val(id_tovar);
    new_url = "/konstruktor.htm?id_outline="+id_tovar;
    window.history.pushState({path:new_url},'',new_url);
    $.post("/jscripts/ajax/change_product.php", {id_tovar:id_tovar },
      function(data){
        obj = jQuery.parseJSON(data);
        $(".info_product_desc").html(obj.text);
      });
    change_product(bg_overlayImage ,name_tovar,id_tovar, bg_backgroundImage);
  });

  var canvasOperations = {
    loadFromUrl : function(){
      fabric.Image.fromURL(src, function(image){
        canvas.add(image.set({
          id : 'canvas'+filename,
          alt : 'canvas'+filename,
          width : canvas.width / 2,
          height : canvas.height / 2,
          perPixelTargetFind : true,
          targetFindTolerance : 4
        }));
      });
    },
  }

  $(".show_menu_konstr").click(function(){
    $(".chuse_product_konstruktor").fadeIn(200);
  });
  $(".chuse_product_konstruktor_top").click(function(){
    $(".chuse_product_konstruktor").fadeOut(200);
  })

  $( function() {
    $("#resize").resizable({autoHide:true,alsoResize:'#preview'});
  } );

  function del_input(id){
    var parent=$(id).parent();
    $(parent).remove();
  }

  function viewCoupon(){
    $(".component").css('border', '0px');
    $('.component').html2canvas({
      onrendered: function (canvas) {
        $(".imageData").attr('src',canvas.toDataURL("image/png"));
        $(".component").css('border', '3px solid #49708A');
      }
    });
  }

  function saveCoupon(){
    var file = $(".imageData").attr('src');
    var campaignName=$(".campaignName").val();
    var mobileNumber=$(".mobileNumber").val();
    $('.sk-circle').css('display','block');
    if(mobileNumber==''){
      alert("Enter Mobile Number");
    }else{
      $.ajax({
        url: 'include/coupon/savePostcard.php',
        data: {file:file,campaignName:campaignName,mobileNumber:mobileNumber},
        type: 'POST',
        success:function(){
          $('.sk-circle').css('display','none');
        }})
      window.location='coupon-builder.php';
    }
  }
  function addNewFile(filename){
    $(".filds_all").append('<div class="field obor"><textarea style="resize:none"  disabled placeholder="file'+filekey+'"></textarea><a onclick="delFile(this,'+filename+')" class="del_fil"></a></div> ');
  }

  function clearAll(){
    canvas.clear();
    filename=0;
    filekey=1;
    $(".field").each(function(){
      $(this).hide();
    })
  }

  function delFile(id,idFile){
    var key=idFile+1;
    var name='canvas'+key;
    console.log(name);
    var lengthCanvas = canvas['_objects'].length;
    for(var k in canvas['_objects']){
      console.log(canvas['_objects'][k]['alt']);
      if(canvas['_objects'][k]['alt']==name){
        canvas['_objects'][k].remove();
        var parent=$(id).parent();
        $(parent).hide();
      }
    }
    console.log(canvas);
    filekey--;
    canvas.renderAll();
    $(".addNewFile").val('');
  }

  function readURL(input){
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        src=e.target.result;
        canvasOperations.loadFromUrl();
      };
      addNewFile(filename);
      reader.readAsDataURL(input.files[0]);
      filename++;
      filekey++;
    }
  }
  $(".del_fil").click();
</script>
<script type="text/javascript">
  $('body').on('hidden.bs.modal', '.modal', function () {
    $('video').trigger('pause');
  });
</script>
<script type="text/javascript">
  CKEDITOR.plugins.add( 'tokens',
  {
 requires : ['richcombo'], //, 'styles' ],
 init : function( editor )
 {
  var config = editor.config,
  lang = editor.lang.format;
    var tags = []; //new Array();
    //this.add('value', 'drop_text', 'drop_label');
    tags[0]=["{$first_name}", "first_name", "first_name"];
    tags[1]=["{$last_name}", "last_name", "last_name"];
    tags[2]=["{$mobile_phone}", "mobile_phone", "mobile_phone"];
    tags[3]=["{$email}", "email", "email"];
    tags[4]=["{$address}", "address", "address"];
    tags[5]=["{$address1}", "address1", "address1"];
    tags[6]=["{$city}", "city", "city"];
    tags[7]=["{$state}", "state", "state"];
    tags[8]=["{$postal_code}", "postal_code", "postal_code"];
    editor.ui.addRichCombo( 'tokens',
    {
      label : "Insert data",
      className : 'cke_format',
      multiSelect : false,

      panel :
      {
       css : [ config.contentsCss, CKEDITOR.getUrl( editor.skinPath + 'editor.css' ) ],
       voiceLabel : lang.panelVoiceLabel
     },
     init : function()
     {
      this.startGroup( "" );
             //this.add('value', 'drop_text', 'drop_label');
             for (var this_tag in tags){
              this.add(tags[this_tag][0], tags[this_tag][1]);
            }
          },
          onClick : function( value )
          {
           editor.focus();
           editor.fire( 'saveSnapshot' );
           editor.insertHtml(value);
           editor.fire( 'saveSnapshot' );
         }
       });
  }
});

</script>
<script>
  CKEDITOR.replace('editor_email',{
    extraPlugins: 'tokens',
    toolbar :
    [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
    { name: 'links', items: [ 'Link', 'Unlink'] },
    { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize','tokens' ] },
    { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
    { name: 'tools', items: [ 'Maximize'] },
    ]
  });
  CKEDITOR.replace('text_editor',
  {
    toolbar :
    [
    { name: 'basicstyles', items : [ 'Bold','Italic' ] },
    { name: 'paragraph', items : [ 'NumberedList','BulletedList' ] },
    { name: 'tools', items : [ 'Maximize','-' ] },
    { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] }
    ]});
  </script>


  <script>
    $("#select2").on('change', function(){
      if($(this).val() == "Religion" ){
        $("#select_2").show();
      } else {
        $("#select_2").hide();
      }

      if($(this).val() == "Birthday"){
        $("#select_2_date").show();
      } else {
        $("#select_2_date").hide();
      }

    });
    function PostBlob(audioBlob, videoBlob, fileName) {
      var formData = new FormData();
      var bd="broadcast_rvmd";
      formData.append('filename', fileName);
      formData.append('audio-blob', audioBlob);
      formData.append('video-blob', videoBlob);
      formData.append('bd',bd );
      var campaign_id=id_campaign;
      var name_rvmd=$(".name_rvmd").val();
      formData.append('name_rvmd', name_rvmd);
      formData.append('id_campaign',campaign_id);
      xhr('include/upload_audio.php', formData, function(ffmpeg_output) {
        preview.src = '../uploads_audio/' + fileName + '-merged.webm';
        preview.play();
        preview.muted = false;
        name_file=fileName + '.webm';
      });
    }
    var record = document.getElementById('record');
    var stop = document.getElementById('stop');
    var audio = document.querySelector('audio');
    var recordVideo = document.getElementById('record-video');
    var preview = document.getElementById('preview');
    var container = document.getElementById('container');
    var input_file=document.getElementById('import_audio');
    var recordAudio, recordVideo;
    function plus_one(){
      i++;
    }
    record.onclick = function() {
      record.disabled = true;
      input_file.disabled=true;
      !window.stream && navigator.getUserMedia({
        audio: true,
        video: true
      }, function(stream) {
        window.stream = stream;
        onstream();
      }, function(error) {
        alert(JSON.stringify(error, null, '\t'));
      });
      window.stream && onstream();
      function onstream() {
        preview.src = window.URL.createObjectURL(stream);
        preview.play();
        preview.muted = true;
        recordAudio = RecordRTC(stream, {
          type: 'audio',
          recorderType: StereoAudioRecorder,
            // bufferSize: 16384,
            onAudioProcessStarted: function() {
              recordVideo.startRecording();
            }
          });
        var videoOnlyStream = new MediaStream();
        videoOnlyStream.addTrack(stream.getVideoTracks()[0]);
        recordVideo = RecordRTC(videoOnlyStream, {
          type: 'video',
            // recorderType: MediaStreamRecorder || WhammyRecorder
          });
        recordAudio.startRecording();
        stop.disabled = false;
      }
    };
    var fileName;
    stop.onclick = function() {
      record.disabled = true;
      stop.disabled = true;
      preview.src = '';
      preview.poster = 'ajax-loader.gif';
      fileName = Math.round(Math.random() * 99999999) + 99999999;
      recordAudio.stopRecording(function() {
        recordVideo.stopRecording(function() {
          PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
        });
      });
    };
    function xhr(url, data, callback) {
      var request = new XMLHttpRequest();
      request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200) {
          callback(request.responseText);
        }
      };
    /*
    request.open('POST', url);
    request.send(data);
    console.log(request.statusText);
    */
    $('.sk-circle').css('display','block');
    send_audio(data);
  }
  function send_audio(data){
    $('.sk-circle').css('display','block');
    $.ajax({
      url:"include/record_audio.php",
      data: data,
      processData: false,
      contentType: false,
      type: 'POST',
      success:function(data){
        $('.sk-circle').css('display','none');
        //console.log(data);
        parsing_audio_json(data);
      }
    })
    $('.sk-circle').css('display','none');
  }
</script>
<script>
  var name;
  var delay_day;
  var campaign=[];
  var option;
  var option_value;
  var assignNum;
  var error;
  var id_campaign;
  var type_Campaign="broadcast";

  function save_new_broadcast()
  {
    try{
      check_name();
      checkAssignNum();
      check_option();
      build_array_campaign();
      get_delay_day();
      set_ajax_query();
    }catch(e){
      alert(error);
    }
  }

  function checkAssignNum(){
    if($(".select_assign_num").val()=='0'){
      error="Enter asssign number";
      throw new SyntaxError("empty_data");
    } else{
      assignNum=$(".select_assign_num").val();
    }    
  }

  function set_ajax_query(){
    $('.sk-circle').css('display','block');
    $.ajax({
      url:"include/broadcast/add_new_broadcast.php",
      data:{name:name,delay_day:delay_day,campaign:campaign,option:option,option_value:option_value,assignNum:assignNum},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
        var json=JSON.parse(data);
                    //console.log(json);
                    id_campaign=json['id'];
                    //alert(id_campaign);
                    $("#accordion").css('display','block');
                  }
                })
  }

  function get_delay_day()
  {
    if($(".default_delay").prop("checked"))
    {
      delay_day=0;
    }else{
      if($(".custom_delay").prop("checked")){
        if($(".delay_day").val()==""){
          error="Enter Delay Day";
          throw new SyntaxError("empty_data");
        }else{
          delay_day=$(".delay_day").val();
        }
      }
    }
  }

  function check_name()
  {
    if($("#name_broadcast").val()==''){
      error="Enter Name Broadcast";
      throw new SyntaxError("empty_data");
    } else{
      name=$("#name_broadcast").val();
    }

  }

  function check_option()
  {
    if($(".select_option").val()=='none'){
      error="Enter Special parameter";
      throw new SyntaxError("empty_data");
    } else{
      option=$(".select_option").val();
    }
    if($(".select_option").val()=='Birthday') {
      if($(".select_2_date").val()==''){
        error="Enter Data";
        throw new SyntaxError("empty_data");
      }else{
        option_value=$(".select_2_date").val();
      }
    }
    if($(".select_option").val()=='Religion'){
      option_value=$(".select_option_value").val();
    }
  }

  function select_all()
  {
    var prop =$(".all_campaign").prop("checked");
    if(prop){
      $(".select_campaign").each(function(){
        $(this).prop("checked",false);
      })
    }else{
      $(".first_campaign").prop("checked",true);
    }
  }

  function update_array()
  {
    $(".all_campaign").prop("checked",false);
  }

  function unchecked_all()
  {
    $(".select_campaign").each(function(){
      $(this).prop("checked",false);
    })
    $(".all_campaign").prop("checked",true);
  }

  function build_array_campaign()
  {
    campaign=[];
    $(".select_campaign").each(function(){
      if($(this).prop("checked")){
        campaign.push($(this).val());
      }
    })
    if(campaign.length==0){
      campaign.push('0');
    }
  }

  function agree_check(id,tab,div)
  {
    $('.'+tab).css('display','none');
    $('.'+div).css('display','block');
  }

  function parsing_audio_json(array)
  {
    var json=JSON.parse(array);
    var dir="../"+json['dir'];
    var out='<audio controls id="'+dir+'">';
    out+='<source src="'+json['dir']+'" type="audio/ogg; codecs=vorbis">';
    out+='<source src="'+json['dir']+'" type="audio/mpeg">';
    out+='</audio><a style="margin-left:10px" href="#del" onclick="delete_audio()">Delete</a>';
    $('.sk-circle').css('display','none');
    $('.audio_list').append(out);
  }


  function load()
  {
    $('.sk-circle').css('display','block');
    var input_file=document.getElementById('import_audio');
    var record = document.getElementById('record');
    input_file.disabled=true;
    record.disabled=true;
    var id=id_campaign;
    var data=new FormData();
    var file = $('.import_audio');
    data.append('upload', file.prop('files')[0]);
    data.append('id', id);
    data.append('bd', 'broadcast_rvmd');
    var name_rvmd=$(".name_rvmd").val();
    data.append('name_rvmd', name_rvmd);
    $.ajax({
      url:"include/upload_audio.php",
      data: data,
      processData: false,
      contentType: false,
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
        parsing_audio_json(data);
        file.val('');
        $('#record').attr('disabled');
        $('#input_file').attr('disabled');
      }
    })
    $('.sk-circle').css('display','none');
  }



  function delete_audio(){
    $('.sk-circle').css('display','block');
    var id=id_campaign;
    var dir1=$('audio').attr('id');
    dir='../'+dir1;
    var bd="broadcast_rvmd";
    var input_file=document.getElementById('import_audio');
    $.ajax({
      url:"include/delete_audio.php",
      data:{dir:dir,id:id,bd:bd},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
        $('.audio_list').html('');
      }
    })
    $('.sk-circle').css('display','none');
    $('#record').removeAttr('disabled');
    input_file.disabled=false;

  }
  function update_audio()
  {
    $('.sk-circle').css('display','block');
    if($('audio').attr("name")=='default'){
      dir_img="../"+$('audio').attr("id");
    }else{
      dir_img=$('audio').attr('id');
    }
    var name_rvmd=$(".name_rvmd").val();
    $.ajax({
      url:"include/update_audio.php",
      data: {dir_img:dir_img,id_campaign:id_campaign,name_rvmd:name_rvmd},
      type: 'POST',
      success:function(data){
        $('.sk-circle').css('display','none');
      }
    })
  }


  function save_text(){
    $('.sk-circle').css('display','block');
    var editor_text = CKEDITOR.instances['text_editor'].getData();
    $.ajax({
      url:"include/broadcast/add_text_to_campaign.php",
      data:{editor_text:editor_text,id_campaign:id_campaign},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
            //window.location='universal-text-center.php';
          }
        })
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },1500);
    $('#postcard_btn').tab('show');
    $('.sk-circle').css('display','none');
  }

  function save_voice(){
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },1500);
    $('#text_btn').tab('show');
    update_audio();
  }

  function save_postcard()
  {
    $('.final_save').css("display","block");
    //$('.success_btn').css("display","inline-block");
    setTimeout(function(){
      //$('.success_btn').css("display","none");
    },1500);
  }

  function email_show(){
    $('.sk-circle').css('display','block');
    var editor_data = CKEDITOR.instances['editor_email'].getData();
    $.ajax({
      url:"include/broadcast/add_email_to_campaign.php",
      data:{editor_data:editor_data,id_campaign:id_campaign},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
            //window.location='email-center.php';
            //alert(data);
          }
        })
    $('#voice_btn').tab('show');
    $('.final_save').css("display","block");
    $('.success_btn').css("display","inline-block");
    setTimeout(function(){
      $('.success_btn').css("display","none");
    },1500);
    $('.sk-circle').css('display','none');
  }

  $(".checkMobile").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl/cmd+A
         (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: Ctrl/cmd+C
         (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: Ctrl/cmd+X
         (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: home, end, left, right
         (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
           return;
         }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });

  function save_postcard(){
    $('.sk-circle').css('display','block');
    $.ajax({
      url:"include/postcard/getPicture.php",
      data:{id_campaign:id_campaign},
      type:"POST",
      success:function(data){
        $('.sk-circle').css('display','none');
        insertPhotoIntoModal(JSON.parse(data));
        $('.final_save').css("display","block");
      }
    })
  }
  function insertPhotoIntoModal(array){
    var out='';
    for(var k in array){
      out+="<img src='"+array[k]+"' alt='alt'>"
    }
    $(".photoDataToInsert").html(out);
  }
  function closeSideBar(){
    $('body').addClass('sidebar-hidden');
  }
  function removeSideBar(){
    $('body').removeClass('sidebar-hidden');
  }
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
</body>

</html>
