<?php
include "include/session.php";
$clientId=getUserId($_SESSION['user_token']);

function getUserId($token){
    $sql="SELECT client_id FROM token WHERE token='".$token."'";
    $query=mysql_query($sql);
    $arrayData=mysql_fetch_assoc($query);
    return $arrayData['client_id'];
}
function getAllPhoneNumbers($userId){
    $sql="SELECT * FROM phone_numbers WHERE user_id='".$userId."' ORDER BY timestamp DESC";
    $query=mysql_query($sql);
    if(mysql_num_fields($query)!=0){
        while ($res=mysql_fetch_assoc($query)) {
            echo '<tr>
            <td>'.$res['number'].'</td>
            <td>'.$res['timestamp'].'</td>
            <td class="table_button" style="width:60px!important;">
            <button onclick="setNumber('.$res['id'].')" type="button" class="btn btn-danger btn_user_table_danger" data-toggle="modal" data-target="#dangerModal">
            <i class="fa fa-trash-o "></i>
            </button>

            </td>
            </tr>';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  body{
    position: relative;
}
.sk-circle{
    display: none;
    z-index: 10001;
    width: 40px;
    height: 40px;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
}
</style>
</head>
<body name="<?php echo $clientId; ?>" class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
</div>
<?php include 'components/header.php'; ?>
<div class="app-body">
    <?php include "sidebar.php"; ?>
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Phone Number</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i> Phone Numbers <button style="float: right;margin-top: 2px;" class="btn btn-success" onclick="addNewNumber(this)">Add New Number</button>
                    <div class="card-actions">
                    </div>
                </div>
                <div class="card-block">
                    <table class="table table-striped table-bordered datatable table-responsive">
                        <thead>
                            <tr>
                                <th>Numbers</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>  
                            <?php getAllPhoneNumbers($clientId);?>                         
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-danger" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this number?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                        <button onclick="deleteNumber()" type="button" data-dismiss="modal" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </main>

</div>
<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<!-- Plugins and scripts required by all views -->
<script src="js/libs/Chart.min.js"></script>
<!-- GenesisUI main scripts -->
<script src="js/app.js"></script>
<!--<script src="js/libs/jquery.dataTables.js"></script>
<script src="js/libs/dataTables.bootstrap4.js"></script>
<script src="js/views/tables.js"></script>
<script>
  function del_user(id){
     $.ajax({
        url:'include/delete_user.php',
        type:'POST',
        data:{id:id},
        success:function(){
          window.location="user-profile-search.php";
      }
  })
 }
</script>-->
<script>
    var numberId;
    function setNumber(id){
        numberId=id;
    }
    function deleteNumber(){
        $.ajax({
            url:"include/deleteNumber.php",
            data:{numberId:numberId},
            type:"POST",
            success:function(){
                window.location=location;
            }
        })  
    }
    function addNewNumber(id){
        var userId=$("body").attr("name");
        $('.sk-circle').css('display','block');
        $(id).attr("disabled","true");
        $.ajax({
            url:"include/addPhoneNumber.php",
            data:{userId:userId},
            type:"POST",
            success:function(data){
                $('.sk-circle').css('display','none');
                $(id).removeAttr("disabled");
                var json=JSON.parse(data);
                if(json['success']==1){
                    window.location=location;
                }else{
                    alert('Something went wrong');
                }
            }
        })
    }
</script>
<script src="js/libs/toastr.js"></script>
<script src="components/user_notifications.js"></script>
</body>

</html>