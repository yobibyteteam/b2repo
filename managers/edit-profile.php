<?php
include 'include/connect_to_bd.php';
include "include/session.php";
if(isset($_GET['token'])){
  $id=$_GET['token'];
  $array_main_data=get_main_data();
  $contact_id=$array_main_data['id'];
  //$assignedCampaign=$array_main_data['assigned_campaign'];
  if($array_main_data['assign_campaign_type']=='campaign'){
    $type='Universal';
  }else{
    $type=$array_main_data['assign_campaign_type'];
  }
  $nameCampaign=getCampaignName($array_main_data['assigned_campaign'],$array_main_data['assign_campaign_type']);
  $assignedCampaign='<option selected value="'.$array_main_data['assigned_campaign'].'/'.$array_main_data['assign_campaign_type'].'">'.$nameCampaign.'('.$type.')</option>';
}else{
  header("Location: contacts.php");
}

function getCampaignName($id,$type){
  $query_model="SELECT name FROM ".$type."_builder WHERE id='".$id."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
   $arrayData=mysql_fetch_assoc($query);
   return $arrayData['name'];
 }
}
function get_alt_data(&$contact_id)
{
  $query_model='SELECT * FROM custom_field_contacts WHERE contacts_id="'.$contact_id.'"';
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    $array_alt_data='';
    while ($res=mysql_fetch_assoc($query)) {
      echo '<div class="form-group"><label>'.$res['label'].'</label><button class="btn btn-outline-danger btn-sm" onclick="del_input_global(this,'.$res['id'].')" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button><input data-placement="bottom" type="text" placeholder='.$res['label'].'  class="alt_input input_data form-control"  value="'.$res['text'].'"></div>';
    }
  }
}

function get_list_campaign()
{
  $token=$_SESSION['user_token'];
  $query_model="SELECT * FROM token WHERE token='".$token."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0)
  {
    $array=mysql_fetch_assoc($query);
    $client_id=$array['client_id'];
    getUniversalCampaign($client_id);
    getDatedCampaign($client_id);
    getHolidayCampaign($client_id);

  }
}
function getUniversalCampaign($client_id){
  global $campaignId;
  $query_model="SELECT id,name FROM campaign_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/campaign">'.$res['name'].'(Universal)</option>';
    }
  }
}
function getHolidayCampaign($client_id){
  global $nameCampaign;
  $query_model="SELECT id,name FROM holiday_builder WHERE client_id='".$client_id."' AND type='user' ";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option  value="'.$res['id'].'/holiday">'.$res['name'].'(Holiday)</option>';
    }
  }
}
function getDatedCampaign($client_id){
  global $campaignId;
  $query_model="SELECT id,name FROM dated_builder WHERE client_id='".$client_id."' AND type='user'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while($res=mysql_fetch_assoc($query)){
      echo '<option value="'.$res['id'].'/dated">'.$res['name'].'(Dated)</option>';
    }
  }
}
function get_main_data()
{
  global $id;
  $query_model='SELECT * FROM contacts WHERE md5_hash="'.$id.'"';
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    return mysql_fetch_assoc($query);
  }else{
    header("Location: contacts.php");
  }
}
$clientId=getClientId($_SESSION['user_token']);
function getClientId($token){
  $query_model="SELECT * FROM token WHERE token='".$token."'";
  $query=mysql_query($query_model);
  $arrayData=mysql_fetch_assoc($query);
  return $arrayData['client_id'];
}

function getNotesList($clientId,$contact_id){
  $query_model="SELECT * FROM notes WHERE client_id='".$clientId."' AND contacts_id='".$contact_id."'";
  $query=mysql_query($query_model);
  if(mysql_num_rows($query)!=0){
    while ($res=mysql_fetch_assoc($query)) {
      $dopField='';
      if(strlen($res['text'])>150){
        $dopField='..';
      }
      echo '<tr><td>'.substr($res['text'],0,150).$dopField.'</td>
      <td>'.$res['date'].'</td>
      <td class="table_button" style="width: 157px!important;">
      <button onclick="deleteNotesId('.$res['id'].')" type="button" class="btn btn-danger btn_user_table_danger" data-toggle="modal" data-target="#dangerModal">
      <i class="fa fa-trash-o "></i>
      </button>
      <button data-toggle="modal" data-target="#myModal" onclick="changeNotes('.$res['id'].')" type="button" class="btn btn-primary">
      <i class="fa fa-edit"></i>
      </button>
      </td></tr>';
    }
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Clever - Bootstrap 4 Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,jQuery,CSS,HTML,RWD,Dashboard,Vue,Vue.js,React,React.js">
  <link rel="shortcut icon" href="img/favicon.png">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <title>CRM System</title>
  <!-- Icons -->
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application -->
  <link href="css/style.css" rel="stylesheet">
  <style>
  .fiieldest{
    margin-top: 15px;
    display: inline-block;
    margin-right: 25px;
  }
  .input-file-row-1:after {
    content: ".";
    display: block;
    clear: both;
    visibility: hidden;
    line-height: 0;
    height: 0;
  }

  .input-file-row-1{
    display: inline-block;
    position: relative;
  }

  html[xmlns] .input-file-row-1{
    display: block;
  }

  * html .input-file-row-1 {
    height: 1%;
  }

  .upload-file-container {
    position: relative;
    height: 137px;
    overflow: hidden;
    background: url(http://i.imgur.com/AeUEdJb.png) top center no-repeat;
    float: left;
    margin-left: 23px;
  }

  .upload-file-container:first-child {
    margin-left: 0;
  }
  .input-group-addon{
    color:inherit!important;
    background-color: inherit!important;
  }

  .upload-file-container > img {
    opacity: 1;
    width: 93px;
    height: 93px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
  }

  .upload-file-container-text{
    font-size: 12px;
    color: #20a8d8;;
    line-height: 17px;
    text-align: center;
    display: block;
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100px;
    height: 35px;
  }

  .upload-file-container-text > span{
    border-bottom: 1px solid #20a8d8;;
    cursor: pointer;
  }

  .upload-file-container input  {
    position: absolute;
    left: 0;
    bottom: 0;
    font-size: 1px;
    opacity: 0;
    filter: alpha(opacity=0);
    margin: 0;
    padding: 0;
    border: none;
    width: 70px;
    height: 50px;
    cursor: pointer;
  }
</style>
</head>
<body id="<?php echo $array_main_data['id']; ?>" class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <?php require_once 'components/header.php'; ?>
  <div class="app-body">
    <?php include "sidebar.php"; ?>
    <!-- Main content -->
    <main class="main">
      <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
        <li class="breadcrumb-item active">Edit Contacts</li>
      </ol>
      <div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <strong>Edit Contacts</strong>
                  <!--<button onclick="window.location='contact-details.php?token=<?php echo $id;?>'" class="btn btn-success" style="float:right;outline: none;">Message</button>-->
                </div>
                <div class="panel-group " id="accordion">
                  <ul class="nav nav-tabs" style="border-bottom: 0!important" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="email_btn" data-toggle="tab" href="#email_center" role="tab" aria-controls="email_center"><span id="span">General information</span> <i class="fa fa-envelope-o "></i></a>
                    </li>
                    <li  class="nav-item">
                      <a  id="voice_btn" class="nav-link" data-toggle="tab" href="#voice_center" role="tab" aria-controls="voice_center"><span id="span">Notes</span>  <i class="fa fa-file-text-o"></i></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="text_btn" data-toggle="tab" href="#text_center" role="tab" aria-controls="text_center"><span id="span">Reminders</span> <i class="fa fa-file-text-o"></i></a>
                    </li>
                  </ul>

                  <div class="tab-content" style="border: 1px solid #e1e6ef!important;">
                    <div class="tab-pane active" id="email_center" role="tabpanel" style="">

                      <div class="form-group close_tab1">

                        <div class="form_input">
                          <div class="form-group">
                            <label>First name</label><button onclick="update_contact_data()" class="btn btn-primary" style=" position: relative;
                            float: right; bottom:7px;">Save</button>
                            <input id="first_name" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data checkempty form-control main_array" value="<?php echo $array_main_data['first_name'] ?>"  placeholder="Enter First Name">
                          </div>

                          <div class="form-group">
                            <label>Last Name</label>
                            <input id="last_name" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="input_data checkempty form-control main_array" value="<?php echo $array_main_data['last_name'] ?>"  placeholder="Enter Last Name">
                          </div>

                          <div class="form-group mobile_phone">
                            <label>Email</label>
                            <input onkeypress="change_input_data(this)"  data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data checkempty form-control"
                            value="<?php echo $array_main_data['email'] ?>"  placeholder="Enter Email ">
                          </div>

                          <div class="form-group">
                            <label>Mobile Phone</label>
                           <!-- <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control"
                            value="<?php echo $array_main_data['mobile_phone'] ?>"  placeholder="Enter Mobile phone" id="edit_mobile_phone">-->

                            <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1">+</span>
                              <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile main_array checkempty input_data form-control"
                              value="<?php echo $array_main_data['mobile_phone'] ?>"  placeholder="Enter Mobile phone" id="edit_mobile_phone" aria-describedby="basic-addon1">
                            </div>
                          </div>

                          <div class="form-group">
                            <label>Alternative Phone</label>
                            <!--<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['alt_phone'] ?>" placeholder="Enter Alternative Phone" id="edit_alternative_phone">-->

                            <div class="input-group">
                              <span class="input-group-addon" id="basic-addon1">+</span>
                              <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile main_array input_data form-control" value="<?php echo $array_main_data['alt_phone'] ?>" placeholder="Enter Alternative Phone" id="edit_alternative_phone" aria-describedby="basic-addon1">
                            </div>
                          </div>

                          <div class="form-group">
                            <label>Address</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data checkempty form-control" value="<?php echo $array_main_data['address'] ?>" placeholder="Enter Address">
                          </div>
                          <div class="form-group">
                            <label>Address1</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['address1'] ?>" placeholder="Enter Address1">
                          </div>
                          <div class="form-group">
                            <label>Country</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class=" checkempty countryData form-control"
                            value="<?php echo $array_main_data['country'] ?>" placeholder="Enter Country">
                          </div>
                          <div class="form-group">
                            <label>City</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control"
                            value="<?php echo $array_main_data['city'] ?>" placeholder="Enter City">
                          </div>
                          <div class="form-group">
                            <label>State</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control" value="<?php echo $array_main_data['state'] ?>" placeholder="State State">
                          </div>
                          <div class="form-group">
                            <label>Postal Code</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile checkempty main_array input_data form-control" value="<?php echo $array_main_data['postal_code'] ?>" placeholder="Postal Code" id="edit_postal_code">
                          </div>
                          <div class="form-group">
                            <label>Date of Birth</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array checkempty input_data form-control" value="<?php echo $array_main_data['date_of_birth'] ?>" placeholder="Enter Date of Birth" id="date_birth">
                          </div>
                          <div class="form-group">
                            <label>Religion</label>
                            <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['religion'] ?>" placeholder="Enter Religion">
                          </div>
                          <div class="form-group has-feedback">
                            <label for="select">Type:</label>
                            <select id="select_customer" name="select" class="main_array form-control">
                              <?php if($array_main_data['type']=='Customer'){
                                echo '<option value="Customer">Customer</option>
                                <option value="Prospect">Prospect</option>';
                              }else{
                               echo '<option value="Prospect">Prospect</option>
                               <option value="Customer">Customer</option>';
                             }
                             ?>
                           </select><br>
                           <label for="select">Status:</label>
                           <select id="select_active" name="select" class="main_array form-control">
                             <?php if($array_main_data['status']=='Active'){
                              echo '<option value="Active">Active</option>
                              <option value="Inactive">Inactive</option>';
                            }else{
                             echo '<option value="Inactive">Inactive</option>
                             <option value="Active">Active</option>';
                           }
                           ?>
                         </select>
                       </div>

                       <div class="form-group has-feedback">
                        <label for="select">Assigned Campaign</label>
                        <select id="select" name="select" class="main_array form-control">
                          <?php  echo  $assignedCampaign;  ?>
                          <?php get_list_campaign(); ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Spouse First Name</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['spouse_first_name'] ?>" placeholder="Enter Spouse First Name">
                      </div>
                      <div class="form-group">
                        <label>Spouse Last Name</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['spouse_last_name'] ?>" placeholder="Enter Spouse Last Name">
                      </div>
                      <div class="form-group">
                        <label>Spouse Email</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" value="<?php echo $array_main_data['spouse_email'] ?>" class="main_array input_data form-control" placeholder="Enter Spouse Email">
                      </div>
                      <div class="form-group">
                        <label>Spouse Mobile</label>
                        <!--<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['spouse_mobile'] ?>" placeholder="Enter Mobile" id="edit_spouse_mobile_phone">-->

                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">+</span>
                          <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile main_array input_data form-control" value="<?php echo $array_main_data['spouse_mobile'] ?>" placeholder="Enter Spouse Mobile" id="edit_spouse_mobile_phone"aria-describedby="basic-addon1">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Spouse Alternative</label>
                        <!--<input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" value="<?php echo $array_main_data['spouse_alternate'] ?>" placeholder="Enter Alternative" id="edit_spouse_alternative_phone">-->

                        <div class="input-group">
                          <span class="input-group-addon" id="basic-addon1">+</span>
                          <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="checkMobile main_array input_data form-control" value="<?php echo $array_main_data['spouse_alternate'] ?>" placeholder="Enter Spouse Alternative" id="edit_spouse_alternative_phone" aria-describedby="basic-addon1">
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Spouse Date of Birth</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Spouse Date of Birth" value="<?php echo $array_main_data['spouse_date_of_birth'] ?>" id="date_birth_sprouse">
                      </div>
                      <div class="form-group">
                        <label>Child Religion</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" value="<?php echo $array_main_data['spouse_religion'] ?>" class="main_array input_data form-control" placeholder="Enter Child Religion">
                      </div>
                      <div class="form-group">
                        <label>Child Name</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Child Name" value="<?php echo $array_main_data['child_name'] ?>">
                      </div>
                      <div class="form-group">
                        <label>Child DOB</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Child DOB" id="date_child_birth" value="<?php echo $array_main_data['child_dob'] ?>">
                      </div>
                      <div class="form-group">
                        <label>Product/Service Purchased</label>
                        <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="main_array input_data form-control" placeholder="Enter Product/Service Purchased" value="<?php echo $array_main_data['product_service_purchased'] ?>">
                      </div>
                      <?php get_alt_data($contact_id); ?>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row" style="background-color: #e1e6ef;">
                          <div class="form-group col-sm-6">
                            <input style="margin-top: 20px" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class="new_input_name form-control"  placeholder="Enter custom name">
                            <label class="checkbox-inline" for="inline-checkbox1">
                              <input style="margin-top: 10px;"  onclick="disp(document.getElementById('select_date'))" type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="">Date field
                            </label>
                          </div>
                          <div class="form-group col-sm-6">
                            <button style="margin-top: 20px" onclick="add_new_input(this)" class="btn btn-primary">Add Custom Field</button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row " style="margin-top:15px">
                     <div class="col-md-12 text-center">
                       <button onclick="update_contact_data()" class="btn btn-primary">Save</button>
                     </div>

                   </div>
                 </div>
               </div>
               <div class="tab-pane" id="voice_center" role="tabpanel">
                 <table class="table table-striped table-bordered datatable table-responsive">
                  <thead>
                    <tr>
                      <th>Notes</th>
                      <th>Time</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody class="notesBlock">
                    <?php getNotesList($clientId,$contact_id); ?>
                  </tbody>
                </table>
                <div class="form-group close_tab2">
                  <div class="form-group has-feedback">
                    <label class="form-control-label" for="text-input">Note:</label>
                    <textarea  onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class=" form-control nameOfNotes"  placeholder=" Enter Notes Name"></textarea>
                  </div>
                  <div class=" py-4 text-center">
                    <button onclick="addNewNotes()"  class="btn btn-primary"> Submit</button>
                  </div>
                </div>
              </div>


              <div class="tab-pane" id="text_center" role="tabpanel">
                <div class="form-group close_tab3">

                  <div class="form-group">
                    <div class="form-group has-feedback">
                      <label class="form-control-label" for="text-input">Date:</label>
                      <input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" class=" form-control delay_date" id="dated" placeholder="Select Date">
                    </div>
                    <div class="form-group has-feedback">
                      <label class="form-control-label" for="text-input">Time:</label>
                      <input name="ffgfdgfd" onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="time" class=" form-control delay_time"  placeholder="Enter Time" id="defaultEntry">
                    </div>
                    <div >
                      <label class=" form-control-label"  for="select2">How many minutes </label>
                      <select class=" form-control delay_min">
                        <option value="15" default>15min</option>
                        <option value="30">30min</option>
                        <option value="45">45min</option>
                        <option value="60">60min</option>
                      </select>
                    </div>



                  </div>
                  <div class=" py-4 text-center">
                    <button class="btn btn-primary" onclick="addNewReminder()"> Reminder</button>
                    <button style="margin-left: 30px;display: none;padding: 0.5rem 2rem;" type="button" class="btn danger_btn btn-outline-danger">Error</button>
                  </div>

                </div>
              </div>
            </div>

          </div>


        </div>

      </div>
    </div>
  </div>
</main>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Note</h4>
      </div>
      <div class="modal-body">
        <textarea style="width: 100%" class="editNote form-control" cols="30" rows="10"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="button" onclick="saveChangeOfNotes()" class="btn btn-success" data-dismiss="modal">Save</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-danger" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Delete</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this note?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="button" onclick="deleteNotes()" data-dismiss="modal" class="btn btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
</div>


<!-- Bootstrap and necessary plugins -->

<script src="js/libs/jquery.min.js"></script>
<script src="js/libs/tether.min.js"></script>
<script src="js/libs/bootstrap.min.js"></script>
<script src="js/libs/pace.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#dated" ).datepicker({ dateFormat: 'mm/dd/yy' });
  } );
  $( function() {
    $( "#date_birth" ).datepicker({ dateFormat: 'mm/dd/yy' });
  } );
  $( function() {
    $( "#date_birth_sprouse" ).datepicker({ dateFormat: 'mm/dd/yy' });
  } );
  $( function() {
    $( "#date_child_birth" ).datepicker({ dateFormat: 'mm/dd/yy' });
  } );
</script>

<!-- GenesisUI main scripts -->

<script src="js/app.js"></script>

<!-- Plugins and scripts required by this views -->
<script src="../js/libs/toastr.min.js"></script>
<script src="js/libs/gauge.min.js"></script>
<script src="js/libs/moment.min.js"></script>


<!-- Custom scripts required by this view -->
<script src="js/views/main.js"></script>
<script>
  var noteId;
  var necessarily_data_array=[];
  function disp(form) {
    if (form.style.display == "none") {
     form.style.display = "block";
   } else {
     form.style.display = "none";
   }
 }

 function del_input_global(id,id_input){
  $.ajax({
    url:"include/contacts/delete_custom_field.php",
    data:{id_input:id_input},
    type:"POST",
    success:function(data){}
  })
  var parent=$(id).parent();
  $(parent).remove();
}

function del_input(id){
  var parent=$(id).parent();
  $(parent).remove();
}

function add_new_input(id){
  var out;
  var new_input_name;
  new_input_name=$('.new_input_name');
  if(new_input_name.val()=='')
  {
    notification(new_input_name,'Enter Name');
  }
  else if($("#inline-checkbox1").prop("checked"))
  {
    out='<div class="form-group"><label>'+new_input_name.val()+'</label><button class="btn btn-outline-danger btn-sm" onclick="del_input(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button><input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text" id="dated" class="alt_input input_data form-control" placeholder="'+new_input_name.val()+'"></div>';

    $('.form_input').append(out);
    new_input_name.val('');
    $('#dated').datepicker();
  }
  else{
    out='<div class="form-group"><label>'+new_input_name.val()+'</label><button class="btn btn-outline-danger btn-sm" onclick="del_input(this)" style="margin-left: 10px; padding: 0px 4px;" href="#del">x</button><input onkeypress="change_input_data(this)" data-toggle="popover" data-placement="bottom" type="text"  class="alt_input input_data form-control" placeholder="'+new_input_name.val()+'"></div>';

    $('.form_input').append(out);
    new_input_name.val('');
  }
}
function notification(id,type_error){
  var parent=$(id).parent(".form-group");
  var title=$(id).find("title");
  alert(title['context']['placeholder']);
  $(parent).addClass('has-danger');
  $(id).addClass('form-control-danger');
  $(id).attr("data-content",type_error);
  $('.danger_btn').css("display","inline-block");
  setTimeout(function(){
    $('.danger_btn').css("display","none");
  },1500);

}
function change_input_data(id){
  var parent=$(id).parent(".form-group");
  $(id).removeClass('form-control-danger');
  $(parent).removeClass('has-danger');
}
function build_array(){
  var i=1;
  necessarily_data_array=[];
  $('.input_data').each(function(){
    necessarily_data_array.push($(this).val());
  });
  alert(necessarily_data_array);
}

function update_contact_data(){
  try{
    check_input_data();
    build_array_1();
    build_array_2();
    var countryData=$(".countryData").val();
    data_array_1.push(countryData);
    send_ajax(data_array_1,data_array_2);
  }catch(e){
    //console.log(e);
  }
}

function parsing_data(arr){
  var json=JSON.parse(arr);
  if(json['success']==1){
    window.location = location;
  }else{
    $(".danger_btn").css('display','inline-block');
    setTimeout(function(){
      $(".danger_btn").css('display','none');
    },1000);
    alert(json['error']);
  }
}

function build_array_1(){
  data_array_1=[];
  $('.main_array').each(function(){
    data_array_1.push($(this).val());
  });
}

function build_array_2(){
  data_array_2=[];
  if($('.alt_input').val()!='' || $('.alt_input')!='undefined'){
    $('.alt_input').each(function(){
      var placeholder=$(this).attr("placeholder");
      data_array_2.push(placeholder,$(this).val());
    });
  }
}

function check_input_data(){
  $('.checkempty').each(function(){
    if($(this).val() == ''){
      notification(this,'Empty Data');
      throw new SyntaxError("empty_data");
    }
  })
}

function addNewNotes(){
  var nameOfNotes=$(".nameOfNotes").val();
  var contactId=$("body").attr("id");
  if(nameOfNotes==''){
    alert("Enter Notes Name");
  }else{
    $.ajax({
      url:"include/addNewNote.php",
      data:{nameOfNotes:nameOfNotes,contactId:contactId},
      type:"POST",
      success:function(data){
        //alert("Success Added");
        window.location=location;
      }
    })
  }
}

function send_ajax(array1,array2){
  var contact_id=$('body').attr('id');
  $.ajax({
    url:"include/contacts/change_contact_data.php",
    data:{array1:array1,array2:array2,contact_id:contact_id},
    type:"POST",
    success:function(data){
      //console.log(data);
      parsing_data(data);
    }
  })
}

function deleteNotes(){
  var id=noteId;
  $.ajax({
    url:"include/deleteNote.php",
    data:{id:id},
    type:"POST",
    success:function(data){
      //alert(data);
      window.location=location;
    }
  })
}

function saveChangeOfNotes(){
  var editNote=$(".editNote").val();
  var id=$(".editNote").attr("name");
  $.ajax({
    url:"include/updateNotesData.php",
    data:{id:id,editNote:editNote},
    type:"POST",
    success:function(){
      window.location=location;
    }
  })
}

function changeNotes(id){
  $(".editNote").val('');
  $.ajax({
    url:"include/getInfoNote.php",
    data:{id:id},
    type:"POST",
    success:function(data){
      $(".editNote").val(data);
      $(".editNote").attr("name",id);
    }
  })
}

function addNewReminder(){
  var delay_min=$(".delay_min").val();
  var delay_time=$(".delay_time").val();
  var delay_date=$(".delay_date").val();
  if(delay_date==''){
    alert("Enter Date");
  }else if(delay_time==''){
    alert("Enter Time");
  }else{
    $.ajax({
      url:"include/addRemind.php",
      data:{delay_min:delay_min,delay_time:delay_time,delay_date:delay_date},
      type:"POST",
      success:function(data){
        window.location=location;
      }
    })
  }
}

function deleteNotesId(id){
  noteId=id;
}

$(".checkMobile").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl/cmd+A
         (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: Ctrl/cmd+C
         (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: Ctrl/cmd+X
         (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
         // Allow: home, end, left, right
         (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
           return;
         }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });
</script>
<script src="components/user_notifications.js"></script>
</body>

</html>
