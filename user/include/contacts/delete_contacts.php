<?php
require_once '../classes/Contact.php';
include '../connect_to_bd.php';
session_start();
$id=$_POST['contacts_id_for_del'];
$contact=new Contact();
try{
	$contact->delete_contacts($id);
	$text="User delete contact";
	$contact->add_to_activity_log($text);
	echo json_encode(array("success" => 1));
}catch(Exception $e){
	echo json_encode(array("success" => 0,"error" => $e->getMessage()));
}

?>